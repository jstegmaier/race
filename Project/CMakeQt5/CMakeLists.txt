## specify minimum version
CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

## the project name
PROJECT(XPIWIT)


# Eliminate a warning when building in Windows that relates
# to static linking of Qt executables to qtmain.lib.
# This policy was introduced in CMake version 2.8.11.
# CMake version 2.8.11.2 warns when the policy is not set
# and uses OLD behavior.
if(POLICY CMP0020)
cmake_policy(SET CMP0020 NEW)
endif()

if(POLICY CMP0043)
cmake_policy(SET CMP0043 NEW)
endif()

# The version number.
set (XPIWIT_VERSION_MAJOR 0)
set (XPIWIT_VERSION_MINOR 2)

SET(CMAKE_MODULE_PATH $ENV{ITKDIR})
SET(CMAKE_MODULE_PATH $ENV{ITKBUILD_VS2012})
SET(CMAKE_MODULE_PATH $ENV{QTDIR})

SET(EXECUTABLE_OUTPUT_PATH ../../Bin/CMake)

## find itk
FIND_PACKAGE(ITK REQUIRED)
INCLUDE( ${ITK_USE_FILE} )

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
## find qt
##set(Qt5Core_DIR "/home/fernando/Qt/5.3/gcc_64/lib/cmake/Qt5Core")
FIND_PACKAGE(Qt5Core REQUIRED)

## find tiff
##FIND_PACKAGE(TIFF REQUIRED tiff)

## specify include directories
INCLUDE_DIRECTORIES(
    ${QT_INCLUDE_DIR}
    ${ITK_INCLUDE_DIR}
        ../../Source/Core/*
        ../../Source/Filter/Base/*
        ../../Source/Filter/ITKCustom
        ../../Source/Filter/ThirdParty
        ../../Source/Filter/XpExtended/*
        ../../Source/Filter/XpWrapper/*
	$ENV{ITKDIR}/Modules/Nonunit/Review/include
)

## cuda support
set(USE_GPU OFF CACHE BOOL "Check if you have a CUDA enabled graphics card to speed up certain operations")
IF(USE_GPU)
	## NVCC Flags 
	SET(CUDA_NVCC_FLAGS "-gencode arch=compute_20,code=sm_20;-gencode arch=compute_20,code=sm_21;-gencode arch=compute_30,code=sm_30;-gencode arch=compute_32,code=sm_32;-gencode arch=compute_35,code=sm_35;-gencode arch=compute_50,code=sm_50" CACHE STRING "Semi-colon delimit multiple arguments")

	## for Watershed in GPU
	include_directories("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/watershed")
	add_subdirectory("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/watershed" "${CMAKE_CURRENT_BINARY_DIR}/CUDA/watershed")
	LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/CUDA/watershed)

	##for morpohological operators in GPU
	include_directories("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/morphologicalOperators")
	add_subdirectory("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/morphologicalOperators" "${CMAKE_CURRENT_BINARY_DIR}/CUDA/morphologicalOperators")
	LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/CUDA/morphologicalOperators)

	##for 3D separable convolution in GPU (adapting Nvidia's SDK sample)
	include_directories("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/separableConvolution3D")
	add_subdirectory("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/separableConvolution3D" "${CMAKE_CURRENT_BINARY_DIR}/CUDA/separableConvolution3D")
	LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/CUDA/separableConvolution3D)


	##for 3D separable convolution in GPU using texture
	include_directories("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/separableConvolution3D_texture")
	add_subdirectory("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/separableConvolution3D_texture" "${CMAKE_CURRENT_BINARY_DIR}/CUDA/separableConvolution3D_texture")
	LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/CUDA/separableConvolution3D_texture)


	##for membrane enhancement (Hessian) in GPU
	include_directories("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/membraneEnhancement")
	add_subdirectory("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/membraneEnhancement" "${CMAKE_CURRENT_BINARY_DIR}/CUDA/membraneEnhancement")
	LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/CUDA/membraneEnhancement)

	## for morpholical reconstruction in GPU
	include_directories("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/morphologicalReconstruction")
	add_subdirectory("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/morphologicalReconstruction" "${CMAKE_CURRENT_BINARY_DIR}/CUDA/morphologicalReconstruction")
	LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/CUDA/morphologicalReconstruction)

	## for median filter in GPU
	include_directories("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/medianFilter2D")
	add_subdirectory("${PROJECT_SOURCE_DIR}/../../Source/Filter/CUDA/medianFilter2D" "${CMAKE_CURRENT_BINARY_DIR}/CUDA/medianFilter2D")
	LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/CUDA/medianFilter2D)
ENDIF()

##for operations using the Nscale library by George Teodoro
set(NSCALE_SRC_DIR "${PROJECT_SOURCE_DIR}/../../ThirdParty/nscale" CACHE FILEPATH " location of the Nscale src code to include all the headers")
set(NSCALE_LIB_DIR "${PROJECT_SOURCE_DIR}/../../ThirdParty/nscale/Buildx64/lib" CACHE FILEPATH " location of the Nscale src code to include all the headers")
set(USE_NSCALE OFF CACHE BOOL "Check if you have compiled the Nscale library to speed up certain operations")

IF( USE_NSCALE )
	MESSAGE("Including NSCALE library files ")
	
	#define all the necessary folders
	include_directories(${OpenCV_INCLUDE_DIR})
	include_directories("${NSCALE_SRC_DIR}/src/segment")
	include_directories("${NSCALE_SRC_DIR}/src/common")
	include_directories("${PROJECT_SOURCE_DIR}/../../ThirdParty/opencv/build/include")
	include_directories("${PROJECT_SOURCE_DIR}/../../ThirdParty/opencv/build/include/opencv")
	include_directories("${PROJECT_SOURCE_DIR}/../../ThirdParty/opencv/build/include/opencv2")
	#add_subdirectory("${NSCALE_SRC_DIR}" "${CMAKE_CURRENT_BINARY_DIR}/NScale")
	
	## additional linker directories
	LINK_DIRECTORIES("${NSCALE_LIB_DIR}") #for Unix
	LINK_DIRECTORIES("${NSCALE_LIB_DIR}/$(ConfigurationName)") #VS variable (expanded during VSconfiguration)
	SET(NSCALE_LIBRARIES "cci_common" "segment")

	#you need openCV as well
	find_package( OpenCV REQUIRED )
ENDIF()


## specify the sources
file(GLOB XPIWIT_SOURCES
        ../../Source/Core/*/*.cpp
        ../../Source/Filter/Base/*/*.cpp
        ../../Source/Filter/Base/*/*.txx
        ../../Source/Filter/ITKCustom/*.cpp
        ../../Source/Filter/ITKCustom/*.txx
        ../../Source/Filter/ITKCustom/*.hxx
        ../../Source/Filter/ThirdParty/*.cpp
        ../../Source/Filter/ThirdParty/*.txx
        ../../Source/Filter/XpExtended/*/*.cpp
        ../../Source/Filter/XpExtended/*/*.txx
        ../../Source/Filter/XpWrapper/*/*.cpp
        ../../Source/Filter/XpWrapper/*/*.txx
)

## specify the headers
file(GLOB XPIWIT_HEADERS
        ../../Source/Core/*/*.h

        ../../Source/Filter/Base/*/*.h
        ../../Source/Filter/ITKCustom/*.h
        ../../Source/Filter/ThirdParty/*.h
        ../../Source/Filter/XpExtended/*/*.h
        ../../Source/Filter/XpWrapper/*/*.h
)

## Compiler flags
if(CMAKE_COMPILER_IS_GNUCXX)
        add_definitions(-DNDEBUG -DITKKIT_CLUSTERVERSION -DITK_IO_FACTORY_REGISTER_MANAGER -DQT_NO_STYLE_GTK)
        ##add_definitions(-DITKKIT_CLUSTERVERSION -DITK_IO_FACTORY_REGISTER_MANAGER -DQT_NO_STYLE_GTK)
        SET(CMAKE_CXX_FLAGS "-O2")
        ##SET(CMAKE_CXX_FLAGS "-O0 -g")
        ##SET(CMAKE_CXX_FLAGS_RELEASE "-O2")
        ##SET(CMAKE_CXX_FLAGS_DEBUG  "-O0 -g")
endif()

#compiler flags for MSVC
# VS 12+ requires /FS when used in parallel compilations
if (MSVC_VERSION GREATER 1799)
    #add_compile_options("/FS")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /FS")
    SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /FS")
	
	if (MSVC_VERSION LESS 1899)
		add_definitions(_VARIADIC_MAX=10)
	endif ()
endif ()

## generate executable using the specified sources and headers
ADD_EXECUTABLE(XPIWIT ${XPIWIT_SOURCES} ${XPIWIT_HEADERS} ${XPIWIT_MOC})

## link libraries to the target
## add_library(libtiff STATIC IMPORTED)
## set_property(TARGET libtiff PROPERTY IMPORTED_LOCATION /media/sf_D_DRIVE/Programming/ITKScreening/third_party/tiff-4.0.2/libtiff/.libs/libtiff.a)

TARGET_LINK_LIBRARIES(XPIWIT ${ITK_LIBRARIES} ${QT_LIBRARIES})

IF(USE_GPU)
	TARGET_LINK_LIBRARIES(XPIWIT Watershed_PBC_CUDAlib MorpholigcalOperators_CUDA_lib MembraneEnhancement_CUDA_lib ConvolutionSeparable3D_CUDA_lib SeparableConvolution3D_CUDA_lib MorphologicalReconstruction_CUDAlib MedianFilter2D_CUDAlib)

	## add definition for CUDA filters
	add_definitions(-DXPIWIT_USE_GPU)
	MESSAGE("Including GPU support. Make sure to set the CUDA related settings to the architecture of your machine (e.g., sm_20, sm_35).")
ENDIF()

IF( USE_NSCALE )
	#TARGET_LINK_LIBRARIES(XPIWIT debug ${NSCALE_LIBRARIES_DEBUG} optimized ${NSCALE_LIBRARIES_RELEASE} ) #in case they have different names
	TARGET_LINK_LIBRARIES(XPIWIT ${NSCALE_LIBRARIES})
	TARGET_LINK_LIBRARIES(XPIWIT ${OpenCV_LIBS})
	
	## add definition for CUDA filters
	add_definitions(-DXPIWIT_USE_NSCALE)
	MESSAGE("Including NScale support. Make sure to set the NScale related settings are properly set and that the library has been compiled.")
ENDIF()

qt5_use_modules(XPIWIT Core)