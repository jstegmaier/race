/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "CMDArguments.h"

// namespace header
#include "../Utilities/Logger.h"

// qt header
#include <QtCore/QFile>
#include <QtCore/QDir>

namespace XPIWIT
{

// the default constructor
CMDArguments::CMDArguments()
{
}


// function to check the inputs
bool CMDArguments::CheckInputs()
{
    bool everythingOk = true;
    QList< int > amountOfImages;
    CMDInOutArgument *tempIOArg = NULL;

    foreach( tempIOArg, mInputArguments )
        amountOfImages.append( tempIOArg->mImages.length() );

    if( amountOfImages.contains( 0 ) )
	{
        Logger::GetInstance()->WriteLine( "Error in input paths" );
        everythingOk = false;
    }

    // find maximum
    mNumberOfImages = 0;
    for( int i = 0; i < amountOfImages.length(); i++ )
	{
        mNumberOfImages = qMax( mNumberOfImages, amountOfImages.at( i ) );
    }

    // check if number of images is the same for every input
    for( int i = 0; i < amountOfImages.length(); i++ )
	{
        if( amountOfImages.at( i ) != 1 && amountOfImages.at( i ) != mNumberOfImages )
		{
            Logger::GetInstance()->WriteLine( "Error in input number: " + QString::number( i ) + " with id: " + mInputArguments.at( i )->mInputId );
            Logger::GetInstance()->WriteLine( "Error in input: Number of input images does not fit maximum number of input images" );
            everythingOk = false;
        }
    }

    // check for default input
    bool defaultIsSet = false;
    foreach( tempIOArg, mInputArguments )
	{
        if( tempIOArg->mIsDefault )
		{
            defaultIsSet = true;
        }
    }

    // set default if not user defined
    if( !defaultIsSet )
	{
        for(int i = 0; i < amountOfImages.length(); i++)
		{
            if( amountOfImages.at( i ) == mNumberOfImages )
			{
                mInputArguments.at( i )->mIsDefault = true;
                defaultIsSet = true;
                break;
            }
        }
    }

    if( mOutputArgument->mIsDir )
	{
        if( !mOutputArgument->mImages.isEmpty() )
		{
            Logger::GetInstance()->WriteLine( "Error in output path processing." );
            everythingOk = false;
        }
    }

    if( mOutputArgument->mIsList )
	{
        if( mOutputArgument->mImages.isEmpty() )
		{
            Logger::GetInstance()->WriteLine( "Error in output: No images found in list file" );
            everythingOk = false;
        }
		else
		{
            if( mOutputArgument->mImages.length() != mNumberOfImages )
			{
                Logger::GetInstance()->WriteLine( "Error in output: Number of output images does not fit number of input images" );
                everythingOk = false;
            }
        }
    }

    return everythingOk;
}


// get the cmd pipeline arguments
CMDPipelineArguments *CMDArguments::GetPipelineArguments( int number )
{
    CMDPipelineArguments *cmdPipelineArguments = new CMDPipelineArguments();
    cmdPipelineArguments->mMetaId = number;

    // inputs
    cmdPipelineArguments->mDefaultIndex = 0;
	int i = 0;
    foreach( CMDInOutArgument *currentInput, mInputArguments )
	{
        int currentIndex = 0;
        if( currentInput->mImages.length() > number )
            currentIndex = number;

        if( currentInput->mIsDefault )
            cmdPipelineArguments->mDefaultIndex = i;

        // set image inputs
        if( currentInput->mIsImage )
		{
            cmdPipelineArguments->mInputImageId.append( currentInput->mInputId );
            cmdPipelineArguments->mInputImagePaths.append( currentInput->mImages.at( currentIndex ) );
            cmdPipelineArguments->mInputDimension.append( currentInput->mDimension );
        }

        // set meta inputs
        if( currentInput->mIsCsv )
		{
            cmdPipelineArguments->mInputMetaId.append( currentInput->mInputId );
            cmdPipelineArguments->mInputMeta.append( currentInput->mImages.at( currentIndex ) );
        }

		i++;
    }

    // output
    cmdPipelineArguments->mStdOutputPath = mOutputArgument->mPath;
    cmdPipelineArguments->mPrefix = mOutputArgument->mPrefix;

    cmdPipelineArguments->mHasImageOutput = false;
    if( mOutputArgument->mIsList ){
        cmdPipelineArguments->mHasImageOutput = true;
        if( mOutputArgument->mImages.length() > number )
            cmdPipelineArguments->mImageOutputPath = mOutputArgument->mImages.at( number );
        else
            cmdPipelineArguments->mImageOutputPath = mOutputArgument->mImages.at( 0 ); // save all outputs on the same file -> warning?

        // set the output to the image root folder
        QStringList path = cmdPipelineArguments->mImageOutputPath.split("/");
        path.removeLast();
        cmdPipelineArguments->mStdOutputPath = path.join("/") + "/";
    }

    cmdPipelineArguments->mXmlPath = mXmlPath;
    cmdPipelineArguments->mSeed = mSeed;

    cmdPipelineArguments->mUseSubFolder = mUseSubFolder;
    cmdPipelineArguments->mSubFolderFormat = mSubFolderFormat;
    cmdPipelineArguments->mOutputFileFormat = mOutputFileFormat;

    cmdPipelineArguments->mFileLoggingEnabled = mFileLoggingEnabled;
    cmdPipelineArguments->mLockFileEnabled = mLockFileEnabled;
	cmdPipelineArguments->mSkipProcessingIfOutputExists = mSkipProcessingIfOutputExists;

    cmdPipelineArguments->mUseMetaDataHeader = mUseMetaDataHeader;
    cmdPipelineArguments->mMetaDataSeparator = mMetaDataSeparator;
    cmdPipelineArguments->mMetaDataDelimitor = mMetaDataDelimitor;
	
    return cmdPipelineArguments;
}


// function to get the number of images
int CMDArguments::GetNumberOfImages()
{
    if( mInputArguments.isEmpty() )
        return 0;

    return mInputArguments.at( 0 )->mImages.length();
}


// function to write the cmd arguments to the log file
void CMDArguments::WriteToLog()
{
    Logger *log = Logger::GetInstance();

    log->WriteSpacer();
    log->WriteLine("CMDArguments content: ");

    log->WriteLine( " - write filter list: " + BoolToString(mWriteFilterList) );
    log->WriteLine( " - filter list path: " + mFilterListPath );

    log->WriteLine( " - xml path: " + mXmlPath );

    foreach( CMDInOutArgument *inputArgument, mInputArguments)
	{
        log->WriteLine( " - input: " + inputArgument->mInputId + ", " + inputArgument->mPath + ", " + inputArgument->mDimension );
        log->Write( "    + is dir: " + BoolToString( inputArgument->mIsDir) );
        log->Write( ", is list: " + BoolToString( inputArgument->mIsList) );
        log->WriteLine( ", is file: " + BoolToString( inputArgument->mIsSingleFile) );
        log->WriteLine( "    + number of images: " + QString::number( inputArgument->mImages.length() ) );
    }

    log->WriteLine( " - output: " + mOutputArgument->mPath + ", " + mOutputArgument->mPrefix );
    log->Write( "    + is dir: " + BoolToString( mOutputArgument->mIsDir) );
    log->WriteLine( ", is list: " + BoolToString( mOutputArgument->mIsList) );
    log->WriteLine( "    + number of images: " + QString::number( mOutputArgument->mImages.length() ) );

    log->WriteLine( " - lock file: " + BoolToString(mLockFileEnabled) );
	log->WriteLine( " - skip processing if output exists: " + BoolToString(mSkipProcessingIfOutputExists) );
	log->WriteLine( " - log file: " + BoolToString(mFileLoggingEnabled) );

    log->WriteLine( " - sub folder: " + BoolToString(mUseSubFolder) );
    log->WriteLine( " - sub folder format: " + mSubFolderFormat.join( ", " ) );

    log->WriteLine( " - output format: " + mOutputFileFormat.join( ", " ) );

    // meta data
    log->WriteLine( " - meta data header: " + BoolToString(mUseMetaDataHeader) );
    log->WriteLine( " - meta data separator: " + mMetaDataSeparator );
    log->WriteLine( " - meta data delimitor: " + mMetaDataDelimitor );

    log->WriteSpacer();
}


// converts a boolean to a string
QString CMDArguments::BoolToString( bool inputBool )
{
    return QString( inputBool ? "true" : "false" );
}


// creates the input folder
void CMDArguments::CreateInputFolder()
{
	if( !mUseSubFolder )
        return;

    // create the folder
	QString outputPath = GetPipelineArguments( 0 )->mStdOutputPath;
    QString tempString;

    // add subfolder
    QString subFolder("input");
    QDir outputDir( outputPath );
    if( !( outputDir.exists( subFolder ) || outputDir.mkdir( subFolder ) ) )	// if folder doesn't exists and can not be created
        throw QString( "Error while creating subfolder. Path: " + outputPath + subFolder );
    outputPath += subFolder + "/";

    // copy xml
    QFile::copy( mXmlPath, outputPath +	mXmlPath.split("/").last() );	// maybe change to a default name "pipeline.xml"

    // create txt file with piping arguments
    QFile inputPipe( outputPath + "arguments.txt" );
    inputPipe.open( QIODevice::WriteOnly | QIODevice::Text );

    // inputs
    for( int i = 0; i < mInputArguments.length(); i++)
	{
        tempString = "--input ";                                     // number
        tempString += mInputArguments.at( i )->mInputId;
        if( mInputArguments.at( i )->mIsDefault )                    // default input
            tempString += "*";
        tempString += ", ";
        tempString += mInputArguments.at( i )->mPath + ", ";         // path
        tempString += mInputArguments.at( i )->mDimension;			 // dimension
        tempString += "\n";
        inputPipe.write( tempString.toUtf8() );
    }

    // output
    tempString = "--output ";
    tempString += mOutputArgument->mPath + ", ";
    tempString += mOutputArgument->mPrefix + "\n";
    inputPipe.write( tempString.toUtf8() );

    // xml
    tempString = "--xml ";
    tempString += mXmlPath + "\n";
    inputPipe.write( tempString.toUtf8() );

    // subfolder
    tempString = "--subfolder ";
    if( mUseSubFolder )
	{
        for( int j = 0; j < mSubFolderFormat.length(); j++ )
		{
            tempString += mSubFolderFormat.at( j );
            if( j + 1 < mSubFolderFormat.length() )
                tempString += ", ";
            else
                tempString += "\n";
        }
    }
	else
	{
        tempString += "off\n";
	}
    inputPipe.write( tempString.toUtf8() );

    // fileformat
    tempString = "--outputformat ";
	for( int j = 0; j < mOutputFileFormat.length(); j++ )
	{
        tempString += mOutputFileFormat.at( j );
        if( j + 1 < mOutputFileFormat.length() )
            tempString += ", ";
        else
            tempString += "\n";
    }
    inputPipe.write( tempString.toUtf8() );

    // lockfile
    if( mLockFileEnabled )
        tempString = "--lockfile on\n";
    else
        tempString = "--lockfile off\n";
    inputPipe.write( tempString.toUtf8() );

	// lockfile
	if( mSkipProcessingIfOutputExists )
        tempString = "--skipProcessingIfOutputExists on\n";
    else
        tempString = "--skipProcessingIfOutputExists off\n";
    inputPipe.write( tempString.toUtf8() );

    // logging
    if( mFileLoggingEnabled )
        tempString = "--logging file\n";
    else
        tempString = "--logging off\n";
    inputPipe.write( tempString.toUtf8() );


    //-- Meta Data --//
    if( mUseMetaDataHeader )
        tempString = "--metaDataHeader on\n";
    else
        tempString = "--metaDataHeader off\n";
    inputPipe.write( tempString.toUtf8() );

	tempString = "--metaDataSeparator " + mMetaDataSeparator + "\n";
    inputPipe.write( tempString.toUtf8() );

    tempString = "--metaDataDelimiter " + mMetaDataDelimitor + "\n";
    inputPipe.write( tempString.toUtf8() );


    // piping end
    tempString = "--end";
    inputPipe.write( tempString.toUtf8() );
}

} // XPIWIT namespace
