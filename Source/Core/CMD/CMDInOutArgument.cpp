/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "CMDInOutArgument.h"

// qt header
#include <QtCore/QFile>
#include <QtCore/QDir>


namespace XPIWIT
{

// the default constructor
CMDInOutArgument::CMDInOutArgument()
{
    mIsInput = false;
    mIsOutput = false;
    mIsImage = false;
    mIsCsv = false;
    mIsDefault = false;
    mIsSingleFile = false;
    mIsDir = false;
    mIsList = false;
}


// function to init the input
void CMDInOutArgument::InitInput( QStringList inputLine )
{
    mInputStringList = inputLine;

    // format: id, path, dimension/csv
    if( inputLine.length() < 3 )
        return;

	// check default marker
    mInputId = mInputStringList.at( 0 );
    if( mInputId.endsWith("*") )
	{
        SetIsDefault();
        mInputId = mInputId.replace( QString("*"), QString("") );
    }

    mPath = mInputStringList.at( 1 );
    mDimension = mInputStringList.at( 2 );

    SetIsInput();
    if( mDimension.compare( "csv", Qt::CaseInsensitive ) == 0 )
        SetIsCsv();
    else
        SetIsImage();

    ProcessPath();
    SetIsInput();
}


// function to init the output
void CMDInOutArgument::InitOutput( QStringList inputLine )
{
    mInputStringList = inputLine;

    // format: path, prefix
    if( inputLine.length() != 1 && inputLine.length() != 2 )
        return;

    mPath = mInputStringList.at( 0 );
	mPrefix = "";
	if( inputLine.length() == 2 )
		mPrefix = mInputStringList.at( 1 );

    // if ends with txt -> List
    if( mPath.endsWith(".txt") )
	{
        ProcessList();
        return;
    }

    if( mPath.endsWith("/") )
	{
        SetIsDir();
    }

    SetIsOutput();
}


// function to process either a path or a file
void CMDInOutArgument::ProcessPath()
{
    // if ends with txt -> List
    if( mPath.endsWith(".txt") )
	{
        ProcessList();
        return;
    }

    // if ends with / -> Dir
    if( mPath.endsWith("/") )
	{
        ProcessDir();
        return;
    }

    // otherwise file
    ProcessImage();
}


// process a whole list of images
void CMDInOutArgument::ProcessList()
{
    SetIsList();

    // open file
    QFile listFile( mPath );
    if( !listFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
        return;

    int lineLength = 0;
    while( lineLength >= 0 )
	{
        char buf[1024];
        lineLength = listFile.readLine( buf, sizeof(buf) );
        if (lineLength != -1)
		{
            mImages.append( QString( buf ).replace("\n", "") );
        }
    }

    listFile.close();
}


// process a complete directory
void CMDInOutArgument::ProcessDir()
{
    SetIsDir();

    QDir inputDir( mPath );
    QStringList allFiles = inputDir.entryList( QDir::Files | QDir::Readable |
                                               QDir::NoDotAndDotDot | QDir::NoSymLinks );
    QStringList clearedFiles;
    if( mIsImage )
	{
        foreach (const QString &str, allFiles)
		{
            // exclude common file extensions that are not images
            if ( str.contains(".txt")	|| str.contains(".csv")  ||
                 str.contains(".m")		|| str.contains(".prjz") ||
                 str.contains(".log")	|| str.contains(".xml")  )
                continue;
            clearedFiles += str;
        }
    }

    if( mIsCsv )
	{
        foreach (const QString &str, allFiles)
		{
            // only csv files
            if ( str.contains(".csv") )
                clearedFiles += str;
        }
    }


    for( int i = 0; i < clearedFiles.length(); i++ )
	{
        mImages.append( mPath + clearedFiles.at( i ) );
    }
}


// function to process the image
void CMDInOutArgument::ProcessImage()
{
    SetIsSingleFile();
    mImages.append( mPath );
}

} //namespace XPIWIT
