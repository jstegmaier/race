/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef CMDINOUTARGUMENT_H
#define CMDINOUTARGUMENT_H

// qt header
#include <QtCore/QString>
#include <QtCore/QStringList>


namespace XPIWIT
{

class CMDInOutArgument
{
public:
    CMDInOutArgument();
    void InitInput( QStringList );
    void InitOutput( QStringList );

    QString mInputId;       // number to map the id
    QString mPath;          // path of the input
    QStringList mImages;    // path of the images
    QString mPrefix;        // prefix for the output
    QString mDimension;     // dimension of the image

    bool mIsInput;
    bool mIsOutput;
    void SetIsInput()   { mIsInput = true; mIsOutput = false; }
    void SetIsOutput()  { mIsInput = false; mIsOutput = true; }

    bool mIsImage;
    bool mIsCsv;
    void SetIsImage()   { mIsImage = true; mIsCsv = false; }
    void SetIsCsv()     { mIsImage = false; mIsCsv = true; }

    bool mIsDefault;
    void SetIsDefault( bool isDefault = true ) { mIsDefault = isDefault; }

    bool mIsSingleFile;
    bool mIsDir;
    bool mIsList;
    void SetIsSingleFile()  { mIsSingleFile = true; mIsDir = false; mIsList = false; }
    void SetIsDir()         { mIsSingleFile = false; mIsDir = true; mIsList = false; }
    void SetIsList()        { mIsSingleFile = false; mIsDir = false; mIsList = true; }

private:
    QStringList mInputStringList;

    void ProcessPath();
    void ProcessList();
    void ProcessDir();
    void ProcessImage();
};

} //namespace XPIWIT

#endif // CMDINOUTARGUMENT_H

