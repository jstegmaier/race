/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef CMDPARSER_H
#define CMDPARSER_H

// namespace header
#include "CMDArguments.h"

// qt header
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QRegExp>


namespace XPIWIT
{

/**
 * @class CMDParser
 * @brief The CMDParser class processes the data from the cmd input.
 *        It can handle command line argumends as well as piped arguments.
 */
class CMDParser
{
public:
    CMDParser();
    ~CMDParser();

    CMDArguments *InitArguments();

    bool mWriteFilterList;
	bool mProcessPipeline;

private:
    CMDArguments *mCmdArguments;

    QStringList mEnableKey;
    QStringList mDisableKey;
    QStringList mSubFolderKeys;
    QStringList mOutputFormatKeys;

    QStringList PipedArguments();
    void ProcessArguments( QStringList arguments );

    QStringList ProcessParameter( QStringList arguments, QString argument );
    QStringList ProcessValues( QStringList arguments, int index );
    void ProcessIO( QStringList arguments, QString argument);
    bool ProcessFlagParameter( QStringList arguments , bool defaultFlag = true );

};

} //namespace XPIWIT

#endif // CMDPARSER_H

