/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef CMDPIPELINEARGUMENTS_H
#define CMDPIPELINEARGUMENTS_H

// qt header
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class CMDPipelineArguments
 * @brief The CMDPipelineArguments class represents all releveant data from the cmd for a pipeline
 */
class CMDPipelineArguments
{
public:
    CMDPipelineArguments()
	{
        mHasImageOutput = false;
        mDefaultIndex = 0;
        mMetaId = 0;
    }

    int mMetaId;

    QStringList mInputMetaId;       // number of the meta input
    QStringList mInputMeta;         // full path of the meta input

    int mDefaultIndex;				// define the default input for write result image name
    QStringList mInputImageId;      // number of the input image
    QStringList mInputImagePaths;   // full path of the input images
    QStringList mInputDimension;    // dimension of the input image

    QString mStdOutputPath;         // out path for all outputs
    QString mPrefix;

    bool mHasImageOutput;           // list mode
    QString mImageOutputPath;       // list mode full image path and name

    QString mXmlPath;               // full path of the xml
    int mSeed;                      // seed
    bool mLockFileEnabled;			// flag to set lockfile
	bool mSkipProcessingIfOutputExists;// if set, processing will be skipped if the output already exits 

    bool mUseSubFolder;				// write result to subfolder
    QStringList mSubFolderFormat;	// format of the foldername

    bool mFileLoggingEnabled;

    QStringList mOutputFileFormat;	// format of the filenames

    // Meta Data Handling
    bool mUseMetaDataHeader;
    QString mMetaDataSeparator;
    QString mMetaDataDelimitor;

private:
};

} //namespace XPIWIT

#endif // CMDPIPELINEARGUMENTS_H

