/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "XPIWITMainObject.h"

// namespace header
#include "../Utilities/Logger.h"
#include "../CMD/CMDParser.h"
#include "../CMD/CMDPipelineArguments.h"
#include "../XML/XMLReader.h"
#include "../XML/XMLCreator.h"
#include "../XML/AbstractPipelineTools.h"

// project header
#include "../../Filter/Base/Management/ProcessObjectPipeline.h"
#include "../../Filter/Base/MetaData/MetaDataBase.h"
#include "../../Filter/Base/MetaData/MetaDataImage.h"
#include "../../Filter/Base/MetaData/MetaDataFilter.h"

// qt header
#include <QtCore/QDateTime>
#include <QtCore/QProcess>

#if WIN64 || WIN32
//
#else
#include <unistd.h>
#endif


namespace XPIWIT
{

// the default constructor
XPIWITMainObject::XPIWITMainObject( QObject* parent ) : QObject( parent )
{
    mNoFailureOccurred = true;
	mWriteFilterList = false;
	mProcessPipeline = false;
}


// the default destructor
XPIWITMainObject::~XPIWITMainObject()
{

}


// function to run the pipeline
void XPIWITMainObject::run()
{
    // initialize the project and open log files
    this->Init();

    // process the cmd/piped arguments and read the xml
    if( mNoFailureOccurred )
        this->ProcessInputData();

	// parse the xml pipeline document
    if( mProcessPipeline && mNoFailureOccurred )
        this->ParseXmlPipeline();

	// write the filter list
	if( mWriteFilterList && mNoFailureOccurred )
        this->WriteFilterList();

    // process the images with the pipeline
    if( mProcessPipeline && mNoFailureOccurred )
        this->ProcessPipeline();

    // free all objects and close files
    this->Finalize();

	// emit the finished signal to trigger application termination
    emit finished();
}


// initialization function
void XPIWITMainObject::Init()
{
}


// process the input data using the cmd parser
void XPIWITMainObject::ProcessInputData()
{
    // CMD
    CMDParser cmdParser;
	try
	{
		mCmdArguments = cmdParser.InitArguments();
	}
	catch(...)
	{
		Logger::GetInstance()->WriteLine( "Error while parsing input arguments." );
		Logger::GetInstance()->WriteLine( "Critical Error - execution aborted" );
		mNoFailureOccurred = false;
	}
	
	// get flags
	mWriteFilterList = cmdParser.mWriteFilterList;
	mProcessPipeline = cmdParser.mProcessPipeline;
}


// parse the XML pipeline and generate an abstract pipeline object
void XPIWITMainObject::ParseXmlPipeline()
{
	// XML
    XMLReader xmlReader;
	try
	{
		xmlReader.readXML( mCmdArguments->mXmlPath );
	}
	catch(...)
	{
		Logger::GetInstance()->WriteLine( "Error while parsing xml pipeline." );
		Logger::GetInstance()->WriteLine( "Critical Error - execution aborted" );
		mNoFailureOccurred = false;
	}

    abstractPipeline = xmlReader.getAbstractPipeline();

    // Sort Pipeline and set flags
    AbstractPipelineTools abstractPipelineTools( &abstractPipeline );
	try
	{
		abstractPipelineTools.OrderPipeline();
	}
	catch(...)
	{
		Logger::GetInstance()->WriteLine( "Error while creating abstract pipeline." );
		Logger::GetInstance()->WriteLine( "Critical Error - execution aborted" );
		mNoFailureOccurred = false;
	}
}


// write the entire filter list of all available filters
void XPIWITMainObject::WriteFilterList()
{
	XMLCreator xmlCreator;
	try
	{
		xmlCreator.WriteXML( mCmdArguments->mFilterListPath );
	}
	catch(...)
	{
		Logger::GetInstance()->WriteLine( "Error while creating filter list." );
	}
}


// process the current pipeline
void XPIWITMainObject::ProcessPipeline()
{
	// write general log
    Logger *logger = Logger::GetInstance();
	CMDPipelineArguments *currentCmdPipelineArguments = mCmdArguments->GetPipelineArguments( 0 );
	logger->SetUseSubfolder( currentCmdPipelineArguments->mUseSubFolder );
	logger->InitLogFile( true, currentCmdPipelineArguments->mStdOutputPath, "Overview" );

    mCmdArguments->WriteToLog();
    abstractPipeline.WriteToLog();

	mCmdArguments->CreateInputFolder();
	logger->ReleaseLogFile();

	// generate the lockfile if the setting is enabled
	bool useLockFile = mCmdArguments->mLockFileEnabled;
	if (useLockFile == true)
	{
		// check if lock already exists for the given node
		QProcess myprocess;
		myprocess.start( "/bin/hostname", QStringList() );

		QByteArray mydata;
		while(myprocess.waitForReadyRead())
			mydata.append(myprocess.readAll());

		// set the hostname 
		const char *hostname = mydata; //getenv( "HOSTNAME" );
		QString lockFile = currentCmdPipelineArguments->mStdOutputPath + QString(hostname) + QString(".lockfile");
		lockFile.remove( "\n" );

		const int sleepTime = 10000;
		quint64 startTime = QDateTime::currentMSecsSinceEpoch();
		srand( mCmdArguments->mSeed );

		if (hostname)
		{
			std::cout << "- Executing XPIWIT on Host: " << (hostname ? hostname : "Hostname not defined!") << std::endl;
			std::cout << "- Random Seed is set to : " << currentCmdPipelineArguments->mSeed << std::endl;

			#if WIN64 || WIN32
				Sleep( rand() % 20000 );
			#else
				usleep( rand() % 20000000 );
			#endif

			QFile file( lockFile );
			QString line;
			while ( file.exists() == true )
			{
				#if WIN64 || WIN32
					Sleep( rand() % 20000 );
				#else
					usleep( rand() % 20000000 );
				#endif

				std::cout << "- Waited for node capacity for " << (QDateTime::currentMSecsSinceEpoch()-startTime)/60000.0 << " minutes..." << std::endl;
			}

			std::cout << "- Waited for node capacity for " << (QDateTime::currentMSecsSinceEpoch()-startTime)/60000.0 << " minutes..." << std::endl;

			std::cout << "- Creating lock file: " << lockFile.toStdString().c_str() << std::endl;
			file.open( QIODevice::ReadWrite );
		}
		else
		{
			std::cout << "- Hostname not defined. Trying to process anyway." << std::endl;
		}
	}

	// start processing //
	ProcessObjectPipeline processObjectPipeline;
	logger->WriteLine( "Start image processing" );
	logger->WriteLine( "" );

	logger->InitGlobalLog( currentCmdPipelineArguments->mStdOutputPath, "", mCmdArguments->mMetaDataSeparator );

	for( int i = 0; i < mCmdArguments->GetNumberOfImages(); i++ )
	{

		QDateTime time = QDateTime::currentDateTime();

		try{
			currentCmdPipelineArguments = mCmdArguments->GetPipelineArguments( i );
			logger->WriteLine( "Process: " + currentCmdPipelineArguments->mInputImagePaths.at( currentCmdPipelineArguments->mDefaultIndex ) );
			processObjectPipeline.run( currentCmdPipelineArguments, abstractPipeline );
			logger->WriteLine( "" );
		}catch(...){
			Logger::GetInstance()->WriteToAll( "Error while executing pipeline for input set ." + QString::number( i ) );
			// print name and path of image
			if( !currentCmdPipelineArguments->mInputImagePaths.isEmpty() ){
				if( currentCmdPipelineArguments->mInputImagePaths.length() > currentCmdPipelineArguments->mDefaultIndex ){
					QString path = currentCmdPipelineArguments->mInputImagePaths.at( currentCmdPipelineArguments->mDefaultIndex );
					Logger::GetInstance()->WriteToAll( "Error for image: " + path.split("/").last() );
					Logger::GetInstance()->WriteToAll( "In path: " + path );
				}
			}
			mNoFailureOccurred = false;
		}

		// write global log line
		// print the time delta of the current action
		// msecsTo doesn't work with Linux?
		float timeDelta = time.msecsTo( QDateTime::currentDateTime() ) / 1000.0;
		
		logger->WriteSpacer();
		logger->WriteLine( "--> Pipeline successfully executed in " + QString::number(timeDelta) + " seconds." );
		WriteGlobalLogFile( QString::number( i + 1 ), QString::number( timeDelta ), currentCmdPipelineArguments );

		logger->ReleaseLogFile();
	}

	WriteCombinedMetaData();

	// remove the lockfile after successful processing
	QProcess myprocess;
	myprocess.start( "/bin/hostname", QStringList() );

	QByteArray mydata;
	while(myprocess.waitForReadyRead())
		mydata.append(myprocess.readAll());

	const char* hostname = mydata;
	QString lockFile = currentCmdPipelineArguments->mStdOutputPath + QString(hostname) + QString(".lockfile");
	lockFile.remove( "\n" );

	// remove the lock file
	std::cout << "- Releasing lock file: " << lockFile.toStdString().c_str() << std::endl;
	QFile::remove( lockFile );
}


// function to write the global log file. Useful if multiple files are processed at once
void XPIWITMainObject::WriteGlobalLogFile( QString id, QString timeDelta, CMDPipelineArguments *pipelineArguments )
{
	QString image = pipelineArguments->mInputImagePaths.at( pipelineArguments->mDefaultIndex );

	MetaDataBase *metaDataBase = MetaDataBase::GetInstance();
	MetaDataImage *curImage = metaDataBase->GetImageMeta( image );

	QStringList line;
	QStringList headerLine;

	line << id << timeDelta << image;
	if( pipelineArguments->mHasImageOutput )
		line << pipelineArguments->mImageOutputPath;

	foreach( MetaDataFilter *i_filter, curImage->GetMetaDataList() )
	{
		headerLine << i_filter->mPostfix;
		line << i_filter->mOutputPath;
	}

	Logger::GetInstance()->WriteGlobalLine( line, pipelineArguments->mHasImageOutput, headerLine );
}


// write combined meta data
void XPIWITMainObject::WriteCombinedMetaData()
{
	MetaDataBase *metaDataBase = MetaDataBase::GetInstance();
	MetaDataImage *metaDataImage = metaDataBase->GetImageMeta( 0 );

	// hierarchy = Base -> List< Image > -> List< Filter >
	// data should be combined for single filter over all images -> bit more effort to get the data

	// create index list
	QList< int > globalMetaDataIndex;
    QList< MetaDataFilter *> metaDataFilterList = metaDataImage->GetMetaDataList();
    for( int i = 0; i < metaDataFilterList.length(); i++ )
	{
        // create indices
        if( !metaDataFilterList.at( i )->mIsMultiDimensional )
            globalMetaDataIndex.append( i );
    }

	// combine all non multidiemsnional data
    for( int i = 0; i < globalMetaDataIndex.length(); i++ )
	{

        MetaDataFilter *tempGlobal = new MetaDataFilter();
		MetaDataFilter *metaFilter = metaDataImage->GetMetaDataList().at( globalMetaDataIndex.at( i ) );

		// meta
		tempGlobal->mFilterId = metaFilter->mFilterId;
		tempGlobal->mFilterName = metaFilter->mFilterName;
		tempGlobal->mPostfix = metaFilter->mPostfix;

		// data
		tempGlobal->mTitle << "id" << metaFilter->mTitle;
		tempGlobal->mType << "int" << metaFilter->mType;

		for (int j = 0; j < metaDataBase->GetNumberImages(); j++)
		{
			metaDataImage = metaDataBase->GetImageMeta( j );
			metaFilter = metaDataImage->GetMetaDataList().at( globalMetaDataIndex.at( i ) );
			QList<float> data = metaFilter->mData.first();

			data.prepend( j+1 );
			tempGlobal->mData.append( data );
		}

        // write to result folder
		CMDPipelineArguments *pipeArg = mCmdArguments->GetPipelineArguments( 0 );

		QString metaOutputPath = pipeArg->mStdOutputPath;
		if( pipeArg->mUseSubFolder )
			metaOutputPath += "result/";
		metaOutputPath += metaFilter->mFilterId + "_" + QDateTime::currentDateTime().toString( "yyyy-MM-dd_hh-mm" ) ;		// "dd_MM_yy_hh_mm"

		tempGlobal->WriteFilter( metaOutputPath, true, pipeArg->mMetaDataSeparator, pipeArg->mMetaDataDelimitor );

        // clean up
        delete( tempGlobal );
    }
}


// clean up and write success message
void XPIWITMainObject::Finalize()
{
	delete mCmdArguments;
	Logger::GetInstance()->WriteLine( "Pipeline processing finished" );
}

} // namespace XPIWIT
