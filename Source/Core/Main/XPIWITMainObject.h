/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef XPIWITMAINOBJECT_H
#define XPIWITMAINOBJECT_H

// namespace header
#include "../CMD/CMDArguments.h"
#include "../XML/AbstractPipeline.h"

// qt header
#include <QtCore/QObject>

class CMDPipelineArguments;


namespace XPIWIT
{
/**
 *	@class XPIWITMainObject
 *	Main class of the XPIWIT Core project
 */
class XPIWITMainObject : public QObject
{
    Q_OBJECT
    public:

        XPIWITMainObject( QObject *parent = 0 );

        ~XPIWITMainObject();

        void Init();

        void ProcessInputData();

		void ParseXmlPipeline();

		void WriteFilterList();

        void ProcessPipeline();

		void WriteGlobalLogFile( QString id, QString timeDelta, CMDPipelineArguments *);
		void WriteCombinedMetaData();

        void Finalize();

        // return true for failure
        bool HasFailureOccured() { return !mNoFailureOccurred; }

    signals:
        void finished();

    public slots:
        void run();

    private:
        bool mProcessPipeline;
		bool mWriteFilterList;
		bool mNoFailureOccurred;


        CMDArguments *mCmdArguments;
        AbstractPipeline abstractPipeline;
};

} // namespace XPIWIT

#endif  // XPIWITMAINOBJECT_H
