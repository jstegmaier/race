/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#define _SCL_SECURE_NO_WARNINGS

// namespace header
#include "XPIWITMainObject.h"
#include "../../Filter/Base/Management/ProcessObjectManager.h"
#include "../../Filter/Base/Management/ProcessObjectBase.h"
#include "../../Filter/Base/Management/ProcessObjectProxy.h"

// qt header
#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

// system header
#include <iostream>
#include <string>

// the namespaces used
using namespace XPIWIT;

ProcessObjectManager* gProcessObjectManager = ProcessObjectManager::GetInstance();

// the main function
int main(int argc, char *argv[])
{
    std::cout << "Starting XPIWIT" << std::endl;

    // create the application
    QCoreApplication app(argc, argv);

    // create the XPIWIT main object
    XPIWITMainObject *xpMainObject = new XPIWITMainObject( &app );

    // handle event loop stuff //

    // finish the event loop when done
    QObject::connect(xpMainObject, SIGNAL( finished() ), &app, SLOT( quit() ) );

    // run xpiwit
    QTimer::singleShot( 0, xpMainObject, SLOT( run() ) );

    // close application
    int returnCode = app.exec();
    bool failureOccurred = xpMainObject->HasFailureOccured();

    if( failureOccurred || returnCode != 0 )
	{
        std::cout << " - ERROR OCCURRED - " << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << " - SUCCESS - " << std::endl;
    return EXIT_SUCCESS;
}
