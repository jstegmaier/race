/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef LOGGER_H
#define LOGGER_H

// qt header
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QFile>
#include <sstream>


namespace XPIWIT
{

class Logger : public QObject
{
    public:
        // singleton methods
        static Logger *GetInstance( QObject* parent = 0 );
        static void DeleteInstance();

        // logger methods //

        // init for use of file and console
        void InitLogFile( bool fileLogging, QString rootDirectory, QString fileName );
        void SetNewLogFile( QString rootDirectory, QString fileName );
        void ReleaseLogFile();
        void SetUseSubfolder( bool useSubfolder ) { m_UseSubfolder = useSubfolder; }

        // writer for default log
        void Write( QString );
        void WriteLine( QString );
        void WriteException( std::exception err );
        void WriteSpacer();

        // special writer methods
        void WriteToConsole( QString );
        void WriteToAll( QString );

        // global log writer
        void InitGlobalLog( QString rootDirectory, QString fileName, QString separator = ";" );
        void WriteGlobalLine( QStringList , bool hasImageOutput , QStringList );

    private:
        static Logger* m_Instance;

        Logger( QObject* parent = 0 );
        ~Logger();

        QString m_StdLogPath;
        QString m_GlobalLogPath;

        std::streambuf *coutbuf;
        std::streambuf *cerrbuf;
        std::streambuf *filebuf;

        std::ostream coutStream;

        QString m_CSVSeparator;
        QFile m_GlobalLogFile;

        bool m_UseSubfolder;
        bool m_LogFileExists;
        bool m_GlobalLogExists;
        bool m_GlobalLogHeaderExists;

        void WriteGlobalLogHeader( bool hasImageOutput , QStringList metaFiles );
};

} // Namespace

#endif // LOGGER_H
