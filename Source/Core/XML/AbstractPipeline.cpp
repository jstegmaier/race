/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "AbstractPipeline.h"

// namespace header
#include "AbstractInput.h"


namespace XPIWIT
{

// default constructor
AbstractPipeline::AbstractPipeline()
{
}


// add an item to the abstract pipeline
void AbstractPipeline::AddItem(AbstractFilter* inputItem)
{
	//if( mPipelineItemSet == NULL )
	//	mPipelineItemSet = new QHash< QString, AbstractFilter* >;

    mPipelineItemSet.insert( inputItem->GetId(), inputItem );
	mPipelineSequence.append( inputItem->GetId() );
}


// remove an item from the abstract pipeline
void AbstractPipeline::RemoveItem(AbstractFilter* item, bool autoDelete)
{
	if (autoDelete == true)
		delete item;
	mPipelineItemSet.remove(item->GetId());
	mPipelineSequence.removeAll(item->GetId());
}


// remove a pipeline item from the abstract pipeline based on an integer id
void AbstractPipeline::RemoveItemById(int id, bool autoDelete)
{
	QString itemId = QString().sprintf("item_%04d", id);
	
	if (autoDelete == true)
		delete mPipelineItemSet.value(itemId);
	mPipelineItemSet.remove(itemId);
	mPipelineSequence.removeAll(itemId);
}

// remove a pipeline item from the abstract pipeline based on an string id
void AbstractPipeline::RemoveItemById(QString itemId, bool autoDelete)
{	
	if (autoDelete == true)
		delete mPipelineItemSet.value(itemId);
	mPipelineItemSet.remove(itemId);
	mPipelineSequence.removeAll(itemId);
}

// return the filter integer id derived from the name
int AbstractPipeline::GetFilterNumberById(QString filterId)
{
	/*
	QString number = filterId.mid(5, 4);
	return number.toInt();
	*/
    return mPipelineSequence.indexOf( filterId );
}


// get the number of filters
int AbstractPipeline::GetNumFilter()
{
    //return mPipelineSequence.length();
	return mPipelineItemSet.size();
}


// write the pipeline structure to the log file
void AbstractPipeline::WriteToLog()
{
    Logger *log = Logger::GetInstance();
    log->WriteSpacer();
    log->WriteLine( "Pipeline sequence:" );

	QHash< QString, AbstractFilter* >::iterator itemIterator = mPipelineItemSet.begin();
	for (; itemIterator != mPipelineItemSet.end(); ++itemIterator)
	{
		AbstractFilter* curItem = itemIterator.value();
		log->WriteLine( "Item id: " + curItem->GetId() );
        log->WriteLine( " - Filter name: " + curItem->GetName() );
	}
	log->WriteSpacer();
}


// get a filter by its number
AbstractFilter* AbstractPipeline::GetFilter( int num )
{
	// convert the number to an item id
	//QString itemId = QString().sprintf("item_%04d", num);
	QString itemId = mPipelineSequence.at( num );

    if( mPipelineItemSet.contains( itemId )) // mPipelineSequence.at(num)
        return mPipelineItemSet.value( itemId ); // mPipelineSequence.at(num)

    Logger::GetInstance()->WriteLine( "SimplifiedPipeline::GetFilter - filter id not found: " + itemId ); //mPipelineSequence.at(num)
    return NULL;
}

// get a filter by its number
AbstractFilter* AbstractPipeline::GetFilter( QString itemId )
{
    if( mPipelineItemSet.contains( itemId )) // mPipelineSequence.at(num)
        return mPipelineItemSet.value( itemId ); // mPipelineSequence.at(num)

    Logger::GetInstance()->WriteLine( "SimplifiedPipeline::GetFilter - filter id not found: " + itemId ); //mPipelineSequence.at(num)
    return NULL;
}

// get the abstract pipeline
QHash<QString, AbstractFilter*>* AbstractPipeline::GetPipelineItemSet()
{
    return &mPipelineItemSet;
}

// get the pipeline sequence
QList< QString > AbstractPipeline::GetPipelineSequence()
{
	return mPipelineSequence;
}

// set the pipeline sequence
void AbstractPipeline::SetPipelineSequence(QList<QString> pipelineSequence)
{
    mPipelineSequence = pipelineSequence;
}


// clear the current pipeline
void AbstractPipeline::Clear()
{
	if(mPipelineItemSet.size() > 0)
	{
		QHash< QString, AbstractFilter* >::iterator itemIterator = mPipelineItemSet.begin();
		for (; itemIterator != mPipelineItemSet.end(); ++itemIterator)
			delete itemIterator.value();

		mPipelineItemSet.clear();
	}

	mPipelineSequence.clear();
}

} //namespace XPIWIT
