/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef ABSTRACTPIPELINE_H
#define ABSTRACTPIPELINE_H

// namespace header
#include "../Utilities/Logger.h"
#include "AbstractFilter.h"

// qt header
#include <QtCore/QString>
#include <QtCore/QHash>
#include <QtCore/QList>


namespace XPIWIT 
{

/**
 * @class AbstractPipeline
 * Holds a list of AbstractFilter
 */
class AbstractPipeline
{
public:
    AbstractPipeline();
	AbstractPipeline operator=(AbstractPipeline abstractPipeline)
	{
		mPipelineItemSet = *abstractPipeline.GetPipelineItemSet();
		mPipelineSequence = abstractPipeline.GetPipelineSequence();
		return *this;
	}

    void AddItem( AbstractFilter* inputItem );
	void RemoveItem( AbstractFilter* item, bool autoDelete = true );
	void RemoveItemById( int id, bool autoDelete = true );
	void RemoveItemById(QString itemId, bool autoDelete=true);

    int GetFilterNumberById( QString filterId );
    int GetNumFilter();
    AbstractFilter* GetFilter( int num );
	AbstractFilter* GetFilter( QString itemId );
	//const AbstractFilter* GetFilterPointer( int num );	

    QHash< QString, AbstractFilter* >* GetPipelineItemSet();
	QList< QString > GetPipelineSequence();
    void SetPipelineSequence( QList< QString > pipelineSequence );

    void Clear();
    void WriteToLog();

private:
    QHash< QString, AbstractFilter* > mPipelineItemSet; // stores the actual items
    QList< QString > mPipelineSequence;	// stores the processing order of the pipeline items, such that inputs are available
};

} //namespace XPIWIT

#endif // ABSTRACTPIPELINE_H
