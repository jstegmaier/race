/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "AbstractPipelineTools.h"

// namespace header
#include "../Utilities/Logger.h"
#include "AbstractFilter.h"
#include "AbstractInput.h"


namespace XPIWIT
{

// the default constructor
AbstractPipelineTools::AbstractPipelineTools( AbstractPipeline *abstractPipeline )
{
    SetAbstractPipeline( abstractPipeline );
}


// function to set the abstract pipeline
void AbstractPipelineTools::SetAbstractPipeline( AbstractPipeline *abstractPipeline )
{
    mAbstractPipeline = abstractPipeline;
    mPipelineItemSet = mAbstractPipeline->GetPipelineItemSet();
	mPipelineSequence.clear();
}


// function to get the abstract pipeline
AbstractPipeline *AbstractPipelineTools::GetAbstractPipeline()
{
    return mAbstractPipeline;
}


// function to sort the pipeline
void AbstractPipelineTools::OrderPipeline()
{
    // idea: two lists
    // first of the format: id -> all required id's (listOfFilterIdsWithRequiredInputs)
    // second: list of ids (mPipelineSequence)
    // first list is initialized with all filter id's and their required inputs, second list is empty
    // now add filters with available inputs to second list and delete from first till first is empty

    QList< QPair< QString, QStringList > > *listOfFilterIdsWithRequiredInputs = new QList< QPair< QString, QStringList > >;     // List< id -> all required id's >
    CreateListOfFilterIds( listOfFilterIdsWithRequiredInputs );

    while( !listOfFilterIdsWithRequiredInputs->isEmpty() )
	{
        int numberOfFilter = FindFirstFilterWithAvailableInputs( listOfFilterIdsWithRequiredInputs );

        if( numberOfFilter == -1 )
            break;

        mPipelineSequence.append( listOfFilterIdsWithRequiredInputs->at( numberOfFilter ).first );  // add filter to sequenz
        listOfFilterIdsWithRequiredInputs->removeAt( numberOfFilter );                              // delete filter from list
    }


    if( !listOfFilterIdsWithRequiredInputs->isEmpty() )
	{
        Logger *log = Logger::GetInstance();
        log->WriteSpacer();
        log->WriteLine( "Error in Pipeline creation" );
        log->WriteLine( "Current filter sequence: " );
        for( int i = 0; i < mPipelineSequence.length(); i++ )
            log->Write( " - " + mPipelineSequence.at( i ) );
        log->WriteLine( "Filter without required inputs. Format: (filterid ( list of required ids )): " );
        for( int i = 0; i < mPipelineSequence.length(); i++ )
            log->WriteLine( " - " + listOfFilterIdsWithRequiredInputs->at( i ).first + " (" + listOfFilterIdsWithRequiredInputs->at( i ).second.join(", ") + " )" );
        log->WriteSpacer();

        throw QString( "Error: SimplifiedPipeline::OrderPipeline: \n IO linking error. Not all filter can be processed" );
    }

	mAbstractPipeline->SetPipelineSequence( mPipelineSequence );

    SetReleaseFlags();
}


// function to create a list of filter ids
void AbstractPipelineTools::CreateListOfFilterIds( QList<QPair<QString, QStringList > > *filterList )
{
    QStringList filterIds = mPipelineItemSet->keys();
    for( int i = 0; i < filterIds.length(); i++ )
	{
        QString curId = filterIds.at( i );
        AbstractFilter* curFilter =  mPipelineItemSet->value( curId );
        QStringList requiredInputIds = curFilter->GetRequiredIds();
        filterList->append( QPair< QString, QStringList >( curId, requiredInputIds ) );
    }
}


// function to find the first filter that has already available inputs
int AbstractPipelineTools::FindFirstFilterWithAvailableInputs( QList<QPair<QString, QStringList> > *filterList )
{
    if( filterList->isEmpty() )
        return -1;

    for( int i = 0; i < filterList->length(); i++ )
	{
        QStringList neededIds = filterList->at( i ).second;
        if( IdsAvailable( neededIds ) )
            return i;
    }

    return -1; // error
}


// function to check if all required ids are available
bool AbstractPipelineTools::IdsAvailable( QStringList idList )
{
    if( idList.isEmpty() )
        return true;

    for( int i = 0; i < idList.length(); i++ ){
        if( !mPipelineSequence.contains( idList.at( i ) ) && idList.at( i ).compare("cmd") != 0 )
            return false;
    }
    return true;
}


// function to set the release data flags
void AbstractPipelineTools::SetReleaseFlags()
{
    QList< QPair< QString, int > > filterIdWithOutput;

    for( int i = mPipelineSequence.length() - 1; i >= 0; i-- ){
        QString curId = mPipelineSequence.at( i );
        AbstractFilter* curFilter =  mPipelineItemSet->value( curId );

        //  get inputs
        QList< AbstractInput> imageInputs = curFilter->GetImageInputs();
        for( int j = 0; j < imageInputs.length(); j++ ){
            AbstractInput input = imageInputs.at( j );
            SetKeepOuputFlags( input.mIdRef, input.mNumberRef );
        }
    }
}


// function to set the keep output flags
void AbstractPipelineTools::SetKeepOuputFlags( QString filterId, int imageNum )
{
    AbstractFilter* curItem = mPipelineItemSet->value( filterId );

	if (curItem != NULL)
		curItem->SetKeepOutput( imageNum );
}

} //namespace XPIWIT
