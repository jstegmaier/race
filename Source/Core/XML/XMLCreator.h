/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef XPIWITGUI

#ifndef XMLCREATOR_H
#define XMLCREATOR_H

// qt header
#include <QtCore/QByteArray>
#include <QtCore/QIODevice>


namespace XPIWIT
{

/**
 * @class XMLCreator
 * Produces a list of all available Filter and its default values
 */
class XMLCreator
{
public:
    /**
     * The constructor.
     */
    XMLCreator();

    /**
      * return the xml
      */
    QByteArray GetXML();

    /**
      * writes the xml
      */
    void WriteXML( QString file );

    /**
     * set or unset the write description flag
     */
    void SetWriteDescription( bool writeDescription ) { mWriteDescription = writeDescription; }

private:

    /**
     * create the XML Document
     */
    void CreateXMLFilterList( QIODevice *dev );

    /**
     * defines if description is written or not
     */
    bool mWriteDescription;

	/**
     * defines if type is written or not
     */
	bool mWriteType;

};

} //namespace XPIWIT

#endif // XMLCREATOR_H

#endif
