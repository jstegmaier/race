/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "XMLReader.h"

namespace XPIWIT
{
// example website: http://developer.nokia.com/Community/Wiki/QXmlStreamReader_to_parse_XML_in_Qt

// the default constructor
XMLReader::XMLReader()
{
//	mFilterList = new QList<AbstractFilter>;
	mMaximumFilterId = 0;
}


// the default destructor
XMLReader::~XMLReader()
{
//	delete(mFilterList);
}


// function to read t he XML file
void XMLReader::readXML( QString file, QStringList disabledFilters )
{
    bool pipelineTagFound = false;
	bool filterListTagFound = false;
    bool itemTagFound = false;

    // just in case
    xml.clear();
    mAbstractPipeline.Clear();

    CheckIdAndAddToList( "cmd" );   // id reserved for cmd inputs

    // open file
    QFile xmlFile(file);
    if( !xmlFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
        throw QString("Error: can't open xml file! Filename: " + file);

    xml.setDevice( &xmlFile );

    bool readPipeline = false;
	bool readFilterList = false;
	// check if filter list
	if( file.endsWith("myfilters.xml")){ readFilterList = true;}

    QXmlStreamReader::TokenType token = xml.readNext();

    while ( !xml.atEnd() && !xml.hasError() )
	{
        // read token
        token = xml.readNext();

        // continue if header ( xml version and encoding )
        if(token == QXmlStreamReader::StartDocument)
		{
            continue;
        }
        
		// continue if comment
        if(token == QXmlStreamReader::Comment)
		{
            continue;
        }

        if(token == QXmlStreamReader::StartElement)
		{
			// Pipeline
            if( xml.name() == "pipeline" )
			{
                readPipeline = true;
                pipelineTagFound = true;
                continue;
            }

            if(readPipeline)
			{
                if( xml.name() == "item" )
				{
                    itemTagFound = true;
                    AbstractFilter* curItem = readItem();

					if (!disabledFilters.contains(curItem->GetName()))
						mAbstractPipeline.AddItem( curItem );

                    continue;
                }
            }
			// Filter list
			if( xml.name() == "filterlist" && readFilterList)
			{
				filterListTagFound = true;
				continue;
			}
			if( xml.name() == "item" && filterListTagFound)
			{
				itemTagFound = true;
                AbstractFilter* curItem = readItem();

				if (!disabledFilters.contains(curItem->GetName()))
					mFilterList.append( curItem );

				continue;
			}
        }
    }

    if( (!pipelineTagFound || !itemTagFound) && !readFilterList )
        throw ErrorMessage("readXML -- No pipeline tag found or no items in xml.");    // throw error message
	
	if( readFilterList && !filterListTagFound)
		throw ErrorMessage("readXML -- could not read filter list \"myfilters.txt\".");		// throw error message

    if ( xml.hasError() )
        throw ErrorMessage("readXML");    // throw error message
}


// function to read a single item
AbstractFilter* XMLReader::readItem()
{
    AbstractFilter* curItem = new AbstractFilter();
	// flag because input and output have image sub tags
    bool readInput = false;

    while( !(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "item") )
	{
        if ( xml.hasError() )
            throw ErrorMessage("readItem");    // throw error message

        if(xml.tokenType() == QXmlStreamReader::StartElement)
		{
            if( xml.name() == "item" )
			{
                QString curId = GetAttribute( "item_id", true );
                CheckIdAndAddToList( curId );
                curItem->SetId( curId );

				QStringRef currentNumber(&curId, curId.length()-4, 4);
				if (currentNumber.toInt() > mMaximumFilterId)
					mMaximumFilterId = currentNumber.toInt();
            }

            if( xml.name() == "name" )
			{
                xml.readNext();
                QString curName = xml.text().toString();
                curItem->SetName( curName );
            }

			if (xml.name() == "description"){
				QString curDes = xml.readElementText();
				curItem->SetDescription( curDes );
			}

            if( xml.name() == "input" ){
                readInput = true;
				curItem->SetNumberImageIn(xml.attributes().value("number_images").toString().toInt());
				curItem->SetNumberMetaIn(xml.attributes().value("number_meta").toString().toInt());
            }

            if( xml.name() == "output" )
			{
                readInput = false;
				curItem->SetNumberImageOut(xml.attributes().value("number_images").toString().toInt());
				curItem->SetNumberMetaOut(xml.attributes().value("number_meta").toString().toInt());
            }
			
			if( xml.name() == "type" )
			{
                QString type = GetAttribute( "type_name" );
				curItem->SetType( type );
				int typeId = GetAttribute( "type_number" ).toInt();
				curItem->SetTypeId( typeId );
            }

            if( readInput )
			{
                if( xml.name() == "image" )
				{
                    QString curIdRef = GetAttribute( "item_id_ref" );
                    int curNumRef = GetAttribute( "number_of_output" ).toInt();
                    int curType = GetAttribute( "type_number" ).toInt();
                    curItem->SetImageInput( curIdRef, curNumRef, curType );
                }

                if( xml.name() == "meta" )
				{
                    QString curIdRef = GetAttribute( "item_id_ref" );
                    int curNumRef = GetAttribute( "number_of_output" ).toInt();
                    curItem->SetMetaInput( curIdRef, curNumRef, 0 );
                }
            }

            // in arguments
            if( xml.name() == "parameter" )
			{
                QString curKey = xml.attributes().value("key").toString();
                QString curVal = xml.attributes().value("value").toString();
				QString curDes = xml.attributes().value("description").toString();
				QString curParType = xml.attributes().value("type").toString();
                curItem->SetParameter( curKey, curVal );
				if( !curDes.isEmpty() ) {
					curItem->AppendParDescription(curDes);
				}
				if( !curParType.isEmpty() ) {
					curItem->AppendParType(curParType.toInt());
				}
            }
        }
        xml.readNext();
    }

    return curItem;
}


// function to validate the existing ids
bool XMLReader::CheckIdAndAddToList( QString id )
{
    if( mIdCheckList.contains( id ) )
	{
        // Error: id is already in use
        throw QString( "Error: XMLReader: Id duplication of id: " + id );
        return false;
    }
	else
	{
        mIdCheckList.append( id );
        return true;
    }
}


// function to get an xml attribute
QString XMLReader::GetAttribute( QString name , bool throwException )
{
    if( xml.attributes().hasAttribute( name ) )
        return xml.attributes().value( name ).toString();

    if( throwException )
        throw ErrorMessage("readXML -- required attribute: " + name + " not set ");    // throw error message

    return QString("");
}


// function to write an error message
QString XMLReader::ErrorMessage( QString methodName )
{
    // create the error message
    return QString( "XMLReader::" + methodName + " Error in XML Processing " +
                    xml.errorString() +
                    "\nat Line: " + QString::number(xml.lineNumber()) );
}

} // namespace XPIWIT
