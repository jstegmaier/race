/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "ImagePointerManager.h"


namespace XPIWIT
{

ImagePointerManager::ImagePointerManager()
{
    SetAllToNull();
}

ImagePointerManager::~ImagePointerManager()
{
	SetAllToNull();
}

void ImagePointerManager::SetReleaseDataFlag(bool releaseDataFlag)
{
	if (mImage3Float.IsNotNull())
		mImage3Float->SetReleaseDataFlag( releaseDataFlag );
}

void ImagePointerManager::DeleteCurrentPointer()
{
	// TODO: ADD OTHER TYPES AS WELL!
	if (mImage3Float.IsNotNull())
		mImage3Float->ReleaseData();
}

void ImagePointerManager::SetAllToNull()
{
    mImageType.first = ProcessObjectType::DATATYPE_UNDEFINED;
    mImageType.second = 0;

    mImage2Char   = NULL;
    mImage2Short  = NULL;
    mImage2Int    = NULL;
    mImage2Long	  = NULL;
    mImage2UChar  = NULL;
	mImage2UShort = NULL;
    mImage2UInt   = NULL;
    mImage2ULong  = NULL;
    mImage2Float  = NULL;
    mImage2Double = NULL;

    mImage3Char   = NULL;
    mImage3Short  = NULL;
    mImage3Int    = NULL;
    mImage3Long	  = NULL;
    mImage3UChar  = NULL;
	mImage3UShort = NULL;
    mImage3UInt   = NULL;
    mImage3ULong  = NULL;	
	mImage3Float  = NULL;
    mImage3Double = NULL;
		
    mImage4Char   = NULL;
    mImage4Short  = NULL;
    mImage4Int    = NULL;
    mImage4Long   = NULL;
    mImage4UChar  = NULL;
	mImage4UShort = NULL;
    mImage4UInt   = NULL;
    mImage4ULong  = NULL;
    mImage4Float  = NULL;
    mImage4Double = NULL;
}

} // namespace XPIWIT
