/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef IMAGEPOINTERMANAGER_H
#define IMAGEPOINTERMANAGER_H

// namespace header
#include "ProcessObjectType.h"

// itk header
#include "ITKDefinitions.h"

// qt header
#include <QtCore/QSettings>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QPair>


namespace XPIWIT
{

/**
 * stores one specific settings.
 */
class ImagePointerManager
{
public:

    /**
     * The default constructor.
     */
    ImagePointerManager();

    /**
     * the destructor.
     */
    virtual ~ImagePointerManager();


    QPair<ProcessObjectType::DataType, int> GetImageType() { return mImageType; }

	void SetReleaseDataFlag(bool releaseDataFlag = true);
    void DeleteCurrentPointer();
	void SetAllToNull();

    //---------------------//
    //---- Set Pointer ----//
    //---------------------//

    // 2D
    void SetPointer(Image2Char::Pointer inImage)   { mImage2Char   = inImage; mImageType.first = ProcessObjectType::DATATYPE_CHAR;   mImageType.second = 2;}
    void SetPointer(Image2Short::Pointer inImage)  { mImage2Short  = inImage; mImageType.first = ProcessObjectType::DATATYPE_SHORT;  mImageType.second = 2;}
    void SetPointer(Image2Int::Pointer inImage)    { mImage2Int    = inImage; mImageType.first = ProcessObjectType::DATATYPE_INT;    mImageType.second = 2;}
    void SetPointer(Image2Long::Pointer inImage)   { mImage2Long   = inImage; mImageType.first = ProcessObjectType::DATATYPE_LONG;   mImageType.second = 2;}
    void SetPointer(Image2UChar::Pointer inImage)  { mImage2UChar  = inImage; mImageType.first = ProcessObjectType::DATATYPE_UCHAR;  mImageType.second = 2;}
    void SetPointer(Image2UShort::Pointer inImage) { mImage2UShort = inImage; mImageType.first = ProcessObjectType::DATATYPE_USHORT; mImageType.second = 2;}
    void SetPointer(Image2UInt::Pointer inImage)   { mImage2UInt   = inImage; mImageType.first = ProcessObjectType::DATATYPE_UINT;   mImageType.second = 2;}
    void SetPointer(Image2ULong::Pointer inImage)  { mImage2ULong  = inImage; mImageType.first = ProcessObjectType::DATATYPE_ULONG;  mImageType.second = 2;}
    void SetPointer(Image2Float::Pointer inImage)  { mImage2Float  = inImage; mImageType.first = ProcessObjectType::DATATYPE_FLOAT;  mImageType.second = 2;}
    void SetPointer(Image2Double::Pointer inImage) { mImage2Double = inImage; mImageType.first = ProcessObjectType::DATATYPE_DOUBLE; mImageType.second = 2;}

    // 3D
    void SetPointer(Image3Char::Pointer inImage)   { mImage3Char   = inImage; mImageType.first = ProcessObjectType::DATATYPE_CHAR;   mImageType.second = 3;}
    void SetPointer(Image3Short::Pointer inImage)  { mImage3Short  = inImage; mImageType.first = ProcessObjectType::DATATYPE_SHORT;  mImageType.second = 3;}
    void SetPointer(Image3Int::Pointer inImage)    { mImage3Int    = inImage; mImageType.first = ProcessObjectType::DATATYPE_INT;    mImageType.second = 3;}
    void SetPointer(Image3Long::Pointer inImage)   { mImage3Long   = inImage; mImageType.first = ProcessObjectType::DATATYPE_LONG;   mImageType.second = 3;}
    void SetPointer(Image3UChar::Pointer inImage)  { mImage3UChar  = inImage; mImageType.first = ProcessObjectType::DATATYPE_UCHAR;  mImageType.second = 3;}
    void SetPointer(Image3UShort::Pointer inImage) { mImage3UShort = inImage; mImageType.first = ProcessObjectType::DATATYPE_USHORT; mImageType.second = 3;}
    void SetPointer(Image3UInt::Pointer inImage)   { mImage3UInt   = inImage; mImageType.first = ProcessObjectType::DATATYPE_UINT;   mImageType.second = 3;}
    void SetPointer(Image3ULong::Pointer inImage)  { mImage3ULong  = inImage; mImageType.first = ProcessObjectType::DATATYPE_ULONG;  mImageType.second = 3;}
    void SetPointer(Image3Float::Pointer inImage)  { mImage3Float  = inImage; mImageType.first = ProcessObjectType::DATATYPE_FLOAT;  mImageType.second = 3;}
    void SetPointer(Image3Double::Pointer inImage) { mImage3Double = inImage; mImageType.first = ProcessObjectType::DATATYPE_DOUBLE; mImageType.second = 3;}

    // 4D
    void SetPointer(Image4Char::Pointer inImage)   { mImage4Char   = inImage; mImageType.first = ProcessObjectType::DATATYPE_CHAR;   mImageType.second = 4;}
    void SetPointer(Image4Short::Pointer inImage)  { mImage4Short  = inImage; mImageType.first = ProcessObjectType::DATATYPE_SHORT;  mImageType.second = 4;}
    void SetPointer(Image4Int::Pointer inImage)    { mImage4Int    = inImage; mImageType.first = ProcessObjectType::DATATYPE_INT;    mImageType.second = 4;}
    void SetPointer(Image4Long::Pointer inImage)   { mImage4Long   = inImage; mImageType.first = ProcessObjectType::DATATYPE_LONG;   mImageType.second = 4;}
    void SetPointer(Image4UChar::Pointer inImage)  { mImage4UChar  = inImage; mImageType.first = ProcessObjectType::DATATYPE_UCHAR;  mImageType.second = 4;}
    void SetPointer(Image4UShort::Pointer inImage) { mImage4UShort = inImage; mImageType.first = ProcessObjectType::DATATYPE_USHORT; mImageType.second = 4;}
    void SetPointer(Image4UInt::Pointer inImage)   { mImage4UInt   = inImage; mImageType.first = ProcessObjectType::DATATYPE_UINT;   mImageType.second = 4;}
    void SetPointer(Image4ULong::Pointer inImage)  { mImage4ULong  = inImage; mImageType.first = ProcessObjectType::DATATYPE_ULONG;  mImageType.second = 4;}
    void SetPointer(Image4Float::Pointer inImage)  { mImage4Float  = inImage; mImageType.first = ProcessObjectType::DATATYPE_FLOAT;  mImageType.second = 4;}
    void SetPointer(Image4Double::Pointer inImage) { mImage4Double = inImage; mImageType.first = ProcessObjectType::DATATYPE_DOUBLE; mImageType.second = 4;}


    //---------------------//
    //---- Get Pointer ----//
    //---------------------//

    // 2D
    void GetPointer(Image2Char::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage2Char; }
    void GetPointer(Image2Short::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage2Short; }
    void GetPointer(Image2Int::Pointer *inImage)     { if(mImageType.second != 0) (*inImage) = mImage2Int; }
    void GetPointer(Image2Long::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage2Long; }
    void GetPointer(Image2UChar::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage2UChar; }
    void GetPointer(Image2UShort::Pointer *inImage)  { if(mImageType.second != 0) (*inImage) = mImage2UShort; }
    void GetPointer(Image2UInt::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage2UInt; }
    void GetPointer(Image2ULong::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage2ULong; }
    void GetPointer(Image2Float::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage2Float; }
    void GetPointer(Image2Double::Pointer *inImage)  { if(mImageType.second != 0) (*inImage) = mImage2Double; }

    // 3D
    void GetPointer(Image3Char::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage3Char; }
    void GetPointer(Image3Short::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage3Short; }
    void GetPointer(Image3Int::Pointer *inImage)     { if(mImageType.second != 0) (*inImage) = mImage3Int; }
    void GetPointer(Image3Long::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage3Long; }
    void GetPointer(Image3UChar::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage3UChar; }
    void GetPointer(Image3UShort::Pointer *inImage)  { if(mImageType.second != 0) (*inImage) = mImage3UShort; }
    void GetPointer(Image3UInt::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage3UInt; }
    void GetPointer(Image3ULong::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage3ULong; }
    void GetPointer(Image3Float::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage3Float; }
    void GetPointer(Image3Double::Pointer *inImage)  { if(mImageType.second != 0) (*inImage) = mImage3Double; }

    // 4D
    void GetPointer(Image4Char::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage4Char; }
    void GetPointer(Image4Short::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage4Short; }
    void GetPointer(Image4Int::Pointer *inImage)     { if(mImageType.second != 0) (*inImage) = mImage4Int; }
    void GetPointer(Image4Long::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage4Long; }
    void GetPointer(Image4UChar::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage4UChar; }
    void GetPointer(Image4UShort::Pointer *inImage)  { if(mImageType.second != 0) (*inImage) = mImage4UShort; }
    void GetPointer(Image4UInt::Pointer *inImage)    { if(mImageType.second != 0) (*inImage) = mImage4UInt; }
    void GetPointer(Image4ULong::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage4ULong; }
    void GetPointer(Image4Float::Pointer *inImage)   { if(mImageType.second != 0) (*inImage) = mImage4Float; }
    void GetPointer(Image4Double::Pointer *inImage)  { if(mImageType.second != 0) (*inImage) = mImage4Double; }

public slots:

private:

    QPair<ProcessObjectType::DataType, int> mImageType;

    /**
     * List of all possible image types
     */
    // 2D
    Image2Char::Pointer mImage2Char;
    Image2Short::Pointer mImage2Short;
    Image2Int::Pointer mImage2Int;
    Image2Long::Pointer mImage2Long;
    Image2UChar::Pointer mImage2UChar;
    Image2UShort::Pointer mImage2UShort;
    Image2UInt::Pointer mImage2UInt;
    Image2ULong::Pointer mImage2ULong;
    Image2Float::Pointer mImage2Float;
    Image2Double::Pointer mImage2Double;

    // 3D
    Image3Char::Pointer mImage3Char;
    Image3Short::Pointer mImage3Short;
    Image3Int::Pointer mImage3Int;
    Image3Long::Pointer mImage3Long;
    Image3UChar::Pointer mImage3UChar;
    Image3UShort::Pointer mImage3UShort;
    Image3UInt::Pointer mImage3UInt;
    Image3ULong::Pointer mImage3ULong;
    Image3Float::Pointer mImage3Float;
    Image3Double::Pointer mImage3Double;

    // 4D
    Image4Char::Pointer mImage4Char;
    Image4Short::Pointer mImage4Short;
    Image4Int::Pointer mImage4Int;
    Image4Long::Pointer mImage4Long;
    Image4UChar::Pointer mImage4UChar;
    Image4UShort::Pointer mImage4UShort;
    Image4UInt::Pointer mImage4UInt;
    Image4ULong::Pointer mImage4ULong;
    Image4Float::Pointer mImage4Float;
    Image4Double::Pointer mImage4Double;
};

} // namespace XPIWIT

#endif // IMAGEPOINTERMANAGER_H
