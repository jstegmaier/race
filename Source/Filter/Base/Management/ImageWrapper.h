/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef IMAGEWRAPPER_H
#define IMAGEWRAPPER_H

// namespace header
#include "ImagePointerManager.h"
#include "itkImageDuplicator.h"
#include "../../XpExtended/WorkflowFilter/CastImageFilterWidget.h"

namespace XPIWIT
{

/**
 * stores one specific settings.
 */
class ImageWrapper
{
public:
    ImageWrapper() { mReleaseData = true; 
					 mRescaleImageFlag = true;
					 mCounter = 0; }

    void SetReleaseData( bool releaseData ) { mReleaseData = releaseData; mImagePointerManager.SetReleaseDataFlag( releaseData ); }
    bool GetReleaseData() { return mReleaseData; }

    ImagePointerManager GetPointerManager() { return mImagePointerManager; }
	void ReleaseImage() { mImagePointerManager.DeleteCurrentPointer(); }

    template< class TImageType >
    typename TImageType::Pointer GetImage()
    {
        // cast image
        typename TImageType::Pointer image = SelectTypeAndCastImage<TImageType>();
        
        return image;
    }

    template< class TImageType >
    void SetImage( typename TImageType::Pointer imagePointer, int counter = 0 ) { mImagePointerManager.SetPointer( imagePointer ); mCounter = counter; }

    QPair< ProcessObjectType::DataType, int > GetImageType() { return mImagePointerManager.GetImageType(); }
    int GetImageDimension() { return mImagePointerManager.GetImageType().second; }
    ProcessObjectType::DataType GetImageDataType() { return mImagePointerManager.GetImageType().first; }

	void SetCounter( int counter ) { mCounter = counter; }
	int GetCounter() { return mCounter; }
	bool decrementCounter() { mCounter -= 1; return mCounter > 0; }

	void SetRescaleFlag( bool flag ) { mRescaleImageFlag = flag; }
	bool GetRescaleFlag() { return mRescaleImageFlag; }

private:

    bool mReleaseData;
    ImagePointerManager mImagePointerManager;

    template< class TImageType >
    typename TImageType::Pointer SelectTypeAndCastImage()
    {
        ProcessObjectType::DataType type = mImagePointerManager.GetImageType().first;
        
        if( type == ProcessObjectType::DATATYPE_CHAR )
            return CastImage< itk::Image< char, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_SHORT )
            return CastImage< itk::Image< short, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_INT )
            return CastImage< itk::Image< int, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_LONG )
            return CastImage< itk::Image< long, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_UCHAR )
            return CastImage< itk::Image< unsigned char, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_USHORT )
            return CastImage< itk::Image< unsigned short, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_UINT )
            return CastImage< itk::Image< unsigned int, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_ULONG )
            return CastImage< itk::Image< unsigned long, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_FLOAT )
            return CastImage< itk::Image< float, TImageType::ImageDimension>, TImageType >();
        
        if( type == ProcessObjectType::DATATYPE_DOUBLE )
            return CastImage< itk::Image< double, TImageType::ImageDimension>, TImageType >();
        
        return NULL;
    }

    template< class TInputImage, class TOutputImage >
    typename TOutputImage::Pointer CastImage()
    {
        typename TInputImage::Pointer inputImage = NULL;
        typename TOutputImage::Pointer outputImage = NULL;
        
        if( mCounter > 0 || !mReleaseData )
        {
            mImagePointerManager.GetPointer( &inputImage );
            
            typename itk::ImageDuplicator<TInputImage>::Pointer duplicator = itk::ImageDuplicator<TInputImage>::New();
            duplicator->SetInputImage( inputImage );
            duplicator->Update();
            typename TInputImage::Pointer inputImage = duplicator->GetOutput();
            
            ImagePointerManager tempImagePointerManager;
            tempImagePointerManager.SetPointer( inputImage );
            
            if( typeid( TInputImage ) == typeid( TOutputImage ) )
            {
                tempImagePointerManager.GetPointer( &outputImage );
            }
            else
            {
                ImageWrapper *imageWrapper = new ImageWrapper();
                imageWrapper->SetImage<TInputImage>( inputImage );
                imageWrapper->SetReleaseData( true );
                imageWrapper->SetRescaleFlag( GetRescaleFlag() );
                outputImage = imageWrapper->GetImage<TOutputImage>();
            }
        }
        else
        {
            // check if types are equal
            if( typeid( TInputImage ) == typeid( TOutputImage ) )
            {
                mImagePointerManager.GetPointer( &outputImage );
            }
            else
            {
                // cast image
                CastImageFilterWidget< TInputImage, TOutputImage  > castImageFilter;
                castImageFilter.SetInputImage( this, 0 );
                castImageFilter.Update();
                outputImage = castImageFilter.GetOutputImage( 0 )->template GetImage<TOutputImage>();
            }
        }
        
        return outputImage;
    }

	int mCounter;
	bool mRescaleImageFlag;
};
    
} // namespace XPIWIT

//#include "ImageWrapper.txx"

#endif // IMAGEWRAPPER_H
