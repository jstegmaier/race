/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "ProcessObjectBase.h"
#include "../../XpExtended/WorkflowFilter/ImageWriterWidget.h"
#include "ImageWrapper.h"

// namespace header
#include "../../../Core/XML/AbstractInput.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageFileWriter.h"

// qt header
#include <QtCore/QDir>


namespace XPIWIT
{

// the default constructor
ProcessObjectBase::ProcessObjectBase() : QObject()
{
    // set the process object name
    mName = "ProcessObjectBase";
    mDescription = "Not Available"; //"Base class for processing objects. Specific description has to be set by derived classes.";
    mIsLastFilter = false;

    // initialize the settings object
    mProcessObjectSettings = new ProcessObjectSettings();
    mObjectType = new ProcessObjectType();

    mDirtyFlag = true;

    // add option to write the output
    mProcessObjectSettings->AddSetting( "WriteResult", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Write output to disk" );
	mProcessObjectSettings->AddSetting( "WriteMetaData", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Write meta data to disk" );
    mProcessObjectSettings->AddSetting( "MaxThreads", "8", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Define the maximum number of concurrent threads" );
}


// the destructor
ProcessObjectBase::~ProcessObjectBase()
{
    // get rid of allocated objects
    delete mObjectType;
    delete mProcessObjectSettings;
}


// release the output image data
void ProcessObjectBase::ReleaseOutputs()
{
	for (int i=0; i<mOutputImages.length(); ++i)
		mOutputImages.at(i)->ReleaseImage();
}


// function to init the fast sift widget
void ProcessObjectBase::Init()
{

}


// prepare the image and meta inputs
void ProcessObjectBase::PrepareInputs()
{
    // print a spacer
    Logger::GetInstance()->WriteSpacer();
    Logger::GetInstance()->WriteLine("Initialize Process Object: " + mName.toUpper() + "..." );
    Logger::GetInstance()->WriteSpacer();

	// Meta information handling
	MetaDataImage *metaDataImage = MetaDataBase::GetInstance()->GetImageMeta( mCMDPipelineArguments->mInputImagePaths.at( mCMDPipelineArguments->mDefaultIndex ) );
	QList< AbstractInput> metaInputs = mAbstractFilter->GetMetaInputs();
    
	// inputs
    for( int i = 0; i < this->mObjectType->GetNumberMetaInputs(); i++ )
	{
		QString id = metaInputs.at( i ).mIdRef;
		// int number = metaInputs.at( i )->mNumberRef; // only one meta output per filter
        mMetaInputs.append( metaDataImage->GetMetaData( id ) );
    }

    // outputs
    for( int i = 0; i < this->mObjectType->GetNumberMetaOutputs(); i++ )
	{
        MetaDataFilter *temp = metaDataImage->CreateMetaOutput( mAbstractFilter->GetId() );
		temp->mFilterName = mAbstractFilter->GetName();
        temp->mFilterId = mAbstractFilter->GetId();
        mMetaOutputs.append( temp );
    }
}


// the basic update function for each process object
void ProcessObjectBase::Update()
{
    // get out filename suffix
    bool writeResult = mProcessObjectSettings->GetSettingValue( "WriteResult" ).toInt() > 0;
	bool writeMetaData = mProcessObjectSettings->GetSettingValue( "WriteMetaData" ).toInt() > 0;

    if(writeResult == true)
	{
		for( int i = 0; i < mOutputImages.length(); i ++){
			ImageWrapper *i_image = mOutputImages.at( i );
            WriteResult( i_image, i + 1 );
        }
    }

	/*
	if( mIsLastFilter && !mOutputImages.isEmpty() ){
		ImageWrapper *i_image = mOutputImages.at( 0 );
		bool tempReleaseDataFlag = i_image->GetReleaseData();
		i_image->SetReleaseData( false );

		ImageWriterWidget imageWriter;
		imageWriter.SetInputImage( i_image, 0 );
	
		QString path = mCMDPipelineArguments->mImageOutputPath;

		imageWriter.SetReaderPath( path );
		imageWriter.Update();

		i_image->SetReleaseData( tempReleaseDataFlag );
	}
	*/

	// write meta data
	if (writeMetaData == true)
	{
		for( int i = 0; i < this->mObjectType->GetNumberMetaOutputs(); i++ )
		{
			mMetaOutputs.at( i )->WriteFilter( CreateOutputPath( mCMDPipelineArguments, mAbstractFilter ), mCMDPipelineArguments->mUseMetaDataHeader, mCMDPipelineArguments->mMetaDataSeparator, mCMDPipelineArguments->mMetaDataDelimitor );
		}
	}

	// set output image counter
	for( int i = 0; i < mOutputImages.length(); i ++)
	{
		int counter = mAbstractFilter->GetKeepOutput( i + 1 );
		mOutputImages.at(i)->SetCounter( counter );

		if( counter <= 0 )
		{
			mOutputImages.at(i)->ReleaseImage();
		}
	}
}


// function to write the results
void ProcessObjectBase::WriteResult(ImageWrapper *imageWrapper, int num)
{
    // check for NULL pointer
    if( imageWrapper == NULL )
        return;

    bool tempReleaseDataFlag = imageWrapper->GetReleaseData();
    imageWrapper->SetReleaseData( false );

    ImageWriterWidget imageWriter;
    imageWriter.SetInputImage( imageWrapper, 0 );
	
	QString path = CreateOutputPath( mCMDPipelineArguments, mAbstractFilter );
	path += "_Out" + QString::number( num ) + ".tif";

	imageWriter.SetReaderPath( path );
    imageWriter.Update();

    imageWrapper->SetReleaseData( tempReleaseDataFlag );
}


// function to set the abstract filter
void ProcessObjectBase::SetAbstractFilter(AbstractFilter* abstractFilter) 
{ 
	mAbstractFilter = abstractFilter; 

	// add the setting
    for(int i = 0; i < mAbstractFilter->GetNumberParameter(); i++)
	{
        QString key = mAbstractFilter->GetParameter( i )[0];
        QString value = mAbstractFilter->GetParameter( i )[1];
        this->SetSetting( key, value );
	}
}


// function to set the input image
void ProcessObjectBase::SetInputImage(ImageWrapper *imageWrapper, int num)
{
    if( num == 0 )
	{
        mInputImages.append( imageWrapper );
        return;
    }

    if( mInputImages.length() > num )
	{
        mInputImages.replace( num, imageWrapper );
    }
	else
	{
        // fill up with NULL pointer
        while( mInputImages.length() < num )
            mInputImages.append( NULL );
        mInputImages.append( imageWrapper );
    }
}


// function to get one of the output images
ImageWrapper *ProcessObjectBase::GetOutputImage( int num )
{
	ImageWrapper *returnWrapper = NULL;

    if( mOutputImages.length() > num ){
		returnWrapper = mOutputImages.at( num );
	}

    return returnWrapper;
}


// initialize the timer
void ProcessObjectBase::StartTimer()
{
    Logger::GetInstance()->WriteLine("Executing Process Object: " + mName.toUpper() + "..." );
    mStartTime = QDateTime::currentMSecsSinceEpoch();
}


// write performance to cout
void ProcessObjectBase::LogPerformance( bool logSettings )
{
    Logger *logger = Logger::GetInstance();
    // write process object name and duration
    float timeDelta = (QDateTime::currentMSecsSinceEpoch() - mStartTime) / 1000.0;

    logger->WriteLine("+ Execution of " + mName.toUpper() + " finished.");
    logger->WriteLine("\t- Duration: " + QString::number(timeDelta) + " seconds.");

    // write the filter settings
    if( logSettings )
        LogSettings();

    // print a spacer
    logger->WriteSpacer();
}


// write the settings values to cout
void ProcessObjectBase::LogSettings()
{
    Logger::GetInstance()->WriteLine( "\t+ Settings:" );
    // iterate trough the settings and print the value
    const int numSettings = mProcessObjectSettings->GetNumSettings();
    for (int i=0; i<numSettings; ++i)
    {
        Logger::GetInstance()->WriteLine( "\t\t- " + mProcessObjectSettings->GetSettingName( i ) + ": " + mProcessObjectSettings->GetSettingValue( i ) );
    }
}


// function to create the output path
QString ProcessObjectBase::CreateOutputPath(CMDPipelineArguments *cmdPipelineArguments, AbstractFilter* abstractFilter)
{
	QString path = cmdPipelineArguments->mStdOutputPath;
	// add folder format
	if( cmdPipelineArguments->mUseSubFolder )
	{
		QString subfolder = "";
		QStringList folderFormat = cmdPipelineArguments->mSubFolderFormat;
		for( int i = 0; i < folderFormat.length(); i++ )
		{
			// add filter id to name
            if( folderFormat.at( i ).compare( "filterid" ) == 0 )
				subfolder += abstractFilter->GetId();
            // add filter name to name
            if( folderFormat.at( i ).compare( "filtername" ) == 0 )
				subfolder += abstractFilter->GetName();

            // seperate values
            if( subfolder.compare("") != 0 && (i+1) != folderFormat.length() )
                subfolder += "_";
		}

		// create path
		QDir outputDir( path );
		if( !( outputDir.exists( subfolder ) || outputDir.mkdir( subfolder ) ) )	// if folder doesn't exists and can not be created
			throw QString( "Error while creating subfolder. Path: " + path + subfolder );

		path += subfolder + "/";
	}

	// add image name
	QStringList fileFormat = cmdPipelineArguments->mOutputFileFormat;
	QString fileName = "";
	for( int i = 0; i < fileFormat.length(); i++ )
	{
        // add filter id to name
        if( fileFormat.at( i ).compare( "prefix" ) == 0 )
            fileName += cmdPipelineArguments->mPrefix;
        // add imagename to name
        if( fileFormat.at( i ).compare( "imagename" ) == 0 )
		{
			QString temp = cmdPipelineArguments->mInputImagePaths.at( cmdPipelineArguments->mDefaultIndex );
			temp = temp.split("/").last();
			QStringList temp2 = temp.split(".");
			temp2.removeLast();
			temp = temp2.join(".");
			fileName += temp;
		}

        // add filterid to name
        if( fileFormat.at( i ).compare( "filterid" ) == 0 )
            fileName += abstractFilter->GetId();
        // add filter name to name
        if( fileFormat.at( i ).compare( "filtername" ) == 0 )
            fileName += abstractFilter->GetName();

        // seperate values
        if( fileName.compare("") != 0 && (i+1) != fileFormat.length() )
            fileName += "_";
    }
	path += fileName;

	return path;
}

} // namespace XPIWIT
