/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef PROCESSOBJECTBASE_H
#define PROCESSOBJECTBASE_H

// namespace header
#include "ProcessObjectSettings.h"
#include "ProcessObjectType.h"
#include "../MetaData/MetaDataBase.h"
#include "../MetaData/MetaDataImage.h"
#include "../MetaData/MetaDataFilter.h"

// project header
#include "../../../Core/XML/AbstractFilter.h"
#include "../../../Core/CMD/CMDPipelineArguments.h"

// itk header
#include "ITKDefinitions.h"

// qt header
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QSettings>
#include <QtCore/QDateTime>
#include <QtCore/QStringList>
#include <QtCore/QList>

// system header

namespace XPIWIT
{
   class ImageWrapper;
/**
 *	@class ProcessObjectBase
 *	The base class for image to image filter wrapper.
 */
class ProcessObjectBase : public QObject
{
    public:

        /**
         * The constructor.
         */
        ProcessObjectBase();


        /**
         * The destructor.
         */
        virtual ~ProcessObjectBase();


        /**
         * Function to init the main window. All widgets, menus etc. are created within this function.
         */
        void Init();


        /**
         * Function to set the pointer of the input iamges
         */
        virtual void PrepareInputs();


        /**
         * Update the filter.
         */
        virtual void Update();


        /**
         * write result image to disk
         */
        void WriteResult( ImageWrapper*, int );


        /**
         * Function to get the name of the filter.
         * @param Returns the name of the current filter.
         */
        const QString& GetName()							{ return mName; }


        /**
         * Function to get the name of the filter.
         * @param Returns the name of the current filter.
         */
        const QString& GetDescription() 					{ return mDescription; }


        /**
         * Function to set an image
         * @param Select the number if object has more than one output
         */
        virtual void SetInputImage(ImageWrapper *imageWrapper, int num);


        /**
         * Function to get a processed image
         * @param Select the number if object has more than one output
         */
        virtual ImageWrapper *GetOutputImage(int num);


        /**
         * Function to get the type of the filter.
         * @param Returns the type of the current filter.
         */
        ProcessObjectType *GetType()					{ return mObjectType; }


        /**
         * Function to set a specific setting.
         * @param
         */
        void SetSetting(const QString& name, const QString& value) { mProcessObjectSettings->SetSettingValue( name, value); }

        /**
         * Function to get the process object settings.
         * @return The process object settings variable.
         */
        ProcessObjectSettings* GetProcessObjectSettings()		{ return mProcessObjectSettings; }


        /**
         * Function to set the dirty flag, i.e. to force update call and get valid output.
         * @param dirtyFlag The new dirty flag value.
         */
        void SetDirtyFlag(bool dirtyFlag)						{ mDirtyFlag = dirtyFlag; }


        /**
         * Function to mark the last filter of the pipeline
         */
        void SetIsLast() { mIsLastFilter = true; }


		/**
		 * Function to release the outputs.
		 */
		void ReleaseOutputs();


        /**
         * Function to start a timer upon filter execution
         */
        void StartTimer();


        /**
         * Function to log the performance, i.e. the running time of the filter.
         */
        void LogPerformance(bool logSettings = true);


        /**
         * Function to log the settings of the filter.
         */
        void LogSettings();


        /**
         * @brief GetAbstractFilter returns the abstract filter object
         */
        AbstractFilter* GetAbstractFilter() { return mAbstractFilter; }


        /**
         * @brief SetAbstractFilter sets the abstract filter object
         */
        void SetAbstractFilter(AbstractFilter* abstractFilter);


		/**
         * @brief GetAbstractFilter returns the abstract filter object
         */
        CMDPipelineArguments *GetCMDPipelineArguments() { return mCMDPipelineArguments; }


        /**
         * @brief SetAbstractFilter sets the abstract filter object
         */
        void SetCMDPipelineArguments(CMDPipelineArguments *cmdPipelineArguments) { mCMDPipelineArguments = cmdPipelineArguments; }


		/**
		 * @brief SetReaderPath sets the path for image and meta reader
		 */
		void SetReaderPath(QString path)	{ mPath = path; }


		/**
		 * @brief CreateOutputPath creates the output path
		 */
		static QString CreateOutputPath(CMDPipelineArguments*, AbstractFilter*);

    protected:
        ProcessObjectSettings* mProcessObjectSettings;  // data structure for the complete process object settings
        QString mName;                                  // the name of the process object
        QString mDescription;                           // the description of the process object
        ProcessObjectType *mObjectType;                 // information for template typecasting

        qint64 mStartTime;                              // start time point for processing time estimations
        bool mDirtyFlag;                                // flag to identify that an update of the output is necessary
        bool mIsLastFilter;                             // flag for the last filter in pipeline

        QList< ImageWrapper *> mInputImages;            // list of input images
        QList< ImageWrapper *> mOutputImages;           // list of output images

        QList< MetaDataFilter* > mMetaInputs;           // list of meta data inputs
        QList< MetaDataFilter* > mMetaOutputs;          // list of meta data outputs

        AbstractFilter* mAbstractFilter;                // xml definition of the filter
        CMDPipelineArguments *mCMDPipelineArguments;    // command line arguments for this pipeline run

		QString mPath;									// path for image and meta reader
};

} // namespace XPIWIT

#endif // PROCESSOBJECTBASE_H
