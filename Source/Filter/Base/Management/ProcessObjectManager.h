/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef PROCESSOBJECTMANAGER_H
#define PROCESSOBJECTMANAGER_H

// include required headers
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QSettings>
#include <QtCore/QDateTime>
#include <QtCore/QMutex>
#include <QtCore/QPair>
#include <QtCore/QString>
#include "../../../Core/Utilities/Logger.h"

namespace XPIWIT
{
	class ProcessObjectBase;
	class ProcessObjectProxyBase;

/**
 *	@class ProcessObjectList
 *	Singleton
 *  Holds a list of all implemented Filters
 */
class ProcessObjectManager : public QObject
{
	Q_OBJECT
    public:

        /**
         * get singleton instance
         */
        static ProcessObjectManager* GetInstance();

        /**
         * delete singleton instance
         */
        static void DeleteInstance();

        /* ------------------------------------ */
        /* --------productive functions-------- */
        /* ------------------------------------ */

        QList<ProcessObjectBase*> GetProcessObjectList();
        ProcessObjectBase* GetProcessObjectInstance(QString name, QStringList types, QList<int> dimensions);

    void Register(ProcessObjectProxyBase* filter);

    
    private:
        /**
         * Singleton Instance
         */
        static ProcessObjectManager* mInstance;

        /**
         * The constructor.
         * @param parent The parent of the main window. Usually should be NULL.
         */
        ProcessObjectManager();

        /**
         * The destructor.
         */
        virtual ~ProcessObjectManager();

        void Init();

		bool StringCompare( QString, QString );

        /**
         * List of all available Filter
         */
        QList<ProcessObjectProxyBase*> mProcessObjectList;
        QList<ProcessObjectBase*> mProcessObjectWidgetList;
    protected:
};

} // namespace XPIWIT

#endif // PROCESSOBJECTMANAGER_H
