/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "ProcessObjectPipeline.h"

// namespace header
#include "ProcessObjectBase.h"
#include "ProcessObjectManager.h"
#include "ImageWrapper.h"
//#include "ProcessObjectList.h"

// project header
#include "../../../Core/XML/AbstractFilter.h"
#include "../../../Core/Utilities/Logger.h"

// itk header

// qt header
#include <QtCore/QString>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>

// system header

namespace XPIWIT
{

// the constructor
ProcessObjectPipeline::ProcessObjectPipeline() 
{
}


// function to execute the specified abstract pipeline
void ProcessObjectPipeline::run( CMDPipelineArguments *cmdPipelineArguments, AbstractPipeline abstractPipeline )
{
	// get the logger instance
	Logger* logger = Logger::GetInstance();
	logger->SetUseSubfolder( cmdPipelineArguments->mUseSubFolder );

	// get the process object manager and the directories
	ProcessObjectManager* processObjectManager = ProcessObjectManager::GetInstance();
	QMap< QString, ProcessObjectBase* > processObjectMap;
	QString resultDir = cmdPipelineArguments->mStdOutputPath;
	QString defaultName = cmdPipelineArguments->mInputImagePaths.at( cmdPipelineArguments->mDefaultIndex );
	defaultName = defaultName.split( "/" ).last();

	// initialize the logger
	logger->InitLogFile( cmdPipelineArguments->mFileLoggingEnabled, resultDir, defaultName );

	// pipeline execution
	int numberOfFilter = abstractPipeline.GetNumFilter();

	// check if the last output already exists and quit the processing if true
	// TODO: replace with more elegant method, such as a method in the process objects directly
	QString lastOutputPath = ProcessObjectBase::CreateOutputPath( cmdPipelineArguments, abstractPipeline.GetFilter( numberOfFilter-1 ) );
	QFileInfo metaResultFile(lastOutputPath + QString("_KeyPoints.csv"));
	QFileInfo imageResultFile( lastOutputPath + QString("_Out1.tif") );
	if ((metaResultFile.exists() || imageResultFile.exists()) && cmdPipelineArguments->mSkipProcessingIfOutputExists == true)
	{
		Logger::GetInstance()->WriteToAll( "- Output file already exists, skipping processing. Disable --skipProcessingIfOutputExists to overwrite existing results." );
		return;
	}
	else
	{
		Logger::GetInstance()->WriteToAll( lastOutputPath + QString("_Out1.tif") +  QString(" does not exist yet.") );
	}

	// iterate over all filters and execute them
	for(int i=0; i<numberOfFilter; i++)
	{
		// get the abstract filter
		AbstractFilter* currentFilter = abstractPipeline.GetFilter( i );
		ProcessObjectBase *processObject = NULL;

		// handle readers separately as they need command line input
		if(currentFilter->IsReader() == true)
		{
			if(currentFilter->GetName().compare( "imagereader", Qt::CaseInsensitive ) == 0)
			{
				// ImageReader
				AbstractInput curInput = currentFilter->GetImageInputs().at( 0 );
				int id = curInput.mNumberRef;
				const QString type = "float";
				int index = cmdPipelineArguments->mInputImageId.indexOf( QString::number( id ) );
				QString path = cmdPipelineArguments->mInputImagePaths.at( index );
				int dimension = cmdPipelineArguments->mInputDimension.at( index ).toInt();

				processObject = processObjectManager->GetProcessObjectInstance( currentFilter->GetName(), QStringList() << type, QList< int >() << dimension );
				processObject->SetReaderPath( path );

				processObjectMap.insert( currentFilter->GetId(), processObject );
			}
			else
			{
				// MetaReader
				AbstractInput curInput = currentFilter->GetMetaInputs().at( 0 );
				int id = curInput.mNumberRef;
				int index = cmdPipelineArguments->mInputMetaId.indexOf( QString::number( id ) );
				QString path = cmdPipelineArguments->mInputMeta.at( index );

				processObject = processObjectManager->GetProcessObjectInstance( currentFilter->GetName(), QStringList() << "float", QList< int >() << 2 );
				processObject->SetReaderPath( path );

				processObjectMap.insert( currentFilter->GetId(), processObject );
			}
		}
		else
		{
			// Filter
			QStringList type;
			QList< int > dimension;

			int numberTypes = currentFilter->GetTypes().length();
			int numberInputs = currentFilter->GetImageInputs().length();

			for( int j = 0; j < numberTypes; j++ )
			{
				type << currentFilter->GetTypes().at( j );
			}

			for( int k = 0; k < numberInputs; k ++ )
			{
				// get the current dimension of the input
				ProcessObjectBase* curBase = processObjectMap.value( currentFilter->GetImageInputs().at( k ).mIdRef );
				if( curBase == NULL )
					throw QString("Error: Unknown reference id: " + currentFilter->GetImageInputs().at( k ).mIdRef + " in filter: " + currentFilter->GetId() );
					
				int outputNumber = currentFilter->GetImageInputs().at( k ).mNumberRef;
				if( outputNumber > curBase->GetType()->GetNumberImageOutputs() )
					throw QString("Error: Filter with id: " + curBase->GetAbstractFilter()->GetId() + " has no ouptut with number: " + QString::number( outputNumber ) );

				int dim = curBase->GetOutputImage( outputNumber - 1 )->GetImageDimension();
				dimension << dim;
			}

			processObject = ProcessObjectManager::GetInstance()->GetProcessObjectInstance( currentFilter->GetName(), QStringList() << type, QList< int >() << dimension );

			if( processObject == NULL )
				throw QString("Error: Filter can not be instantiated.");

			processObjectMap.insert( currentFilter->GetId(), processObject );

			// set inputs
			for( int j = 0; j < numberInputs; j++ )
			{
				ProcessObjectBase *curBase = processObjectMap.value( currentFilter->GetImageInputs().at( j ).mIdRef );
				int num = currentFilter->GetImageInputs().at( j ).mNumberRef - 1;
				ImageWrapper *curImageWrapper = curBase->GetOutputImage( num );
				bool currentCounter = curImageWrapper->decrementCounter();
				if (currentCounter == 0)
					curImageWrapper->SetReleaseData( true );

				processObject->SetInputImage( curImageWrapper, j ); 
			}
		}

		// execute object
		processObject->SetAbstractFilter( currentFilter );
		processObject->SetCMDPipelineArguments( cmdPipelineArguments );

		if( i == (numberOfFilter-1))
			processObject->SetIsLast();

		try
		{
			processObject->Update();
		}
		catch(...)
		{
			throw QString("Error");
		}
	}

	// delete the intermediate filters
	QMap< QString, ProcessObjectBase* >::iterator mapIterator = processObjectMap.begin();
	for (; mapIterator != processObjectMap.end(); ++mapIterator)
		mapIterator.value()->ReleaseOutputs();

	// write globale line
}

} // namespace XPIWIT
