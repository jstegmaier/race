/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "ProcessObjectSettings.h"

// namespace header

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkProcessObject.h"


namespace XPIWIT
{

// set the setting by name
void ProcessObjectSettings::SetSettingValue(const QString& name, QString value)
{
    // iterate trough the existing settings and return appropriate setting value
    const int numSettings = mSettings.size();
    for (int i=0; i<numSettings; ++i)
    {
        if (mSettings[i]->GetSettingName() == name)
        {
            mSettings[i]->SetSettingValue( value );
            return;
        }
    }

    // print error message if setting was not found
    Logger::GetInstance()->WriteLine( "SETTING NOT FOUND! " + name );
}


// returns setting value by the specified name
QString ProcessObjectSettings::GetSettingValue(const QString& name)
{
    // iterate trough the existing settings and return appropriate setting value
    const int numSettings = mSettings.size();
    for (int i=0; i<numSettings; ++i)
    {
        if (mSettings[i]->GetSettingName() == name)
            return mSettings[i]->GetSettingValue();
    }

    // print error message if setting was not found
    Logger::GetInstance()->WriteLine( "SETTING NOT FOUND! " + name );

    return "";
}

} // namespace XPIWIT
