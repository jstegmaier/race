/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "ProcessObjectType.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkProcessObject.h"


namespace XPIWIT
{

// the default constructor
ProcessObjectType::ProcessObjectType()
{
    ProcessObjectType(ProcessObjectType::FILTERTYPE_UNSPECIFIED, 0, 0, 0, 0);
}


// more advanced constructor
ProcessObjectType::ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut)
{
    QPair<DataType, int> undefType(ProcessObjectType::DATATYPE_UNDEFINED, 0);
    ProcessObjectType(filterType, numImageIn, numImageOut, numMetaIn, numMetaOut, undefType);
}


// more advanced constructor
ProcessObjectType::ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut, QPair<DataType, int> inputType)
{
    mFilterType = filterType;

    mNumberImageInputs = numImageIn;
    mNumberImageOutputs = numImageOut;
    mNumberMetaInputs = numMetaIn;
    mNumberMetaOutputs = numMetaOut;
    //SetImageType(0, inputType);
}


// more advanced constructor
ProcessObjectType::ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut, QPair<DataType, int> inputType, QPair<DataType, int> outputType)
{
    mFilterType = filterType;

    mNumberImageInputs = numImageIn;
    mNumberImageOutputs = numImageOut;
    mNumberMetaInputs = numMetaIn;
    mNumberMetaOutputs = numMetaOut;
    //SetImageType(0, inputType);
    //SetImageType(1, outputType);
}


// the destructor
ProcessObjectType::~ProcessObjectType()
{
}


// function to specify the image type
void ProcessObjectType::SetImageType(int num, DataType dataType, int dimension)
{
    SetImageType(num, QPair<DataType, int>(dataType, dimension));
}


// function to specify the image type
void ProcessObjectType::SetImageType(int num, QPair<DataType, int> imageType)
{
    if(num == 0){
        mImageTypes.clear();
        for(int i = 0; i < 6; i++)
            mImageTypes.append(imageType);
    }else{
        mImageTypes.replace(num, imageType);
    }
}

} // namespace XPIWIT
