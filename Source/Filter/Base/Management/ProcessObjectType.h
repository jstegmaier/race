/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef PROCESSOBJECTTYPE_H
#define PROCESSOBJECTTYPE_H

// qt header
#include <QtCore/QSettings>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QPair>


namespace XPIWIT
{

/**
 * stores one specific settings.
 */
class ProcessObjectType : public QObject
{
    public:
        /**
         * Enumeration for different processing object filter types.
         */
        enum FilterType
        {
            FILTERTYPE_READER = 0,
            FILTERTYPE_FILTER = 1,
            FILTERTYPE_MAPPER = 2,
            FILTERTYPE_WRITER = 3,
            FILTERTYPE_TILINGSTART = 4,
            FILTERTYPE_TILINGEND = 5,
            FILTERTYPE_CURRENTTILE = 6,
            FILTERTYPE_ORIGINALIMAGE = 7,
            FILTERTYPE_UNSPECIFIED = 8
        };

        /**
         * Enumeration for different processing object data types.
         */
        enum DataType
        {
            DATATYPE_UNDEFINED = 0,
            DATATYPE_CHAR = 1,
            DATATYPE_SHORT = 2,
            DATATYPE_INT = 3,
            DATATYPE_LONG = 4,
            DATATYPE_UCHAR = 5,
            DATATYPE_USHORT = 6,
            DATATYPE_UINT = 7,
            DATATYPE_ULONG = 8,
            DATATYPE_FLOAT = 9,
            DATATYPE_DOUBLE = 10
        };

        /**
         * The default constructor.
         */
        ProcessObjectType();
        ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut);
        ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut, QPair<DataType, int> inputType);
        ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut, QPair<DataType, int> inputType, QPair<DataType, int> outputType );

        /**
         * the destructor.
         */
        virtual ~ProcessObjectType();

        /**
         * setter and getter.
         */
        void SetFilterType(FilterType filterType) { mFilterType = filterType; }
        FilterType GetFilterType() { return mFilterType; }

		void SetNumberTypes( int number )			{ mNumberTypes = number; }

        void SetNumberImageInputs(int numImageIn)   { mNumberImageInputs = numImageIn; }
		void AppendImageInputType( int type )		{ mImageInputTypes.append( type ); }
        void SetNumberImageOutputs(int numImageOut) { mNumberImageOutputs = numImageOut; }
		void AppendImageOutputType( int type )		{ mImageOutputTypes.append( type ); }

        void SetNumberMetaInputs( int numMetaIn)    { mNumberMetaInputs = numMetaIn; }
		void AppendMetaInputType( QString type )	{ mMetaInputTypes.append( type ); }
        void SetNumberMetaOutputs( int numMetaOut)  { mNumberMetaOutputs = numMetaOut; }
		void AppendMetaOutputType( QString type )	{ mMetaOutputTypes.append( type ); }

        void SetImageType(int num, DataType dataType, int dimension);
        void SetImageType(int num, QPair<DataType, int> imageType);

		int GetNumberTypes()						{ return mNumberTypes; }
        int GetNumberImageInputs()					{ return mNumberImageInputs; }
		QList<int> GetImageInputTypes()				{ return mImageInputTypes; }
        int GetNumberImageOutputs()					{ return mNumberImageOutputs; }
		QList<int> GetImageOutputTypes()			{ return mImageOutputTypes; }
        int GetNumberMetaInputs()					{ return mNumberMetaInputs; }
		QStringList GetMetaInputTypes()				{ return mMetaInputTypes; }
        int GetNumberMetaOutputs()					{ return mNumberMetaOutputs; }
		QStringList GetMetaOutputTypes()			{ return mMetaOutputTypes; }

        DataType GetImageDataType(int num = 0)           { return mImageTypes.at(num).first; }
        int GetImageDimension(int num = 0)               { return mImageTypes.at(num).second; }
        QPair<DataType, int> GetImageType(int num = 0)   { return mImageTypes.at(num); }

    private:
        FilterType mFilterType;

		int mNumberTypes;
        int mNumberImageInputs;
		QList<int> mImageInputTypes;
        int mNumberImageOutputs;
		QList<int> mImageOutputTypes;
        int mNumberMetaInputs;
		QStringList mMetaInputTypes;
        int mNumberMetaOutputs;
		QStringList mMetaOutputTypes;

        QList< QPair<DataType, int> > mImageTypes;
};

} // namespace XPIWIT

#endif // PROCESSOBJECTTYPE_H
