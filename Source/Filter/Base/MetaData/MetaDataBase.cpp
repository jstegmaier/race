/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "MetaDataBase.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// qt header
#include <QtCore/QMutex>


namespace XPIWIT
{

MetaDataBase *MetaDataBase::mInstance = 0;

// returns the singleton instance
MetaDataBase *MetaDataBase::GetInstance()
{
    static QMutex mutex;
    if (!mInstance)
    {
        mutex.lock();

        if (!mInstance)
            mInstance = new MetaDataBase();

        mutex.unlock();
    }
    return mInstance;
}


// delete the instance
void MetaDataBase::DeleteInstance()
{
    static QMutex mutex;
    mutex.lock();

    delete mInstance;
    mInstance = 0;

    mutex.unlock();
}


// the default constructor
MetaDataBase::MetaDataBase()
{

}


// default constructor
MetaDataBase::~MetaDataBase()
{
    qDeleteAll( mMetaDataImages );
    mMetaDataImages.clear();
}


// function to set teh image
void MetaDataBase::SetImage(QString imageId, MetaDataImage *imageMeta)
{
    mImageIds.append( imageId );
    mMetaDataImages.append( imageMeta );
}


// function to retrieve the meta data
MetaDataImage *MetaDataBase::GetImageMeta(QString imageId)
{
    if( mImageIds.contains( imageId ) )
	{
        return mMetaDataImages.at( mImageIds.indexOf( imageId ) );
	}
	else
	{
		MetaDataImage *metaDataImage = new MetaDataImage();
		SetImage( imageId, metaDataImage );
		return metaDataImage;
	}
}


// function to retrieve the meta data by index
MetaDataImage *MetaDataBase::GetImageMeta(int index)
{
	if( mMetaDataImages.length() > index )
		return mMetaDataImages.at( index );
	return NULL;
}

}// XPIWIT namespace
