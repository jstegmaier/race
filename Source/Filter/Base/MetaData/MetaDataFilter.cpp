/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "MetaDataFilter.h"

// qt header
#include<QtCore/QString>
#include<QtCore/QFile>


namespace XPIWIT
{

// default constructor
MetaDataFilter::MetaDataFilter()
{
    mFilterName = "";
    mFilterId = "";
    mPostfix = "";

    mWriteMetaData = true;
    mIsMultiDimensional = false;
}


// default destructor
MetaDataFilter::~MetaDataFilter()
{
}


// function to retrieve the feature index by a given name
const int MetaDataFilter::GetFeatureIndex(const QString& featureName)
{
	// iterate over all feature names and return if a match is found
	for (int i=0; i<mTitle.length(); ++i)
		if (mTitle[i] == featureName)
			return i;

	return -1;
}


// writes the meta data to a csv file
void MetaDataFilter::WriteFilter( QString path, bool useHeader, QString separator, QString delimiter )
{
    // create file name and set the output path
    path += "_"  + mPostfix + ".csv";
	mOutputPath = path;

	// open the output file for writing
    QFile csvFile( path );
    csvFile.open( QIODevice::WriteOnly );

    // check if feature identifiers should be written to disk
    if( useHeader )
	{
        QString temp = "";
        for( int i = 0; i < mTitle.length(); i++ )
		{
			// add separator - space?
            if( i > 0 )
                temp = separator;
            temp += mTitle.at( i );
            csvFile.write( temp.toLatin1() );
        }
        csvFile.write("\n");
    }

	// iterate over all features and write them to the file
    for( int i = 0; i < mData.length(); i++ )
	{
        for( int j = 0; j < mData.at(i).length(); j++ )
		{
            QString temp = "";
            QString value = "";

			// add separator - space?
            if( j > 0 )
                temp = separator;

			if( !mType.isEmpty() && mType.at(j).compare( "float" ) == 0 )
			{
				// set the value and change delimiter for float variables
				value = QString::number(mData.at(i).at(j));
				value = value.replace( ".", delimiter );
			}
			else
			{
				// directly write the number for integer data types
				value = QString::number(int(mData.at(i).at(j)));
			}

			// write feature
            temp += value;
            csvFile.write( temp.toLatin1() );
        }

		// next line
        csvFile.write("\n");
    }

	// close the output file
    csvFile.close();
}

} // XPIWIT namespace
