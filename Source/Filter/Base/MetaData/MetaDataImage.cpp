/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "MetaDataImage.h"


namespace XPIWIT
{

// default constructor
MetaDataImage::MetaDataImage()
{
	mFilterIds.clear();
	mMetaDataFilters.clear();
}


// default destructor
MetaDataImage::~MetaDataImage()
{
    qDeleteAll( mMetaDataFilters );
    mMetaDataFilters.clear();
}


// function to set the meta data
void MetaDataImage::SetMetaData(QString filterId, MetaDataFilter *metaDataFilter)
{
    mFilterIds.append( filterId );
    mMetaDataFilters.append( metaDataFilter );
}


// function to create the meta output object
MetaDataFilter *MetaDataImage::CreateMetaOutput(QString filterId)
{
    MetaDataFilter *temp = new MetaDataFilter();
    SetMetaData( filterId, temp );
    return temp;
}


// function to get the meta data
MetaDataFilter *MetaDataImage::GetMetaData(QString filterId)
{
    if( mFilterIds.contains( filterId ) )
        return mMetaDataFilters.at( mFilterIds.indexOf( filterId ) );

    return NULL;
}

}// XPIWIT namespace
