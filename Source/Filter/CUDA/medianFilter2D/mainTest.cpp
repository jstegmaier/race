/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include "external/Nathan/tictoc.h"
#include "medianFilter2D.h"


typedef unsigned short imageType;//define here any type you want

using namespace std;


int writeImage(void* im,long long int imSizeBytes,const char* filename)
{
	FILE* fid = fopen(filename,"wb");
	if(fid == NULL)
	{
		cout<<"ERROR: at writeImage opening file "<<filename<<endl;
		return 1;
	}
	cout<<"Writing file "<<filename<<endl;
	fwrite(im,sizeof(char),imSizeBytes,fid);
	fclose(fid);

	return 0;
}

int main( int argc, const char** argv )
{

	int imSize[dimsImageSlice+1];
	int radiusMedianFilter;
	
	imageType* im = NULL;

	int devCUDA = 0;

	//printf("Device CUDA used is %s\n",getNameDeviceCUDA(devCUDA));
	
	//setup parameters
	if(argc == 1)
	{
		imSize[0] = 410; imSize[1] = 350; 
		radiusMedianFilter = 2;
	}else if(argc == 4)
	{
		imSize[0] = atoi(argv[1]);
		imSize[1] = atoi(argv[2]);
		radiusMedianFilter = atoi(argv[3]);
	}else{
		cout<<"ERROR: at mainTest. Number of input arguments is incorrect"<<endl;
		return 2;
	}

	
	cout<<"Testing CUDA 2D median filter with radius "<<radiusMedianFilter<<" and image size "<<imSize[0]<<"x"<<imSize[1]<<endl;
	
	long long int imN = 1;	
	for(int ii=0;ii<dimsImageSlice;ii++)
	{
		imN *= (long long int) (imSize[ii]);
	}

	//allocate memory
	im = new imageType[imN];

	//fill in with random values
	/* initialize random seed: */
	srand ( time(NULL) );
	
	for(long long int ii=0;ii<imN;ii++)
		im[ii] = 100.0f*(float)(rand()/((float)RAND_MAX));

	//write out input image
	writeImage(im,imN*sizeof(imageType),"E:/temp/testMedianFilter_input.bin");
	
	//calculate convolution
	cout<<"Calculating median filter..."<<endl;
	TicTocTimer timerF=tic();
	int numIter = 1;
	for(int ii=0;ii<numIter;ii++)
	{
		if ( medianFilterCUDA(im,imSize,radiusMedianFilter,devCUDA) > 0 )
			exit(3);
	}
	cout<<"\nMedian filter calculated successfully in "<<toc(&timerF)/(float)numIter<<" secs"<<endl;
	

	//write out results
	writeImage(im,imN*sizeof(imageType),"E:/temp/testMedianFilter_result.bin");
	
	delete[] im;
	im = NULL;
	cout<<endl<<endl;
	//----------------------------------------------------------------------------------------

	//test for a whole stack
	imSize[dimsImageSlice ] = 51;
	cout<<"Testing CUDA 2D median filter with radius "<<radiusMedianFilter<<" and stack slice by slice with  size "<<imSize[0]<<"x"<<imSize[1]<<"x"<<imSize[2]<<endl;
	
	imN = 1;	
	for(int ii=0;ii<dimsImageSlice+1;ii++)
	{
		imN *= (long long int) (imSize[ii]);
	}

	//allocate memory
	im = new imageType[imN];

	//fill in with random values
	/* initialize random seed: */
	srand ( time(NULL) );
	
	for(long long int ii=0;ii<imN;ii++)
		im[ii] = 100.0f*(float)(rand()/((float)RAND_MAX));

	//write out input image
	writeImage(im,imN*sizeof(imageType),"E:/temp/testMedianFilterSliceBySlice_input.bin");
	
	//calculate convolution
	cout<<"Calculating median filter..."<<endl;
	timerF=tic();
	numIter = 1;
	for(int ii=0;ii<numIter;ii++)
	{
		if ( medianFilterCUDASliceBySlice(im,imSize,radiusMedianFilter,devCUDA) > 0 )
			exit(3);
	}
	cout<<"\nMedian filter calculated successfully in "<<toc(&timerF)/(float)numIter<<" secs"<<endl;
	

	//write out results
	writeImage(im,imN*sizeof(imageType),"E:/temp/testMedianFilterSliceBySlice_result.bin");


	//deallocate memory
	delete[] im;

	return 0;
}
