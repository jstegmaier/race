/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
 /**
  * \brief Code to calculate 2D median filter in CUDA using templates and different window sizes
  * \Note: this code can be easily used for any non-linear processing in a neighborhhod block around each pixel with a simple modoifcation. Analogous to blockproc by Matlab
  */

#ifndef __MEDIAN_FILTER_2D_CUDA_H__
#define __MEDIAN_FILTER_2D_CUDA_H__


//define constants

#ifndef CUDA_CONSTANTS_FA
#define CUDA_CONSTANTS_FA
static const int MAX_THREADS_CUDA = 1024; //adjust it for your GPU. This is correct for a 2.0 architecture
static const int MAX_BLOCKS_CUDA = 65535;
static const int BLOCK_SIDE = 32; //we load squares into share memory. 32*32 = 1024, which is the maximum number of threads per block for CUDA arch 2.0. 
#endif

#ifndef DIMS_IMAGE_SLICE
#define DIMS_IMAGE_SLICE
static const int dimsImageSlice = 2;//so thing can be set at co0mpile time

#endif



/*
	\brief Main function to calculaye median filter with CUDA
	
	\param[in/out]		im			pointer to image in host. It is overwritten by the median result
	\param[in]			imDim		array of length dimsImageSlice indicating the image dimensions. imDim[0] is the fastest running index in memory.
	\param[in]			radius		radius of the median filter. Median filter window size is 2*radius +1. So a 3x3 median filter has radius = 1
	\param[in]			devCUDA		in case you have multiple GPU in the same computer
*/

template<class imgType>
int medianFilterCUDA(imgType* im,int* imDim,int radius,int devCUDA);


/*
	\brief apply medianFilter2D to a 3D stack slice by slice (or an RGB stack)
	
*/

template<class imgType>
int medianFilterCUDASliceBySlice(imgType* im,int* imDim,int radius,int devCUDA);




#endif //__CONVOLUTION_3D_FFT_H__