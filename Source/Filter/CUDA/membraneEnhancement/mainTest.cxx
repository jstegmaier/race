/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include "external/Nathan/tictoc.h"
#include "membraneEnhancement.h"


typedef unsigned short imageType;//define here any type you want

using namespace std;

int writeImage(void* im,long long int imSizeBytes,const char* filename)
{
	FILE* fid = fopen(filename,"wb");
	if(fid == NULL)
	{
		cout<<"ERROR: at writeImage opening file "<<filename<<endl;
		return 1;
	}
	cout<<"Writing file "<<filename<<endl;
	fwrite(im,sizeof(char),imSizeBytes,fid);
	fclose(fid);

	return 0;
}

int main( int argc, const char** argv )
{

	cout<<"=============DEBUGGING:membrane enhancement using CUDA =================="<<endl;
	int devCUDA = 0;
	int numCalls = 1;//in case process is too fast
	
	float gamma = 0.1;
	float beta = 1.0;
	float sigma[3] = {2.0, 2.0, 0.4};
	bool ScaleObjectnessMeasure = true;


	//easy synthetic image
	
	//string filename("E:/temp/testWatershed/syntheticIm.bin");
	//int64_t imgDims[dimsImage] = {32, 63, 40};
	
	

	//easy 2D synthetic image	
	//string filename("E:/temp/testWatershed/syntheticIm2D.bin");
	//int64_t imgDims[dimsImage] = {32, 63, 1};
	
	

	//real zebrafish image cropped		
	
	//string filename("E:/temp/testWatershed/imCrop.bin");
	//int64_t imgDims[dimsImage] = {101,   151,    41};
	
	

	//real zebrafish not cropped (large dataset)
	
	//string filename("E:/temp/testWatershed/im.bin");
	//int64_t imgDims[dimsImage] = { 1818 ,       1792 ,        50};//I had to reduce the number of Z planes to test it in Win32 mode	
	

	//test dataset for XPIWIT code
	
	string filename("E:/temp/testWatershed/imHessian.bin");
	int64_t imgDims[dimsImage] = { 121 ,       51 ,        31};
	
	//---------------------------------------


	//calculate image size
	int64_t imSize = 1;
	for(int ii = 0; ii < dimsImage; ii++)
		imSize *= imgDims[ii];

	//read binary stream
	ifstream imIn(filename.c_str(), ios::binary | ios::in );
	unsigned short int *img = new unsigned short int[imSize];
	imIn.read((char*)img, sizeof(short unsigned int) * imSize );

	//convert to float
	float *imgFloat = new float[imSize];
	for(std::int64_t ii = 0; ii < imSize; ii++)
		imgFloat[ii] = ((float) img[ii]) / 65535.0f;
	

	TicTocTimer tt = tic();
	
	//declare class
	membraneEnhancement_CUDA<float> wPBC(imgFloat, imgDims, devCUDA);//use Gaussian derivatives


	//perform morphological operation
	wPBC.imageObjectness(sigma, beta, gamma, ScaleObjectnessMeasure);
	





	
	//------debugging: check result---------------------	
	wPBC.getImageObjectnessFromGPU(imgFloat);
	cout<<"Total time per call took "<<toc(&tt)<<endl;
	cout<<"==========DEBUGGING: writing result to E:/temp/testWatershed/imCrop_L.bin in float32 with size "<<imgDims[0]<<"x"<<imgDims[1]<<"x"<<imgDims[2]<<endl;		
	ofstream out( "E:/temp/testWatershed/imCrop_L.bin", ios::binary | ios::out );
	out.write((char*)(imgFloat), sizeof(float) * imSize);	
	//---------------------------------------------------
	

	//release memory
	delete[] img;
	delete[] imgFloat;
	
	return 0;
}
