/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
 /**
  * \brief Code to calculate 2D median filter in CUDA using templates and different window sizes
  */

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <cstdint>
#include <algorithm>
#include <assert.h>
#include <unordered_map>
#include "membraneEnhancement.h"
#include "book.h"
#include "cuda.h"

#include "../separableConvolution3D_texture/convolutionTexture_common.h"

#if defined(_WIN32) || defined(_WIN64)
#define NOMINMAX //it messes with limits
#endif

//#define PRECISE_CUDA_TIMING_HESSIAN //uncommment this for precise cuda timing

using namespace std;



__constant__ std::int64_t imgDimsCUDA[dimsImage];

__device__ static const double PI_=3.14159265358979311600;
__device__ static const double SQRT3_=1.73205080756887719318;
//__device__ static const int ObjectDimension = 2;





//==================================================
template<class T>
__device__ inline void swapCUDA(T & a, T & b)
{	
	T tmp = a;
	a = b;
	b = tmp;
};
//=========================================================================
//determinant for 3x3 symmetric matrix
template<class T>
__device__ inline void sort3elementsAbs(T a[3])
{
	if (fabs(a[0]) > fabs(a[1])) 
		swapCUDA(a[0],a[1]);
	if (fabs(a[1]) > fabs(a[2])) 
		swapCUDA(a[1],a[2]);
	if (fabs(a[0]) > fabs(a[1])) 
		swapCUDA(a[0],a[1]);
}

//=========================================================================
//determinant for 3x3 symmetric matrix
template<class T>
__device__ inline double determinantSymmetricW_3D(const T *W_k)
{
	return W_k[0]*(W_k[3]*W_k[5]-W_k[4]*W_k[4])-W_k[1]*(W_k[1]*W_k[5]-W_k[2]*W_k[4])+W_k[2]*(W_k[1]*W_k[4]-W_k[2]*W_k[3]);
}

//===========================================================================
//analytical solution for eigenvalues 3x3 real symmetric matrices
//formula for eigenvalues from http://en.wikipedia.org/wiki/Eigenvalue_algorithm#Eigenvalues_of_3.C3.973_matrices
template<class T, bool calculateEigenvectors>
__device__ inline void  eig3(const T *w, T *d, T *v)
{

	T m,p,q, phi;	

	//calculate determinant to check if matrix is singular
	q = determinantSymmetricW_3D(w);

	if(fabs(q)<1e-24)//we consider matrix is singular
	{
		d[0]=0.0;
		//solve a quadratic equation
		m=-w[0]-w[3]-w[5];
		q=-w[1]*w[1]-w[2]*w[2]-w[4]*w[4]+w[0]*w[3]+w[0]*w[5]+w[3]*w[5];
		p=m*m-4.0*q;
		if(p<0) p=0.0;//to avoid numerical errors (symmetric matrix should have real eigenvalues)
		else p=sqrt(p);
		d[1]=0.5*(-m+p);
		d[2]=0.5*(-m-p);

	}else{//matrix not singular
		m=(w[0]+w[3]+w[5])/3.0;//trace of w /3
		q=0.5*((w[0]-m)*((w[3]-m)*(w[5]-m)-w[4]*w[4])-w[1]*(w[1]*(w[5]-m)-w[2]*w[4])+w[2]*(w[1]*w[4]-w[2]*(w[3]-m)));//determinant(a-mI)/2
		p=(2.0*(w[1]*w[1]+w[2]*w[2]+w[4]*w[4])+(w[0]-m)*(w[0]-m)+(w[3]-m)*(w[3]-m)+(w[5]-m)*(w[5]-m))/6.0;


		//NOTE: the follow formula assume accurate computation and therefor q/p^(3/2) should be in range of [1,-1],
		//but in real code, because of numerical errors, it must be checked. Thus, in case abs(q) >= abs(p^(3/2)), set phi = 0;
		phi= q / pow(p,(T)1.5);
		if(phi <= -1)
			phi = PI_ / 3.0;
		else if (phi >= 1)
			phi = 0;
		else 
			phi = acos(phi)/3.0;

		p = sqrt(p);

		//eigenvalues
		d[0] = m + 2.0*p*cos(phi);//in GPU it is better to repeat operations than to store in local registers
		d[1] = m - p*(cos(phi) + SQRT3_*sin(phi));
		d[2] = m - p*(cos(phi) - SQRT3_*sin(phi));
	}

	if( calculateEigenvectors )
	{
		int vIsZero=0;
		//eigenvectors
		v[0]=w[1]*w[4]-w[2]*(w[3]-d[0]); v[1]=w[2]*w[1]-w[4]*(w[0]-d[0]); v[2]=(w[0]-d[0])*(w[3]-d[0])-w[1]*w[1];
		v[3]=w[1]*w[4]-w[2]*(w[3]-d[1]); v[4]=w[2]*w[1]-w[4]*(w[0]-d[1]); v[5]=(w[0]-d[1])*(w[3]-d[1])-w[1]*w[1];
		v[6]=w[1]*w[4]-w[2]*(w[3]-d[2]); v[7]=w[2]*w[1]-w[4]*(w[0]-d[2]); v[8]=(w[0]-d[2])*(w[3]-d[2])-w[1]*w[1];	   


		//normalize eigenvectors
		phi=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
		if(phi>1e-12){ v[0]/=phi;v[1]/=phi;v[2]/=phi;}
		else{//numerically seems zero: we need to try the other pair of vectors to form the null space (it could be that v1 and v2 were parallel)
			v[0]=w[1]*(w[5]-d[0])-w[2]*w[4];v[1]=w[2]*w[2]-(w[5]-d[0])*(w[0]-d[0]);v[2]=(w[0]-d[0])*w[4]-w[1]*w[2];
			phi=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
			if(phi>1e-12){v[0]/=phi;v[1]/=phi;v[2]/=phi;}
			else vIsZero+=1;
		}    


		phi=sqrt(v[3]*v[3]+v[4]*v[4]+v[5]*v[5]);
		if(phi>1e-12){ v[3]/=phi;v[4]/=phi;v[5]/=phi;}
		else{//numerically seems zero: we need to try the 
			v[3]=w[1]*(w[5]-d[1])-w[2]*w[4];v[4]=w[2]*w[2]-(w[5]-d[1])*(w[0]-d[1]);v[5]=(w[0]-d[1])*w[4]-w[1]*w[2];
			phi=sqrt(v[3]*v[3]+v[4]*v[4]+v[5]*v[5]);
			if(phi>1e-12){v[3]/=phi;v[4]/=phi;v[5]/=phi;}
			else vIsZero+=2;
		}

		phi=sqrt(v[6]*v[6]+v[7]*v[7]+v[8]*v[8]);
		if(phi>1e-12) {v[6]/=phi;v[7]/=phi;v[8]/=phi;}
		else{//numerically seems zero: we need to try the 
			v[6]=w[1]*(w[5]-d[2])-w[2]*w[4];v[7]=w[2]*w[2]-(w[5]-d[2])*(w[0]-d[2]);v[8]=(w[0]-d[2])*w[4]-w[1]*w[2];
			phi=sqrt(v[6]*v[6]+v[7]*v[7]+v[8]*v[8]);
			if(phi>1e-12){v[6]/=phi;v[7]/=phi;v[8]/=phi;}
			else vIsZero+=4;
		}

		//adjust v in case some eigenvalues are zeros
		switch(vIsZero)
		{
		case 1:
			v[0]=v[4]*v[8]-v[5]*v[7];
			v[1]=v[5]*v[6]-v[3]*v[8];
			v[2]=v[4]*v[6]-v[3]*v[7];
			break;

		case 2:
			v[3]=v[1]*v[8]-v[2]*v[7];
			v[4]=v[2]*v[6]-v[0]*v[8];
			v[5]=v[1]*v[6]-v[0]*v[7];
			break;

		case 4:
			v[6]=v[4]*v[2]-v[5]*v[1];
			v[7]=v[5]*v[0]-v[3]*v[2];
			v[8]=v[4]*v[0]-v[3]*v[1];
			break;
		case 3:
			phi=sqrt(v[7]*v[7]+v[6]*v[6]);
			if(phi<1e-12)//it means first eigenvector is [0 0 1]
			{v[3]=1.0;v[4]=0.0;v[5]=0.0;}
			else{ v[3]=-v[7]/phi;v[4]=v[6]/phi;v[5]=0.0;}
			v[0]=v[4]*v[8]-v[5]*v[7];
			v[1]=v[5]*v[6]-v[3]*v[8];
			v[2]=v[3]*v[7]-v[4]*v[6];
			break;

		case 6:
			phi=sqrt(v[1]*v[1]+v[0]*v[0]);
			if(phi<1e-12)//it means first eigenvector is [0 0 1]
			{v[6]=1.0;v[7]=0.0;v[8]=0.0;}
			else{ v[6]=-v[1]/phi;v[7]=v[0]/phi;v[8]=0.0;}
			v[3]=v[1]*v[8]-v[2]*v[7];
			v[4]=v[2]*v[6]-v[0]*v[8];
			v[5]=v[0]*v[7]-v[1]*v[6];
			break;

		case 5:
			phi=sqrt(v[4]*v[4]+v[5]*v[5]);
			if(phi<1e-12)//it means first eigenvector is [0 0 1]
			{v[0]=1.0;v[1]=0.0;v[2]=0.0;}
			else{ v[0]=-v[4]/phi;v[1]=v[5]/phi;v[2]=0.0;}
			v[6]=v[4]*v[2]-v[5]*v[1];
			v[7]=v[5]*v[0]-v[3]*v[2];
			v[8]=v[1]*v[3]-v[4]*v[0];
			break;

		case 7://matrix is basically zero: so we set eigenvectors to identity matrix
			v[1]=v[2]=v[3]=v[5]=v[6]=v[7]=0.0;
			v[0]=v[4]=v[8]=1.0;
			break;

		}

		//make sure determinant is +1 for teh rotation matrix
		phi=v[0]*(v[4]*v[8]-v[5]*v[7])-v[1]*(v[3]*v[8]-v[5]*v[6])+v[2]*(v[3]*v[7]-v[4]*v[6]);
		if(phi<0)
		{
			v[0]=-v[0];v[1]=-v[1];v[2]=-v[2];
		}

	}
}

//===========================================================================
//analytical solution for eigenvalues 3x3 real symmetric matrices
//formula for eigenvalues from http://en.wikipedia.org/wiki/Eigenvalue_algorithm#Eigenvalues_of_3.C3.973_matrices
template<class T>
__device__ inline void  eig3inPlace(T *w)
{

	T m,p,q, phi;	

	//calculate determinant to check if matrix is singular
	q = determinantSymmetricW_3D(w);

	if(fabs(q)<1e-24)//we consider matrix is singular
	{		
		//solve a quadratic equation
		m=-w[0]-w[3]-w[5];
		q=-w[1]*w[1]-w[2]*w[2]-w[4]*w[4]+w[0]*w[3]+w[0]*w[5]+w[3]*w[5];
		p=m*m-4.0*q;
		if(p<0) p=0.0;//to avoid numerical errors (symmetric matrix should have real eigenvalues)
		else p=sqrt(p);
		w[1]=0.5*(-m+p);
		w[2]=0.5*(-m-p);
		w[0]=0.0;
	}else{//matrix not singular
		m=(w[0]+w[3]+w[5])/3.0;//trace of w /3
		q=0.5*((w[0]-m)*((w[3]-m)*(w[5]-m)-w[4]*w[4])-w[1]*(w[1]*(w[5]-m)-w[2]*w[4])+w[2]*(w[1]*w[4]-w[2]*(w[3]-m)));//determinant(a-mI)/2
		p=(2.0*(w[1]*w[1]+w[2]*w[2]+w[4]*w[4])+(w[0]-m)*(w[0]-m)+(w[3]-m)*(w[3]-m)+(w[5]-m)*(w[5]-m))/6.0;


		//NOTE: the follow formula assume accurate computation and therefor q/p^(3/2) should be in range of [1,-1],
		//but in real code, because of numerical errors, it must be checked. Thus, in case abs(q) >= abs(p^(3/2)), set phi = 0;
		phi= q / pow(p,(T)1.5);
		if(phi <= -1)
			phi = PI_ / 3.0;
		else if (phi >= 1)
			phi = 0;
		else 
			phi = acos(phi)/3.0;

		p = sqrt(p);

		//eigenvalues
		w[0] = m + 2.0*p*cos(phi);//in GPU it is better to repeat operations than to store in local registers
		w[1] = m - p*(cos(phi) + SQRT3_*sin(phi));
		w[2] = m - p*(cos(phi) - SQRT3_*sin(phi));
	}	
}



//========================================================
template<class imageType>
__global__ void initializeHessian(const imageType *img_CUDA, float *Hessian_CUDA, const std::int64_t imSize)
{	
	std::int64_t pos = ( (std::int64_t) ( (threadIdx.x + blockIdx.x * blockDim.x)) ) + imgDimsCUDA[0] * (  ( (std::int64_t) (threadIdx.y + blockIdx.y * blockDim.y) )   + imgDimsCUDA[1] * ( (std::int64_t)  (threadIdx.z + blockIdx.z * blockDim.z)) );

	if( pos < imSize )
	{
		float aux = img_CUDA[pos];
		for(int ii = 0; ii < 6; ii++)
		{
			Hessian_CUDA[pos] = aux;
			pos += imSize;
		}
	}

}



//========================================================
template<bool ScaleObjectnessMeasure>
__global__ void objectnessMeasureKernel(const float *Hessian_CUDA, float* imgObject_CUDA, const std::int64_t imSize, const float beta, const float gamma)
{
	float Hessian[6];
	std::int64_t pos = ( (std::int64_t) ( (threadIdx.x + blockIdx.x * blockDim.x)) ) + imgDimsCUDA[0] * (  ( (std::int64_t) (threadIdx.y + blockIdx.y * blockDim.y) )   + imgDimsCUDA[1] * ( (std::int64_t)  (threadIdx.z + blockIdx.z * blockDim.z)) );	

	if( pos >= imSize )
		return;

	//load from global memory
	for(int ii = 0; ii < 6; ii++)
	{
		Hessian[ii] = Hessian_CUDA[ pos + ii * imSize ];
	}

	//calculate eigenvalues 
	eig3inPlace<float>(Hessian);//we reuse the Hessian to save registers

	// Sort the eigenvalues by magnitude but retain their sign.
	// The eigenvalues are to be sorted |e1|<=|e2|<=...<=|eN|
	sort3elementsAbs<float>(Hessian);
	

	//calculate objectness
	//this is adapting ITK code from template< typename TInputImage, typename TOutputImage > void HessianToObjectnessMeasureImageFilter< TInputImage, TOutputImage >::ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId)
	//we assume the following values: ObjectDimension = 2 and ImageDimension = 3 -> alpha does not matter and some formulas are simplified [check our paper supplementary materials]
	// initialize the objectness measure	
	float objectnessMeasure  = 0.0f;
	if( Hessian[2] < 0 )
	{
		if ( fabs(beta) > 0.0 )
		{
			objectnessMeasure = exp( -0.5 * (Hessian[1] * Hessian[1]) / (Hessian[2] * Hessian[2] * (beta * beta) ) );
		}

		if ( fabs(gamma) > 0.0 )
		{
			float norm = 0.0;
			for ( int i = 0; i < dimsImage; i++ )
			{
				norm += Hessian[i] * Hessian[i];
			}
			objectnessMeasure *= 1.0 - exp( -0.5 * norm / (gamma * gamma) );
		}

		// in case, scale by largest absolute eigenvalue
		if ( ScaleObjectnessMeasure )
		{
			objectnessMeasure *= fabs(Hessian[2]);
		}
	}

	//write out result
	__syncthreads();//for coalescent write
	imgObject_CUDA[pos] = objectnessMeasure;//now image has objectness measure		

	//if( threadIdx.x == 0 && blockIdx.x == 0 && threadIdx.y == 0 && blockIdx.y == 0  && threadIdx.z == 0 && blockIdx.z == 0 )
	//	printf("===========DEBUGGING CUDA KERNEL!!!!====================\n");
	//imgObject_CUDA[pos] = Hessian_CUDA[pos + 0 * imSize];
}

//========================================================
template<bool ScaleObjectnessMeasure>
__global__ void objectnessMeasureKernel_block(const float *Hessian_CUDA, float* imgObject_CUDA, const std::int64_t imSize, const float beta, const float gamma, const int iniSlice, const int endSlice, const std::int64_t offset)
{
	float Hessian[6];
	//position within the block (imgDims[0] = blockDims[0] and imgDims[1] = blockDims[1])
	std::int64_t pos = ( (std::int64_t) ( (threadIdx.x + blockIdx.x * blockDim.x)) ) + imgDimsCUDA[0] * (  ( (std::int64_t) (threadIdx.y + blockIdx.y * blockDim.y) )   + imgDimsCUDA[1] * ( (std::int64_t)  (threadIdx.z + blockIdx.z * blockDim.z)) );	

	if( pos >= imSize )
		return;

	if( threadIdx.z + blockIdx.z * blockDim.z < iniSlice || (threadIdx.z + blockIdx.z * blockDim.z) >= endSlice )//this is part of the padding
		return;

	//load from global memory
	for(int ii = 0; ii < 6; ii++)
	{
		Hessian[ii] = Hessian_CUDA[ pos + ii * imSize ];
	}

	//calculate eigenvalues 
	eig3inPlace<float>(Hessian);//we reuse the Hessian to save registers

	// Sort the eigenvalues by magnitude but retain their sign.
	// The eigenvalues are to be sorted |e1|<=|e2|<=...<=|eN|
	sort3elementsAbs<float>(Hessian);
	

	//calculate objectness
	//this is adapting ITK code from template< typename TInputImage, typename TOutputImage > void HessianToObjectnessMeasureImageFilter< TInputImage, TOutputImage >::ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId)
	//we assume the following values: ObjectDimension = 2 and ImageDimension = 3 -> alpha does not matter and some formulas are simplified [check our paper supplementary materials]
	// initialize the objectness measure	
	float objectnessMeasure  = 0.0f;
	if( Hessian[2] < 0 )
	{
		if ( fabs(beta) > 0.0 )
		{
			objectnessMeasure = exp( -0.5 * (Hessian[1] * Hessian[1]) / (Hessian[2] * Hessian[2] * (beta * beta) ) );
		}

		if ( fabs(gamma) > 0.0 )
		{
			float norm = 0.0;
			for ( int i = 0; i < dimsImage; i++ )
			{
				norm += Hessian[i] * Hessian[i];
			}
			objectnessMeasure *= 1.0 - exp( -0.5 * norm / (gamma * gamma) );
		}

		// in case, scale by largest absolute eigenvalue
		if ( ScaleObjectnessMeasure )
		{
			objectnessMeasure *= fabs(Hessian[2]);
		}
	}

	//write out result
	__syncthreads();//for coalescent write
	imgObject_CUDA[pos + offset] = objectnessMeasure;//now image has objectness measure		

	//if( threadIdx.x == 0 && blockIdx.x == 0 && threadIdx.y == 0 && blockIdx.y == 0  && threadIdx.z == 0 && blockIdx.z == 0 )
	//	printf("===========DEBUGGING CUDA KERNEL!!!!====================\n");
	//imgObject_CUDA[pos] = Hessian_CUDA[pos + 0 * imSize];
}



//=========================================================================
//========================================================================

template<class imgType>
membraneEnhancement_CUDA<imgType>::membraneEnhancement_CUDA()
{
	img = NULL;
	memset(imgDims, 0 , sizeof(std::int64_t) * dimsImage );
	
	imgObject_CUDA = NULL;
};

template<class imgType>
membraneEnhancement_CUDA<imgType>::~membraneEnhancement_CUDA()
{
	//*img is not deallocated by this destructor

	//deallocate cuda elements		
	if( imgObject_CUDA != NULL )
		HANDLE_ERROR( cudaFree( imgObject_CUDA ));
};

template<class imgType>
membraneEnhancement_CUDA<imgType>::membraneEnhancement_CUDA(imgType* img_, std::int64_t *imgDims_, int devCUDA)
{
	img = img_;
	memcpy(imgDims, imgDims_, sizeof(std::int64_t) * dimsImage );	

	initializeGPU(devCUDA);
};


template<class imgType>
void membraneEnhancement_CUDA<imgType>::initializeGPU(int devCUDA)
{

	HANDLE_ERROR( cudaSetDevice( devCUDA ) );

	//copy image dimensions to GPU
	cudaMemcpyToSymbol(imgDimsCUDA, imgDims, dimsImage * sizeof(std::int64_t) );		

	imgObject_CUDA = NULL;

}

//========================================================
template<class imgType>
void membraneEnhancement_CUDA<imgType>::getImageObjectnessFromGPU(float* img_)
{
	HANDLE_ERROR(cudaMemcpy(img_, imgObject_CUDA, numElements() * sizeof(float), cudaMemcpyDeviceToHost));	
}


//=====================================================
template<class imgType>
void membraneEnhancement_CUDA<imgType>::imageObjectnessSingleBlock(float sigma[dimsImage], float beta, float gamma, bool ScaleObjectnessMeasure)
{
	
	//calculate pixel size based on sigma
	float maxSigma = sigma[0];
	for( int ii = 1; ii < dimsImage; ii++)
		if( sigma[ii] > maxSigma )
			maxSigma = sigma[ii];

	int kernelRadius = ceil(3.0f * maxSigma );//3 * sigma is the minimum to have the same precision as ITK		
	float stepSize[dimsImage];
	for( int ii = 0; ii < dimsImage; ii++)
		stepSize[ii] = sigma[ii] / maxSigma;

	//allocate memory if imgObject_CUDA == NULL
	if( imgObject_CUDA == NULL )
		HANDLE_ERROR(cudaMalloc((void**)&(imgObject_CUDA), numElements() * sizeof(float)));

	//allocate memory for Hessian
	float* Hessian_CUDA;
	HANDLE_ERROR(cudaMalloc((void**)&(Hessian_CUDA), 6 * numElements() * sizeof(float)));



#ifdef PRECISE_CUDA_TIMING_HESSIAN
	cudaEvent_t start, stop;
	float time;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);
#endif
	//calculate Hessian using separable convolution
	if( stepSize[0] == stepSize[1] == 1.0f )//especial case that alloows to safe time in memory copying
		HessianWithGaussianDerivativesGPU_AnisotropyZ (img, Hessian_CUDA, imgDims, maxSigma, stepSize[2], kernelRadius);
	else
		HessianWithGaussianDerivativesGPU_texture (img, Hessian_CUDA, imgDims, maxSigma, stepSize, kernelRadius);


#ifdef PRECISE_CUDA_TIMING_HESSIAN
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&time, start, stop);
	printf ("Time for Hessian separable convolution kernel with size = %d was %f ms\n", 2* kernelRadius + 1, time);

	cudaEventRecord(start, 0);
#endif

	int numThreadsPerDimension[3] = {32, 4, 4};
	dim3 blockGrid((int)ceil((float)imgDims[0] / (float) numThreadsPerDimension[0]), (int)ceil((float)imgDims[1] / (float) numThreadsPerDimension[1]), (int)ceil((float)imgDims[2] / (float) numThreadsPerDimension[2]));
	dim3 threadsGrid(32, 4, 4);//to have coalescence as much as possible

	if( ScaleObjectnessMeasure == true )
	{
		objectnessMeasureKernel<true><<<blockGrid, threadsGrid>>>(Hessian_CUDA, imgObject_CUDA, numElements(), beta, gamma); HANDLE_ERROR_KERNEL;
	}else{
		objectnessMeasureKernel<false><<<blockGrid, threadsGrid>>>(Hessian_CUDA, imgObject_CUDA, numElements(), beta, gamma); HANDLE_ERROR_KERNEL;
	}


#ifdef PRECISE_CUDA_TIMING_HESSIAN
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&time, start, stop);
	printf ("Time for Objectness measure kernel with size = %d was %f ms\n", 2 * kernelRadius + 1, time);
#endif

	//release memory
	HANDLE_ERROR( cudaFree( Hessian_CUDA) );


}

//=====================================================
//iniSlice and endSlice are both included. We pad the image appropriately to disregard border effects from kernel
template<class imgType>
void membraneEnhancement_CUDA<imgType>::imageObjectnessSliceWise(float sigma[dimsImage], float beta, float gamma, bool ScaleObjectnessMeasure, std::int64_t iniSlice, std::int64_t endSlice, float* HessianBlock_CUDA)
{
	
	//calculate pixel size based on sigma
	float maxSigma = sigma[0];
	for( int ii = 1; ii < dimsImage; ii++)
		if( sigma[ii] > maxSigma )
			maxSigma = sigma[ii];

	int kernelRadius = ceil(3.0f * maxSigma );//3 * sigma is the minimum to have the same precision as ITK		
	float stepSize[dimsImage];
	for( int ii = 0; ii < dimsImage; ii++)
		stepSize[ii] = sigma[ii] / maxSigma;

	std::int64_t paddingZlb = ceil(stepSize[2] * kernelRadius);			
	std::int64_t paddingZub = paddingZlb;

	//recalibrate paddingZ for first and last block
	if( iniSlice - paddingZlb < 0 )//initial block
	{
		paddingZlb = min(paddingZlb - iniSlice, std::int64_t(0));
	}
	
	if( endSlice + paddingZub >= imgDims[2] )
	{
		paddingZub = imgDims[2] - endSlice - 1;
	}

	const std::int64_t numElementsPerBlock = (endSlice - iniSlice + 1 + paddingZlb + paddingZub) * imgDims[0] * imgDims[1];
	std::int64_t imgDimsBlock[dimsImage] = {imgDims[0], imgDims[1], (endSlice - iniSlice + 1 + paddingZlb + paddingZub)};
	const std::int64_t offset = (iniSlice - paddingZlb) * imgDims[0] * imgDims[1];
	
	//calculate Hessian using separable convolution
	if( stepSize[0] == stepSize[1] == 1.0f )//especial case that alloows to safe time in memory copying
		HessianWithGaussianDerivativesGPU_AnisotropyZ (&(img[offset]), HessianBlock_CUDA, imgDimsBlock, maxSigma, stepSize[2], kernelRadius);
	else
		HessianWithGaussianDerivativesGPU_texture (&(img[offset]), HessianBlock_CUDA, imgDimsBlock, maxSigma, stepSize, kernelRadius);


	int numThreadsPerDimension[3] = {32, 4, 4};
	dim3 blockGrid((int)ceil((float)imgDimsBlock[0] / (float) numThreadsPerDimension[0]), (int)ceil((float)imgDimsBlock[1] / (float) numThreadsPerDimension[1]), (int)ceil((float)imgDimsBlock[2] / (float) numThreadsPerDimension[2]));
	dim3 threadsGrid(32, 4, 4);//to have coalescence as much as possible

	if( ScaleObjectnessMeasure == true )
	{
		objectnessMeasureKernel_block<true><<<blockGrid, threadsGrid>>>(HessianBlock_CUDA, imgObject_CUDA, numElementsPerBlock, beta, gamma, paddingZlb, imgDimsBlock[2] - paddingZub , offset); HANDLE_ERROR_KERNEL;
	}else{
		objectnessMeasureKernel_block<false><<<blockGrid, threadsGrid>>>(HessianBlock_CUDA, imgObject_CUDA, numElementsPerBlock, beta, gamma, paddingZlb, imgDimsBlock[2] - paddingZub , offset); HANDLE_ERROR_KERNEL;
	}

}

//=============================================================
template<class imgType>
void membraneEnhancement_CUDA<imgType>::imageObjectness(float sigma[dimsImage], float beta, float gamma, bool ScaleObjectnessMeasure)
{

	//check amount of memory available in GPU
	size_t freeGPUmem = 0, totalGPUmem = 0;
	HANDLE_ERROR( cudaMemGetInfo( &freeGPUmem, &totalGPUmem) ); 
	freeGPUmem -= 104857600;//We leave 100MB as a safety margin
	
	if( ((6 + 2) * numElements() * sizeof(float)) < freeGPUmem )//we can process all in one block. 
	{
		imageObjectnessSingleBlock(sigma, beta, gamma, ScaleObjectnessMeasure);
	}else{//we need blocks

		//allocate memory if imgObject_CUDA == NULL	
		if( imgObject_CUDA == NULL )
			HANDLE_ERROR(cudaMalloc((void**)&(imgObject_CUDA), numElements() * sizeof(float)));

		//calculate pixel size based on sigma
		float maxSigma = sigma[0];
		for( int ii = 1; ii < dimsImage; ii++)
			if( sigma[ii] > maxSigma )
				maxSigma = sigma[ii];

		int kernelRadius = ceil(3.0f * maxSigma );//3 * sigma is the minimum to have the same precision as ITK		
		float stepSize[dimsImage];
		for( int ii = 0; ii < dimsImage; ii++)
			stepSize[ii] = sigma[ii] / maxSigma;

		std::int64_t paddingZ = ceil(stepSize[2] * kernelRadius);
		

		std::int64_t numSlicesPerBlock = floor( (float)freeGPUmem / (float)(imgDims[0] * imgDims[1] * (6+2) * sizeof(float)) );
		

		if( numSlicesPerBlock <= 2 * paddingZ )
		{
			cout<<"ERROR: membraneEnhancement_CUDA<imgType>::imageObjectness: GPU does not have enough memory to calculate Hessian even using blocks. Aborting execution"<<endl;
			exit(10);
		}
		
		//allocate memory for Hessian block		
		const std::int64_t numElementsPerBlock = (numSlicesPerBlock + paddingZ + paddingZ) * imgDims[0] * imgDims[1];
		
		float* HessianBlock_CUDA;
		HANDLE_ERROR(cudaMalloc((void**)&(HessianBlock_CUDA), 6 * numElementsPerBlock * sizeof(float)));

		//calculate results for each block		
		int stepSlice = numSlicesPerBlock - 2 * paddingZ;
		int endSlice = -1, iniSlice = 0;
		while(iniSlice < imgDims[2] )
		{			
			endSlice += stepSlice;
			endSlice = min( endSlice, int(imgDims[2]-1) );
			imageObjectnessSliceWise(sigma, beta, gamma, ScaleObjectnessMeasure, iniSlice, endSlice, HessianBlock_CUDA);
			iniSlice = endSlice + 1;
		}		

		HANDLE_ERROR( cudaFree( HessianBlock_CUDA ) );
	}



}
//=====================================================
//declare all the possible types so template compiles properly
//template membraneEnhancement_CUDA<unsigned short int>;
//template membraneEnhancement_CUDA<unsigned char>;
template membraneEnhancement_CUDA<float>;



