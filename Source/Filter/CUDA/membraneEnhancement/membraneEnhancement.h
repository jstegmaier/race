/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
 /**
  * \brief Code to enhance surface-like elements based on Hessian
  */

#ifndef __AMATF_MEMBRANE_ENHANCEMENT_CUDA_H__
#define __AMATF_MEMBRANE_ENHANCEMENT_CUDA_H__

#include <cstdint>

//define constants
#ifndef DIMS_IMAGE_CONST //to protect agains teh same constant define in other places in the code
#define DIMS_IMAGE_CONST
	const static int dimsImage = 3;//to be able to precompile code
#endif


#ifndef CUDA_CONSTANTS_FA
#define CUDA_CONSTANTS_FA
static const int MAX_THREADS_CUDA_MEH = 1024; //maximum number of threads per block desired for thi sapplication. 
static const int MAX_BLOCKS_CUDA = 65535; //maximum number of blocks
#endif

 class cudaArray; // forward declaration of cudaArray so we do not need to include cuda headers here

//================================================================================

/*
	\brief Main class containing all the methods to calculaye watershed + Persistence Based CLustering (PBC) in the GPU
	
*/
template<class imgType> 
class membraneEnhancement_CUDA
{
public:
	
	
	//constructors
	membraneEnhancement_CUDA();
	~membraneEnhancement_CUDA();//destructor does not deallocate memory pointer to image (but it deallocates all the CUDA elements)
	membraneEnhancement_CUDA(imgType* img_, std::int64_t *imgDims_, int devCUDA);	
			
	void initializeGPU(int devCUDA);
				
	void imageObjectness(float sigma[dimsImage], float beta, float gamma, bool ScaleObjectnessMeasure);
	
	//short inline functions
	std::int64_t numElements();	
	void getImageObjectnessFromGPU(float* img_);
	
	imgType* getImgPointer(){ return img;};

	

	//-----------------------------debugging functions--------------------------------	


protected:

private:


	//CPU variables
	std::int64_t imgDims[dimsImage];//image size
	imgType* img;//pointer to the image	

	//CUDA variables
	float *imgObject_CUDA; // pointer to image objectness results


	void imageObjectnessSingleBlock(float sigma[dimsImage], float beta, float gamma, bool ScaleObjectnessMeasure);
	void imageObjectnessSliceWise(float sigma[dimsImage], float beta, float gamma, bool ScaleObjectnessMeasure, std::int64_t iniSlice, std::int64_t endSlice, float* HessianBlock_CUDA);
};

//==================================================================
template<class imgType>
inline std::int64_t membraneEnhancement_CUDA<imgType>::numElements()
{
	std::int64_t size = 1;
	for(int ii = 0; ii < dimsImage; ii++)
		size *= imgDims[ii];
	return size;
}

//=============================================================


#endif //__AMATF_MEMBRANE_ENHANCEMENT_CUDA_H__