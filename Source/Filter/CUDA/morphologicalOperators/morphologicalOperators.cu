/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
 /**
  * \brief Code to calculate 2D median filter in CUDA using templates and different window sizes
  */

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <cstdint>
#include <algorithm>
#include <assert.h>
#include <unordered_map>
#include "morpohologicalOperators.h"
#include "book.h"
#include "cuda.h"

#if defined(_WIN32) || defined(_WIN64)
#define NOMINMAX //it messes with limits
#endif

//#define PROFILE_CODE_BLOCKS_WPBC//uncomment this line to print out timing

using namespace std;


__constant__ std::int64_t imDimCUDA[3];//image dimensions 
__constant__ int structureElementsOffset[MAX_NUMBER_STRUCTURE_ELEMENTS];//offset for each neighbor (including image coordinates). It can be int because maximum offset < 2GB (no single plane is 2 GB) in our images 

template<class imageType>
__device__ inline imageType min_CUDA(const imageType a, const imageType b)
{
  return a < b ? a : b;
}

template<class imageType>
__device__ inline imageType max_CUDA(const imageType a, const imageType b)
{
  return a > b ? a : b;
}

//dim is the dim along which the offset is applied. dim = 0->x; dim=1->1; dim=2->2
template<class imageType, int dim>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_MOP) maskBorderKernel(imageType *img_CUDA, const std::int64_t *foregroundVec_CUDA, std::int64_t numElementsForeground, const int maxOffset)
{
	std::int64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
	std::int64_t pos, posAux, xyz;

	while( tid < numElementsForeground )
	{
		pos = foregroundVec_CUDA[tid];
		posAux = pos;
		for(int ii = 0; ii < dim +1; ii++)
		{
			xyz = posAux % imDimCUDA[ii];			
			posAux -= xyz;
			posAux /= imDimCUDA[ii];
		}

		if( xyz - maxOffset < 0 || xyz + maxOffset >= imDimCUDA[dim] )//this is a border pixel
			img_CUDA[pos] = 0;

		__syncthreads();
		tid += (long long int ) (blockDim.x * gridDim.x);
	}
}

template<class imageType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_MOP) dilationWithForegroundPixelIdxList(const imageType *img_CUDA,const std::int64_t imSize, imageType *tempBuffer_CUDA,const std::int64_t *foregroundVec_CUDA, std::int64_t numElementsForeground, int numNeigh)
{
	std::int64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
	std::int64_t pos, posNeigh;
	imageType val, valNeigh;

	while( tid < numElementsForeground )
	{
		pos = foregroundVec_CUDA[tid];
		val = img_CUDA[pos];

		//check the entire neighborhood
		for(int jj=0; jj < numNeigh; jj++)
		{			
			//check if element is in the image border
			posNeigh = pos + (std::int64_t)structureElementsOffset[jj];
			if( posNeigh < 0 || posNeigh >= imSize ) //this is technically incorrect since it can "loop around" an image. However, it is 8x faster than checking for x,y,z offsets and in our images usually border elements are part of the background
				continue;

			val = max_CUDA<imageType>(val, img_CUDA[posNeigh]);
		}		
		__syncthreads();//for coalescent write
		tempBuffer_CUDA[tid] = val;//I need this tempBuffer to avoid overwriting the image before finishing all the pixels	
		tid += (long long int ) (blockDim.x * gridDim.x);	
	}
}

//======================================================================
//exactly the same as above but changing max to min
template<class imageType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_MOP) erosionWithForegroundPixelIdxList(const imageType *img_CUDA, const std::int64_t imSize, imageType *tempBuffer_CUDA,const std::int64_t *foregroundVec_CUDA, std::int64_t numElementsForeground, int numNeigh)
{
	std::int64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
	std::int64_t pos, posNeigh;
	imageType val, valNeigh;

	while( tid < numElementsForeground )
	{
		pos = foregroundVec_CUDA[tid];
		val = img_CUDA[pos];

		//check the entire neighborhood
		for(int jj=0; jj < numNeigh; jj++)
		{			
			//check if element is in the image border
			posNeigh = pos + (std::int64_t)structureElementsOffset[jj];
			if( posNeigh < 0 || posNeigh >= imSize )
				continue;

			val = min_CUDA<imageType>(val, img_CUDA[posNeigh]);
		}		
		__syncthreads();//for coalescent write
		tempBuffer_CUDA[tid] = val;//I need this tempBuffer to avoid overwriting the image before finishing all the pixels	
		tid += (long long int ) (blockDim.x * gridDim.x);	
	}
}

//========================================================================================================

template<class imageType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_MOP) swapBuffers(imageType *img_CUDA, imageType *tempBuffer,const std::int64_t *foregroundVec_CUDA, std::int64_t numElementsForeground)
{
	std::int64_t tid = blockIdx.x * blockDim.x + threadIdx.x, pos;
	imageType val;
	while( tid < numElementsForeground )
	{
		pos = foregroundVec_CUDA[tid];
		val = tempBuffer[tid];		
		img_CUDA[pos] = val;
		tid += (long long int ) (blockDim.x * gridDim.x);	
	}
}


//=========================================================================
//========================================================================

template<class imgType>
morpohologicaloperators_CUDA<imgType>::morpohologicaloperators_CUDA()
{
	img = NULL;
	memset(imgDims, 0 , sizeof(std::int64_t) * dimsImage );
	numNeigh = 0;

	img_CUDA = NULL;
	foregroundVec_CUDA = NULL;
	tempBuffer_CUDA = NULL;
};

template<class imgType>
morpohologicaloperators_CUDA<imgType>::~morpohologicaloperators_CUDA()
{
	//*img is not deallocated by this destructor

	//deallocate cuda elements	
	if( img_CUDA != NULL )
	{
		HANDLE_ERROR( cudaFree( img_CUDA ) );
	}
	if( foregroundVec_CUDA != NULL )
	{
		HANDLE_ERROR( cudaFree( foregroundVec_CUDA ) );
	}
	if( tempBuffer_CUDA != NULL )
	{
		HANDLE_ERROR( cudaFree( tempBuffer_CUDA ) );
	}
};

template<class imgType>
morpohologicaloperators_CUDA<imgType>::morpohologicaloperators_CUDA(imgType* img_, std::int64_t *imgDims_, int devCUDA)
{
	img = img_;
	memcpy(imgDims, imgDims_, sizeof(std::int64_t) * dimsImage );
	numNeigh = 0;

	initializeGPU(devCUDA);
};


template<class imgType>
void morpohologicaloperators_CUDA<imgType>::initializeGPU(int devCUDA)
{
	
	HANDLE_ERROR( cudaSetDevice( devCUDA ) );
	
	//copy image dimensions to constant memory
	HANDLE_ERROR(cudaMemcpyToSymbol(imDimCUDA,imgDims, dimsImage * sizeof(std::int64_t)));

	//allocate memory for image	
	HANDLE_ERROR(cudaMalloc((void**)&(img_CUDA), numElements() * sizeof(imgType)));
	//copy image to GPU
	HANDLE_ERROR(cudaMemcpy(img_CUDA, img, numElements() * sizeof(imgType), cudaMemcpyHostToDevice));
	
	//foreground is unknown yet
	foregroundVec_CUDA = NULL;
	tempBuffer_CUDA = NULL;
	
}
//========================================================
template<class imgType>
void morpohologicaloperators_CUDA<imgType>::getImageFromGPU(imgType* img_)
{
	if( img_ == NULL )//overwrite *img
	{
		HANDLE_ERROR(cudaMemcpy(img, img_CUDA, numElements() * sizeof(imgType), cudaMemcpyDeviceToHost));
	}else{
		HANDLE_ERROR(cudaMemcpy(img_, img_CUDA, numElements() * sizeof(imgType), cudaMemcpyDeviceToHost));
	}
}


//===========================================================
template<class imgType>
void morpohologicaloperators_CUDA<imgType>::updateStructuralKernel(const std::vector<int>& structuralOffsets)
{
	numNeigh = structuralOffsets.size();
	if( structuralOffsets.size() > MAX_NUMBER_STRUCTURE_ELEMENTS )
	{
		printf("ERROR: morpohologicaloperators_CUDA<imgType>::updateStructuralKernel: number of neighbors in structuring element %d exceed maximum allowed. Change variables MAX_NUMBER_STRUCTURE_ELEMENTS %d and recompile the code\n", (int)numNeigh, (int)MAX_NUMBER_STRUCTURE_ELEMENTS);
		exit(2);
	}

	HANDLE_ERROR(cudaMemcpyToSymbol(structureElementsOffset,&(structuralOffsets[0]), structuralOffsets.size() * sizeof(int)));	
}

//==========================================================
template<class imgType>
void morpohologicaloperators_CUDA<imgType>::copyForegroundVecToCUDA()
{
	//allocate memory
	if( foregroundVec_CUDA == NULL ) 
	{
		HANDLE_ERROR( cudaMalloc( (void**)&(foregroundVec_CUDA), foregroundVec.size() * sizeof(std::int64_t) ) );	
	}
	if( tempBuffer_CUDA == NULL ) 
	{
		HANDLE_ERROR( cudaMalloc( (void**)&(tempBuffer_CUDA), foregroundVec.size() * sizeof(imgType) ) );	
	}
	//copy elements to foregroundVec
	HANDLE_ERROR( cudaMemcpy( foregroundVec_CUDA, &(foregroundVec[0]), foregroundVec.size() * sizeof(std::int64_t) , cudaMemcpyHostToDevice) );
}

//===================================================================
template<class imgType>
int morpohologicaloperators_CUDA<imgType>::morphologicalDilation(const std::vector<int>& structuralOffsetsCoord)
{
	std::int64_t Nf = foregroundVec.size();
	int numBlocks_Nf = std::min( (int) ceil(((float)Nf) / (float) MAX_THREADS_CUDA_MOP), MAX_BLOCKS_CUDA);

	//calculate morphological operation
	dilationWithForegroundPixelIdxList<imgType><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, numElements(),tempBuffer_CUDA, foregroundVec_CUDA, Nf, numNeigh);HANDLE_ERROR_KERNEL;	
	//update image
	swapBuffers<imgType><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, tempBuffer_CUDA,foregroundVec_CUDA, Nf); HANDLE_ERROR_KERNEL;	


	//set all the elements that are in the border wrt to structure elements with zero (this is optional, but ITK does it)
	if( structuralOffsetsCoord.empty() == false )
	{
		int maxOffset, ii;
		
		//loop unrolling since we need a constant value to call kernel
		ii = 0;
		maxOffset = 0;
		for(size_t jj = ii; jj < structuralOffsetsCoord.size(); jj+=3)
			maxOffset = std::max(maxOffset, abs(structuralOffsetsCoord[jj]));		
		if( maxOffset > 0 ){//call kernel to max offset
			maskBorderKernel<imgType, 0><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, foregroundVec_CUDA, Nf, maxOffset);	HANDLE_ERROR_KERNEL;}			
		
		ii = 1;
		maxOffset = 0;
		for(size_t jj = ii; jj < structuralOffsetsCoord.size(); jj+=3)
			maxOffset = std::max(maxOffset, abs(structuralOffsetsCoord[jj]));		
		if( maxOffset > 0 ){//call kernel to max offset
			maskBorderKernel<imgType, 1><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, foregroundVec_CUDA, Nf, maxOffset);	HANDLE_ERROR_KERNEL;}

		ii = 2;
		maxOffset = 0;
		for(size_t jj = ii; jj < structuralOffsetsCoord.size(); jj+=3)
			maxOffset = std::max(maxOffset, abs(structuralOffsetsCoord[jj]));		
		if( maxOffset > 0 ){//call kernel to max offset
			maskBorderKernel<imgType, 2><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, foregroundVec_CUDA, Nf, maxOffset);	HANDLE_ERROR_KERNEL;}
		
	}


	return 0;
}

//===============================================================
template<class imgType>
int morpohologicaloperators_CUDA<imgType>::morphologicalErosion(const std::vector<int>& structuralOffsetsCoord)
{
	std::int64_t Nf = foregroundVec.size();
	int numBlocks_Nf = std::min( (int) ceil(((float)Nf) / (float) MAX_THREADS_CUDA_MOP), MAX_BLOCKS_CUDA);

	//calculate morphological operation
	erosionWithForegroundPixelIdxList<imgType><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, numElements(), tempBuffer_CUDA,foregroundVec_CUDA, Nf, numNeigh);HANDLE_ERROR_KERNEL;
	//update image
	swapBuffers<imgType><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, tempBuffer_CUDA,foregroundVec_CUDA, Nf); HANDLE_ERROR_KERNEL;	


	//set all the elements that are in the border wrt to structure elements with zero (this is optional, but ITK does it)
	if( structuralOffsetsCoord.empty() == false )
	{
		int maxOffset, ii;
		
		//loop unrolling since we need a constant value to call kernel
		ii = 0;
		maxOffset = 0;
		for(size_t jj = ii; jj < structuralOffsetsCoord.size(); jj+=3)
			maxOffset = std::max(maxOffset, abs(structuralOffsetsCoord[jj]));		
		if( maxOffset > 0 ){//call kernel to max offset
			maskBorderKernel<imgType, 0><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, foregroundVec_CUDA, Nf, maxOffset);	HANDLE_ERROR_KERNEL;}			
		
		ii = 1;
		maxOffset = 0;
		for(size_t jj = ii; jj < structuralOffsetsCoord.size(); jj+=3)
			maxOffset = std::max(maxOffset, abs(structuralOffsetsCoord[jj]));		
		if( maxOffset > 0 ){//call kernel to max offset
			maskBorderKernel<imgType, 1><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, foregroundVec_CUDA, Nf, maxOffset);	HANDLE_ERROR_KERNEL;}

		ii = 2;
		maxOffset = 0;
		for(size_t jj = ii; jj < structuralOffsetsCoord.size(); jj+=3)
			maxOffset = std::max(maxOffset, abs(structuralOffsetsCoord[jj]));		
		if( maxOffset > 0 ){//call kernel to max offset
			maskBorderKernel<imgType, 2><<<numBlocks_Nf,MAX_THREADS_CUDA_MOP>>>(img_CUDA, foregroundVec_CUDA, Nf, maxOffset);	HANDLE_ERROR_KERNEL;}
		
	}
	return 0;
}
//=====================================================
//declare all the possible types so template compiles properly
template morpohologicaloperators_CUDA<unsigned short int>;
template morpohologicaloperators_CUDA<unsigned char>;
template morpohologicaloperators_CUDA<float>;



