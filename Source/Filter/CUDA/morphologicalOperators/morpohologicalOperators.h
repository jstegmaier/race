/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
 /**
  * \brief Code to calculate erosion and dilation in images
  */

#ifndef __AMATF_MORPOHOLOGICAL_OPERATORS_CUDA_H__
#define __AMATF_MORPOHOLOGICAL_OPERATORS_CUDA_H__

#include <cstdint>

//define constants
#ifndef DIMS_IMAGE_CONST //to protect agains teh same constant define in other places in the code
#define DIMS_IMAGE_CONST
	const static int dimsImage = 3;//to be able to precompile code
#endif


#define MAX_NUMBER_STRUCTURE_ELEMENTS 300 //uper bound on the maximum number of non-zero to check structure elements

#ifndef CUDA_CONSTANTS_FA
#define CUDA_CONSTANTS_FA
static const int MAX_THREADS_CUDA_MOP = 1024; //maximum number of threads per block desired for thi sapplication. 
static const int MAX_BLOCKS_CUDA = 65535; //maximum number of blocks
#endif


//================================================================================

/*
	\brief Main class containing all the methods to calculaye watershed + Persistence Based CLustering (PBC) in the GPU
	
*/
template<class imgType> 
class morpohologicaloperators_CUDA
{
public:
	
	std::vector<std::int64_t> foregroundVec;//index of the foreground elements

	//constructors
	morpohologicaloperators_CUDA();
	~morpohologicaloperators_CUDA();//destructor does not deallocate memory pointer to image (but it deallocates all the CUDA elements)
	morpohologicaloperators_CUDA(imgType* img_, std::int64_t *imgDims_, int devCUDA);
			
	void initializeGPU(int devCUDA);
	
	template<class maskType>
	void initializeForegroundWithMask(const maskType *mask, bool copyToGPU);
	void copyForegroundVecToCUDA();
	void updateStructuralKernel(const std::vector<int>& structuralOffsets);

	int morphologicalDilation(const std::vector<int>& structuralOffsetsCoord);//maximum value in the neighborhood
	int morphologicalErosion(const std::vector<int>& structuralOffsetsCoord);//lower value in the neighborhood
	
	//short inline functions
	std::int64_t numElements();
	size_t numElementsForeground(){ return foregroundVec.size();};
	void getImageFromGPU(imgType* img_);
	imgType* getImgPointer(){ return img;};

	

	//-----------------------------debugging functions--------------------------------	


protected:

private:
	std::int64_t imgDims[dimsImage];//image size
	imgType* img;//pointer to the image
	int numNeigh;//number of elements in the structural elements

	//CUDA variables
	imgType *img_CUDA; //pointer to the image in the GPU
	std::int64_t *foregroundVec_CUDA;//copy of foregroundVec in the GPU
	imgType *tempBuffer_CUDA;//temporary buffer to hold out_of_place block operations

};
//==================================================================
template<class imgType>
inline std::int64_t morpohologicaloperators_CUDA<imgType>::numElements()
{
	std::int64_t size = 1;
	for(int ii = 0; ii < dimsImage; ii++)
		size *= imgDims[ii];
	return size;
}


//===========================================================

//==================================================================
template<class imgType>
template<class maskType>
void morpohologicaloperators_CUDA<imgType>::initializeForegroundWithMask(const maskType *mask, bool copyToGPU)
{
	std::int64_t imSize = numElements();
	foregroundVec.clear();
	foregroundVec.reserve(imSize/3);
	for(std::int64_t ii = 0; ii < imSize; ii++)
	{
		if( mask[ii] != 0 )
			foregroundVec.push_back(ii);
	}

	if( copyToGPU == true )
		copyForegroundVecToCUDA();
}
//=============================================================


#endif //__AMATF_MORPOHOLOGICAL_OPERATORS_CUDA_H__