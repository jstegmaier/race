/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <cstdint>
#include <assert.h>
#include "morphologicalReconstruction.h"
#include "book.h"
#include "cuda.h"

#if defined(_WIN32) || defined(_WIN64)
#define NOMINMAX //it messes with limits
#endif

//#define DEBUG_ITERATIONS_HMAXIMA //uncomment this to debug hMaxima iterations



using namespace std;


__constant__ std::int64_t imDimCUDA[dimsImage];//image dimensions
__constant__ std::int64_t conn3DdimCUDA[74];//offset for each neighbor (it alreayd includes image dimensions). 74 is just an upper bound, so the array might not be full when conn3D = 6 or 28. 


template<class imageType>
__device__ inline imageType min_CUDA(const imageType a, const imageType b)
{
  return a < b ? a : b;
}

template<class imageType>
__device__ inline imageType max_CUDA(const imageType a, const imageType b)
{
  return a > b ? a : b;
}


//================================================================================================
/*
//Following ideas from [1]P. Karas, �Efficient computation of morphological greyscale reconstruction,� in OASIcs-OpenAccess Series in Informatics, 2011, vol. 16.
TODO: what happens if we need fully connected??
template<class imageType>
__global__ void MR_hMaximaIteration_forward_y(const imageType *img_CUDA, imageType *img_buffer)
{
	std::int64_t pos = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );
	std::int64_t imSize = imDimCUDA[0] * imDimCUDA[1] * imDimCUDA[2];

	if( pos >= imSize )
		return;

	if( voxelIsStable[pos] == true )//no need to update
		return;

	//find the maxima around the neighborhood
	imageType val = img_buffer[pos];
	imageType valOrig = val;//to check if it needs update 
	std::int64_t posNeigh;

	for(int ii = 0; ii < conn3D; ii++)
	{
		posNeigh = pos + conn3DdimCUDA[ii];
		if(  posNeigh < 0 || posNeigh >= imSize )
			continue;

		val = max_CUDA<imageType>(val, img_buffer[posNeigh]);
	}

	__syncthreads();
	//find the minimum between original image and marker image
	val = min_CUDA<imageType>(val, img_CUDA[pos]);

	//update variables	
	if( val != valOrig ){
		voxelIsStable[pos] = false;
		img_buffer[pos] = val;
	}else{
		//printf("pos = %d is stable\n", (int)pos);
		voxelIsStable[pos] = true;
	}
}
*/

//========================================================================

template<class imageType>
__global__ void MR_hMaximaIteration(const imageType *img_CUDA, imageType *img_buffer, bool *voxelIsStable, int conn3D)
{
	std::int64_t pos = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );
	std::int64_t imSize = imDimCUDA[0] * imDimCUDA[1] * imDimCUDA[2];

	if( pos >= imSize )
		return;

	if( voxelIsStable[pos] == true )//no need to update
		return;

	//find the maxima around the neighborhood
	imageType val = img_buffer[pos];
	imageType valOrig = val;//to check if it needs update 
	std::int64_t posNeigh;

	for(int ii = 0; ii < conn3D; ii++)
	{
		posNeigh = pos + conn3DdimCUDA[ii];
		if(  posNeigh < 0 || posNeigh >= imSize )
			continue;

		val = max_CUDA<imageType>(val, img_buffer[posNeigh]);
	}

	__syncthreads();
	//find the minimum between original image and marker image
	val = min_CUDA<imageType>(val, img_CUDA[pos]);

	//update variables	
	if( val != valOrig ){
		voxelIsStable[pos] = false;
		img_buffer[pos] = val;
	}else{
		//printf("pos = %d is stable\n", (int)pos);
		voxelIsStable[pos] = true;
	}
}

//========================================================================
template<class imageType>
__global__ void MR_updateVoxelFlag(const bool *voxelIsStable, bool *voxelIsStable_copy, int conn3D)
{
	std::int64_t pos = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );
	std::int64_t imSize = imDimCUDA[0] * imDimCUDA[1] * imDimCUDA[2];

	if( pos >= imSize )
		return;
	
	//check if any neighbor has been updated
	std::int64_t posNeigh;
	
	int ii = 0;
	bool flag = voxelIsStable[pos];

	while( flag == true && ii < conn3D )
	{	
		posNeigh = pos + conn3DdimCUDA[ii];
		if(  posNeigh >= 0 && posNeigh < imSize )
		{
			flag &= voxelIsStable[posNeigh];
		}
		ii++;		
	}

	//write out result
	__syncthreads();	
	voxelIsStable_copy[pos] = flag;
	
}

//========================================================================================
template<class imageType>
__global__ void  MR_InitializeImageBufferKernel(const imageType *img_CUDA, imageType *img_buffer, const imageType levelH, std::int64_t imSize ) 
{
    
	std::int64_t tid = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );		

	if( tid >= imSize )
		return;

	img_buffer[tid] = img_CUDA[tid] - levelH;	
}

//========================================================================================
//template especialization for unsigned integers to avoid wrapping value after subtraction
//template especialization for unsigned integers to avoid wrapping value after subtraction
template<>
__global__ void  MR_InitializeImageBufferKernel<std::uint8_t>(const std::uint8_t *img_CUDA, std::uint8_t *img_buffer, const std::uint8_t levelH, std::int64_t imSize ) 
{
    
	std::int64_t tid = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );		

	if( tid >= imSize )
		return;

	std::uint8_t aux = img_CUDA[tid];

	if( aux <= levelH )//wrapping around with unsigned integers
		img_buffer[tid] = 0;
	else
		img_buffer[tid] = aux - levelH;
}

//template especialization for unsigned integers to avoid wrapping value after subtraction
template<>
__global__ void  MR_InitializeImageBufferKernel<std::uint16_t>(const std::uint16_t *img_CUDA, std::uint16_t *img_buffer, const std::uint16_t levelH, std::int64_t imSize ) 
{
    
	std::int64_t tid = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );		

	if( tid >= imSize )
		return;

	std::uint16_t aux = img_CUDA[tid];

	if( aux <= levelH )//wrapping around with unsigned integers
		img_buffer[tid] = 0;
	else
		img_buffer[tid] = aux - levelH;
}

template<>
__global__ void  MR_InitializeImageBufferKernel<std::uint32_t>(const std::uint32_t *img_CUDA, std::uint32_t *img_buffer, const std::uint32_t levelH, std::int64_t imSize ) 
{
    
	std::int64_t tid = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );		

	if( tid >= imSize )
		return;

	std::uint32_t aux = img_CUDA[tid];

	if( aux <= levelH )//wrapping around with unsigned integers
		img_buffer[tid] = 0;
	else
		img_buffer[tid] = aux - levelH;
}

//========================================================================================
//from http://stackoverflow.com/questions/12505750/how-can-a-global-function-return-a-value-or-break-out-like-c-c-does
//check if we need to exit or not. As soon as 1 thread finds a point in the frontier -> we are done
__global__ void  MR_CheckExitFlagKernel(volatile bool *found, const bool *frontierFlagCUDA, std::int64_t imSize ) 
{
    volatile __shared__ bool someoneFoundIt;
	std::int64_t tid = std::int64_t(threadIdx.x + blockIdx.x * blockDim.x ) + imDimCUDA[0] * ( std::int64_t(threadIdx.y + blockIdx.y * blockDim.y ) + imDimCUDA[1] * std::int64_t(threadIdx.z + blockIdx.z * blockDim.z) );
	bool iFoundIt;	

    // initialize shared status
    if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0) 
		someoneFoundIt = *found;
    
	__syncthreads();

	if( tid >= imSize )
		return;

	if(!someoneFoundIt) 
	{
		if( frontierFlagCUDA[tid] == false )//frontier is active
			iFoundIt = true;	
		else
			iFoundIt = false;	

		// if I found it, tell everyone they can exit
		if (iFoundIt == true) 
		{ 
			someoneFoundIt = true; 
			(*found) = true; 
		}
	}
}


//========================================================================

template<class imgType>
morphologicalReconstruction_CUDA<imgType>::morphologicalReconstruction_CUDA()
{
	img = NULL;
	memset(imgDims, 0 , sizeof(std::int64_t) * dimsImage );
	conn3D = 0;

	img_CUDA = NULL;
};

template<class imgType>
morphologicalReconstruction_CUDA<imgType>::~morphologicalReconstruction_CUDA()
{
	//std::cout<<"DEBUGGING: morphologicalReconstruction_CUDA<imgType>::~morphologicalReconstruction_CUDA() is being called to deallocate CUDA elements"<<std::endl;
	//deallocate cuda elements	
	if( img_CUDA != NULL )
	{
		HANDLE_ERROR( cudaFree( img_CUDA ) );
	}
};

template<class imgType>
morphologicalReconstruction_CUDA<imgType>::morphologicalReconstruction_CUDA(imgType* img_, std::int64_t *imgDims_, int conn3D_, int devCUDA)
{
	img = img_;
	memcpy(imgDims, imgDims_, sizeof(std::int64_t) * dimsImage );
	conn3D = conn3D_;

	initializeGPU(devCUDA);
};


template<class imgType>
void morphologicalReconstruction_CUDA<imgType>::initializeGPU(int devCUDA)
{

	//cout<<"DEBUGGING: morphologicalReconstruction_CUDA<imgType>::initializeGPU is being called"<<endl;

	HANDLE_ERROR( cudaSetDevice( devCUDA ) );
	

	//generate neighborhood object
	std::int64_t *neighOffset = buildNeighboorhoodConnectivity();
	

	//constant memory
	HANDLE_ERROR(cudaMemcpyToSymbol(imDimCUDA,imgDims, dimsImage * sizeof(std::int64_t)));
	HANDLE_ERROR(cudaMemcpyToSymbol(conn3DdimCUDA,neighOffset, conn3D * sizeof(std::int64_t)));
		
	//allocate memory for image	
	HANDLE_ERROR(cudaMalloc((void**)&(img_CUDA), numElements() * sizeof(imgType)));
	//copy image to GPU
	HANDLE_ERROR(cudaMemcpy(img_CUDA, img, numElements() * sizeof(imgType), cudaMemcpyHostToDevice));

	//release memory
	delete[] neighOffset;
}


//========================================================
template<class imgType>
void morphologicalReconstruction_CUDA<imgType>::getImageFromGPU(imgType* img_)
{
	if( img_ == NULL )//overwrite *img
	{
		HANDLE_ERROR(cudaMemcpy(img, img_CUDA, numElements() * sizeof(imgType), cudaMemcpyDeviceToHost));
	}else{
		HANDLE_ERROR(cudaMemcpy(img_, img_CUDA, numElements() * sizeof(imgType), cudaMemcpyDeviceToHost));
	}
}

//========================================================

template<class imgType>
int morphologicalReconstruction_CUDA<imgType>::hMaximaFilter(imgType levelH)
{

	if( levelH < 0 )
	{
		cout<<"ERROR: morphologicalReconstruction_CUDA<imgType>::hMaximaFilter: levelH = "<<levelH<<" and it cannot be negative for hMaxima filter"<<endl;
		return 2;
	}

	std::int64_t imSize = numElements();
	
	//set threads and block dimensions
	cout<<"=============DEBUGGING: try different combinations==================="<<endl;
	int numThreadsPerDimension[3] = {16, 4, 4};

	if( conn3D == 4 || conn3D == 8 )//2D elements
	{
		numThreadsPerDimension[0] = 16;
		numThreadsPerDimension[1] = 16;
		numThreadsPerDimension[2] = 1;
	}

	dim3 blocksGrid((int)ceil((float)imgDims[0] / (float) numThreadsPerDimension[0]), (int)ceil((float)imgDims[1] / (float) numThreadsPerDimension[1]), (int)ceil((float)imgDims[2] / (float) numThreadsPerDimension[2]));
	dim3 threadsGrid(numThreadsPerDimension[0], numThreadsPerDimension[1], numThreadsPerDimension[2]);//to have coalescence as much as possible

	//allocate memory for out-of-place result
	imgType* img_buffer;
	HANDLE_ERROR(cudaMalloc((void**)&(img_buffer), imSize * sizeof(imgType)));

	
	MR_InitializeImageBufferKernel<imgType><<<blocksGrid,threadsGrid>>>(img_CUDA, img_buffer, levelH, imSize ) ;HANDLE_ERROR_KERNEL;

	//allocate memory to keep track of which pixels need to be updated
	bool *voxelIsStable, *voxelIsStable_copy, *voxelIsStable_swap;//false->needs update
	HANDLE_ERROR(cudaMalloc((void**)&(voxelIsStable), imSize * sizeof(bool)));
	HANDLE_ERROR(cudaMemset(voxelIsStable, 0, imSize * sizeof(bool)));//in the first pass, all elements are updated
	HANDLE_ERROR(cudaMalloc((void**)&(voxelIsStable_copy), imSize * sizeof(bool)));
	HANDLE_ERROR(cudaMemset(voxelIsStable_copy, 0, imSize * sizeof(bool)));//in the first pass, all elements are updated

	//declare zero-copy memory to setup flag to indicate when watershed is over
	bool *flagExit, *flagExitCUDA;
	HANDLE_ERROR( cudaHostAlloc( (void**)&(flagExit), sizeof(bool), cudaHostAllocDefault ) );
	HANDLE_ERROR( cudaMalloc( (void**)&(flagExitCUDA), sizeof(bool)) );

	//main loop
	int iter = 0;
	while(1)
	{

#ifdef DEBUG_ITERATIONS_HMAXIMA
		bool *voxeIsStableHOST = new bool[imSize];
		HANDLE_ERROR( cudaMemcpy(voxeIsStableHOST, voxelIsStable, sizeof(bool) * imSize, cudaMemcpyDeviceToHost) );
		std::int64_t numUpdates = 0;
		for(int ii = 0; ii < imSize; ii++)
			if( voxeIsStableHOST[ii] == false )
				numUpdates++;

		cout<<"DEBUGGING: hMaxima iter = "<<iter<<" needs to update "<<numUpdates<<" voxels out of "<<imSize<<endl;
		delete[] voxeIsStableHOST;
#endif

		//perform hmaxima filter
		MR_hMaximaIteration<imgType><<<blocksGrid,threadsGrid>>>(img_CUDA, img_buffer, voxelIsStable, conn3D); HANDLE_ERROR_KERNEL;

		
		//check which voxels need ot be updated
		MR_updateVoxelFlag<imgType><<<blocksGrid,threadsGrid>>>(voxelIsStable, voxelIsStable_copy, conn3D);HANDLE_ERROR_KERNEL;

		
		//switch pointers
		voxelIsStable_swap = voxelIsStable;
		voxelIsStable = voxelIsStable_copy;
		voxelIsStable_copy = voxelIsStable_swap;

		//check if algorithm temrinated
		//reset flagExit
		HANDLE_ERROR(cudaMemset(flagExitCUDA, 0, sizeof(bool)));

		MR_CheckExitFlagKernel<<<blocksGrid,threadsGrid>>>(flagExitCUDA, voxelIsStable, imSize);HANDLE_ERROR_KERNEL;
		HANDLE_ERROR( cudaMemcpy( flagExit, flagExitCUDA, sizeof(bool) , cudaMemcpyDeviceToHost) );
		if( (*flagExit) == false )
		{
			break;
		}		

		iter++;
	}
	


	//copy image from buffer to original image
	HANDLE_ERROR( cudaMemcpy(img_CUDA, img_buffer, sizeof(imgType) * imSize, cudaMemcpyDeviceToDevice) );

	//release memory
	HANDLE_ERROR( cudaFree( img_buffer ) );
	HANDLE_ERROR( cudaFree( voxelIsStable ) );
	HANDLE_ERROR( cudaFree( voxelIsStable_copy ) );
	HANDLE_ERROR( cudaFreeHost( flagExit ) );
	HANDLE_ERROR( cudaFree( flagExitCUDA ) );


	return 0;
}

//===================================================================
template<class imgType>
std::int64_t* morphologicalReconstruction_CUDA<imgType>::buildNeighboorhoodConnectivity()
{
	std::int64_t *neighOffset= new std::int64_t[conn3D];
	//std::int64_t boundarySize[dimsImage];//margin we need to calculate connected components
	//VIP: NEIGHOFFSET HAS TO BE IN ORDER (FROM LOWEST OFFSET TO HIGHEST OFFSET IN ORDER TO CALCULATE FAST IF WE ARE IN A BORDER)
	switch(conn3D)
	{
	case 4://4 connected component (so considering neighbors only in 2D slices)
		{
			neighOffset[1]=-1;neighOffset[2]=1;
			neighOffset[0]=-imgDims[0];neighOffset[3]=imgDims[0];			
			
			break;
		}
	case 6://6 connected components as neighborhood
		{
			neighOffset[2]=-1;neighOffset[3]=1;
			neighOffset[1]=-imgDims[0]; neighOffset[4]=imgDims[0];
			neighOffset[0]=-imgDims[0]*imgDims[1]; neighOffset[5]=imgDims[0]*imgDims[1];         

			
			break;
		}
	case 8://8 connected component (so considering neighbors only in 2D slices)
		{
			int countW=0;
			for(std::int64_t yy=-1;yy<=1;yy++)
			{
				for(std::int64_t xx=-1;xx<=1;xx++)
				{					
					if( xx == 0 && yy == 0 )
						continue;
					neighOffset[countW++] = xx + imgDims[0] * yy;
				}
			}

			if(countW!=conn3D)
			{
				cout<<"ERROR: at watershedPersistanceAgglomeration: Window size structure has not been completed correctly"<<endl;
				return NULL;
			}			
			break;
		}
	case 26://a cube around teh pixel in 3D
		{
			int countW=0;
			for(std::int64_t zz=-1;zz<=1;zz++)
				for(std::int64_t yy=-1;yy<=1;yy++)
					for(std::int64_t xx=-1;xx<=1;xx++)
					{
						if( xx == 0 && yy == 0 && zz == 0)
							continue;
						neighOffset[countW++] = xx+imgDims[0]*(yy+imgDims[1]*zz);
					}
					if(countW!=conn3D)
					{
						cout<<"ERROR: at watershedPersistanceAgglomeration: Window size structure has not been completed correctly"<<endl;
						return NULL;
					}
					break;
		}

	case 74://especial case for nuclei in DLSM: [2,2,1] radius windows to make sure local maxima are not that local
		{
			int countW=0;
			for(std::int64_t zz=-1;zz<=1;zz++)
				for(std::int64_t yy=-2;yy<=2;yy++)
					for(std::int64_t xx=-2;xx<=2;xx++)
					{
						if( xx == 0 && yy == 0 && zz == 0)
							continue;
						neighOffset[countW++]=xx+imgDims[0]*(yy+imgDims[1]*zz);
					}
					if(countW!=conn3D)
					{
						cout<<"ERROR: at watershedPersistanceAgglomeration: Window size structure has not been completed correctly"<<endl;
						return NULL;
					}					
					break;
		}		
	default:
		cout<<"ERROR: at watershedPersistanceAgglomeration: Code not ready for these connected components"<<endl;
		return NULL;
	}

	return neighOffset;
}

//=====================================================
//declare all the possible types so template compiles properly
template morphologicalReconstruction_CUDA<unsigned short int>;
template morphologicalReconstruction_CUDA<unsigned char>;
template morphologicalReconstruction_CUDA<float>;

