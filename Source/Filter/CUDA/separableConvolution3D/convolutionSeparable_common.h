/*
 * Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 * Modified by Fernando Amat 2014 in order to extend it to 3D
 */



#ifndef AMATF_CONVOLUTIONSEPARABLE_COMMON_H
#define AMATF_CONVOLUTIONSEPARABLE_COMMON_H

#include <vector>
#include <cstdint>

#define MAX_KERNEL_LENGTH 100 //maximum kernel length

//define constants
#ifndef DIMS_IMAGE_CONST //to protect agains teh same constant define in other places in the code
#define DIMS_IMAGE_CONST
	const static int dimsImage = 3;//to be able to precompile code
#endif


#ifndef CUDA_CONSTANTS_FA
#define CUDA_CONSTANTS_FA
static const int MAX_BLOCKS_CUDA = 65535; //maximum number of blocks
#endif
static const int MAX_THREADS_CUDA_SCONV = 512; //maximum number of threads per block desired for thi sapplication. 


template<class imgType> 
class separableConvolution_CUDA
{
public:
	

	//constructors
	separableConvolution_CUDA();
	~separableConvolution_CUDA();//destructor does not deallocate memory pointer to image (but it deallocates all the CUDA elements)
	separableConvolution_CUDA(imgType* img_, std::int64_t *imgDims_, int devCUDA);
			
	void initializeGPU(int devCUDA);
		
	void updateKernel(const std::vector<float>& h_Kernel);
	void updateImageDimensionConstant(const std::int64_t imgDims_[dimsImage]);


	int separableConvolutionOutOfPlace();//performs separable convolution along all three directions making a copy of the image in the GPU. 
	

	int separableConvolutionInPlace();//performs separable convolution along all three directions without making a copy of the image in the GPU. Approximately 7X slower than out-of-place but saves half the memory
	int separableConvolutionInPlace(float* externImgCUDA, std::int64_t imgDims_[dimsImage], int kernelRadius_);//the same but for external calls (image needs to be loaded into CUDA already)
	int separableConvolutionInPlace(float* externImgCUDA, std::int64_t imgDims_[dimsImage], int kernelRadius_, int dim_);//the same but for external calls (image needs to be loaded into CUDA already) and doing convolution only along one direction

	//short inline functions
	std::int64_t numElements();
	void getImageFromGPU(imgType* img_);
	imgType* getImgPointer(){ return img;};

	

	//-----------------------------debugging functions--------------------------------	


protected:

private:
	std::int64_t imgDims[dimsImage];//image size
	imgType* img;//pointer to the image	
	int kernel_radius;
	
	//CUDA variables
	imgType *img_CUDA; //pointer to the image in the GPU
	float *img_CUDAbuffer;//to hold intermediate results

};
//==================================================================
template<class imgType>
inline std::int64_t separableConvolution_CUDA<imgType>::numElements()
{
	std::int64_t size = 1;
	for(int ii = 0; ii < dimsImage; ii++)
		size *= imgDims[ii];
	return size;
}


////////////////////////////////////////////////////////////////////////////////
// GPU convolution
////////////////////////////////////////////////////////////////////////////////
extern "C" void setConvolutionKernel(float *h_Kernel);

extern "C" void convolutionRowsGPU(
    float *d_Dst,
    float *d_Src,
    int imageW,
    int imageH
);

extern "C" void convolutionColumnsGPU(
    float *d_Dst,
    float *d_Src,
    int imageW,
    int imageH
);



#endif
