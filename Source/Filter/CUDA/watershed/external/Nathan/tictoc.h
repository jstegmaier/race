/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
#pragma once
#ifdef __cplusplus
extern "C"{
#endif
// ---------
// Profiling
// ---------
#include <stdint.h>

typedef struct _tic_toc_timer
{ uint64_t last; // last observation on the counter
  uint64_t rate; // counts per second  
} TicTocTimer;

TicTocTimer tic(void);
double      toc(TicTocTimer *last); // returns time since last in seconds and updates timer 
double      toc(TicTocTimer *last); // returns time since last in seconds and updates timer


#ifdef __cplusplus
}
#endif
                    
