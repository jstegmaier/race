/**
 * Efficient Membrane Segmentation in Large-Scale Microscopy Images.
 * Copyright (C) 2014 J. Stegmaier, F. Amat, A. Bartschat, P. J. Keller, R. Mikut and Howard Hughes Medical Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, R. Mikut, P. J. Keller: 
 * "Efficient membrane segmentation in large-scale microscopy images", submitted manuscript, 2014.
 */
 
 /**
  * \brief Code to calculate 2D median filter in CUDA using templates and different window sizes
  */

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <cstdint>
#include <algorithm>
#include <queue>
#include <assert.h>
#include <unordered_map>
#include "watershedPBC.h"
#include "book.h"
#include "cuda.h"

#if defined(_WIN32) || defined(_WIN64)
#define NOMINMAX //it messes with limits
#endif

//#define PROFILE_CODE_BLOCKS_WPBC//uncomment this line to print out timing

using namespace std;


__constant__ long long int imDimCUDA[dimsImage];//image dimensions
__constant__ long long int conn3DdimCUDA[74];//offset for each neighbor (it alreayd includes image dimensions). 74 is just an upper bound, so the array might not be full when conn3D = 6 or 28. 
__constant__ int conn3DoffsetCUDA[74 * 3];//offset for each neighbor (without image dimensions). [x_0, y_0, z_0, x_1, ...]. 74 is just an upper bound, so the array might not be full when conn3D = 6 or 28. 



//==================================================
template<class imageType>
__device__ inline void swapCUDA(imageType & a, imageType & b)
{
	// Alternative swap doesn't use a temporary register:
	// a ^= b;
	// b ^= a;
	// a ^= b;
	
    imageType tmp = a;
    a = b;
    b = tmp;
};





//===========================================================================================

template<class imageType, class imgLabelType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_NMstate_Kernel(const imageType* img_CUDA, imageType* img_CUDAcopy, const imgLabelType* L_CUDA, imgLabelType* L_CUDAcopy, int *offsetNeighborIdxCUDA, uint8_t *frontierFlagCUDA, long long int numElementsForeground)
{
	long long int posIdx = blockIdx.x * blockDim.x + threadIdx.x;
	int offsetNeighborIdx;

	while( posIdx < numElementsForeground )
	{		
		//check if we have a neighbor that is higher
		offsetNeighborIdx = offsetNeighborIdxCUDA[posIdx];
		
		if( offsetNeighborIdx != 0 && frontierFlagCUDA[posIdx] == 1 ) //point is in NM state and needs update
		{
			//easy case: we are in NM state (non-maxima) -most pixels will follow this state-
			L_CUDAcopy[posIdx] = L_CUDA[posIdx + (long long int)offsetNeighborIdx];
			img_CUDAcopy[posIdx] = img_CUDA[posIdx + (long long int)offsetNeighborIdx];		
		}
		//check if we need more threads
		posIdx += (long long int ) (blockDim.x * gridDim.x);
		__syncthreads();
	}
}

//===========================================================================================================
//very few points are in this state (where foregroundIdxCUDA[tid] == 0) but we have to take care of them

//offsetNeighborEqualIdxCUDA[conn3D * seedPointsN] = [Nequal(x_0)_0, Nequal(x_1)_0,..., Nequal(x_N)_conn3D] : for coalescent access. Basically it stores Nequal(u) for the points that are in MP state after initialization. Each element has conn3D neighbors 
template<class imageType, class imgLabelType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_MPstate_Kernel(const imageType* img_CUDA, imageType* img_CUDAcopy, const imgLabelType* L_CUDA, imgLabelType* L_CUDAcopy, int *offsetNeighborIdxCUDA, const int *offsetNeighborEqualIdxCUDA, long long int *seedPoints, long long int seedPointsN, int conn3D)
{
	long long int tid = blockIdx.x * blockDim.x + threadIdx.x, posIdx;
	int offsetNeighborIdx;

	while( tid < seedPointsN )
	{
		//idx within foreground elements to process. We do not have coalescent access but in each round we have to process much fewer elements since not all of them receive upodated information from neighbors		
		posIdx = seedPoints[tid];
		//check if we have a neighbor that is higher
		offsetNeighborIdx = offsetNeighborIdxCUDA[posIdx];
		if( offsetNeighborIdx == 0 )//this element is still in MP state
		{
			imageType val = img_CUDA[posIdx];
			long long int posConn3D = tid;
			//check each neighborhood
			for(int ii = 0; ii < conn3D; ii++)
			{
				offsetNeighborIdx = offsetNeighborEqualIdxCUDA[posConn3D];//for coalescent access
				if( offsetNeighborIdx != 0 )
				{
					imageType valNeigh = img_CUDA[posIdx + (long long int)offsetNeighborIdx];

					if( valNeigh <= val )
					{
						L_CUDAcopy[posIdx] = min(L_CUDAcopy[posIdx], L_CUDA[posIdx + (long long int)offsetNeighborIdx] );
					}else{//states switches to NM						
						offsetNeighborIdxCUDA[posIdx] = offsetNeighborIdx;						
						img_CUDAcopy[posIdx] = img_CUDA[posIdx + (long long int)offsetNeighborIdx];	
						L_CUDAcopy[posIdx] = L_CUDA[posIdx + (long long int)offsetNeighborIdx];	
						break;
					}
				}
				posConn3D += seedPointsN;
			}
		}
		//check if we need more threads
		tid += (long long int ) (blockDim.x * gridDim.x);
		__syncthreads();
	}	
}


//============================================================================================================

template<class imageType, class imgLabelType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_SwapBuffers_Kernel(imageType* img_CUDA, const imageType* img_CUDAcopy, imgLabelType* L_CUDA, const imgLabelType* L_CUDAcopy, long long int numElementsForeground)
{
	long long int tid = blockIdx.x * blockDim.x + threadIdx.x;	
	while( tid < numElementsForeground )
	{
		L_CUDA[tid] = L_CUDAcopy[tid];		
		img_CUDA[tid] = img_CUDAcopy[tid];

		tid += (long long int ) (blockDim.x * gridDim.x);
	}
}


//============================================================================================================
//uint8_t frontierFlag[i] = {0->no need to update this voxel in next iteration, 1->update voxel and voxel is on NM state, 2->update voxel and voxel is in MP state}
template<class imageType, class imgLabelType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_UpdateSeedPoints_Kernel(const imageType* img_CUDA, const imageType* img_CUDAcopy, const imgLabelType* L_CUDA, const imgLabelType* L_CUDAcopy, const int *offsetNeighborIdxCUDA, uint8_t *frontierFlagCUDA ,long long int numElementsForeground )
{
	long long int tid = blockIdx.x * blockDim.x + threadIdx.x, auxOffset;
	imgLabelType Lnew, Lold;
	imageType valNew, valOld;
	uint8_t flag;
	int offsetNeighborIdx;

	while( tid < numElementsForeground )
	{
		//check if the value of the neighbor has changed
		flag = 0;
		offsetNeighborIdx = offsetNeighborIdxCUDA[tid];
		auxOffset = tid + (long long int)offsetNeighborIdx;
		Lnew = L_CUDAcopy[auxOffset];
		Lold = L_CUDA[auxOffset];
		valNew = img_CUDAcopy[auxOffset];
		valOld = img_CUDA[auxOffset];		

		if( offsetNeighborIdx != 0 && (Lnew != Lold || valNew != valOld) )//the update for MP state is done separately
			flag = 1;

		__syncthreads();//colaescent write

		frontierFlagCUDA[tid] = flag;
		tid += (long long int ) (blockDim.x * gridDim.x);		
	}

	
}

template<class imageType, class imgLabelType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_UpdateSeedPoints_MPstate_Kernel(const imageType* img_CUDA, const imageType* img_CUDAcopy, const imgLabelType* L_CUDA, const imgLabelType* L_CUDAcopy, const int *offsetNeighborIdxCUDA, uint8_t *frontierFlagCUDA , int *offsetNeighborEqualIdxCUDA, long long int *seedPoints, long long int seedPointsN, int conn3D )
{
	long long int tid = blockIdx.x * blockDim.x + threadIdx.x, posIdx, auxOffset;
	int offsetNeighborIdx;
	imgLabelType Lnew, Lold;
	imageType valNew, valOld;
	uint8_t flag;

	while( tid < seedPointsN )
	{
		//idx within foreground elements to process. We do not have coalescent access but in each round we have to process much fewer elements since not all of them receive upodated information from neighbors		
		posIdx = seedPoints[tid];
		//check if we have a neighbor that is higher
		offsetNeighborIdx = offsetNeighborIdxCUDA[posIdx];
		if( offsetNeighborIdx == 0 )//this element is still in MP state
		{	
			flag = 0;
			long long int posConn3D = tid;
			//check each neighborhood
			for(int ii = 0; ii < conn3D; ii++)
			{
				offsetNeighborIdx = offsetNeighborEqualIdxCUDA[posConn3D];//for coalescent access
				if( offsetNeighborIdx != 0 )
				{
					auxOffset = posIdx + (long long int)offsetNeighborIdx;
					valOld = img_CUDA[auxOffset];
					valNew = img_CUDAcopy[auxOffset];
					Lold = L_CUDA[auxOffset];
					Lnew = L_CUDAcopy[auxOffset];
					if( Lnew != Lold || valNew != valOld )//the update for MP state is done separately
					{
						flag = 2;
						break;
					}
				}
				posConn3D += seedPointsN;
			}
		}else{//some elements that were initially in the MP state have switched to NM, so we should not touch the flag in this kernel
			flag = 1;
		}
		//check if we need more threads
		__syncthreads();
		if( flag != 1 )
			frontierFlagCUDA[posIdx] = flag;
		tid += (long long int ) (blockDim.x * gridDim.x);		
	}	
}

//========================================================================================
//from http://stackoverflow.com/questions/12505750/how-can-a-global-function-return-a-value-or-break-out-like-c-c-does
//check if we need to exit or not. As soon as 1 thread finds a point in the frontier -> we are done
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_CheckExitFlag_Kernel(volatile bool *found, const uint8_t *frontierFlagCUDA, long long int numElementsForeground ) 
{
    volatile __shared__ bool someoneFoundIt;
	long long int tid = blockIdx.x * blockDim.x + threadIdx.x;
	bool iFoundIt;

    // initialize shared status
    if (threadIdx.x == 0) 
		someoneFoundIt = *found;
    
	__syncthreads();

	while(!someoneFoundIt && tid < numElementsForeground) 
	{
		if( frontierFlagCUDA[tid] > 0 )//frontier is active
			iFoundIt = true;	
		else
			iFoundIt = false;	

		// if I found it, tell everyone they can exit
		if (iFoundIt == true) 
		{ 
			someoneFoundIt = true; 
			(*found) = true; 
		}

		// if someone in another block found it, tell 
		// everyone in my block they can exit
		if (threadIdx.x == 0 && (*found) == true) 
			someoneFoundIt = true;

		__syncthreads();
		tid += (long long int ) (blockDim.x * gridDim.x);
	}
}

//============================================================================================
//initialize L: if element is in MP state->set L_CUDAcopy to the right value and L_CUDA to zero, otherwiseset L_CUDA and L_CUDAcopy to the right value
template<class imgLabelType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_initL_Kernel(imgLabelType* L_CUDA, imgLabelType* L_CUDAcopy, const int *offsetNeighborIdxCUDA,  long long int numElementsForeground, const imgLabelType labelOffset)
{
	long long int tid = blockIdx.x * blockDim.x + threadIdx.x;
	imgLabelType L;
	int offsetNeighborIdx;

	while( tid < numElementsForeground )
	{
		offsetNeighborIdx = offsetNeighborIdxCUDA[tid];
		L = L_CUDA[tid];

		if( L == 0 )//this element's label needs to be initialized
			L_CUDAcopy[tid] = labelOffset + tid + 1;//this might violate the constraint of imgLabelType = int32 -> 2GB max. However, usually number of foreground objects is not > 2GB indexing
		else
			L_CUDAcopy[tid] = L;


		if( offsetNeighborIdx == 0 || L > 0 )//MP state or glbal maximum
		{
			L = 0;
		}else{
			L = labelOffset + tid + 1;
		}
		__syncthreads();//for coalescent write
		L_CUDA[tid] = L;
		tid += (long long int ) (blockDim.x * gridDim.x);	
	}
}

//============================================================================================
template<class imageType>
__global__ void __launch_bounds__(MAX_THREADS_CUDA_WPBC) CA_watershedPBCSortedSeedNoTau_initNeighbor_Kernel(const imageType* imgFull_CUDA, const imageType* img_CUDA, const int64 *foregroundVecCUDA, int *offsetNeighborIdxCUDA,  long long int numElementsForeground, int conn3D, int64 imSize)
{
	long long int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int64 pos, neighOffset;
	imageType val, valMax, valNeigh;
	int valMaxOffset;
	int xyz[dimsImage];

	while( tid < numElementsForeground )
	{
		pos = foregroundVecCUDA[tid];
		val = img_CUDA[tid];

		//calculate xyz coordinates for pos
		neighOffset = pos;//reuse variables
		for (int ii = 0; ii < dimsImage; ii++)
		{
			xyz[ii] = neighOffset % imDimCUDA[ii];
			neighOffset -= xyz[ii];
			neighOffset /= imDimCUDA[ii];
		}

		//check the entire neighborhood
		valMax = val;
		valMaxOffset = 0;

		for(int jj=0; jj < conn3D; jj++)
		{			
			
			//check if neighbor is out of bounds
			neighOffset = 0;
			for(int ii = 0; ii < dimsImage; ii++)
			{
				if (xyz[ii] + conn3DoffsetCUDA[jj * 3 + ii] < 0 || xyz[ii] + conn3DoffsetCUDA[jj * 3 + ii] >= imDimCUDA[ii])
				{
					neighOffset = -1;
					break;
				}
			}
			if (neighOffset < 0)//neighbor out of bounds
				continue;
			
			neighOffset = conn3DdimCUDA[jj];
			valNeigh = imgFull_CUDA[pos + neighOffset];
			if( valNeigh > valMax )
			{
				valMaxOffset = neighOffset;
				valMax = valNeigh;
			}						
		}		
		__syncthreads();//for coalescent write
		offsetNeighborIdxCUDA[tid] = valMaxOffset;	
		tid += (long long int ) (blockDim.x * gridDim.x);	
	}
}

//========================================================================

template<class imgType>
watershedPBC_CUDA<imgType>::watershedPBC_CUDA()
{
	img = NULL;
	memset(imgDims, 0 , sizeof(int64) * dimsImage );
	conn3D = 0;

	L_CUDA = NULL;
};

template<class imgType>
watershedPBC_CUDA<imgType>::~watershedPBC_CUDA()
{
	//std::cout<<"DEBUGGING: watershedPBC_CUDA<imgType>::~watershedPBC_CUDA() is being called to deallocate CUDA elements"<<std::endl;
	//deallocate cuda elements	
	if( L_CUDA != NULL )
	{
		HANDLE_ERROR( cudaFree( L_CUDA ) );
	}
};

template<class imgType>
watershedPBC_CUDA<imgType>::watershedPBC_CUDA(imgType* img_, int64 *imgDims_, int conn3D_, int devCUDA)
{
	img = img_;
	memcpy(imgDims, imgDims_, sizeof(int64) * dimsImage );
	conn3D = conn3D_;

	initializeGPU(devCUDA);
};


template<class imgType>
void watershedPBC_CUDA<imgType>::initializeGPU(int devCUDA)
{

	//cout<<"DEBUGGING: watershedPBC_CUDA<imgType>::initializeGPU is being called"<<endl;

	HANDLE_ERROR( cudaSetDevice( devCUDA ) );
	

	//generate neighborhood object
	int *xyzConn = new int[3 * conn3D];
	long long int *neighOffset = buildNeighboorhoodConnectivity(xyzConn);
	

	//constant memory
	HANDLE_ERROR(cudaMemcpyToSymbol(imDimCUDA,imgDims, dimsImage * sizeof(int64)));
	HANDLE_ERROR(cudaMemcpyToSymbol(conn3DdimCUDA,neighOffset, conn3D * sizeof(long long int)));
	HANDLE_ERROR(cudaMemcpyToSymbol(conn3DoffsetCUDA,xyzConn, 3 * conn3D * sizeof(int)));
		
	L_CUDA = NULL;

	//release memory
	delete[] neighOffset;
	delete[] xyzConn;
}


//=========================================================
template<class imgType>
void watershedPBC_CUDA<imgType>::getForegroundLabelsFromGPU(imgLabelTypeCUDA* L)
{
	HANDLE_ERROR(cudaMemcpy(L, L_CUDA, foregroundVec.size() * sizeof(imgLabelTypeCUDA), cudaMemcpyDeviceToHost));
}


//========================================================


//=============================================================
template<class imgType>
template<class maskType>
int watershedPBC_CUDA<imgType>::waterhsedWithSortedElementsNoTau(imgType backgroundThr, maskType* mask)
{
#ifdef PROFILE_CODE_BLOCKS_WPBC
	time_t start, end;
	time_t startTotal, endTotal;
	time(&start);
	time(&startTotal);
#endif
	//keep only foreground elements (in general our images are very sparse)	
	
	int64 imSize = numElements();
	foregroundVec.clear();
	foregroundVec.reserve (imSize / 3);	
	vector<imgType> foregroundVecVal;
	foregroundVecVal.reserve (imSize / 3);	

	uint32_t *mapImIdx2FgndIdx = new uint32_t[imSize];	
	memset( mapImIdx2FgndIdx, 0, sizeof(uint32_t) * imSize );

	uint32_t countFgnd = 1;//0 implies the point is background 
	imgType imMax = 0;
	if( mask == NULL )//use the backgroundThr to select foreground
	{
		for(int64 ii = 0; ii < imSize; ii++)
		{		
			if( img[ii] > backgroundThr )
			{			
				mapImIdx2FgndIdx[ii] = countFgnd;
				countFgnd++;
				foregroundVec.push_back( ii );						
				foregroundVecVal.push_back( img[ii] );
				imMax = std::max( imMax, img[ii] );
			}

		}
	}else{//use the mask to select foreground
		for(int64 ii = 0; ii < imSize; ii++)
		{		
			if( mask[ii] != 0 )
			{			
				mapImIdx2FgndIdx[ii] = countFgnd;
				countFgnd++;
				foregroundVec.push_back( ii );						
				foregroundVecVal.push_back( img[ii] );						
				imMax = std::max( imMax, img[ii] );
			}
		}
	}
	size_t Nf = foregroundVec.size();
#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&end);
	cout<<"Thresholding and hash table in the CPU took "<<difftime(end,start)<< " secs for "<<Nf<<" voxels"<<endl;
#endif

	if( Nf >= std::numeric_limits<uint32_t>::max() )
	{
		cout<<"ERROR: waterhsedWithSortedElementsNoTau cannot handle more than "<<std::numeric_limits<uint32_t>::max()<<" foreground elements"<<endl;
		return 10;
	}


	//======================================================================================
	//---------------initialization state------------------------------
#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&start);
#endif
	//allocate and copy foreground image values to GPU
	imgType *img_CUDA;
	HANDLE_ERROR( cudaMalloc( (void**)&(img_CUDA), Nf * sizeof(imgType) ) );			
	HANDLE_ERROR( cudaMemcpy( img_CUDA, &(foregroundVecVal[0]), Nf * sizeof(imgType) , cudaMemcpyHostToDevice) );
	

	//the full image is only needed for this initial state
	imgType *imgFull_CUDA;
	HANDLE_ERROR( cudaMalloc( (void**)&(imgFull_CUDA), imSize * sizeof(imgType) ) );
	HANDLE_ERROR( cudaMemcpy( imgFull_CUDA, img, imSize * sizeof(imgType) , cudaMemcpyHostToDevice) );


	int *offsetNeighborIdx, *offsetNeighborIdxCUDA;
	HANDLE_ERROR( cudaHostAlloc( (void**)&(offsetNeighborIdx), Nf * sizeof(int), cudaHostAllocDefault ) );	
	HANDLE_ERROR( cudaMalloc( (void**)&(offsetNeighborIdxCUDA), Nf * sizeof(int) ) );

	int64 *foregroundVecCUDA;
	HANDLE_ERROR( cudaMalloc( (void**)&(foregroundVecCUDA), Nf * sizeof(int64) ) );
	HANDLE_ERROR( cudaMemcpy( foregroundVecCUDA, &(foregroundVec[0]), Nf * sizeof(int64) , cudaMemcpyHostToDevice) );


	//find absolute offset for each foreground element
	int numBlocks_Nf = std::min( (int) ceil(((float)Nf) / (float) MAX_THREADS_CUDA_WPBC), MAX_BLOCKS_CUDA);
	CA_watershedPBCSortedSeedNoTau_initNeighbor_Kernel<imgType><<<numBlocks_Nf,MAX_THREADS_CUDA_WPBC>>>(imgFull_CUDA, img_CUDA, foregroundVecCUDA, offsetNeighborIdxCUDA,  Nf, conn3D, imSize);HANDLE_ERROR_KERNEL;

	//finish neighborhood initialization in CPU
	HANDLE_ERROR( cudaMemcpy( offsetNeighborIdx, offsetNeighborIdxCUDA, Nf * sizeof(int) , cudaMemcpyDeviceToHost) );

	vector<int64> seedPoints_MPstate;//records index of foreground elements in the MP state after initialization
	seedPoints_MPstate.reserve(Nf / 100);//very few elements expected in MP category
	
	vector<int64> seedPoints_GMstate;//records index of foreground elements in the Global Maximum (GM) state
	seedPoints_GMstate.reserve(Nf / 100);//very few elements expected in MP category

	int64 pos;
	int valMaxOffset;	
	uint32_t auxM;

	for(uint32_t ii = 0; ii < Nf; ii++)
	{
		pos = foregroundVec[ii];
		valMaxOffset = offsetNeighborIdx[ii];
		if( valMaxOffset != 0 ) //NM state -> tansform absolute offset to relative offset
		{			
			auxM = mapImIdx2FgndIdx[ pos + (int64) valMaxOffset];
			if( auxM == 0 )//background element
				continue;
			else
				auxM--;
			if( auxM >= ii )
			{
				offsetNeighborIdx[ii] = auxM - ii;
			}
			else
			{
				valMaxOffset = ii - auxM;
				offsetNeighborIdx[ii] = -valMaxOffset;
			}			

		}else if(img[pos] == imMax){//element in MP state with maximum image value (special case since this is an absolute maximum)
			seedPoints_GMstate.push_back(ii);	
		}else{//MP state -> calculate Nequal set
			seedPoints_MPstate.push_back(ii);								
		}
	}
	
	//now calculate neighborhood for Nequal: it is more efficient since we can directly transpose data for coalescent access and we can also pin memory for faster transfer
	int  *offsetNeighborEqualIdx;
	HANDLE_ERROR( cudaHostAlloc( (void**)&(offsetNeighborEqualIdx), seedPoints_MPstate.size() * conn3D * sizeof(int), cudaHostAllocDefault ) );	
	memset(offsetNeighborEqualIdx, 0, seedPoints_MPstate.size() * conn3D * sizeof(int));//set ot zero by default to avoid push_back

	int *xyzConn = new int[3 * conn3D];
	int xyz[3];
	long long int *neighOffset = buildNeighboorhoodConnectivity(xyzConn);
	imgType val, valNeigh;
	int64 posNeigh, offsetS;
	for(size_t ii = 0; ii < seedPoints_MPstate.size(); ii++)
	{
		int64 kk = seedPoints_MPstate[ii];
		val = foregroundVecVal[kk];
		pos = foregroundVec[kk];
		offsetS = ii;
		//calculate xyz coordinates of given position
		posNeigh = pos;
		for (int aa = 0; aa < dimsImage; aa++)
		{
			xyz[aa] = posNeigh % imgDims[aa];			
			posNeigh -= xyz[aa];
			posNeigh /= imgDims[aa];
		}

		for(int jj=0; jj < conn3D; jj++)
		{			
			//check this is a border pixel
			int aa = 0;
			for(; aa < dimsImage; aa++)
			{
				if (xyz[aa] + xyzConn[jj * 3 + aa] < 0 || xyz[aa] + xyzConn[jj * 3 + aa] >= imgDims[aa])
				{
					break;
				}
			}
			if( aa < dimsImage)
				continue;

			posNeigh = pos + neighOffset[jj];
			valNeigh = img[posNeigh];
			if( valNeigh == val )
			{
				auxM = mapImIdx2FgndIdx[ posNeigh ];
				if( auxM == 0 )
					continue;//background element
				else
					auxM--;
				if( auxM >= kk )
				{
					offsetNeighborEqualIdx[offsetS] = auxM - kk;					
				}
				else
				{
					valMaxOffset = kk - auxM;
					offsetNeighborEqualIdx[offsetS] = -valMaxOffset;
				}
			}
			offsetS += seedPoints_MPstate.size();
		}	
	}

	//==================================================================
	//find connected components with maximum image value in the GM state (to get a jump sart for the algorithm and avoid a large number of iterations in clear plateaus)
	imgLabelTypeCUDA *labels_GMstate;//stores connected components labels
	HANDLE_ERROR( cudaHostAlloc( (void**)&(labels_GMstate), Nf * sizeof(imgLabelTypeCUDA), cudaHostAllocDefault ) );	//to know which ones have been visited
	memset( labels_GMstate, 0, sizeof( imgLabelTypeCUDA ) * Nf );//set to zero to know which elements were visited
	imgLabelTypeCUDA labels_GMstateN = 0;//keep count of number of labels

	for(size_t ii = 0; ii < seedPoints_GMstate.size(); ii++)
	{
		int64 kk = seedPoints_GMstate[ii];

		if( labels_GMstate[kk] != 0 )
			continue;//this one has been visited already

		labels_GMstateN++;

		std::queue<int64> q;//to traverse connected component
		q.push( kk );//index within the foreground vector
		
		labels_GMstate[kk] = labels_GMstateN;//update label
		//we do not update offsetNeighborIdx since the first element is root

		while( q.empty() == false )
		{
			kk = q.front();
			q.pop();			

			//check if we can propagate to other neighbors
			pos = foregroundVec[kk];
			//calculate xyz coordinates of given position
			posNeigh = pos;
			for (int aa = 0; aa < dimsImage; aa++)
			{
				xyz[aa] = posNeigh % imgDims[aa];
				posNeigh -= xyz[aa];
				posNeigh /= imgDims[aa];
			}
			for(int jj=0; jj < conn3D; jj++)
			{
				//check if this is a border pixel
				int ii = 0;
				for(; ii < dimsImage; ii++)
				{					
					if (xyz[ii] + xyzConn[jj * 3 + ii] < 0 || xyz[ii] + xyzConn[jj * 3 + ii] >= imgDims[ii])
					{
						break;
					}
				}
				if( ii < dimsImage )
					continue;
				
				//check if the neighbor has laredy been traversed
				posNeigh = pos + neighOffset[jj];
				auxM = mapImIdx2FgndIdx[ posNeigh ];
				if( auxM == 0 )
					continue;//background element
				else
					auxM--;

				if( labels_GMstate[ auxM ] > 0 )//element was already visited
					continue;

				//check if it still highest image value to add it to the queue
				valNeigh = img[posNeigh];
				if( valNeigh == imMax )
				{
					//add to queue
					q.push( auxM );
					labels_GMstate[auxM] = labels_GMstateN;//update label
					//update offset neighborhood (to consider them NM state)
					if( kk >= auxM )
					{
						offsetNeighborIdx[auxM] = kk - auxM;
					}
					else
					{
						valMaxOffset = auxM - kk;
						offsetNeighborIdx[auxM] = -valMaxOffset;
					}
					
				}
			}
		}
	}


	//=========end of connected components=======================================================

	//release memory that is not needed anymore
	delete[] mapImIdx2FgndIdx;
	delete[] neighOffset;
	delete[] xyzConn;
	HANDLE_ERROR( cudaFree( imgFull_CUDA ) );
	HANDLE_ERROR( cudaFree( foregroundVecCUDA ) );
			

#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&end);	
	cout<<"Initialization state step in the GPU took "<<difftime(end,start)<< " secs for "<<Nf<<" voxels"<<endl;
#endif


	//-------------------upload final initialization values to GPU-----------------
#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&start);
#endif
	int* offsetNeighborEqualIdxCUDA;
	int64* seedPointsNeighborEqualCUDA;
	HANDLE_ERROR( cudaMalloc( (void**)&(offsetNeighborEqualIdxCUDA),  seedPoints_MPstate.size() * conn3D * sizeof(int) ) );
	HANDLE_ERROR( cudaMalloc( (void**)&(seedPointsNeighborEqualCUDA), seedPoints_MPstate.size() * sizeof(int64) ) );
	
	//transfer elements to GPU
	HANDLE_ERROR( cudaMemcpy( offsetNeighborIdxCUDA, offsetNeighborIdx, Nf * sizeof(int) , cudaMemcpyHostToDevice) );
	HANDLE_ERROR( cudaMemcpy( seedPointsNeighborEqualCUDA, &(seedPoints_MPstate[0]), seedPoints_MPstate.size() * sizeof(int64) , cudaMemcpyHostToDevice) );
	HANDLE_ERROR( cudaMemcpy( offsetNeighborEqualIdxCUDA, offsetNeighborEqualIdx, seedPoints_MPstate.size() * conn3D * sizeof(int) , cudaMemcpyHostToDevice) );
	
	//release memory	
	HANDLE_ERROR( cudaFreeHost( offsetNeighborIdx ) );
	HANDLE_ERROR( cudaFreeHost( offsetNeighborEqualIdx ) );

#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&end);
	cout<<"Transferring initialization variables to the GPU took "<<difftime(end,start)<< " secs for "<<Nf<<" voxels"<<endl;
#endif
	//--------------------end of initialization--------------------------
		 

	//----------------------allocate and copy the rest of elements to the GPU-------------

	//pinned memory to exchnage frontier information efficiently
	uint8_t *frontierFlagCUDA;	
	HANDLE_ERROR( cudaMalloc( (void**)&(frontierFlagCUDA), Nf * sizeof(uint8_t) ) );

	//copy image values
	imgType *img_CUDAcopy; 		
	HANDLE_ERROR( cudaMalloc( (void**)&(img_CUDAcopy), Nf * sizeof(imgType) ) );
	HANDLE_ERROR( cudaMemcpy( img_CUDAcopy, &(foregroundVecVal[0]), Nf * sizeof(imgType) , cudaMemcpyHostToDevice) );
	foregroundVecVal.clear();

	//------------------------------------------------------------------
#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&start);
#endif
	//initialize L: if element is in MP state->set L_CUDAcopy to the right value and L_CUDA to zero, otherwiseset L_CUDA and L_CUDAcopy to the right value
	if( Nf > std::numeric_limits<imgLabelTypeCUDA>::max() )
	{
		cout<<"ERROR:waterhsedWithSortedElementsNoTau: number of foreground elements "<< Nf<<" is above the maximum number of labels allowed "<<std::numeric_limits<imgLabelTypeCUDA>::max()<<". Change the typedef of imgLabelTypeCUDA and recompile the code"<<endl;
		return 10;
	}

	if( L_CUDA != NULL ) // deallocat previous version
	{
		HANDLE_ERROR( cudaFree( L_CUDA ) );
	}

	imgLabelTypeCUDA *L_CUDAcopy;
	HANDLE_ERROR( cudaMalloc( (void**)&(L_CUDA), Nf * sizeof(imgLabelTypeCUDA) ) );
	HANDLE_ERROR( cudaMalloc( (void**)&(L_CUDAcopy), Nf * sizeof(imgLabelTypeCUDA) ) );	
	//copy labels_GMstate to L_CUDA so we can initialize L with a jump start 
	HANDLE_ERROR( cudaMemcpy( L_CUDA, labels_GMstate , Nf * sizeof(imgLabelTypeCUDA) , cudaMemcpyHostToDevice) );

	CA_watershedPBCSortedSeedNoTau_initL_Kernel<imgLabelTypeCUDA><<<numBlocks_Nf,MAX_THREADS_CUDA_WPBC>>>(L_CUDA, L_CUDAcopy, offsetNeighborIdxCUDA, Nf, labels_GMstateN);HANDLE_ERROR_KERNEL;

	//release memory we do not need any longer
	HANDLE_ERROR( cudaFreeHost( labels_GMstate ) );

	//declare zero-copy memory to setup flag to indicate when watershed is over
	bool *flagExit, *flagExitCUDA;
	HANDLE_ERROR( cudaHostAlloc( (void**)&(flagExit), sizeof(bool), cudaHostAllocDefault ) );
	HANDLE_ERROR( cudaMalloc( (void**)&(flagExitCUDA), sizeof(bool)) );

	int debugIter = 0;	
	int numBlocks_MP = std::min( (int) ceil(((float)seedPoints_MPstate.size()) / (float) MAX_THREADS_CUDA_WPBC), MAX_BLOCKS_CUDA);
	while(1)//exit will depend when we complete all seed points
	{				
		//propagate frontier
		CA_watershedPBCSortedSeedNoTau_UpdateSeedPoints_Kernel<imgType, imgLabelTypeCUDA><<<numBlocks_Nf,MAX_THREADS_CUDA_WPBC>>>(img_CUDA, img_CUDAcopy, L_CUDA, L_CUDAcopy, offsetNeighborIdxCUDA, frontierFlagCUDA , Nf);HANDLE_ERROR_KERNEL;
		CA_watershedPBCSortedSeedNoTau_UpdateSeedPoints_MPstate_Kernel<imgType, imgLabelTypeCUDA><<<numBlocks_MP,MAX_THREADS_CUDA_WPBC>>>(img_CUDA, img_CUDAcopy, L_CUDA, L_CUDAcopy, offsetNeighborIdxCUDA, frontierFlagCUDA , offsetNeighborEqualIdxCUDA, seedPointsNeighborEqualCUDA, seedPoints_MPstate.size(), conn3D );HANDLE_ERROR_KERNEL;

		//copy new solution to img_CUDA and L_CUDA buffers
		CA_watershedPBCSortedSeedNoTau_SwapBuffers_Kernel<imgType><<<numBlocks_Nf,MAX_THREADS_CUDA_WPBC>>>(img_CUDA, img_CUDAcopy, L_CUDA, L_CUDAcopy, Nf); HANDLE_ERROR_KERNEL;
			

		//check if algorithm terminated
		//reset flagExit
		HANDLE_ERROR(cudaMemset(flagExitCUDA,0,sizeof(bool)));
		CA_watershedPBCSortedSeedNoTau_CheckExitFlag_Kernel<<<numBlocks_Nf,MAX_THREADS_CUDA_WPBC>>>(flagExitCUDA, frontierFlagCUDA, Nf );HANDLE_ERROR_KERNEL;
		HANDLE_ERROR( cudaMemcpy( flagExit, flagExitCUDA, sizeof(bool) , cudaMemcpyDeviceToHost) );
		if( (*flagExit) == false )
		{
			break;
		}		

		//update point in NM state
		CA_watershedPBCSortedSeedNoTau_NMstate_Kernel<imgType, imgLabelTypeCUDA><<<numBlocks_Nf,MAX_THREADS_CUDA_WPBC>>>(img_CUDA, img_CUDAcopy, L_CUDA, L_CUDAcopy, offsetNeighborIdxCUDA, frontierFlagCUDA, Nf);HANDLE_ERROR_KERNEL;

		
		//calculate MP state
		CA_watershedPBCSortedSeedNoTau_MPstate_Kernel<imgType, imgLabelTypeCUDA><<<numBlocks_MP,MAX_THREADS_CUDA_WPBC>>>(img_CUDA, img_CUDAcopy, L_CUDA, L_CUDAcopy, offsetNeighborIdxCUDA, offsetNeighborEqualIdxCUDA, seedPointsNeighborEqualCUDA, seedPoints_MPstate.size(), conn3D);HANDLE_ERROR_KERNEL;
					
		debugIter++;	

		//safeguard. Number of iterations depends on the largest watershed region. It can take close to 1000 iteration if there is a large plateu in the image
		if( debugIter > 10000 )
		{
			printf("==========DEBUGGING: CUDA watershed: leaving without convergence!!!====================\n");
			break;
		}
	}
	
#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&end);
	cout<<"Main loop for CA watershed in the GPU took "<<difftime(end,start)<< " secs for "<<debugIter<<" iterations"<<endl;

	printf("=======Profiling watershed: numIters = %d; numForegroundElem = %d; numNM = %d; numMP = %d\n", debugIter, (int)Nf, (int)(Nf - seedPoints_MPstate.size()), (int)seedPoints_MPstate.size());
#endif
	
	//deallocate memory
	HANDLE_ERROR( cudaFreeHost( flagExit ) );
	HANDLE_ERROR( cudaFree( flagExitCUDA ) );
	HANDLE_ERROR( cudaFree( frontierFlagCUDA ) );
	HANDLE_ERROR( cudaFree( offsetNeighborEqualIdxCUDA ) );
	HANDLE_ERROR( cudaFree( offsetNeighborIdxCUDA ) );
	HANDLE_ERROR( cudaFree( seedPointsNeighborEqualCUDA ) );
	HANDLE_ERROR( cudaFree( img_CUDA ) );
	HANDLE_ERROR( cudaFree( img_CUDAcopy ) );	
	HANDLE_ERROR( cudaFree( L_CUDAcopy ) );
	
	
	
#ifdef PROFILE_CODE_BLOCKS_WPBC
	time(&endTotal);	
	printf("=======Profiling watershed: total time in main Watershed CUDA function =%g secs\n", difftime(endTotal,startTotal));
#endif
	

	return 0;
}


//===========================================================
template<class imgType>
void watershedPBC_CUDA<imgType>::debugSortPerBlock(int devCUDA)
{

	cout<<"DEBUG: testing debugBitonicSortPerBlock"<<endl;

	const int vecLength = 50;
	const int numThreadsPerBlock = 32;//it has to be less than vecLength to test the code appropiately


	//generate sequence
	imgType *vec = new imgType[vecLength];
	imgType *vecSort = new imgType[vecLength];

	/* initialize random seed: */
	//srand (time(NULL));
	srand( 10 );

	for(int ii = 0; ii < vecLength; ii++)
	{
		vec[ii] = rand() % 100 + 1;//number between 1 and 100
	}

	//initialize CUDA memory
	HANDLE_ERROR( cudaSetDevice( devCUDA ) );

	//allocate memory in CUDA (input and output)
	imgType* vecCUDA;
	HANDLE_ERROR( cudaMalloc( (void**)&(vecCUDA), vecLength * sizeof(imgType) ) );

	//transfer input: image and image dimensions
	HANDLE_ERROR(cudaMemcpy(vecCUDA, vec, vecLength * sizeof(imgType), cudaMemcpyHostToDevice));
	
	//run kernel	
	dim3 threads( numThreadsPerBlock);	
	dim3 blocks(1);	
	//debugSortkernel<imgType> <<<blocks, threads>>>(vecCUDA, vecLength);HANDLE_ERROR_KERNEL;

	//copy result to host
	HANDLE_ERROR( cudaMemcpy(vecSort, vecCUDA, vecLength * sizeof(imgType), cudaMemcpyDeviceToHost) );


	//write out results
	cout<<"Original vector"<<endl;
	for(int ii = 0; ii < vecLength; ii++)
	{
		cout<<vec[ii] <<",";
	}
	cout<<endl<<"Sorted (Descending) vector"<<endl;
	for(int ii = 0; ii < vecLength; ii++)
	{
		cout<<vecSort[ii] <<",";
	}

	cout<<"Checking if elements are sorted...";
	for(int ii = 0; ii < vecLength-1; ii++)
	{
		if( vecSort[ii] < vecSort[ii+1] )
		{
			cout<<"ERROR at element !!!!!!"<<ii<<endl;
		}
	}

	//deallocate memory
	HANDLE_ERROR( cudaFree( vecCUDA ) );

	
	//release memory
	delete[] vec;
	delete[] vecSort;

}

//===================================================================
template<class imgType>
long long int* watershedPBC_CUDA<imgType>::buildNeighboorhoodConnectivity(int* xyzOffset)
{
	int64 *neighOffset= new int64[conn3D];
	//int64 boundarySize[dimsImage];//margin we need to calculate connected components
	//VIP: NEIGHOFFSET HAS TO BE IN ORDER (FROM LOWEST OFFSET TO HIGHEST OFFSET IN ORDER TO CALCULATE FAST IF WE ARE IN A BORDER)
	switch(conn3D)
	{
	case 4://4 connected component (so considering neighbors only in 2D slices)
		{
			neighOffset[1]=-1;neighOffset[2]=1;
			neighOffset[0]=-imgDims[0];neighOffset[3]=imgDims[0];

			if( xyzOffset!= NULL )
			{
				xyzOffset[0] = 0;	xyzOffset[1] = -1;	xyzOffset[2] = 0;
				xyzOffset[3] = -1;	xyzOffset[4] = 0;	xyzOffset[5] = 0;
				xyzOffset[6] = 1;	xyzOffset[7] = 0;	xyzOffset[8] = 0;
				xyzOffset[9] = 0;	xyzOffset[10] = 1;	xyzOffset[11] = 0;
			}		

			
			break;
		}
	case 6://6 connected components as neighborhood
		{
			neighOffset[2]=-1;neighOffset[3]=1;
			neighOffset[1]=-imgDims[0]; neighOffset[4]=imgDims[0];
			neighOffset[0]=-imgDims[0]*imgDims[1]; neighOffset[5]=imgDims[0]*imgDims[1];         

			if( xyzOffset!= NULL )
			{
				xyzOffset[0] = 0;	xyzOffset[1] = 0;	xyzOffset[2] = -1;
				xyzOffset[3] = 0;	xyzOffset[4] = -1;	xyzOffset[5] = 0;
				xyzOffset[6] = -1;	xyzOffset[7] = 0;	xyzOffset[8] = 0;
				xyzOffset[9] = 1;	xyzOffset[10] = 0;	xyzOffset[11] = 0;
				xyzOffset[12] = 0;	xyzOffset[13] = 1;	xyzOffset[14] = 0;
				xyzOffset[15] = 0;	xyzOffset[16] = 0;	xyzOffset[17] = 1;
			}
			
			break;
		}
	case 8://8 connected component (so considering neighbors only in 2D slices)
		{
			int countW=0;
			int countO = 0;
			for(int64 yy=-1;yy<=1;yy++)
			{
				for(int64 xx=-1;xx<=1;xx++)
				{					
					if( xx == 0 && yy == 0 )
						continue;
					neighOffset[countW++] = xx + imgDims[0] * yy;
					if( xyzOffset != NULL )
					{
						xyzOffset[countO++] = xx;
						xyzOffset[countO++] = yy;
						xyzOffset[countO++] = 0;
					}
				}
			}

			if(countW!=conn3D)
			{
				cout<<"ERROR: at watershedPersistanceAgglomeration: Window size structure has not been completed correctly"<<endl;
				return NULL;
			}			
			break;
		}
	case 26://a cube around teh pixel in 3D
		{
			int countW=0;
			int countO=0;
			for(int64 zz=-1;zz<=1;zz++)
				for(int64 yy=-1;yy<=1;yy++)
					for(int64 xx=-1;xx<=1;xx++)
					{
						if( xx == 0 && yy == 0 && zz == 0)
							continue;
						neighOffset[countW++] = xx+imgDims[0]*(yy+imgDims[1]*zz);
						if( xyzOffset != NULL )
						{
							xyzOffset[countO++] = xx;
							xyzOffset[countO++] = yy;
							xyzOffset[countO++] = zz;
						}
					}
					if(countW!=conn3D)
					{
						cout<<"ERROR: at watershedPersistanceAgglomeration: Window size structure has not been completed correctly"<<endl;
						return NULL;
					}
					break;
		}

	case 74://especial case for nuclei in DLSM: [2,2,1] radius windows to make sure local maxima are not that local
		{
			int countW=0;
			int countO=0;
			for(int64 zz=-1;zz<=1;zz++)
				for(int64 yy=-2;yy<=2;yy++)
					for(int64 xx=-2;xx<=2;xx++)
					{
						if( xx == 0 && yy == 0 && zz == 0)
							continue;
						neighOffset[countW++]=xx+imgDims[0]*(yy+imgDims[1]*zz);

						if( xyzOffset != NULL )
						{
							xyzOffset[countO++] = xx;
							xyzOffset[countO++] = yy;
							xyzOffset[countO++] = zz;
						}
					}
					if(countW!=conn3D)
					{
						cout<<"ERROR: at watershedPersistanceAgglomeration: Window size structure has not been completed correctly"<<endl;
						return NULL;
					}					
					break;
		}		
	default:
		cout<<"ERROR: at watershedPersistanceAgglomeration: Code not ready for these connected components"<<endl;
		return NULL;
	}

	return neighOffset;
}

//=====================================================
//declare all the possible types so template compiles properly
template watershedPBC_CUDA<unsigned short int>;
template watershedPBC_CUDA<unsigned char>;
template watershedPBC_CUDA<float>;


template int watershedPBC_CUDA<unsigned short int>::waterhsedWithSortedElementsNoTau<unsigned char>(unsigned short int backgroundThr, unsigned char* mask);
template int watershedPBC_CUDA<unsigned char>::waterhsedWithSortedElementsNoTau<unsigned char>(unsigned char backgroundThr, unsigned char* mask);
template int watershedPBC_CUDA<float>::waterhsedWithSortedElementsNoTau<unsigned char>(float backgroundThr, unsigned char* mask);

template int watershedPBC_CUDA<unsigned short int>::waterhsedWithSortedElementsNoTau<unsigned short int>(unsigned short int backgroundThr, unsigned short int* mask);
template int watershedPBC_CUDA<unsigned char>::waterhsedWithSortedElementsNoTau<unsigned short int>(unsigned char backgroundThr, unsigned short int* mask);
template int watershedPBC_CUDA<float>::waterhsedWithSortedElementsNoTau<unsigned short int>(float backgroundThr, unsigned short int* mask);

template int watershedPBC_CUDA<unsigned short int>::waterhsedWithSortedElementsNoTau<float>(unsigned short int backgroundThr, float* mask);
template int watershedPBC_CUDA<unsigned char>::waterhsedWithSortedElementsNoTau<float>(unsigned char backgroundThr, float* mask);
template int watershedPBC_CUDA<float>::waterhsedWithSortedElementsNoTau<float>(float backgroundThr, float* mask);

template int watershedPBC_CUDA<unsigned short int>::waterhsedWithSortedElementsNoTau<bool>(unsigned short int backgroundThr, bool* mask);
template int watershedPBC_CUDA<unsigned char>::waterhsedWithSortedElementsNoTau<bool>(unsigned char backgroundThr, bool* mask);
template int watershedPBC_CUDA<float>::waterhsedWithSortedElementsNoTau<bool>(float backgroundThr, bool* mask);
