/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef __XPIWIT_EXTRACTREGIONPROPSIMAGEFILTER_H
#define __XPIWIT_EXTRACTREGIONPROPSIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkMatrix.h"
#include <string>
#include <QString>
#include "../Base/MetaData/MetaDataFilter.h"

namespace itk
{

/**
 * @class RegionProps container class for region properties.
 * used to store information such as index, scale, intensity, ...
 */
class RegionProps
{
    public:
        /**
         * The constructor to instantly set all contained variables.
         * scheme: id, size, xpos, ypos, zpos, xsize, ysize, zsize, weightedx, weightedy, weightedz, minaxis, medaxis, majaxis, eccentricity, elongation, orientation, integratedintensity, ...
         *         sigma, seedIntensity, seedScale, minIntensityForeground, maxIntensityForeground, minIntensityBackground, maxIntensityBackground, volumeForeground, volumeBackground, foregroundIntensity, backgroundIntensity, fg/bg ratio
         */
        RegionProps(const unsigned int id, const unsigned int volume,
                    const unsigned int x, const unsigned int y, const unsigned int z,
                    const int width, const int height, const int depth,
                    const float weightedX, const float weightedY, const float weightedZ,
                    const float minorAxis, const float mediumAxis, const float majorAxis,
                    const float eccentricity, const float elongation, const float orientation,
                    const float integratedIntensity, const float regionSigma,
                    const float seedIntensity, const float seedScale,
                    const float minIntensityForeground, const float maxIntensityForeground, const float minIntensityBackground, const float maxIntensityBackground,
                    const int volumeForeground, const int volumeBackground,
                    const float intensityForeground, const float intensityBackground, const float foregroundBackgroundRatio, const float integratedGradientMagnitude)
        {
            m_ID = id;
            m_Volume = volume;
            m_X = x;
            m_Y = y;
            m_Z = z;
            m_Width = width;
            m_Height = height;
            m_Depth = depth;
            m_WeightedX = weightedX;
            m_WeightedY = weightedY;
            m_WeightedZ = weightedZ;
            m_MinorAxis = minorAxis;
            m_MediumAxis = mediumAxis;
            m_MajorAxis = majorAxis;
            m_Eccentricity = eccentricity;
            m_Elongation = elongation;
            m_Orientation = orientation;
            m_IntegratedIntensity = integratedIntensity;
            m_SeedIntensity = seedIntensity;
            m_SeedScale = seedScale;
            m_RegionSigma = regionSigma;
            m_MinIntensityForeground = minIntensityForeground;
            m_MaxIntensityForeground = maxIntensityForeground;
            m_MinIntensityBackground = minIntensityBackground;
            m_MaxIntensityBackground = maxIntensityBackground;
            m_VolumeForeground = volumeForeground;
            m_VolumeBackground = volumeBackground;
            m_IntensityForeground = intensityForeground;
            m_IntensityBackground = intensityBackground;
            m_ForegroundBackgroundRatio = foregroundBackgroundRatio;
            m_IntegratedGradientMagnitude = integratedGradientMagnitude;
        }

        ~RegionProps() {}

        itkGetMacro( ID, unsigned int);
        itkGetMacro( Volume, unsigned int);
        itkGetMacro( X, unsigned int);
        itkGetMacro( Y, unsigned int);
        itkGetMacro( Z, unsigned int);
        itkGetMacro( Width, int);
        itkGetMacro( Height, int);
        itkGetMacro( Depth, int);
        itkGetMacro( WeightedX, float);
        itkGetMacro( WeightedY, float);
        itkGetMacro( WeightedZ, float);
        itkGetMacro( IntegratedIntensity, float);
        itkGetMacro( MinorAxis, float);
        itkGetMacro( MediumAxis, float);
        itkGetMacro( MajorAxis, float);
        itkGetMacro( Eccentricity, float);
        itkGetMacro( Elongation, float);
        itkGetMacro( Orientation, float);
        itkGetMacro( RegionSigma, float);
        itkGetMacro( SeedScale, float );
        itkGetMacro( MinIntensityForeground, float );
        itkGetMacro( MaxIntensityForeground, float );
        itkGetMacro( SeedIntensity, float );
        itkGetMacro( MinIntensityBackground, float );
        itkGetMacro( MaxIntensityBackground, float );
        itkGetMacro( VolumeForeground, unsigned int );
        itkGetMacro( VolumeBackground, unsigned int );
        itkGetMacro( IntensityForeground, float );
        itkGetMacro( IntensityBackground, float );
        itkGetMacro( ForegroundBackgroundRatio, float );
        itkGetMacro( IntegratedGradientMagnitude, float );

    private:

        unsigned int m_ID;
        unsigned int m_Volume;
        unsigned int m_X;
        unsigned int m_Y;
        unsigned int m_Z;
        int m_Width;
        int m_Height;
        int m_Depth;
        float m_WeightedX;
        float m_WeightedY;
        float m_WeightedZ;
        float m_IntegratedIntensity;
        float m_MinorAxis;
        float m_MediumAxis;
        float m_MajorAxis;
        float m_Eccentricity;
        float m_Elongation;
        float m_Orientation;
        float m_RegionSigma;
        float m_SeedIntensity;
        float m_SeedScale;
        float m_MinIntensityForeground;
        float m_MaxIntensityForeground;
        float m_MinIntensityBackground;
        float m_MaxIntensityBackground;
        unsigned int m_VolumeForeground;
        unsigned int m_VolumeBackground;
        float m_IntensityForeground;
        float m_IntensityBackground;
        float m_ForegroundBackgroundRatio;
        float m_IntegratedGradientMagnitude;
};


/**
 * @class ExtractRegionPropsImageFilter
 * Used to extract properties of binary connected regions within an image. Input image can be either
 * binary or a label image. In case of a binary image, a label image is automatically generated.
 */
template <class TInputImageType>
class ITK_EXPORT ExtractRegionPropsImageFilter : public ImageToImageFilter<TInputImageType, TInputImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TInputImageType::ImageDimension);
        typedef ExtractRegionPropsImageFilter Self;
        typedef ImageToImageFilter<TInputImageType, TInputImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;

        itkNewMacro(Self);
        typedef TInputImageType InputImageType;

        /**
         * The constructor.
         */
        ExtractRegionPropsImageFilter();

        /**
         * The destructor.
         */
        virtual ~ExtractRegionPropsImageFilter();

        /**
         * set and get function for the member variables
         */
        itkGetMacro( FileName, std::string);
        itkSetMacro( FileName, std::string);
        itkGetMacro( BinaryInput, bool);
        itkSetMacro( BinaryInput, bool);
        itkGetMacro( LogResults, bool);
        itkSetMacro( LogResults, bool);
        itkSetMacro( IntensityImage, typename InputImageType::Pointer );
        itkGetMacro( IntensityImage, typename InputImageType::Pointer );
        itkSetMacro( CentroidOffset, int );
        itkGetMacro( CentroidOffset, int );
        itkSetMacro( FullyConnected, bool );
        itkGetMacro( FullyConnected, bool );

        void SetMetaObject( XPIWIT::MetaDataFilter* metaObject ){ m_MetaObject = metaObject; }

    protected:
        // typedef abbreviations for some filters
        typedef typename itk::BinaryImageToLabelMapFilter<InputImageType> BinaryImageToLabelMapFilterType;
        typedef typename itk::LabelMapToLabelImageFilter<LabelMap< LabelObject< SizeValueType, TInputImageType::ImageDimension > >, InputImageType> LabelMapToLabelImageFilterType;
        typedef typename itk::LabelGeometryImageFilter<InputImageType, InputImageType> LabelGeometryImageFilterType;

        /**
         * Functions for data generation.
         */
        void GenerateData();

        XPIWIT::MetaDataFilter *m_MetaObject;						// meta data object to store results
        std::string m_FileName;								// the file to store the extracted information to
        bool m_BinaryInput;									// flag to switch between binary or labeled input
        typename InputImageType::Pointer m_IntensityImage;	// intensity image. If provided mean intensity and the like can also be extracted
        int m_CentroidOffset;								// offset that is added to extracted centroids, e.g. useful if converting to matlab representation
        bool m_FullyConnected;								// determines the connectivity of the regions to be identified.
        bool m_LogResults;									// if true the regions are additionally written to the cout
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkExtractRegionPropsImageFilter.txx"
#endif

#endif
