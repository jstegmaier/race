/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// include required headers
#include "itkFastMorphologicalOperatorsImageFilter.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkConstantBoundaryCondition.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage, class TStructuringElement>
FastMorphologicalOperatorsImageFilter<TImageType, TOutputImage,TStructuringElement>::FastMorphologicalOperatorsImageFilter()
{
	m_Type = 0;
	m_MaskImage = NULL;
	m_IgnoreBorderRegions = true;
}


// the destructor
template <class TImageType, class TOutputImage, class TStructuringElement>
FastMorphologicalOperatorsImageFilter<TImageType, TOutputImage,TStructuringElement>::~FastMorphologicalOperatorsImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage, class TStructuringElement>
void FastMorphologicalOperatorsImageFilter<TImageType, TOutputImage,TStructuringElement>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
}


// the thread generate data
template <class TImageType, class TOutputImage, class TStructuringElement>
void FastMorphologicalOperatorsImageFilter<TImageType, TOutputImage,TStructuringElement>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	// define the output region to be without the border
	typename TOutputImage::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();
    typename TOutputImage::RegionType regionWithoutBorders;
    typename TOutputImage::RegionType::IndexType regionWithoutBordersIndex;
    typename TOutputImage::RegionType::SizeType regionWithoutBordersSize;
    for (int i=0; i<TImageType::ImageDimension; ++i)
    {
        regionWithoutBordersIndex[i] = outputRegionForThread.GetIndex(i)+m_Kernel.GetRadius()[i];
        regionWithoutBordersSize[i] = outputRegionForThread.GetSize(i)-2*m_Kernel.GetRadius()[i];

		if (i==TImageType::ImageDimension-1)
		{
			regionWithoutBordersIndex[i] = vnl_math_max((unsigned int)m_Kernel.GetRadius()[i], (unsigned int)outputRegionForThread.GetIndex(i));
			regionWithoutBordersSize[i] = vnl_math_min((unsigned int)(regionWithoutBordersIndex[i]+outputRegionForThread.GetSize(i)), (unsigned int)(largestPossibleRegion.GetSize()[i]-m_Kernel.GetRadius()[i])) - regionWithoutBordersIndex[i];
		}
    }

    // set the region
    regionWithoutBorders.SetIndex(regionWithoutBordersIndex);
    regionWithoutBorders.SetSize(regionWithoutBordersSize);

	// set the region to process
	typename TOutputImage::RegionType processRegion = outputRegionForThread;
	if (m_IgnoreBorderRegions == true)
		processRegion = regionWithoutBorders;

	//typedef itk::ConstantBoundaryCondition<InputImageType> BoundaryConditionType;
	itk::ConstNeighborhoodIterator< TImageType, itk::ZeroFluxNeumannBoundaryCondition<TImageType, TImageType> > inputIterator( m_Kernel.GetRadius(), input, processRegion );
	itk::ImageRegionConstIterator< TImageType > maskIterator( m_MaskImage, processRegion );
    itk::ImageRegionIterator< TOutputImage > outputIterator( output, processRegion );
	inputIterator.SetNeedToUseBoundaryCondition( !m_IgnoreBorderRegions );
	
	inputIterator.GoToBegin();
	maskIterator.GoToBegin();
	outputIterator.GoToBegin();

	unsigned int numNeighbors = inputIterator.Size();
	float minimumValue, maximumValue;
	float currentValue;
	unsigned int j;

	// iterate trough the seed region and set voxels for the initial LSF
	while (outputIterator.IsAtEnd() == false)
	{
		// initialize the maximum value
		minimumValue = FLT_MAX;
		maximumValue = 0.0;

        // if value is below threshold skip further calculations
		if (maskIterator.Value() > 0 || m_MaskImage == NULL)
        {
            // calculate the min/max of the neighbours
            for (j=0; j<numNeighbors; ++j)
            {
				if (m_Kernel[j] > 0)
				{
					currentValue = inputIterator.GetPixel(j);
					if (currentValue > maximumValue)
						maximumValue = currentValue;

					if (currentValue < minimumValue)
						minimumValue = currentValue;
				}
            }

			if (m_Type == 0)
				outputIterator.Set( minimumValue );
			else if (m_Type == 1)
				outputIterator.Set( maximumValue );
		}
		else
		{
			outputIterator.Set( 0 );
		}

		++inputIterator;
		++maskIterator;
		++outputIterator;
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage, class TStructuringElement>
void FastMorphologicalOperatorsImageFilter<TImageType, TOutputImage,TStructuringElement>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
