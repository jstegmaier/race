/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef __XPIWIT_LABELIMAGETOUNCERTAINTYMAPFILTER_H
#define __XPIWIT_LABELIMAGETOUNCERTAINTYMAPFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkExtractKeyPointsImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
//#include "itkMultiplyByConstantImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkMacro.h"
#include <string>
#include <QHash>

#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class SeedBasedTwandSegmentationFilter
 * Fast seed based segmentation filter, e.g. to extract stained nuclei from microscopy images.
 * Yields a relatively good tradeoff between speed vs. processing time and is therefore suited for
 * processing large image stacks. The filter has to be provided with a seed point file.
 */
template <class TImageType>
class ITK_EXPORT LabelImageToUncertaintyMapFilter : public ImageToImageFilter<TImageType, TImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);
        typedef LabelImageToUncertaintyMapFilter Self;
        typedef ImageToImageFilter<TImageType,TImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;
        itkNewMacro(Self);

        // definition of variable type abbreviations
        typedef typename TImageType::PixelType PixelType;
        typedef SeedPoint<TImageType> SeedPointType;
        typedef RegionProps RegionPropsType;
        typedef typename TImageType::RegionType InputImageRegionType;
        typedef typename TImageType::RegionType OutputImageRegionType;

        /**
         * The default constructor.
         */
        LabelImageToUncertaintyMapFilter();

        /**
         * The destructor.
         */
        virtual ~LabelImageToUncertaintyMapFilter();

		itkGetMacro( UncertaintyFeature, unsigned int);
        itkSetMacro( UncertaintyFeature, unsigned int);
		itkGetMacro( LabelOffset, int);
        itkSetMacro( LabelOffset, int);
		itkGetMacro( IntensityScale, float);
        itkSetMacro( IntensityScale, float);
		
        /**
         * Set and get methods for the member variables.
         */
        void SetInputMetaFilter( MetaDataFilter *metaFilter ) { m_InputMetaFilter = metaFilter; }
        void SetOutputMetaFilter( MetaDataFilter *metaFilter ) { m_OutputMetaFilter = metaFilter; }

    protected:

        /**
         * Functions for data generation.
         * @param outputRegionForThread the region to be processed.
         * @param threadId the id of the current thread.
         */
        void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);

        /**
         * functions for prepareing and post-processing the threaded generation.
         */
        void BeforeThreadedGenerateData();
        void AfterThreadedGenerateData();

        float m_MinimumRegionSigma;						// minimum standard deviation of the intensity values of an extracted region

        float m_MinimumWeightedGradientNormalDotProduct;		// specifies the minimum of the weighted dot product and gradient normal
        float m_WeightingKernelSizeMultiplicator;							// specifies the radius of the core of the weighting kernel
        float m_WeightingKernelStdDev;							// standard deviation of the gaussian of the weighting kernel
        float m_GradientImageStdDev;							// the standard deviation for gaussian blurring before gradient calculation

        bool m_WriteRegionProps;								// if not checked region props are not stored
        bool m_Segment3D;								// (NOT IMPLEMENTED YET) for 2D processing of the algorithm
        bool m_FuseSeedPoints;							// indicates if nearby seed points should be merged
        bool m_LabelOutput;								// if set to true, the result image contains unique labels for each segment, otherwise the original intensity is used
        bool m_RandomLabels;							// if set to true, random intensities are used for each label, otherwise succesive integers are used
        float m_SeedPointMinDistance;					// the minimum distance of seedpoints. close seed points will be merged.
		float m_IntensityScale;							// scales the input image intensity with the provided factor
        int   m_NumKeyPoints;							// number of seed points that should be processed
        std::vector<SeedPointType>* m_SeedPoints;		// the actual location info of the seeds
        std::vector<int>* m_SeedPointCombinations;		// the actual location info of the seeds
        std::vector<RegionPropsType>* m_RegionProps;	// the extracted region properties

		int m_LabelOffset;								// offset to convert between C-index and Matlab index labels
		unsigned int m_UncertaintyFeature;
        MetaDataFilter *m_InputMetaFilter;              // key points input meta data
        MetaDataFilter *m_OutputMetaFilter;             // region props output
		QHash<unsigned int, float> m_LabelToUncertaintyHash;
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkLabelImageToUncertaintyMapFilter.hxx"
#endif

#endif
