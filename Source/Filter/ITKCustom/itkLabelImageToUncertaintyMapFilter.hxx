/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef __XPIWIT_LabelImageToUncertaintyMapFilter_HXX
#define __XPIWIT_LabelImageToUncertaintyMapFilter_HXX

// include required headers
#include "itkLabelImageToUncertaintyMapFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkGradientImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
LabelImageToUncertaintyMapFilter<TImageType>::LabelImageToUncertaintyMapFilter()
{
    m_InputMetaFilter = NULL;
    m_OutputMetaFilter = NULL;
	m_UncertaintyFeature = 0;
	m_IntensityScale = 1.0f;
	m_LabelOffset = 0;
}


// the destructor
template <class TImageType>
LabelImageToUncertaintyMapFilter<TImageType>::~LabelImageToUncertaintyMapFilter()
{
}


// before threaded generate data
template <class TImageType>
void LabelImageToUncertaintyMapFilter<TImageType>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    output->FillBuffer( 0 );

	// convert the meta data into a hash table for faster access
	const unsigned int numObjects = m_InputMetaFilter->mData.length();
	m_LabelToUncertaintyHash.reserve( numObjects );
	for (unsigned int i=0; i<numObjects; ++i)
		m_LabelToUncertaintyHash.insert(m_InputMetaFilter->mData[i].at(0), m_InputMetaFilter->mData[i].at(m_UncertaintyFeature));
}


// the thread generate data
template <class TImageType>
void LabelImageToUncertaintyMapFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	// initialize the image iterators
	typename itk::ImageRegionConstIterator<TImageType> inputIterator(input, outputRegionForThread);
	typename itk::ImageRegionIterator<TImageType> outputIterator(output, outputRegionForThread);
	inputIterator.GoToBegin();
	outputIterator.GoToBegin();

	// iterate over the thread region and set the uncertainty value as new intensity
	while(!inputIterator.IsAtEnd())
	{
		unsigned int currentValue = (unsigned int)(0.5f+m_IntensityScale*inputIterator.Value()+m_LabelOffset);
		if (currentValue > 0)
			outputIterator.Set(m_LabelToUncertaintyHash[currentValue]);

		++inputIterator;
		++outputIterator;
	}
}


// after threaded generate data
template <class TImageType>
void LabelImageToUncertaintyMapFilter<TImageType>::AfterThreadedGenerateData()
{
}

} // end namespace itk

#endif
