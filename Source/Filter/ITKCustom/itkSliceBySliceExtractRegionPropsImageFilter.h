/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifndef __XPIWITCORE_SLICEBYSLICEEXTRACTREGIONPROPSIMAGEFILTER_H
#define __XPIWITCORE_SLICEBYSLICEEXTRACTREGIONPROPSIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"

#include "itkExtractImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include <string>

using namespace XPIWIT;

namespace itk {

struct QPairRegionPropsComparer
{
	template < typename T1, typename T2 >
	bool operator() (const QPair< T1, T2 >& a, const QPair< T1, T2 >& b)
	{
		return a.second < b.second;
	}
};


template <class TImageType, class TOutputImage>
class ITK_EXPORT SliceBySliceExtractRegionPropsImageFilter : public ImageToImageFilter<TImageType, TOutputImage>
{
	public:
		/** Extract dimension from input and output image. */
		itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);

		typedef SliceBySliceExtractRegionPropsImageFilter Self;
		typedef ImageToImageFilter<TImageType,TOutputImage> Superclass;
		typedef SmartPointer<Self> Pointer;
		typedef SmartPointer<const Self> ConstPointer;

		itkNewMacro(Self);

		itkGetMacro( DebugOutput, bool);
        itkSetMacro( DebugOutput, bool);
		itkSetMacro( CalculateOrientedBoundingBox, bool);
		itkGetMacro( CalculateOrientedBoundingBox, bool);
		itkSetMacro( CalculateOrientedIntensityRegions, bool);
		itkGetMacro( CalculateOrientedIntensityRegions, bool);
		itkSetMacro( CalculateOrientedLabelRegions, bool);
		itkGetMacro( CalculateOrientedLabelRegions, bool);
		itkSetMacro( CalculatePixelIndices, bool);
		itkGetMacro( CalculatePixelIndices, bool);
		itkSetMacro( BinaryInput, bool);
		itkGetMacro( BinaryInput, bool);
		itkSetMacro( FullyConnected, bool);
		itkGetMacro( FullyConnected, bool);
        itkSetMacro( InputForegroundValue, float);
        itkGetMacro( InputForegroundValue, float);
		itkSetMacro( OutputBackgroundValue, float);
        itkGetMacro( OutputBackgroundValue, float);
		itkSetMacro( MinSlice, unsigned int);
        itkGetMacro( MinSlice, unsigned int);
		itkSetMacro( MaxSlice, unsigned int);
        itkGetMacro( MaxSlice, unsigned int);
		itkSetMacro( MinimumSeedArea, unsigned int);
        itkGetMacro( MinimumSeedArea, unsigned int);
		itkSetMacro( MaximumVolume, int);
        itkGetMacro( MaximumVolume, int);

		//itkSetMacro( IntensityImage, TImageType::Pointer );
        //itkGetMacro( IntensityImage, TImageType::Pointer );

		void SetInputMetaFilter( MetaDataFilter *metaFilter ) { m_InputMetaFilter = metaFilter; }
        void SetOutputMetaFilter( MetaDataFilter *metaFilter ) { m_OutputMetaFilter = metaFilter; }

		void SetIntensityImage(const TImageType* image)        { m_IntensityImage = image; }
		void SetSeedImage(const TImageType* image)               { m_SeedImage = image; }
		
		typedef TImageType InputImageType;
		typedef typename TImageType::PixelType PixelType;

		typedef typename TImageType::RegionType InputImageRegionType;
		typedef typename TOutputImage::RegionType OutputImageRegionType;
		
		// typedef abbreviations for some filters
		typedef typename itk::Image< unsigned int, TImageType::ImageDimension >  LabelType;
        typedef typename itk::BinaryImageToLabelMapFilter<LabelType> BinaryImageToLabelMapFilterType;
        typedef typename itk::LabelMapToLabelImageFilter<LabelMap< LabelObject< SizeValueType, TImageType::ImageDimension > >, LabelType> LabelMapToLabelImageFilterType;
        typedef typename itk::LabelGeometryImageFilter<LabelType, TImageType> LabelGeometryImageFilterType;

	protected:
		SliceBySliceExtractRegionPropsImageFilter();
		virtual ~SliceBySliceExtractRegionPropsImageFilter();
		
		/**
		 * Functions for data generation.
		 */
		void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);
		//unsigned int SplitRequestedRegion(unsigned int i, unsigned int num, OutputImageRegionType& splitRegion);

		/** 
		 * functions for prepareing and post-processing the threaded generation.
		 */
		void BeforeThreadedGenerateData();
		void AfterThreadedGenerateData();

		bool m_DebugOutput;
		bool m_CalculateOrientedBoundingBox;
		bool m_CalculateOrientedIntensityRegions;
		bool m_CalculateOrientedLabelRegions;
		bool m_CalculatePixelIndices;
		bool m_BinaryInput;
		bool m_FullyConnected;
		float m_InputForegroundValue;
		float m_OutputBackgroundValue;
		unsigned int m_MinSlice;
		unsigned int m_MaxSlice;
		unsigned int m_MinimumSeedArea;
		int m_MaximumVolume;

		unsigned int m_NumSlices;
		QList<unsigned int> m_NumSegments;
		QList< QList< QPair< QList<float>, float > > > m_SegmentFeatures;

		const TImageType* m_IntensityImage;
		const TImageType* m_SeedImage;

		MetaDataFilter *m_InputMetaFilter;              // key points input meta data
        MetaDataFilter *m_OutputMetaFilter;             // region props output
};

} /* namespace itk */

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSliceBySliceExtractRegionPropsImageFilter.hxx"
#endif

#endif
