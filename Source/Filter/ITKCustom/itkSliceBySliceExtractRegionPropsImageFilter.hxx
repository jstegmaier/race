/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// include required headers
#include "itkSliceBySliceExtractRegionPropsImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"
#include "../Base/MetaData/MetaDataFilter.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
SliceBySliceExtractRegionPropsImageFilter<TImageType, TOutputImage>::SliceBySliceExtractRegionPropsImageFilter()
{
	m_MinSlice = 0;
	m_MaxSlice = 100000;
	m_CalculateOrientedBoundingBox = false;
	m_CalculateOrientedIntensityRegions = false;
	m_CalculateOrientedLabelRegions = false;
	m_CalculatePixelIndices = false;
	m_BinaryInput = false;
	m_FullyConnected = false;
	m_InputForegroundValue = 1;
	m_OutputBackgroundValue = 0;
	m_DebugOutput = false;
	m_NumSlices = 0;
	m_IntensityImage = NULL;
	m_SeedImage = NULL;
	m_MinimumSeedArea = 0;
	m_MaximumVolume = -1;
}


// the destructor
template <class TImageType, class TOutputImage>
SliceBySliceExtractRegionPropsImageFilter<TImageType, TOutputImage>::~SliceBySliceExtractRegionPropsImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceExtractRegionPropsImageFilter<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	m_NumSlices = input->GetLargestPossibleRegion().GetSize(TImageType::ImageDimension-1);

	for (int i=0; i<m_NumSlices; ++i)
	{
		m_NumSegments.append(0);
		m_SegmentFeatures.append(QList<QPair<QList<float>, float > >() );
	}
}


// the thread generate data
template <class TImageType, class TOutputImage>
void SliceBySliceExtractRegionPropsImageFilter<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
	
	// initialize the regions to process
	typename TImageType::RegionType currentRegion;
	typename TImageType::IndexType currentIndex;
	typename TImageType::SizeType currentSize;

	for (int j=0; j<TImageType::ImageDimension; ++j)
	{
		currentIndex[j] = outputRegionForThread.GetIndex(j);
		currentSize[j] = outputRegionForThread.GetSize(j);
	}
	currentSize[2] = 1;
	currentRegion.SetIndex( currentIndex );
	currentRegion.SetSize( currentSize );
	
	// create a temporary image used to extract slices
	typename LabelType::Pointer tmpImage = LabelType::New();
	typename LabelType::Pointer tmpLabelImage = LabelType::New();
	tmpImage->SetRegions(currentRegion);
	tmpImage->SetSpacing( input->GetSpacing() );
	tmpImage->Allocate();
	tmpImage->FillBuffer(0);

	// create a temporary intensity image to extract slices of the original image
	typename TImageType::Pointer tmpIntensityImage = NULL;
	typename TImageType::Pointer tmpSeedImage = NULL;
	
	if (m_IntensityImage != NULL)
	{
		tmpIntensityImage = TImageType::New();
		tmpIntensityImage->SetRegions(currentRegion);
		tmpIntensityImage->SetSpacing( input->GetSpacing() );
		tmpIntensityImage->Allocate();
		tmpIntensityImage->FillBuffer(0);
	}

	// iterate over all slices and extract the region props separately for each slice
	for (int i=0; i<outputRegionForThread.GetSize(2); ++i)
	{
		// set the current index
		const unsigned int currentSlice = outputRegionForThread.GetIndex(2)+i;
		currentIndex[2] = currentSlice;
		currentRegion.SetIndex( currentIndex );
		currentRegion.SetSize( currentSize );

		// only process the valid range of slices
		if (currentSlice < m_MinSlice || currentSlice > m_MaxSlice)
			continue;

		// extract the slice to perform the watershed on
		ImageRegionConstIterator<TImageType> inputIterator( input, currentRegion );
		ImageRegionIterator<LabelType> sliceIterator( tmpImage, tmpImage->GetLargestPossibleRegion() );
		ImageRegionConstIterator<TImageType> intensityInputIterator( m_IntensityImage, currentRegion );
		ImageRegionConstIterator<TImageType> seedInputIterator( m_SeedImage, currentRegion );
		ImageRegionIterator<TImageType> intensitySliceIterator( tmpIntensityImage, tmpIntensityImage->GetLargestPossibleRegion() );
		ImageRegionIterator<TImageType> outputIterator( output, currentRegion );
		
		// reset the iterators
		inputIterator.GoToBegin();
		sliceIterator.GoToBegin();
		outputIterator.GoToBegin();
		intensityInputIterator.GoToBegin();
		intensitySliceIterator.GoToBegin();
		seedInputIterator.GoToBegin();

		float intensityMultiplier = 1.0;
		if( typeid( typename TOutputImage::PixelType ) == typeid( float ) ||
			typeid( typename TOutputImage::PixelType ) == typeid( double ) ) {
			intensityMultiplier = 1.0;
		}

		// initialize a hash map for the seeds
		unsigned int currentLabel = 0;
		unsigned int currentSeed = 0;
		QHash< unsigned int, QHash<unsigned int, unsigned int> > seedHashMap;

		// initialize the histogram
		while(inputIterator.IsAtEnd() == false)
		{
			// copy the input value
			currentLabel = intensityMultiplier * inputIterator.Value();
			currentSeed = intensityMultiplier * seedInputIterator.Value();
			
			// set the slice values for intensity and label slice
			sliceIterator.Set( currentLabel );
			intensitySliceIterator.Set( intensityInputIterator.Value() );
			outputIterator.Set( inputIterator.Value() );
					
			// update the seed hash map
			if (seedHashMap.contains(currentLabel))
			{	
				if (seedHashMap[currentLabel].contains(currentSeed))
					seedHashMap[currentLabel][currentSeed] = seedHashMap[currentLabel][currentSeed]+1;
				else
					seedHashMap[currentLabel][currentSeed] = 1;
			}
			else
			{
				seedHashMap[currentLabel] = QHash<unsigned int, unsigned int>();
				seedHashMap[currentLabel][currentSeed] = 1;
			}

			// increment the iterators
			++inputIterator;
			++sliceIterator;
			++outputIterator;
			++intensityInputIterator;
			++intensitySliceIterator;
			++seedInputIterator;
		}

		// initialize the geometry extraction filter
		typename LabelGeometryImageFilterType::Pointer labelGeometryFilter =  LabelGeometryImageFilterType::New();
		labelGeometryFilter->SetCalculateOrientedBoundingBox( m_CalculateOrientedBoundingBox );
		labelGeometryFilter->SetCalculateOrientedIntensityRegions( m_CalculateOrientedIntensityRegions );
		labelGeometryFilter->SetCalculateOrientedLabelRegions( m_CalculateOrientedLabelRegions );
		labelGeometryFilter->SetCalculatePixelIndices( m_CalculatePixelIndices );
		labelGeometryFilter->SetReleaseDataFlag( true );

		if (m_BinaryInput == true)
		{
			// convert the binary image into a label map
			typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
			binaryImageToLabelMapFilter->SetFullyConnected( m_FullyConnected );
			binaryImageToLabelMapFilter->SetInputForegroundValue( m_InputForegroundValue );
			binaryImageToLabelMapFilter->SetOutputBackgroundValue( m_OutputBackgroundValue );
			binaryImageToLabelMapFilter->SetInput( tmpImage );
			binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
			itkTryCatch( binaryImageToLabelMapFilter->Update(), "SliceBySliceExtractRegionProps --> BinaryImageToLabelMapFilter" );

			// convert the label map into a label image
			typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
			labelMapToLabelImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
			labelMapToLabelImageFilter->SetReleaseDataFlag( true );
			itkTryCatch( labelMapToLabelImageFilter->Update(), "SliceBySliceExtractRegionProps --> LabelMapToLabelImage" );

			// set the extraction filter input
			tmpLabelImage = labelMapToLabelImageFilter->GetOutput();
			labelGeometryFilter->SetInput( labelMapToLabelImageFilter->GetOutput() );			
		}
		else
		{
			// directly get the information from the prelabeled input
			tmpLabelImage = tmpImage;
			labelGeometryFilter->SetInput( tmpImage );
		}

		// update the geometry extraction filter
		tmpLabelImage->SetReleaseDataFlag( false );
		if (m_IntensityImage != NULL)
			labelGeometryFilter->SetIntensityInput( tmpIntensityImage );
		itkTryCatch( labelGeometryFilter->Update(), "SliceBySliceExtractRegionPropsFilter" );

		if (m_DebugOutput == true)
			Logger::GetInstance()->WriteLine( "- SliceBySliceExtractRegionPropsFilter: Found " + QString::number(labelGeometryFilter->GetNumberOfLabels()) + " connected regions in the binary image." );

		///////////////// REMOVE TOO LARGE REGIONS ///////////////////
		if (m_MaximumVolume > 0)
		{
			if (m_DebugOutput == true)
				Logger::GetInstance()->WriteLine( "- SliceBySliceExtractRegionPropsFilter: Removing connected regions in the binary image that are larger than " + QString::number(m_MaximumVolume) + "." );

			ImageRegionConstIterator<LabelType> labelImageIterator( tmpLabelImage, tmpLabelImage->GetLargestPossibleRegion() );
			outputIterator.GoToBegin();
			labelImageIterator.GoToBegin();

			while (outputIterator.IsAtEnd() == false)
			{
				if (outputIterator.Value() > 0 && labelGeometryFilter->GetVolume(labelImageIterator.Value()) >= m_MaximumVolume)
				{
					outputIterator.Set( 0 );

					if (m_DebugOutput == true)
						Logger::GetInstance()->WriteLine( "Settings label " + QString::number(labelImageIterator.Value()) + " to zero as the volume is " + QString::number(labelGeometryFilter->GetVolume(labelImageIterator.Value())) + "." );
				}

				++outputIterator;
				++labelImageIterator;
			}
		}

		//////////////// END REMOVE TOO LARGE REGIONS ///////////////


		// get the labels
		typename LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryFilter->GetLabels();
		typename LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;

		// initialize the slice result array
		m_NumSegments[currentSlice] = labelGeometryFilter->GetNumberOfLabels();

		unsigned int currentKeyPointID = 0;
		for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
		{
			typename LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;

			//if (currentSlice == 102)
			//	Logger::GetInstance()->WriteLine( "- Here I am " + QString::number(labelValue) + " connected regions in the binary image." );

			QList<float> currentSegment;
			currentSegment << labelValue; // id
			currentSegment << labelGeometryFilter->GetVolume(labelValue);					// size
			currentSegment << labelGeometryFilter->GetCentroid(labelValue)[0];				// xpos			// centroid
			currentSegment << labelGeometryFilter->GetCentroid(labelValue)[1];				// ypos
			if( TImageType::ImageDimension >= 3 )
				currentSegment << currentSlice;			// zpos
		
			// bounding box size
			currentSegment << labelGeometryFilter->GetBoundingBoxSize(labelValue)[0];			
			currentSegment << labelGeometryFilter->GetBoundingBoxSize(labelValue)[1];
			if( TImageType::ImageDimension >= 3 )
				currentSegment << labelGeometryFilter->GetBoundingBoxSize(labelValue)[2];

			currentSegment << labelGeometryFilter->GetIntegratedIntensity(labelValue);		// integrated intensity
			currentSegment << labelGeometryFilter->GetMajorAxisLength(labelValue);			// major axis length
			currentSegment << labelGeometryFilter->GetMinorAxisLength(labelValue);			// minor axis length
			currentSegment << labelGeometryFilter->GetEccentricity(labelValue);				// eccentricity
			currentSegment << labelGeometryFilter->GetElongation(labelValue);				// elongation
			currentSegment << labelGeometryFilter->GetOrientation(labelValue);				// orientation

			// determine the maximum seed label
			unsigned int maxKeyValue = 0;
			unsigned int maxSeedValue = 0;
			if (seedHashMap.contains(labelValue))
			{
				QHash<unsigned int, unsigned int>::const_iterator seedIterator = seedHashMap.value(labelValue).constBegin();
				while (seedIterator != seedHashMap.value(labelValue).constEnd())
				{
					if (seedIterator.value() > maxSeedValue && seedIterator.key() > 0)
					{
						maxSeedValue = seedIterator.value();
						maxKeyValue = seedIterator.key();
					}

					++seedIterator;
				}
			}

			if (maxSeedValue < m_MinimumSeedArea)
				maxKeyValue = 0;

			currentSegment << maxKeyValue;		  														// the label
			currentSegment << 0;
			//currentSegment << std::max<float>((float)labelGeometryFilter->GetBoundingBoxSize(labelValue)[0], (float)labelGeometryFilter->GetBoundingBoxSize(labelValue)[1]) / 
			//				  std::min<float>((float)labelGeometryFilter->GetBoundingBoxSize(labelValue)[0], (float)labelGeometryFilter->GetBoundingBoxSize(labelValue)[1]);

			currentSegment << ((float)labelGeometryFilter->GetBoundingBoxSize(labelValue)[0] / (float)labelGeometryFilter->GetBoundingBoxSize(labelValue)[1]);

			m_SegmentFeatures[currentSlice].append( QPair<QList<float>, float>(currentSegment, labelValue) );

			++currentKeyPointID;
		}
		
		// perform the sorting of all regions such that the labels are in order
		qStableSort(m_SegmentFeatures[currentSlice].begin(), m_SegmentFeatures[currentSlice].end(), QPairRegionPropsComparer());
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceExtractRegionPropsImageFilter<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
	// setup the meta description
	QStringList metaDescription;                    
	QStringList metaType;
    metaDescription << "id";						metaType << "int";
	metaDescription << "volume";					metaType << "int";
	metaDescription << "xpos";						metaType << "int";	// centroid
	metaDescription << "ypos";						metaType << "int";
	if( TImageType::ImageDimension >= 3 )
		metaDescription << "zpos";					metaType << "int";

	metaDescription << "xsize";						metaType << "int";	// bounding box size
	metaDescription << "ysize";						metaType << "int";
	if( TImageType::ImageDimension >= 3 )
		metaDescription << "zsize";					metaType << "int";

	metaDescription << "meanIntensity";				metaType << "float";
	metaDescription << "majorAxisLength";			metaType << "float";
	metaDescription << "minorAxisLength";			metaType << "float";
	metaDescription << "eccentricity";				metaType << "float";
	metaDescription << "elongation";				metaType << "float";
	metaDescription << "orientation";				metaType << "float";
	metaDescription << "seedLabel";					metaType << "int";
	metaDescription << "jiLabel";					metaType << "int";
	metaDescription << "sizeRatio";					metaType << "float";

	// write to meta object
	m_OutputMetaFilter->mTitle = metaDescription;
	m_OutputMetaFilter->mType = metaType;

	for (int i=0; i<m_NumSlices; ++i)
	{
		for (int j=0; j<m_NumSegments[i]; ++j)
		{
			m_OutputMetaFilter->mData.append(m_SegmentFeatures[i][j].first);
		}
	}
}

} // end namespace itk
