/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// include required headers
#include "itkSliceBySliceWatershedImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
SliceBySliceWatershedImageFilter<TImageType, TOutputImage>::SliceBySliceWatershedImageFilter()
{
	m_Level = 0.0;
	m_MarkWatershedLine = 1;
	m_FullyConnected = 0;
	m_MinSlice = 0;
	m_MaxSlice = 10000;
	m_DebugOutput = false;
}


// the destructor
template <class TImageType, class TOutputImage>
SliceBySliceWatershedImageFilter<TImageType, TOutputImage>::~SliceBySliceWatershedImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceWatershedImageFilter<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
}


// the thread generate data
template <class TImageType, class TOutputImage>
void SliceBySliceWatershedImageFilter<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	// initialize the regions to process
	typename TImageType::RegionType currentRegion;
	typename TImageType::IndexType currentIndex;
	typename TImageType::SizeType currentSize;

	for (int j=0; j<TImageType::ImageDimension; ++j)
	{
		currentIndex[j] = outputRegionForThread.GetIndex(j);
		currentSize[j] = outputRegionForThread.GetSize(j);
	}
	currentSize[2] = 1;
	currentRegion.SetIndex( currentIndex );
	currentRegion.SetSize( currentSize );

	// create a temporary image used to extract slices
	typename TImageType::Pointer tmpImage = TImageType::New();
	tmpImage->SetRegions(currentRegion);
	tmpImage->SetSpacing( input->GetSpacing() );
	tmpImage->Allocate();
	tmpImage->FillBuffer(0);

	if (m_DebugOutput == true)
		Logger::GetInstance()->WriteLine( "- Processing " + QString::number(outputRegionForThread.GetSize(2)) + " slices using thread " + QString::number(threadId) );

	// iterate over all slices and perform watershed separately for each slice
	for (int i=0; i<outputRegionForThread.GetSize(2); ++i)
	{
		// set the current index
		currentIndex[2] = outputRegionForThread.GetIndex(2)+i;
		currentRegion.SetIndex( currentIndex );
		currentRegion.SetSize( currentSize );

		// only process the valid range of slices
		if (currentIndex[2] < m_MinSlice || currentIndex[2] > m_MaxSlice)
			continue;
		
		// extract the slice to perform the watershed on
		ImageRegionConstIterator<TImageType> inputIterator( input, currentRegion );
		ImageRegionIterator<TImageType> sliceIterator( tmpImage, tmpImage->GetLargestPossibleRegion() );
		inputIterator.GoToBegin();
		sliceIterator.GoToBegin();
		
		while(inputIterator.IsAtEnd() == false)
		{
			sliceIterator.Set( inputIterator.Value() );

			++inputIterator;
			++sliceIterator;
		}

		// set parameters and input image
		typedef itk::MorphologicalWatershedImageFilter<TImageType, TOutputImage> WatershedFilterType;
		typename WatershedFilterType::Pointer watershedFilter = WatershedFilterType::New();
		watershedFilter->SetLevel( m_Level );
		watershedFilter->SetMarkWatershedLine( m_MarkWatershedLine );
		watershedFilter->SetFullyConnected( m_FullyConnected );
		watershedFilter->SetInput( tmpImage );
		itkTryCatch( watershedFilter->Update(), "Exception Caught: Updating watershed filter on a slice." );

		// initialize the seed region iterator
		ImageRegionIterator<TOutputImage> watershedIterator( watershedFilter->GetOutput(), tmpImage->GetLargestPossibleRegion() );
		ImageRegionIterator<TOutputImage> outputIterator( output, currentRegion );

		watershedIterator.GoToBegin();
		outputIterator.GoToBegin();

		float maxValue = 0;

		// iterate trough the seed region and set voxels for the initial LSF
		while (outputIterator.IsAtEnd() == false)
		{
			outputIterator.Set( watershedIterator.Value() );

			if (watershedIterator.Value() > maxValue)
				maxValue = watershedIterator.Value();

			++outputIterator;
			++watershedIterator;
		}

		if (m_DebugOutput == true)
			Logger::GetInstance()->WriteLine( "- MaxValue in Slice " + QString::number(currentIndex[2]) + ": " + QString::number(maxValue) );
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceWatershedImageFilter<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
