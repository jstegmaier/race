/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "../../../Core/Utilities/Logger.h"
#include "ACMEWatershedSegmentationFilterWidget.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"


// itk header
#include "itkRGBPixel.h"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkInvertIntensityImageFilter.h"
#include "itkMorphologicalWatershedImageFilter.h"
#include "itkLabelOverlayImageFilter.h"
#include "itkImageFileWriter.h"
#include "itkImageRegionIterator.h"
#include "itkShapeRelabelImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "../../ThirdParty/ACME/WatershedImageFilter/itkGradientWeightedDistanceImageFilter.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
ACMEWatershedSegmentationFilterWidget<TInputImage>::ACMEWatershedSegmentationFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ACMEWatershedSegmentationFilterWidget<TInputImage>::GetName();
    this->mDescription = "ACMEWatershedSegmentationFilter.";
    this->mDescription += "Extracts connected components using watershed and distance map transforms. Input should be a preprocessed intensity image and a tensor voting image (See Mosaliganti et al. 2012).";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Threshold", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The threshold to get rid of background noise." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
ACMEWatershedSegmentationFilterWidget<TInputImage>::~ACMEWatershedSegmentationFilterWidget()
{
}


// the update function
template <class TInputImage>
void ACMEWatershedSegmentationFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    const float threshold = processObjectSettings->GetSettingValue( "Threshold" ).toDouble();

	const int Dimension = TInputImage::ImageDimension;
	typedef itk::Image< unsigned char, Dimension >   FeatureImageType;
	typedef itk::Image< double, Dimension >          InputImageType;
	typedef itk::Image< int, Dimension >             SegmentImageType;
	typedef itk::ImageFileReader< FeatureImageType > FeatureReaderType;
	typedef itk::ImageFileReader< InputImageType >   InputReaderType;
	typedef itk::ImageFileReader< SegmentImageType >   SegmentReaderType;
	typedef itk::RescaleIntensityImageFilter< TInputImage, FeatureImageType > RescaleIntensityFloatToUCharType;
	typedef itk::RescaleIntensityImageFilter< SegmentImageType, TInputImage > RescaleIntensityIntToFloatType;
	typedef itk::BinaryThresholdImageFilter< InputImageType, SegmentImageType > ThresholdType;
	typedef itk::GradientWeightedDistanceImageFilter< FeatureImageType, InputImageType, SegmentImageType > DistanceFilterType;
	typedef itk::InvertIntensityImageFilter< InputImageType, InputImageType > RInvertType;
	typedef itk::MorphologicalWatershedImageFilter< InputImageType, SegmentImageType > WatershedFilterType;
	typedef itk::ShapeRelabelImageFilter< SegmentImageType > RelabelFilterType;
	typedef itk::ImageRegionIterator< FeatureImageType > FeatureIteratorType;
	typedef itk::ImageRegionIterator< SegmentImageType > SegmentIteratorType;
	
	double m_Alpha = 1.0;
	double m_ThresholdMin = threshold;


	typename TInputImage::Pointer inputImage0 = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer inputImage1 = mInputImages.at(1)->template GetImage<TInputImage>();
		
	// convert the raw intensity image to uchar
	typename RescaleIntensityFloatToUCharType::Pointer rescaleIntensityImage = RescaleIntensityFloatToUCharType::New();
	rescaleIntensityImage->SetInput( inputImage0 );		// this->mInput0
	rescaleIntensityImage->SetOutputMinimum( 0 );
	rescaleIntensityImage->SetOutputMaximum( 255 );
	itkTryCatch( rescaleIntensityImage->Update(), "Exception Caught: Converting intensity image from float to uchar." );

	// convert the Tensor saliency image to uchar
	typename RescaleIntensityFloatToUCharType::Pointer rescaleTensorVotingImage = RescaleIntensityFloatToUCharType::New();
	rescaleTensorVotingImage->SetInput( inputImage1 );	// this->mInput1
	rescaleTensorVotingImage->SetOutputMinimum( 0 );
	rescaleTensorVotingImage->SetOutputMaximum( 255 );
	itkTryCatch( rescaleTensorVotingImage->Update(), "Exception Caught: Converting tensor voting image from float to uchar." );
	
	typename SegmentImageType::Pointer m_ForegroundImg = SegmentImageType::New();
	m_ForegroundImg->CopyInformation( rescaleTensorVotingImage->GetOutput() );
	m_ForegroundImg->SetRegions( rescaleTensorVotingImage->GetOutput()->GetLargestPossibleRegion() );
	m_ForegroundImg->Allocate();
	m_ForegroundImg->FillBuffer( 0 );
	
	SegmentIteratorType fIt ( m_ForegroundImg, m_ForegroundImg->GetLargestPossibleRegion() );
	FeatureIteratorType sIt ( rescaleTensorVotingImage->GetOutput(), rescaleTensorVotingImage->GetOutput()->GetLargestPossibleRegion() );
	
	fIt.GoToBegin();
	sIt.GoToBegin();
	while ( !fIt.IsAtEnd() )
	{
		if ( sIt.Get() > m_ThresholdMin )
		{
			fIt.Set ( 1 );
		}
		else
		{
			fIt.Set ( 0 );
		}

		++sIt;
		++fIt;
	}
	
	typename DistanceFilterType::Pointer distFilter = DistanceFilterType::New();
	distFilter->SetInput ( rescaleIntensityImage->GetOutput() );
	distFilter->SetUseLevelSet( false );
	distFilter->SetLargestCellRadius( 8.0 );
	distFilter->SetForeground ( m_ForegroundImg );
	distFilter->SetNucleiSigma ( 0.4 );
	distFilter->SetAlpha( m_Alpha );
	itkTryCatch( distFilter->Update(), "Exception Caught: distance filter." );
	std::cout << "Computed distance map" << std::endl;
	
	typename RInvertType::Pointer idistance = RInvertType::New();
	idistance->SetInput( distFilter->GetOutput() );
	idistance->SetMaximum( 10.0 );
	itkTryCatch( idistance->Update(), "Exception Caught: distance map calculation." );
	std::cout << "Inverted distance map" << std::endl;
		
	typename WatershedFilterType::Pointer wshed = WatershedFilterType::New();
	wshed->SetInput( idistance->GetOutput() );
	wshed->SetMarkWatershedLine( false );
	wshed->SetLevel( 1.0 );
	wshed->FullyConnectedOn();
	wshed->SetNumberOfThreads( 12 );
	itkTryCatch( wshed->Update(), "Exception Caught: Watershed segmentation." );
	typename SegmentImageType::Pointer output = wshed->GetOutput();
	output->DisconnectPipeline();
	std::cout << "Computed watershed segmentation" << std::endl;
	
	typename RelabelFilterType::Pointer relabel = RelabelFilterType::New();
	relabel->SetInput ( output );
	relabel->SetBackgroundValue ( 0 );
	relabel->SetReverseOrdering ( false );
	itkTryCatch( relabel->Update(), "Exception Caught: relabeling the watershed segmentation result." );

	typename RescaleIntensityIntToFloatType::Pointer rescaleSegmentationImage = RescaleIntensityIntToFloatType::New();
	rescaleSegmentationImage->SetInput( relabel->GetOutput() );
	rescaleSegmentationImage->SetOutputMinimum(0.0f);
	rescaleSegmentationImage->SetOutputMaximum(1.0f);
	itkTryCatch( rescaleSegmentationImage->Update(), "Exception Caught: Rescale intensity of segmentation label image." );

	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( rescaleSegmentationImage->GetOutput() );
	mOutputImages.append( outputImage );
	
	// update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ACMEWatershedSegmentationFilterWidget<Image3Float> > ACMEWatershedSegmentationFilterWidgetImage3Float;
static ProcessObjectProxy< ACMEWatershedSegmentationFilterWidget<Image3UShort> > ACMEWatershedSegmentationFilterWidgetImage3UShort;

} // namespace XPIWIT
