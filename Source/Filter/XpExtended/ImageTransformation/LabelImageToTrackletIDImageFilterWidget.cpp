/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "LabelImageToTrackletIDImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkLabelImageToUncertaintyMapFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
LabelImageToTrackletIDImageFilterWidget<TInputImage>::LabelImageToTrackletIDImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = LabelImageToTrackletIDImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Converts the provided label image to a tracklet id image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("RegionProps");
    this->mObjectType->SetNumberMetaOutputs(0);
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "TrackletIDIndex", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The tracklet id index used for the label conversion." );
	processObjectSettings->AddSetting( "LabelOffset", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The offset added to the image labels, if csv labels are shifted (e.g. C -> Matlab convention)." );
	processObjectSettings->AddSetting( "IntensityScale", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Intensity scale: If set, the input intensities are multiplied by this factor." );



    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
LabelImageToTrackletIDImageFilterWidget<TInputImage>::~LabelImageToTrackletIDImageFilterWidget()
{
}


// the update function
template< class TInputImage >
void LabelImageToTrackletIDImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	MetaDataFilter* regionProps = this->mMetaInputs.at(0);

    // get filter parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int trackletIdIndex = processObjectSettings->GetSettingValue( "TrackletIDIndex" ).toInt();
	const int labelOffset = processObjectSettings->GetSettingValue( "LabelOffset" ).toInt();
	const float intensityScale = processObjectSettings->GetSettingValue( "IntensityScale" ).toFloat();
	
    // setup the filters
    typedef itk::LabelImageToUncertaintyMapFilter<TInputImage> LabelImageToUncertaintyMapFilterType;
    typename LabelImageToUncertaintyMapFilterType::Pointer labelToUncertaintyMapFilter = LabelImageToUncertaintyMapFilterType::New();
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
    // initialize level sets segmentation
    labelToUncertaintyMapFilter->SetInput( inputImage );
    labelToUncertaintyMapFilter->SetNumberOfThreads( maxThreads );
    labelToUncertaintyMapFilter->SetInputMetaFilter( regionProps );
	labelToUncertaintyMapFilter->SetIntensityScale( intensityScale );
	labelToUncertaintyMapFilter->SetLabelOffset( labelOffset );
	labelToUncertaintyMapFilter->SetUncertaintyFeature( trackletIdIndex );
	labelToUncertaintyMapFilter->SetReleaseDataFlag( false );

    // perform and set the output
    itkTryCatch( labelToUncertaintyMapFilter->Update(), "Label converison filter Called in LabelImageToTrackletIDImageFilterWidget." );
    
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( labelToUncertaintyMapFilter->GetOutput() );
	outputImage->SetRescaleFlag( false );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LabelImageToTrackletIDImageFilterWidget<Image2Float> > LabelImageToTrackletIDImageFilterWidgetImage2Float;
static ProcessObjectProxy< LabelImageToTrackletIDImageFilterWidget<Image3Float> > LabelImageToTrackletIDImageFilterWidgetImage3Float;
static ProcessObjectProxy< LabelImageToTrackletIDImageFilterWidget<Image2UShort> > LabelImageToTrackletIDImageFilterWidgetImage2UShort;
static ProcessObjectProxy< LabelImageToTrackletIDImageFilterWidget<Image3UShort> > LabelImageToTrackletIDImageFilterWidgetImage3UShort;


} // namespace XPIWIT
