/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "FastMorphologicalOperatorsFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkFastMorphologicalOperatorsImageFilter.h"
#include "itkBinaryBallStructuringElement.h"

// system header
#include <float.h>

// XPIWIT namespace
namespace XPIWIT {

// the default constructor
template< class TInputImage >
FastMorphologicalOperatorsFilterWidget<TInputImage>::FastMorphologicalOperatorsFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = FastMorphologicalOperatorsFilterWidget<TInputImage>::GetName();
    this->mDescription = "Morphological operators with masking and without slow border handling.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);

    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);

    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the filter kernel." );
	processObjectSettings->AddSetting( "MinRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Minimum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
	processObjectSettings->AddSetting( "MaxRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
    processObjectSettings->AddSetting( "FilterMask3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D kernel." );
	processObjectSettings->AddSetting( "UseRadiusRange", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Iteratively apply different radii to close the image (Used e.g. for viscous watershed)." );
	processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the image spacing will be used to scale the radii in the different dimensions." );
	processObjectSettings->AddSetting( "IgnoreBorderRegions", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, border regions will be skipped, otherwise the Neumann boundary condition (closest valid pixel value) is used." );
	processObjectSettings->AddSetting( "Type", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The operation to use: EROSION = 0, DILATION = 1, CLOSING = 2, OPENING = 3, ASF OPENING->CLOSING: 4, ASF CLOSING->OPENING: 5." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
FastMorphologicalOperatorsFilterWidget<TInputImage>::~FastMorphologicalOperatorsFilterWidget()
{
}


// update the filter
template< class TInputImage >
void FastMorphologicalOperatorsFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const int filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool useRadiusRange = processObjectSettings->GetSettingValue( "UseRadiusRange" ).toInt() > 0;
	const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
	const bool ignoreBorderRegions = processObjectSettings->GetSettingValue( "IgnoreBorderRegions" ).toInt() > 0;
	float minRadius = processObjectSettings->GetSettingValue( "MinRadius" ).toFloat();
	float maxRadius = processObjectSettings->GetSettingValue( "MaxRadius" ).toFloat();
	const unsigned int type = processObjectSettings->GetSettingValue( "Type" ).toInt();
	
	// get images
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer maskImage = mInputImages.at(1)->template GetImage<TInputImage>();

	// if single radius should be used, set min and max accordingly
	if (useRadiusRange == false)
	{
		minRadius = radius;
		maxRadius = radius;
	}

	// typedefs for the filter and the structure element
    typedef itk::BinaryBallStructuringElement< typename TInputImage::PixelType, TInputImage::ImageDimension > StructuringElementType;
	typedef itk::FastMorphologicalOperatorsImageFilter<TInputImage, TInputImage, StructuringElementType> MorphologicalImageFilterType;
	
	typename TInputImage::SpacingType spacing = inputImage->GetSpacing();
	for (int j=minRadius; j<=maxRadius; ++j)
	{
		// create the ball structuring element
		typename TInputImage::SizeType radius;
		radius[0] = j;
		radius[1] = j;
		radius[2] = j;

		if (useImageSpacing == true)
		{
			radius[0] = int(0.5 + (float)radius[0] / spacing[0]);
			radius[1] = int(0.5 + (float)radius[1] / spacing[1]);
			radius[2] = int(0.5 + (float)radius[2] / spacing[2]);
		}
		
		if (filterMask3D == false)
			radius[2] = 0;

		StructuringElementType structuringElement;
		structuringElement.SetRadius( radius );
		structuringElement.CreateStructuringElement();

		// create closing filter
		typename MorphologicalImageFilterType::Pointer morphologicalErosionFilter = MorphologicalImageFilterType::New();
		morphologicalErosionFilter->SetReleaseDataFlag( true );
		morphologicalErosionFilter->SetKernel( structuringElement );
		morphologicalErosionFilter->SetMaskImage( maskImage );
		morphologicalErosionFilter->SetNumberOfThreads( maxThreads );
		morphologicalErosionFilter->SetIgnoreBorderRegions( ignoreBorderRegions );
		morphologicalErosionFilter->SetType( 0 );

		typename MorphologicalImageFilterType::Pointer morphologicalErosionFilter2 = MorphologicalImageFilterType::New();
		morphologicalErosionFilter2->SetReleaseDataFlag( true );
		morphologicalErosionFilter2->SetKernel( structuringElement );
		morphologicalErosionFilter2->SetMaskImage( maskImage );
		morphologicalErosionFilter2->SetNumberOfThreads( maxThreads );
		morphologicalErosionFilter2->SetIgnoreBorderRegions( ignoreBorderRegions );
		morphologicalErosionFilter2->SetType( 0 );

		typename MorphologicalImageFilterType::Pointer morphologicalDilationFilter = MorphologicalImageFilterType::New();
		morphologicalDilationFilter->SetReleaseDataFlag( true );
		morphologicalDilationFilter->SetKernel( structuringElement );
		morphologicalDilationFilter->SetMaskImage( maskImage );
		morphologicalDilationFilter->SetNumberOfThreads( maxThreads );
		morphologicalDilationFilter->SetIgnoreBorderRegions( ignoreBorderRegions );
		morphologicalDilationFilter->SetType( 1 );

		typename MorphologicalImageFilterType::Pointer morphologicalDilationFilter2 = MorphologicalImageFilterType::New();
		morphologicalDilationFilter2->SetReleaseDataFlag( true );
		morphologicalDilationFilter2->SetKernel( structuringElement );
		morphologicalDilationFilter2->SetMaskImage( maskImage );
		morphologicalDilationFilter2->SetNumberOfThreads( maxThreads );
		morphologicalDilationFilter2->SetIgnoreBorderRegions( ignoreBorderRegions );
		morphologicalDilationFilter2->SetType( 1 );

		if (type == 0)
		{
			morphologicalErosionFilter->SetInput( inputImage );
			itkTryCatch( morphologicalErosionFilter->Update(), "Exception Caught: Updating the morphological erosion operator." );
			inputImage = morphologicalErosionFilter->GetOutput();
		}
		else if (type == 1)
		{
			morphologicalDilationFilter->SetInput( inputImage );
			itkTryCatch( morphologicalDilationFilter->Update(), "Exception Caught: Updating the morphological dilation operator." );
			inputImage = morphologicalDilationFilter->GetOutput();
		}
		else if (type == 2)
		{
			morphologicalDilationFilter->SetInput( inputImage );
			morphologicalErosionFilter->SetInput( morphologicalDilationFilter->GetOutput() );
			itkTryCatch( morphologicalErosionFilter->Update(), "Exception Caught: Updating the morphological closing operator." );
			inputImage = morphologicalErosionFilter->GetOutput();
		}
		else if (type == 3)
		{
			morphologicalErosionFilter->SetInput( inputImage );
			morphologicalDilationFilter->SetInput( morphologicalErosionFilter->GetOutput() );
			itkTryCatch( morphologicalDilationFilter->Update(), "Exception Caught: Updating the morphological opening operator." );
			inputImage = morphologicalDilationFilter->GetOutput();
		}
		else if (type == 4)
		{
			morphologicalErosionFilter->SetInput( inputImage );
			morphologicalDilationFilter->SetInput( morphologicalErosionFilter->GetOutput() );
			morphologicalDilationFilter2->SetInput( morphologicalDilationFilter->GetOutput() );
			morphologicalErosionFilter2->SetInput( morphologicalDilationFilter2->GetOutput() );
			itkTryCatch( morphologicalErosionFilter2->Update(), "Exception Caught: Updating the ASF with morphological opening followed by closing operator." );
			inputImage = morphologicalErosionFilter2->GetOutput();
		}
		else if (type == 5)
		{
			morphologicalDilationFilter->SetInput( inputImage );
			morphologicalErosionFilter->SetInput( morphologicalDilationFilter->GetOutput() );
			morphologicalErosionFilter2->SetInput( morphologicalErosionFilter->GetOutput() );
			morphologicalDilationFilter2->SetInput( morphologicalErosionFilter2->GetOutput() );
			itkTryCatch( morphologicalDilationFilter2->Update(), "Exception Caught: Updating the ASF with morphological closing followed by opening operator." );
			inputImage = morphologicalDilationFilter2->GetOutput();
		}
	}
	
	inputImage->SetReleaseDataFlag( false );

    // set the output image
    ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( inputImage );
    mOutputImages.append( outputWrapper );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< FastMorphologicalOperatorsFilterWidget<Image2Float> > FastMorphologicalOperatorsFilterWidgetImage2Float;
static ProcessObjectProxy< FastMorphologicalOperatorsFilterWidget<Image3Float> > FastMorphologicalOperatorsFilterWidgetImage3Float;
static ProcessObjectProxy< FastMorphologicalOperatorsFilterWidget<Image2UShort> > FastMorphologicalOperatorsFilterWidgetImage2UShort;
static ProcessObjectProxy< FastMorphologicalOperatorsFilterWidget<Image3UShort> > FastMorphologicalOperatorsFilterWidgetImage3UShort;

} // namespace XPIWIT
