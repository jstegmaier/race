/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifdef XPIWIT_USE_GPU

// project header
#include "GPUFastMorphologicalOperatorsFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

#include "../../CUDA/morphologicalOperators/morpohologicalOperators.h"

// itk header
#include "itkBinaryBallStructuringElement.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"

// system header
#include <float.h>
#include <vector>
#include <fstream>

// XPIWIT namespace
namespace XPIWIT {

// the default constructor
template< class TInputImage >
GPUFastMorphologicalOperatorsFilterWidget<TInputImage>::GPUFastMorphologicalOperatorsFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = GPUFastMorphologicalOperatorsFilterWidget<TInputImage>::GetName();
    this->mDescription = "Morphological operators with masking and without slow border handling.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the filter kernel." );
	processObjectSettings->AddSetting( "MinRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Minimum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
	processObjectSettings->AddSetting( "MaxRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
    processObjectSettings->AddSetting( "FilterMask3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D kernel." );
	processObjectSettings->AddSetting( "UseRadiusRange", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Iteratively apply different radii to close the image (Used e.g. for viscous watershed)." );
	processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the image spacing will be used to scale the radii in the different dimensions." );
	processObjectSettings->AddSetting( "IgnoreBorderRegions", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, border regions will be skipped, otherwise the Neumann boundary condition (closest valid pixel value) is used." );
	processObjectSettings->AddSetting( "Type", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The operation to use: EROSION = 0, DILATION = 1, CLOSING = 2, OPENING = 3, ASF OPENING->CLOSING: 4, ASF CLOSING->OPENING: 5." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
GPUFastMorphologicalOperatorsFilterWidget<TInputImage>::~GPUFastMorphologicalOperatorsFilterWidget()
{
}


// update the filter
template< class TInputImage >
void GPUFastMorphologicalOperatorsFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const int filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool useRadiusRange = processObjectSettings->GetSettingValue( "UseRadiusRange" ).toInt() > 0;
	const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
	const bool ignoreBorderRegions = processObjectSettings->GetSettingValue( "IgnoreBorderRegions" ).toInt() > 0;
	float minRadius = processObjectSettings->GetSettingValue( "MinRadius" ).toFloat();
	float maxRadius = processObjectSettings->GetSettingValue( "MaxRadius" ).toFloat();
	const unsigned int type = processObjectSettings->GetSettingValue( "Type" ).toInt();
	
	// get images
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer maskImage = mInputImages.at(1)->template GetImage<TInputImage>();
	
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	// if single radius should be used, set min and max accordingly
	if (useRadiusRange == false)
	{
		minRadius = radius;
		maxRadius = radius;
	}

	// typedefs for the filter and the structure element
	typedef itk::Image< unsigned short, TInputImage::ImageDimension > GPUInputImageType;
	typedef itk::IntensityWindowingImageFilter< TInputImage, GPUInputImageType > RescaleIntensityInputFilterType;
    typedef itk::BinaryBallStructuringElement< typename TInputImage::PixelType, TInputImage::ImageDimension > StructuringElementType;
	//typedef itk::FastMorphologicalOperatorsImageFilter<TInputImage, TInputImage, StructuringElementType> MorphologicalImageFilterType;
	
	typename TInputImage::SpacingType spacing = inputImage->GetSpacing();
		

	// rescale the intensity range to 0-65535
	typename RescaleIntensityInputFilterType::Pointer rescaleInputImage = RescaleIntensityInputFilterType::New();
	rescaleInputImage->SetInput( inputImage );
	rescaleInputImage->SetWindowMinimum( 0.0 );
	rescaleInputImage->SetWindowMaximum( 1.0 );
	rescaleInputImage->SetOutputMinimum( 0 );
	rescaleInputImage->SetOutputMaximum( 65535 );
	itkTryCatch( rescaleInputImage->Update(), "Exception Caught: Rescale intensity input image for watershed segmentation." );

	//read image dimensions
	std::int64_t imgDims[dimsImage], imSize = 1;	
	TInputImage::RegionType region = inputImage->GetLargestPossibleRegion();
	TInputImage::SizeType imSizeVec = region.GetSize();
	for(int ii = 0; ii < TInputImage::ImageDimension; ii++)
	{
		imgDims[ii] = imSizeVec[ii];
		imSize *= imSizeVec[ii];
	}

	//get pointer to image data and mask within ITK class
	GPUInputImageType::PixelType *img = rescaleInputImage->GetOutput()->GetBufferPointer();
	TInputImage::PixelType *mask = maskImage->GetBufferPointer();

	//declare GPU morphoogical operator class
	int devCUDA = 0;//TODO amatf: add this as a parameter in the XML file


	//set everything outside the mask to zero
	for(std::int64_t ii = 0; ii < imSize; ii++)
	{
		if( mask[ii] == 0 )
			img[ii] = 0;
	}

	//TODO amatf: get pixel type from GPUInputImageType
	morpohologicaloperators_CUDA<GPUInputImageType::PixelType> wPBC(img, imgDims, devCUDA);

	//upload foreground piuxelIdx to GPU		
	wPBC.initializeForegroundWithMask<TInputImage::PixelType>(mask, true);
	
	std::vector<int> structuralOffsets, structuralOffsetsCoord;
	structuralOffsets.reserve(maxRadius * maxRadius * maxRadius);
	structuralOffsetsCoord.reserve(maxRadius * maxRadius * maxRadius * 3);
	for (int j=minRadius; j<=maxRadius; ++j)
	{
		// create the ball structuring element
		typename TInputImage::SizeType radius;
		radius[0] = j;
		radius[1] = j;
		radius[2] = j;

		if (useImageSpacing == true)
		{
			radius[0] = int(0.5 + (float)radius[0] / spacing[0]);
			radius[1] = int(0.5 + (float)radius[1] / spacing[1]);
			radius[2] = int(0.5 + (float)radius[2] / spacing[2]);
		}
		
		if (filterMask3D == false)
			radius[2] = 0;

		StructuringElementType structuringElement;
		structuringElement.SetRadius( radius );
		structuringElement.CreateStructuringElement();

		
		//parse itk structuring element to our own convention for GPU
		structuralOffsets.clear();
		structuralOffsetsCoord.clear();
		int aux, cumOffset, countIter = 0;
		for(StructuringElementType::Iterator iter = structuringElement.Begin(); iter != structuringElement.End(); ++iter, countIter++)
		{
			if( (*iter) != 0 )//active neighboring pixel
			{
				StructuringElementType::OffsetType offset = structuringElement.GetOffset(countIter);				
				aux = 0;
				cumOffset = 1;
				for(int ii = 0; ii < TInputImage::ImageDimension; ii++)
				{
					aux += offset[ii] * cumOffset;
					cumOffset *= imgDims[ii];
					structuralOffsetsCoord.push_back(offset[ii]);
				}
				if( TInputImage::ImageDimension < 3 )
					structuralOffsetsCoord.push_back(0);

				structuralOffsets.push_back(aux);
			}
		}


		//upload structural element
		wPBC.updateStructuralKernel(structuralOffsets);		

		if (type == 0)
		{
			wPBC.morphologicalErosion(structuralOffsetsCoord);
		}
		else if (type == 1)
		{
			wPBC.morphologicalDilation(structuralOffsetsCoord);
		}
		else if (type == 2)
		{
			wPBC.morphologicalDilation(structuralOffsetsCoord);
			wPBC.morphologicalErosion(structuralOffsetsCoord);			
		}
		else if (type == 3)
		{
			wPBC.morphologicalErosion(structuralOffsetsCoord);			
			wPBC.morphologicalDilation(structuralOffsetsCoord);			
		}
		else if (type == 4)
		{
			wPBC.morphologicalErosion(structuralOffsetsCoord);			
			wPBC.morphologicalDilation(structuralOffsetsCoord);			
			wPBC.morphologicalDilation(structuralOffsetsCoord);
			wPBC.morphologicalErosion(structuralOffsetsCoord);			
		}
		else if (type == 5)
		{
			wPBC.morphologicalDilation(structuralOffsetsCoord);
			wPBC.morphologicalErosion(structuralOffsetsCoord);			
			wPBC.morphologicalErosion(structuralOffsetsCoord);			
			wPBC.morphologicalDilation(structuralOffsetsCoord);						
		}
	}
	
	//recover final result from GPU (goes to rescaleInputImage->GetOutput()->GetBufferPointer() )
	wPBC.getImageFromGPU(NULL);

	//TODO:amatf set to zero borders of the image that cannot be calculated appropriately( this is the flag:ignore border regions ) (add this to the GPU code)

	//rescale image back
	// rescale the watershed result to the internal format
	typedef itk::IntensityWindowingImageFilter< GPUInputImageType, TInputImage > RescaleIntensityFilterType;
	typename RescaleIntensityFilterType::Pointer rescaleIntensityFilter = RescaleIntensityFilterType::New();
	rescaleIntensityFilter->SetInput( rescaleInputImage->GetOutput() );
	rescaleIntensityFilter->SetWindowMinimum( 0 );
	rescaleIntensityFilter->SetWindowMaximum( 65535 );
	rescaleIntensityFilter->SetOutputMinimum( 0.0 );
	rescaleIntensityFilter->SetOutputMaximum( 1.0 );
	rescaleIntensityFilter->SetNumberOfThreads( maxThreads );
	itkTryCatch( rescaleIntensityFilter->Update(), "Exception Caught: Rescale watershed intensity to [0, 1] range." );

	///////////////////// PLACE YOUR CODE HERE /////////////////////////////

    // set the output image
    ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( rescaleIntensityFilter->GetOutput() );
    mOutputImages.append( outputWrapper );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< GPUFastMorphologicalOperatorsFilterWidget<Image2Float> > GPUFastMorphologicalOperatorsFilterWidgetImage2Float;
static ProcessObjectProxy< GPUFastMorphologicalOperatorsFilterWidget<Image3Float> > GPUFastMorphologicalOperatorsFilterWidgetImage3Float;
static ProcessObjectProxy< GPUFastMorphologicalOperatorsFilterWidget<Image2UShort> > GPUFastMorphologicalOperatorsFilterWidgetImage2UShort;
static ProcessObjectProxy< GPUFastMorphologicalOperatorsFilterWidget<Image3UShort> > GPUFastMorphologicalOperatorsFilterWidgetImage3UShort;

} // namespace XPIWIT

#endif