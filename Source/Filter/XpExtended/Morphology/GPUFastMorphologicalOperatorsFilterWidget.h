/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifdef XPIWIT_USE_GPU

#ifndef GPUFASTMORPHOLOGICALOPERATORSFILTERWIDGET_H
#define GPUFASTMORPHOLOGICALOPERATORSFILTERWIDGET_H

// project header
#include "../../Base/Management/ProcessObjectBase.h"
#include "../../Base/Management/ProcessObjectProxy.h"


namespace XPIWIT
{

/**
 *	@class GPUFastMorphologicalOperatorsFilterWidget
 *	The base class for image to image filter widgets.
 */
template< class TInputImage >
class GPUFastMorphologicalOperatorsFilterWidget : public ProcessObjectBase
{
    public:

        /**
         * The constructor.
         * @param parent The parent of the main window. Usually should be NULL.
         */
        GPUFastMorphologicalOperatorsFilterWidget();


        /**
         * The destructor.
         */
        virtual ~GPUFastMorphologicalOperatorsFilterWidget();


        /**
         * Overload basic operations for the process object.
         */
        void Update();

		/**
		 * Static functions used for the filter factory.
		 */
		static QString GetName() { return "GPUFastMorphologicalOperatorsFilter"; }
		static QString GetType() { return (typeid(float) == typeid(typename TInputImage::PixelType)) ? "float" : "ushort"; }
		static int GetDimension() { return TInputImage::ImageDimension; }
    private:
};

} // namespace XPIWIT

//#include "GPUFastMorphologicalOperatorsFilterWidget.txx"

#endif

#endif