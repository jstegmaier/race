/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "MorphologicalWatershedFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkSliceBySliceWatershedImageFilter.h"

// itk header
#include "MorphologicalWatershedFilterWidget.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkImageRegionIterator.h"
#include "itkMorphologicalWatershedImageFilter.h"
#include "itkPasteImageFilter.h"
#include "itkExtractImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
MorphologicalWatershedFilterWidget<TInputImage>::MorphologicalWatershedFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = MorphologicalWatershedFilterWidget<TInputImage>::GetName();
    this->mDescription = "Morphological Watershed Filter. ";
    this->mDescription += "Performs watershed segmentation of the input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
    this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Level", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Initial level of the watershed." );
	processObjectSettings->AddSetting( "MarkWatershedLine", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, watershed lines are highlighted by zero values." );
    processObjectSettings->AddSetting( "Segment3D", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D watershed segmentation." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled 8-neighborhood (2D) or 27-neighborhood (3D) is used." );
	processObjectSettings->AddSetting( "DebugOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled some debug output is printed." );
	processObjectSettings->AddSetting( "MinSlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices larger than this number are processed." );
	processObjectSettings->AddSetting( "MaxSlice", "100000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices smaller than this number are processed." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
MorphologicalWatershedFilterWidget<TInputImage>::~MorphologicalWatershedFilterWidget()
{
}


// update the filter
template< class TInputImage>
void MorphologicalWatershedFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const bool segment3D = processObjectSettings->GetSettingValue( "Segment3D" ).toInt() > 0;
	const bool markWatershedLine = processObjectSettings->GetSettingValue( "MarkWatershedLine" ).toInt() > 0;
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const bool debugOutput = processObjectSettings->GetSettingValue( "DebugOutput" ).toInt() > 0;
	const float level = processObjectSettings->GetSettingValue( "Level" ).toFloat();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int minSlice = processObjectSettings->GetSettingValue( "MinSlice" ).toInt();
	const int maxSlice = processObjectSettings->GetSettingValue( "MaxSlice" ).toInt();

	// pointer to the input image
	typename TInputImage::Pointer input = mInputImages.at(0)->template GetImage<TInputImage>();

	// typedefs for the watershed
	typedef itk::Image< unsigned short, TInputImage::ImageDimension > WatershedInputImageType;
	typedef itk::Image< int, TInputImage::ImageDimension > SegmentationImageType;
	typedef itk::IntensityWindowingImageFilter< TInputImage, WatershedInputImageType > RescaleIntensityWatershedInputFilterType;
	typedef itk::MorphologicalWatershedImageFilter< WatershedInputImageType, SegmentationImageType > WatershedFilterType;
	typedef itk::SliceBySliceWatershedImageFilter<WatershedInputImageType, SegmentationImageType> SliceBySliceWatershedImageFilterType;

	// rescale the intensity range to 0-255
	typename RescaleIntensityWatershedInputFilterType::Pointer rescaleInputImage = RescaleIntensityWatershedInputFilterType::New();
	rescaleInputImage->SetInput( input );
	rescaleInputImage->SetWindowMinimum( 0.0 );
	rescaleInputImage->SetWindowMaximum( 1.0 );
	rescaleInputImage->SetOutputMinimum( 0 );
	rescaleInputImage->SetOutputMaximum( 65535 );
	itkTryCatch( rescaleInputImage->Update(), "Exception Caught: Rescale intensity input image for watershed segmentation." );

	unsigned short* mydata = rescaleInputImage->GetOutput()->GetBufferPointer();

	// temporary image to store the watershed results
	typename SegmentationImageType::Pointer tmpImage;

	// perform slice based segmentation
	if (segment3D == false && TInputImage::ImageDimension == 3)
	{
		// instantiate and setup the slice by slice watershed filter
		typename SliceBySliceWatershedImageFilterType::Pointer sliceBySliceWatershedFilter = SliceBySliceWatershedImageFilterType::New();
		sliceBySliceWatershedFilter->SetInput( rescaleInputImage->GetOutput() );
		sliceBySliceWatershedFilter->SetLevel( level );
		sliceBySliceWatershedFilter->SetMarkWatershedLine( markWatershedLine );
		sliceBySliceWatershedFilter->SetDebugOutput( debugOutput );
		sliceBySliceWatershedFilter->SetMinSlice( minSlice );
		sliceBySliceWatershedFilter->SetMaxSlice( maxSlice );
		sliceBySliceWatershedFilter->SetNumberOfThreads( maxThreads );
		sliceBySliceWatershedFilter->SetFullyConnected( fullyConnected );
		itkTryCatch( sliceBySliceWatershedFilter->Update(), "Exception Caught: Updating slice by slice watershed filter." );

		tmpImage = sliceBySliceWatershedFilter->GetOutput();
	}
	else
	{
		// set parameters and input image
		typename WatershedFilterType::Pointer watershedFilter = WatershedFilterType::New();
		watershedFilter->SetLevel( level );
		watershedFilter->SetMarkWatershedLine( markWatershedLine );
		watershedFilter->SetFullyConnected( fullyConnected );
		watershedFilter->SetNumberOfThreads( maxThreads );
		watershedFilter->SetInput( rescaleInputImage->GetOutput() );

		// update the 3D watershed algorithm
		itkTryCatch( watershedFilter->Update(), "Exception Caught: Updating the 3D morphological watershed filter." );
		tmpImage = watershedFilter->GetOutput();
	}

	/*
	// rescale the watershed result to the internal format
	typedef itk::IntensityWindowingImageFilter< SegmentationImageType, TInputImage > RescaleIntensityFilterType;
	typename RescaleIntensityFilterType::Pointer rescaleIntensityFilter = RescaleIntensityFilterType::New();
	rescaleIntensityFilter->SetInput( tmpImage );
	rescaleIntensityFilter->SetWindowMinimum( 0 );
	rescaleIntensityFilter->SetWindowMaximum( 65535 );
	if (markWatershedLine == true)
		rescaleIntensityFilter->SetOutputMinimum( 0.0 );
	else
		rescaleIntensityFilter->SetOutputMinimum( 1.0/65535.0 );

	rescaleIntensityFilter->SetOutputMaximum( 1.0 );
	rescaleIntensityFilter->SetNumberOfThreads( maxThreads );
	itkTryCatch( rescaleIntensityFilter->Update(), "Exception Caught: Rescale watershed intensity to [0, 1] range." );*/

    // set the output image
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetRescaleFlag( false );
    outputWrapper->SetImage<SegmentationImageType>( tmpImage ); // rescaleIntensityFilter->GetOutput()
    mOutputImages.append( outputWrapper );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MorphologicalWatershedFilterWidget<Image2Float> > MorphologicalWatershedFilterWidgetImage2Float;
static ProcessObjectProxy< MorphologicalWatershedFilterWidget<Image3Float> > MorphologicalWatershedFilterWidgetImage3Float;
static ProcessObjectProxy< MorphologicalWatershedFilterWidget<Image2UShort> > MorphologicalWatershedFilterWidgetImage2UShort;
static ProcessObjectProxy< MorphologicalWatershedFilterWidget<Image3UShort> > MorphologicalWatershedFilterWidgetImage3UShort;
    
} // namespace XPIWIT
