/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifdef XPIWIT_USE_NSCALE

// namespace header
#include "NScaleHMaximaFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkHMaximaImageFilter.h"

// qt header

// system header
#include <vector>

//other headers
#ifdef XPIWIT_USE_NSCALE
#include "opencv2/opencv.hpp"
#include "MorphologicOperations.h"
#endif

namespace XPIWIT {

	using namespace std;

template < class TImageType1 >
NScaleHMaximaFilterWidget< TImageType1 >::NScaleHMaximaFilterWidget() : ProcessObjectBase()
{
	this->mName = NScaleHMaximaFilterWidget<TImageType1>::GetName();
	this->mDescription = "Supresses local maxima for which the height is smaller than the specified baseline";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);


	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Height", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The height above which maxima should be searched." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Fully connected flag uses 8/26 neighborhood if enabled." );

	// initialize the widget
	ProcessObjectBase::Init();
}


template < class TImageType1 >
NScaleHMaximaFilterWidget< TImageType1 >::~NScaleHMaximaFilterWidget()
{
}


template < class TImageType1 >
void NScaleHMaximaFilterWidget< TImageType1 >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float height = processObjectSettings->GetSettingValue( "Height" ).toFloat();
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	
	// get images
	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	// --> Result image should be written into the output wrapper, i.e. replace "inputImage" in line 102 by the processed image


#ifdef XPIWIT_USE_NSCALE

	//read image dimensions
	typename TImageType1::RegionType region = inputImage->GetLargestPossibleRegion();
	typename TImageType1::SizeType imSize = region.GetSize();
	int64 Nim = 1;
	//int64 imgDims[dimsImage]; // changed this to 3 dimensions, as dimsImage is only defined if CUDA is additionally used
	int64 imgDims[3];
	for (int ii = 0; ii < TImageType1::ImageDimension; ii++)
	{
		imgDims[ii] = imSize[ii];
		Nim *= imSize[ii];
	}
	const int64 sliceSize = imgDims[0] * imgDims[1];

	int conn3D = 4;//default for 2D
	//get pointer to image data within ITK class
	typename TImageType1::PixelType *img = inputImage->GetBufferPointer();


	/*
	CVTypes
	#define CV_8U   0
	#define CV_8S   1
	#define CV_16U  2
	#define CV_16S  3
	#define CV_32S  4
	#define CV_32F  5
	#define CV_64F  6
	*/
	int CVtype = 0;
	switch (sizeof(typename TImageType1::PixelType))
	{
	case 1:
		CVtype = 0;
		break;
	case 2:
		CVtype = 2;
		break;

	case 4:
		CVtype = 5;
		break;
	}


	std::vector<Mat> outCV;
	//setup watershed parameters depending on input parameters
	if (TImageType1::ImageDimension == 2) //2D image
	{		
		if (fullyConnected == true)
			conn3D = 8;

		Mat image(imgDims[1], imgDims[0], CVtype, img);
		Mat marker = image.clone() - height;

		outCV.push_back(nscale::imreconstruct<typename TImageType1::PixelType>(marker, image, conn3D));
	}
	else{//3D image stack		
		
		
		if (fullyConnected == true)
			conn3D = 26;
		else
			conn3D = 6;

		std::vector<Mat> imageCV;
		int64 offset = 0;

		for (int64 ii = 0; ii < imgDims[2]; ii++)
		{
			Mat aux(imgDims[1], imgDims[0], CVtype, &(img[offset])); // does not copy	;
			imageCV.push_back(aux);
			offset += sliceSize;
		}
		 outCV = nscale::imhmax3D(imageCV, height, conn3D);
	}


	// temporary image to store the hmaxima results
	typename TImageType1::Pointer tmpImage;
	tmpImage = TImageType1::New();

	typename TImageType1::IndexType start;
	typename TImageType1::SizeType size;
	for (int ii = 0; ii < TImageType1::ImageDimension; ii++)
	{
		size[ii] = imgDims[ii];
		start[ii] = 0;
	}
	typename TImageType1::RegionType region2(start, size);
	tmpImage->SetRegions(region2);
	tmpImage->Allocate();
	tmpImage->SetSpacing(inputImage->GetSpacing());

	typename TImageType1::PixelType *L = tmpImage->GetBufferPointer();//get pointer to image data within ITK class

	//parse output back to XPIWIT format
	int64 offsetW = 0;
	for (auto m : outCV)
	{
		for (int i = 0; i < m.rows; ++i)
		{
			for (int j = 0; j < m.cols; ++j)
			{
				L[offsetW++] = m.at<typename TImageType1::PixelType>(i, j);
			}
		}
	}
#endif
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TImageType1>( tmpImage );
	outputImage->SetRescaleFlag( false );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< NScaleHMaximaFilterWidget<Image2Float> > NScaleHMaximaImageFilterWrapperImage2Float;
static ProcessObjectProxy< NScaleHMaximaFilterWidget<Image3Float> > NScaleHMaximaImageFilterWrapperImage3Float;
static ProcessObjectProxy< NScaleHMaximaFilterWidget<Image2UShort> > NScaleHMaximaImageFilterWrapperImage2UShort;
static ProcessObjectProxy< NScaleHMaximaFilterWidget<Image3UShort> > NScaleHMaximaImageFilterWrapperImage3UShort;

} // namespace XPIWIT

#endif