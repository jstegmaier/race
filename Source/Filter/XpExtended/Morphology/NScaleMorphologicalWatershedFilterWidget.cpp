/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifdef XPIWIT_USE_NSCALE

// project header
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "NScaleMorphologicalWatershedFilterWidget.h"
#include "itkIntensityWindowingImageFilter.h"

// qt header

// system header
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <thread>
#include <atomic>

//other headers
#ifdef XPIWIT_USE_NSCALE
#include "opencv2/opencv.hpp"
#include "MorphologicOperations.h"
#include "PixelOperations.h"
#include "NeighborOperations.h"
#endif

namespace XPIWIT {


//function to calculate multi-thread watershed
void watershedThread(unsigned short int* img, int32_t* L, int64_t maxSlice, const int64_t* imgDims, std::atomic<int64_t> *frameId, const float level, int conn3D)
{

	int64_t ff;
	int64_t sliceSize = imgDims[0] * imgDims[1];

	while (1)
	{
		ff = atomic_fetch_add(frameId, (int64_t)1);

		//check if we can access data or we cannot read longer
		if (ff >= maxSlice)
			break;

		unsigned short int *imgSlice = &(img[ff * sliceSize]);

		//from http://answers.opencv.org/question/8202/using-external-image-data-in-a-cvmat/
		//from http://stackoverflow.com/questions/22511747/how-do-i-pass-ownership-of-pixel-data-to-cvmat
		//Remember to free/delete the pointer external_mem after usage. The Mat wrapped does not release the memory for you when it is destructed
		//in OpenCV cols is the fastest running index (=imgDims[0])
		cv::Mat imgSliceCV(imgDims[1], imgDims[0], CV_16U, imgSlice); // does not copy		

		//call hminima filter
		cv::Mat aux = nscale::imhmin<::uint16_t>(imgSliceCV, level, conn3D);


		//call watershed
		//cv::Mat waterResult = nscale::watershed(aux, conn3D);//output is int32_t
		cv::Mat waterResult = nscale::watershedCC(aux, conn3D);//output is int32_t. Watershed version with connected components

		//copy results to output pointer: openCV data might not be contiguous
		//memcpy(&(L[ff*sliceSize]), waterResult.data, sizeof(int32_t) * sliceSize);
		int64_t offsetW = ff * sliceSize;
		int64_t countW = 0;
		for (int i = 0; i < waterResult.rows; ++i)
		{
			for (int j = 0; j < waterResult.cols; ++j)
			{
				L[offsetW + countW] = waterResult.at<int>(i, j);
				countW++;
			}
		}
	}
}


// the default constructor
template< class TInputImage >
NScaleMorphologicalWatershedFilterWidget<TInputImage>::NScaleMorphologicalWatershedFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = NScaleMorphologicalWatershedFilterWidget<TInputImage>::GetName();
    this->mDescription = "Morphological Watershed Filter. ";
    this->mDescription += "Performs watershed segmentation of the input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);

    this->mObjectType->SetNumberImageInputs(1);
    this->mObjectType->AppendImageInputType(1);

	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Level", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Initial level of the watershed." );
	processObjectSettings->AddSetting( "MarkWatershedLine", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, watershed lines are highlighted by zero values." );
    processObjectSettings->AddSetting( "Segment3D", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D watershed segmentation." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled 8-neighborhood (2D) or 27-neighborhood (3D) is used." );
	processObjectSettings->AddSetting( "DebugOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled some debug output is printed." );
	processObjectSettings->AddSetting( "MinSlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices larger than this number are processed." );
	processObjectSettings->AddSetting( "MaxSlice", "100000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices smaller than this number are processed." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TImageType1 >
NScaleMorphologicalWatershedFilterWidget<TImageType1>::~NScaleMorphologicalWatershedFilterWidget()
{
}


// update the filter
template< class TImageType1>
void NScaleMorphologicalWatershedFilterWidget<TImageType1>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const bool segment3D = processObjectSettings->GetSettingValue( "Segment3D" ).toInt() > 0;
	const bool markWatershedLine = processObjectSettings->GetSettingValue( "MarkWatershedLine" ).toInt() > 0;
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const bool debugOutput = processObjectSettings->GetSettingValue( "DebugOutput" ).toInt() > 0;
	const float level = processObjectSettings->GetSettingValue( "Level" ).toFloat();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	int64_t minSlice = processObjectSettings->GetSettingValue( "MinSlice" ).toInt();
	int64_t maxSlice = processObjectSettings->GetSettingValue( "MaxSlice" ).toInt();

	// pointer to the input image	
	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();


#ifdef XPIWIT_USE_NSCALE
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	// --> Result image should be written into the output wrapper, i.e. replace "inputImage" in line 126 by the processed image
	// typedefs for the watershed
	typedef typename itk::Image< unsigned short, TImageType1::ImageDimension > WatershedInputImageType;
	typedef typename itk::Image< int32_t, TImageType1::ImageDimension > SegmentationImageType;
	typedef typename itk::IntensityWindowingImageFilter< TImageType1, WatershedInputImageType > RescaleIntensityWatershedInputFilterType;
	//typedef itk::MorphologicalWatershedImageFilter< WatershedInputImageType, SegmentationImageType > WatershedFilterType;
	//typedef itk::SliceBySliceWatershedImageFilter<WatershedInputImageType, SegmentationImageType> SliceBySliceWatershedImageFilterType;


	//read image dimensions
	typename TImageType1::RegionType region = inputImage->GetLargestPossibleRegion();
	typename TImageType1::SizeType imSize = region.GetSize();
	int64_t Nim = 1;
	//int64 imgDims[dimsImage]; // changed this to 3 dimensions, as dimsImage is only defined if CUDA is additionally used
	int64 imgDims[3];
	for (int ii = 0; ii < TImageType1::ImageDimension; ii++)
	{
		imgDims[ii] = imSize[ii];
		Nim *= imSize[ii];
	}

	// rescale the intensity range to 0-65535
	typename RescaleIntensityWatershedInputFilterType::Pointer rescaleInputImage = RescaleIntensityWatershedInputFilterType::New();
	rescaleInputImage->SetInput(inputImage);
	rescaleInputImage->SetWindowMinimum(0.0);
	rescaleInputImage->SetWindowMaximum(1.0);
	rescaleInputImage->SetOutputMinimum(0);
	rescaleInputImage->SetOutputMaximum(65535);
	itkTryCatch(rescaleInputImage->Update(), "Exception Caught: Rescale intensity input image for watershed segmentation.");

	int conn3D = 4;//default for 2D
	//get pointer to image data within ITK class
	unsigned short int *img = rescaleInputImage->GetOutput()->GetBufferPointer();

	//setup watershed parameters depending on input parameters
	if (TImageType1::ImageDimension == 2) //2D image
	{
		imgDims[2] = 1;//our watershed code treats everything as 3D
		if (fullyConnected == true)
			conn3D = 8;

	}
	else{//3D image stack		
		if (segment3D == false)//perform slie by slice
		{
			if (fullyConnected == true)
				conn3D = 8;
			else
				conn3D = 4;
		}
		else{//full 3D watershed
			if (fullyConnected == true)
				conn3D = 26;
			else
				conn3D = 6;
		}
	}

	//restrict slices that are only background
	const int64_t sliceSize = imgDims[0] * imgDims[1];
	int64_t imIdx = minSlice * sliceSize;
	unsigned short int bb = img[imIdx];
	while (img[imIdx] == bb)
		imIdx++;
	minSlice = floor(float(imIdx) / float(sliceSize));

	imIdx = std::min(Nim, maxSlice * sliceSize) - 1;
	bb = img[imIdx];
	while (img[imIdx] == bb)
		imIdx--;
	maxSlice = imgDims[2] - floor(float(Nim - 1 - imIdx) / float(sliceSize));	


	// temporary image to store the watershed results
	typename SegmentationImageType::Pointer tmpImage;
	tmpImage = SegmentationImageType::New();

	typename SegmentationImageType::IndexType start;
	typename SegmentationImageType::SizeType size;
	for (int ii = 0; ii < TImageType1::ImageDimension; ii++)
	{
		size[ii] = imgDims[ii];
		start[ii] = 0;
	}
	typename SegmentationImageType::RegionType region2(start, size);
	tmpImage->SetRegions(region2);
	tmpImage->Allocate();
	tmpImage->SetSpacing(inputImage->GetSpacing());

	int32_t *L = tmpImage->GetBufferPointer();//get pointer to image data within ITK class
	
	std::atomic<int64_t> frameId;//counter shared all workers;
	atomic_store(&frameId, minSlice);

	
	// start the working threads
	std::vector<std::thread> threads;	
	for (int i = 0; i < maxThreads; ++i)
	{
		threads.push_back( std::thread(watershedThread, img, L, maxSlice, imgDims, &frameId, level, conn3D) );
	}
	//wait for the workers to finish
	for (auto& t : threads)
	{
		t.join();
	}
	

	// rescale the watershed result to the internal format
	typedef itk::IntensityWindowingImageFilter< SegmentationImageType, TImageType1 > RescaleIntensityFilterType;
	typename RescaleIntensityFilterType::Pointer rescaleIntensityFilter = RescaleIntensityFilterType::New();
	rescaleIntensityFilter->SetInput(tmpImage);
	rescaleIntensityFilter->SetWindowMinimum(0);
	rescaleIntensityFilter->SetWindowMaximum(65535);//we could have more than 65535 labels
	//rescaleIntensityFilter->SetOutputMinimum(0.0);
	//rescaleIntensityFilter->SetOutputMaximum(1.0);
	// Changed 09.04.2015 as NScale watershed does not seem to work with the current pipeline
	rescaleIntensityFilter->SetOutputMinimum(0);
	rescaleIntensityFilter->SetOutputMaximum(65535);
	rescaleIntensityFilter->SetNumberOfThreads(maxThreads);
	itkTryCatch(rescaleIntensityFilter->Update(), "Exception Caught: Rescale watershed intensity to [0, 1] range.");

	///////////////////// PLACE YOUR CODE HERE /////////////////////////////

#endif
    // set the output image
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TImageType1>(rescaleIntensityFilter->GetOutput());
	outputImage->SetRescaleFlag( false );
	mOutputImages.append( outputImage );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< NScaleMorphologicalWatershedFilterWidget<Image2Float> > NScaleMorphologicalWatershedFilterWidgetImage2Float;
static ProcessObjectProxy< NScaleMorphologicalWatershedFilterWidget<Image3Float> > NScaleMorphologicalWatershedFilterWidgetImage3Float;
static ProcessObjectProxy< NScaleMorphologicalWatershedFilterWidget<Image2UShort> > NScaleMorphologicalWatershedFilterWidgetImage2UShort;
static ProcessObjectProxy< NScaleMorphologicalWatershedFilterWidget<Image3UShort> > NScaleMorphologicalWatershedFilterWidgetImage3UShort;

} // namespace XPIWIT

#endif