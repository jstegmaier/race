/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "LabelImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkThresholdImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
LabelImageFilterWidget<TInputImage>::LabelImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = LabelImageFilterWidget<TInputImage>::GetName();
    this->mDescription = "Label disjoint regions of a binary image with a unique id.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
LabelImageFilterWidget<TInputImage>::~LabelImageFilterWidget()
{
}


// the update function
template< class TInputImage >
void LabelImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool useOtsu = processObjectSettings->GetSettingValue( "UseOtsu" ).toInt() > 0;
    const float lowerThreshold = processObjectSettings->GetSettingValue( "LowerThreshold" ).toDouble();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    bool inPlace = processObjectSettings->GetSettingValue( "InPlace" ).toInt() > 0;

    // define internal integer data type
    typedef typename itk::Image<PixelUShort, TInputImage::ImageDimension> InternalImageType;
	typename InternalImageType::Pointer inputImage = mInputImages.at(0)->template GetImage<InternalImageType>();
    

    typedef typename itk::BinaryImageToLabelMapFilter<InternalImageType> BinaryImageToLabelMapFilterType;
    typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
    binaryImageToLabelMapFilter->SetInput( inputImage );
    binaryImageToLabelMapFilter->FullyConnectedOn();
    binaryImageToLabelMapFilter->SetNumberOfThreads( maxThreads );
    binaryImageToLabelMapFilter->Update();

    typedef typename itk::LabelMapToLabelImageFilter<typename BinaryImageToLabelMapFilterType::OutputImageType, InternalImageType> LabelMapToLabelImageFilterType;
    typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
    labelMapToLabelImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
    labelMapToLabelImageFilter->SetReleaseDataFlag( releaseDataFlag );
    labelMapToLabelImageFilter->SetNumberOfThreads(maxThreads);
    labelMapToLabelImageFilter->Update();

    // set the output
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<InternalImageType>( labelMapToLabelImageFilter->GetOutput() );
	outputImage->SetRescaleFlag( false );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LabelImageFilterWidget<Image2Float> > LabelImageFilterWidgetImage2Float;
static ProcessObjectProxy< LabelImageFilterWidget<Image3Float> > LabelImageFilterWidgetImage3Float;
static ProcessObjectProxy< LabelImageFilterWidget<Image2UShort> > LabelImageFilterWidgetImage2UShort;
static ProcessObjectProxy< LabelImageFilterWidget<Image3UShort> > LabelImageFilterWidgetImage3UShort;


} // namespace XPIWIT
