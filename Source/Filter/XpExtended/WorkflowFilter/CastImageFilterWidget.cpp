/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "CastImageFilterWidget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../Base/Management/ImageWrapper.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkCastImageFilter.h"
#include "itkNumericTraits.h"


// XPIWIT namespace
namespace XPIWIT {

// the default constructor
template< class TInputImage, class TOutputImage >
CastImageFilterWidget< TInputImage, TOutputImage >::CastImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = CastImageFilterWidget<TInputImage, TOutputImage>::GetName();
    this->mDescription = "Cast the input image to the output image and resacles the intensity.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(2);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(2);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage, class TOutputImage >
CastImageFilterWidget< TInputImage, TOutputImage >::~CastImageFilterWidget()
{
}


// the update function
template< class TInputImage, class TOutputImage >
void CastImageFilterWidget< TInputImage, TOutputImage >::Update()
{
    // nothing to do here
    if( typeid( typename TInputImage::PixelType ) == typeid( typename TOutputImage::PixelType ) ){
		mOutputImages.append( mInputImages.at(0) );
        return;
	}

	// start the timer
    ProcessObjectBase::StartTimer();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    Logger::GetInstance()->WriteLine( "- Cast image: (" + QString::fromStdString( typeid( typename TInputImage::PixelType ).name() ) + " -> " + QString::fromStdString( typeid( typename TOutputImage::PixelType ).name() ) + ")" );
    
    // get the parameter values
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool rescaleImage = mInputImages.at(0)->GetRescaleFlag();

	// get the input image
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	inputImage->SetReleaseDataFlag( true );

	// specify the maximum minimum calculator
	//typename itk::MinimumMaximumImageCalculator<TInputImage>::Pointer minMaxInput = itk::MinimumMaximumImageCalculator<TInputImage>::New();
    //minMaxInput->SetImage( inputImage );
    //minMaxInput->Compute();

	// write intensity transform to the console
	Logger::GetInstance()->WriteToAll( QString("Rescale flag: ") + QString::number( rescaleImage ) );
//	Logger::GetInstance()->WriteToAll( QString("Input - minimum: ") + QString::number( minMaxInput->GetMinimum() ) + QString(" maximum: ") + QString::number( minMaxInput->GetMaximum() ) );

//	Logger::GetInstance()->WriteLine( QString("Logging took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
//	timer0 = QDateTime::currentMSecsSinceEpoch();

    // TODO: cases for intensity and labled images
    // cast integral data types if max fits
    //if( typeid( TInputImage::PixelType  ) != typeid( float )  &&
    //    typeid( TInputImage::PixelType  ) != typeid( double ) &&
    //    typeid( TOutputImage::PixelType ) != typeid( float )  &&
    //    typeid( TOutputImage::PixelType ) != typeid( double ) ){

	if( rescaleImage == false )
	{
		
		/*
		Logger::GetInstance()->WriteLine( "- Jop bin drin!");

		// calculate the minimum and maximum (TODO: why again here??)
		//typename itk::MinimumMaximumImageCalculator<TInputImage>::Pointer minMaxCalc = itk::MinimumMaximumImageCalculator<TInputImage>::New();
        //minMaxCalc->SetImage( inputImage );
        //minMaxCalc->Compute();
        typename TInputImage::PixelType minimum = minMaxInput->GetMinimum();
        typename TInputImage::PixelType maximum = minMaxInput->GetMaximum();

        // is it possible to cast without data loss
        if( itk::NumericTraits< typename TOutputImage::PixelType >::min() <= minimum &&
            itk::NumericTraits< typename TOutputImage::PixelType >::max() >= maximum )
		{
			Logger::GetInstance()->WriteLine( "- Possible Data Loss by the Type Cast!");
		}



		// write cast ranges to log
		Logger::GetInstance()->WriteLine( "- Cast image Without Rescale: [" + QString::number( minimum ) + ", " + QString::number( maximum ) + "] -> [" + QString::number( minimum ) + ", " + QString::number( maximum ) + "]");
		*/

		// perform the cast
        typename itk::CastImageFilter<TInputImage, TOutputImage>::Pointer castFilter = itk::CastImageFilter<TInputImage, TOutputImage>::New();
        castFilter->SetInput( inputImage );
		castFilter->SetReleaseDataFlag( true );

        try
        {
            castFilter->Update();
            ImageWrapper *outputImage = new ImageWrapper();
			outputImage->SetRescaleFlag( rescaleImage );
            outputImage->SetImage<TOutputImage>( castFilter->GetOutput() );
            mOutputImages.append( outputImage );

            // log the performance
            ProcessObjectBase::LogPerformance();
            return;
        }
        catch( itk::ExceptionObject & err )
        {
            Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
            Logger::GetInstance()->WriteException( err );
            return;
        }

    }


    // rescale the intensity to 16Bit with minimum of 0 and maximum of 65535
    typename itk::IntensityWindowingImageFilter<TInputImage, TOutputImage>::Pointer rescaleIntensity = itk::IntensityWindowingImageFilter<TInputImage, TOutputImage>::New();
    rescaleIntensity->SetInput( inputImage );
	rescaleIntensity->SetReleaseDataFlag( true );

    // set input range
    if( typeid( typename TInputImage::PixelType ) == typeid( float ) ||
            typeid( typename TInputImage::PixelType ) == typeid( double ) )
	{
        // floating point types
        rescaleIntensity->SetWindowMinimum( itk::NumericTraits< typename TInputImage::PixelType >::ZeroValue() );
        rescaleIntensity->SetWindowMaximum( itk::NumericTraits< typename TInputImage::PixelType >::OneValue() );
    }
	else
	{
        // integer types
        rescaleIntensity->SetWindowMinimum( itk::NumericTraits< typename TInputImage::PixelType >::min() );
        rescaleIntensity->SetWindowMaximum( itk::NumericTraits< typename TInputImage::PixelType >::max() );
    }

    // set output range
    if( typeid( typename TOutputImage::PixelType ) == typeid( float ) ||
            typeid( typename TOutputImage::PixelType ) == typeid( double ) )
	{
        // floating point types
        rescaleIntensity->SetOutputMinimum( itk::NumericTraits< typename TOutputImage::PixelType >::ZeroValue() );
        rescaleIntensity->SetOutputMaximum( itk::NumericTraits< typename TOutputImage::PixelType >::OneValue() );
    }
	else
	{
        // integer types
        rescaleIntensity->SetOutputMinimum( itk::NumericTraits< typename TOutputImage::PixelType >::min() );
        rescaleIntensity->SetOutputMaximum( itk::NumericTraits< typename TOutputImage::PixelType >::max() );
    }

    try
    {
		Logger::GetInstance()->WriteLine( "- Windowing image: [" + QString::number( rescaleIntensity->GetWindowMinimum() ) + ", " + QString::number( rescaleIntensity->GetWindowMaximum() ) + "] -> [" + QString::number( rescaleIntensity->GetOutputMinimum() ) + ", " + QString::number( rescaleIntensity->GetOutputMaximum() ) + "]");
        rescaleIntensity->Update();

        ImageWrapper* outputImage = new ImageWrapper();
        outputImage->SetImage<TOutputImage>( rescaleIntensity->GetOutput() );
        mOutputImages.append( outputImage );
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }

    // update the process widget
    //ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance( false );
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< CastImageFilterWidget<Image2Float, Image2UShort> > CastImageFilterWidgetImage2Float;
static ProcessObjectProxy< CastImageFilterWidget<Image3Float, Image3UShort> > CastImageFilterWidgetImage3Float;
static ProcessObjectProxy< CastImageFilterWidget<Image2UShort, Image2Float> > CastImageFilterWidgetImage2UShort;
static ProcessObjectProxy< CastImageFilterWidget<Image3UShort, Image3Float> > CastImageFilterWidgetImage3UShort;

// explicit template instantiations for the cast filter
template class CastImageFilterWidget<Image2Char, Image2Float>;
template class CastImageFilterWidget<Image2Char, Image2UChar>;
template class CastImageFilterWidget<Image3Char, Image3Float>;
template class CastImageFilterWidget<Image3Char, Image3UChar>;
template class CastImageFilterWidget<Image2Double, Image2Float>;
template class CastImageFilterWidget<Image2Double, Image2UChar>;
template class CastImageFilterWidget<Image2UShort, Image2UChar>;
template class CastImageFilterWidget<Image2Char, Image2UShort>;
template class CastImageFilterWidget<Image3Char, Image3UShort>;
template class CastImageFilterWidget<Image2Double, Image2UShort>;
template class CastImageFilterWidget<Image3Double, Image3Float>;
template class CastImageFilterWidget<Image3Double, Image3UChar>;
template class CastImageFilterWidget<Image2Float, Image2UChar>;
template class CastImageFilterWidget<Image2Float, Image2UShort>;
template class CastImageFilterWidget<Image3Float, Image3UChar>;
template class CastImageFilterWidget<Image2UChar, Image2Float>;
template class CastImageFilterWidget<Image2UChar, Image2UShort>;
template class CastImageFilterWidget<Image3UChar, Image3Float>;
template class CastImageFilterWidget<Image3UChar, Image3UShort>;
template class CastImageFilterWidget<Image2Int, Image2Float>;
template class CastImageFilterWidget<Image3UShort, Image3UChar>;
template class CastImageFilterWidget<Image3UShort, Image3Float>;
template class CastImageFilterWidget<Image2UShort, Image2Float>;
template class CastImageFilterWidget<Image3Short, Image3UShort>;
template class CastImageFilterWidget<Image3Short, Image3UChar>;
template class CastImageFilterWidget<Image3Short, Image3Float>;
template class CastImageFilterWidget<Image2Short, Image2UShort>;
template class CastImageFilterWidget<Image2Short, Image2UChar>;
template class CastImageFilterWidget<Image2Short, Image2Float>;
template class CastImageFilterWidget<Image3ULong, Image3UChar>;
template class CastImageFilterWidget<Image3ULong, Image3Float>;
template class CastImageFilterWidget<Image2ULong, Image2UShort>;
template class CastImageFilterWidget<Image2ULong, Image2UChar>;
template class CastImageFilterWidget<Image2ULong, Image2Float>;
template class CastImageFilterWidget<Image3Long, Image3UShort>;
template class CastImageFilterWidget<Image2Long, Image3UChar>;
template class CastImageFilterWidget<Image3Long, Image3Float>;
template class CastImageFilterWidget<Image2Long, Image2UChar>;
template class CastImageFilterWidget<Image2Long, Image2Float>;
template class CastImageFilterWidget<Image3UInt, Image3UShort>;
template class CastImageFilterWidget<Image3UInt, Image3UChar>;
template class CastImageFilterWidget<Image3UInt, Image3Float>;
template class CastImageFilterWidget<Image2UInt, Image2UShort>;
template class CastImageFilterWidget<Image2UInt, Image2UChar>;
template class CastImageFilterWidget<Image2UInt, Image2Float>;
template class CastImageFilterWidget<Image3Int, Image3UShort>;
template class CastImageFilterWidget<Image3Int, Image3UChar>;
template class CastImageFilterWidget<Image3Int, Image3Float>;
template class CastImageFilterWidget<Image2Int, Image2UShort>;
template class CastImageFilterWidget<Image2Int, Image2UChar>;
template class CastImageFilterWidget<Image3Float, Image3UShort>;
template class CastImageFilterWidget<Image3Double, Image3UShort>;
template class CastImageFilterWidget<Image2Long, Image2UShort>;
template class CastImageFilterWidget<Image3ULong, Image3UShort>;
template class CastImageFilterWidget<Image3Long, Image3UChar>;

} // namespace XPIWIT
