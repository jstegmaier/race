/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// namespace header
#include "ImageWriterWidget.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageFileWriter.h"


namespace XPIWIT
{

// the default constructor
ImageWriterWidget::ImageWriterWidget() : ProcessObjectBase()
{
    // set type
    this->mName = ImageWriterWidget::GetName();
    this->mDescription = "Writes image to disk";
    
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_WRITER);
	this->mObjectType->SetNumberTypes(0);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(0);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // get process object settings pointer
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // add settings
    processObjectSettings->AddSetting( "Precision", "16", ProcessObjectSetting::SETTINGVALUETYPE_INT, "set precision in bit (8/16 Bit)" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
ImageWriterWidget::~ImageWriterWidget()
{
}


// update the reader
void ImageWriterWidget::Update()
{
    // start timer and get the process object settings object
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the widget settings
    const int maxThreads		= processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const int precision         = processObjectSettings->GetSettingValue( "Precision" ).toInt();

    ImageWrapper *imageWrapper = this->mInputImages.at(0);
    QPair< ProcessObjectType::DataType, int > imageType = imageWrapper->GetImageType();

    if( imageType.second == 2 ){
        if( precision == 16 )
            WriteImage< Image2UShort >();
        else
            WriteImage< Image2UChar >();
    }

    if( imageType.second == 3 ){
        if( precision == 16 )
            WriteImage< Image3UShort >();
        else
            WriteImage< Image3UChar >();
    }

    // log the performance
    ProcessObjectBase::LogPerformance();
}


// function to write the image
template <class TImageType>
void ImageWriterWidget::WriteImage()
{
    ImageWrapper *imageWrapper = this->mInputImages.at(0);

    typename TImageType::Pointer image = imageWrapper->GetImage<TImageType>();
    ProcessObjectBase::StartTimer();


    typename itk::ImageFileWriter< TImageType >::Pointer writer = itk::ImageFileWriter< TImageType >::New();
    writer->SetReleaseDataFlag( false );
	writer->SetFileName( this->mPath.toStdString() );
    writer->SetInput( image );
    try
    {
        writer->Update();
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ImageWriterWidget > ImageWriterWidgetImage2Float;


} // namespace XPIWIT
