/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "MetaReaderWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// qt header
#include <QFile>


namespace XPIWIT
{

// the default constructor
MetaReaderWidget::MetaReaderWidget() : ProcessObjectBase()
{
    // set type
    this->mName = MetaReaderWidget::GetName();
    this->mDescription = "Reads csv from disk";
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_READER);
	this->mObjectType->SetNumberTypes(0);
    this->mObjectType->SetNumberImageInputs(0);
    this->mObjectType->SetNumberImageOutputs(0);
    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("Variable");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("Variable");

    // get process object settings pointer
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Separator", ";", ProcessObjectSetting::SETTINGVALUETYPE_STRING, "Defines the used separator" );
	processObjectSettings->AddSetting( "Delimitor", ".", ProcessObjectSetting::SETTINGVALUETYPE_STRING, "Defines the used delimitor" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
MetaReaderWidget::~MetaReaderWidget()
{
}


// update the reader
void MetaReaderWidget::Update()
{
    // start timer and get the process object settings object
    ProcessObjectBase::StartTimer();
	ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	const QString separator = processObjectSettings->GetSettingValue("Separator");
    const QString delimitor = processObjectSettings->GetSettingValue("Delimitor");

	QFile csvFile( mPath ); // this->mImagePath
	if( csvFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
		metaOutput->mIsMultiDimensional = false;							// one line per image

		// get postfix from file path
		QString postfix = "";	//this->mImagePath;
		postfix = postfix.split("_").last();	// get postfix
		postfix = postfix.split(".").first();	// delete file ending
		metaOutput->mPostfix = postfix;

		// read header line
		QString line = csvFile.readLine();
		line.replace("\n", "");
		metaOutput->mTitle = line.split( separator );

		// read data
		line = csvFile.readLine();
		line.replace("\n", "");

		QStringList currentLineStrings = line.split( separator );
		QList<float> currentLine;

		for (int i=0; i<currentLineStrings.length(); ++i)
			currentLine.append( currentLineStrings.at(i).toFloat() );

		metaOutput->mData.append( currentLine );
		
		while( !csvFile.atEnd() )
		{
			// if more than one switch MultiDimensional flag to true
			metaOutput->mIsMultiDimensional = true;		
			line = csvFile.readLine();
			line.replace("\n", "");

			currentLineStrings = line.split( separator );
			currentLine.clear();
		
			for (int i=0; i<currentLineStrings.length(); ++i)
				currentLine.append( currentLineStrings.at(i).toFloat() );

			metaOutput->mData.append( currentLine );
		}

		csvFile.close();
	}
	else
	{
		// error occured
	}

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MetaReaderWidget > MetaReaderWidgetCSV;
    
} // namespace XPIWIT
