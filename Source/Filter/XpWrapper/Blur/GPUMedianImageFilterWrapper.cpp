/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifdef XPIWIT_USE_GPU

// project header
#include "GPUMedianImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../CUDA/medianFilter2D/medianFilter2D.h"


namespace XPIWIT {

// the default constructor
template< class TInputImage >
GPUMedianImageFilterWrapper<TInputImage>::GPUMedianImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = GPUMedianImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Median Filter. ";
    this->mDescription += "Filters the input with a median kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the filter kernel (manhattan distance)." );
    processObjectSettings->AddSetting( "FilterMask3D", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D kernel." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
GPUMedianImageFilterWrapper<TInputImage>::~GPUMedianImageFilterWrapper()
{
}


// update the filter
template< class TInputImage >
void GPUMedianImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int size = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const int filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	int devCUDA = 0;//TODO amatf: add this as a parameter in the XML file	
	int dims[3];		

	//read image dimensions
	TInputImage::RegionType region = inputImage->GetLargestPossibleRegion();
	TInputImage::SizeType imSize = region.GetSize();
	for(int ii = 0; ii < TInputImage::ImageDimension; ii++)
		dims[ii] = imSize[ii];
	if( TInputImage::ImageDimension < 3 )
		dims[2] = 1;

	//get pointer to image data within ITK class
	 TInputImage::PixelType *img = inputImage->GetBufferPointer();
	//calculate median filter
	if( TInputImage::ImageDimension <= 2 )
		medianFilterCUDASliceBySlice<TInputImage::PixelType>(img, dims, size, devCUDA);	
	else if (filterMask3D == false)//median filter slice by slice
		medianFilterCUDASliceBySlice<TInputImage::PixelType>(img, dims, size, devCUDA);	
	else
	{
		//TODO amatf implement a full 3D median filter
		medianFilterCUDASliceBySlice<TInputImage::PixelType>(img, dims, size, devCUDA);	
	}
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	
    // update the filter
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( inputImage );
    mOutputImages.append( outputImage );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< GPUMedianImageFilterWrapper<Image2Float> > GPUMedianImageFilterWrapperImage2Float;
static ProcessObjectProxy< GPUMedianImageFilterWrapper<Image3Float> > GPUMedianImageFilterWrapperImage3Float;
static ProcessObjectProxy< GPUMedianImageFilterWrapper<Image2UShort> > GPUMedianImageFilterWrapperImage2UShort;
static ProcessObjectProxy< GPUMedianImageFilterWrapper<Image3UShort> > GPUMedianImageFilterWrapperImage3UShort;

} // namespace XPIWIT

#endif