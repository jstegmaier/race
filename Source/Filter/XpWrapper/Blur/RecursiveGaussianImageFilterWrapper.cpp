/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "RecursiveGaussianImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkRecursiveGaussianImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
RecursiveGaussianImageFilterWrapper< TInputImage >::RecursiveGaussianImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = RecursiveGaussianImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "IIR convolution with an approximation of a Gaussian kernel. ";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sigma of the gaussian kernel." );
	processObjectSettings->AddSetting( "Normalize", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Normalize across scale." );
	processObjectSettings->AddSetting( "Order", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The order of the filter (0, 1, 2 for zero, first and second order derivatives)." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the default destructor
template < class TInputImage >
RecursiveGaussianImageFilterWrapper< TInputImage >::~RecursiveGaussianImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void RecursiveGaussianImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int order = processObjectSettings->GetSettingValue( "Order" ).toInt();
	const double sigma = processObjectSettings->GetSettingValue( "Sigma" ).toDouble();
	const bool normalize = processObjectSettings->GetSettingValue( "Normalize" ).toInt() > 0;
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::RecursiveGaussianImageFilter<TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetSigma( sigma );

	if (order == 0)
		filter->SetZeroOrder();
	else if (order == 1)
		filter->SetFirstOrder();
	else
		filter->SetSecondOrder();

	filter->SetNormalizeAcrossScale( normalize );
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: RecursiveGaussianImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< RecursiveGaussianImageFilterWrapper<Image2Float> > RecursiveGaussianImageFilterWrapperImage2Float;
static ProcessObjectProxy< RecursiveGaussianImageFilterWrapper<Image3Float> > RecursiveGaussianImageFilterWrapperImage3Float;
static ProcessObjectProxy< RecursiveGaussianImageFilterWrapper<Image2UShort> > RecursiveGaussianImageFilterWrapperImage2UShort;
static ProcessObjectProxy< RecursiveGaussianImageFilterWrapper<Image3UShort> > RecursiveGaussianImageFilterWrapperImage3UShort;

} // namespace XPIWIT

