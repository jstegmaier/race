/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifdef XPIWIT_USE_GPU

// namespace header
#include "GPUHessianToObjectnessMeasureImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

#include "../../CUDA/membraneEnhancement/membraneEnhancement.h"

// itk header
#include "itkHessianToObjectnessMeasureImageFilter.h"
#include "itkHessianRecursiveGaussianImageFilter.h"

// qt header

// system header

namespace XPIWIT {

template < class TImageType >
GPUHessianToObjectnessMeasureImageFilterWrapper< TImageType >::GPUHessianToObjectnessMeasureImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = GPUHessianToObjectnessMeasureImageFilterWrapper<TImageType>::GetName();
	this->mDescription = "Uses the hessian eigenvalues to enhance specific structures in the image. GPU filter!";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance used by the Hessian calculation." );
	processObjectSettings->AddSetting( "Alpha", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha parameter for the objectness filter." );
	processObjectSettings->AddSetting( "Beta", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Beta parameter for the objectness filter." );
	processObjectSettings->AddSetting( "Gamma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Gamma parameter for the objectness filter." );
	processObjectSettings->AddSetting( "ScaleObjectnessMeasure", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Scales the object measure if set on." );
	processObjectSettings->AddSetting( "ObjectDimension", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Set the dimension of the objects about to be emphasized." );
	processObjectSettings->AddSetting( "BrightObject", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Gamma parameter for the objectness filter." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


template < class TImageType >
GPUHessianToObjectnessMeasureImageFilterWrapper< TImageType >::~GPUHessianToObjectnessMeasureImageFilterWrapper()
{
}


template < class TImageType >
void GPUHessianToObjectnessMeasureImageFilterWrapper< TImageType >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float sigma = processObjectSettings->GetSettingValue( "Sigma" ).toFloat();
	const float alpha = processObjectSettings->GetSettingValue( "Alpha" ).toFloat();
	const float beta = processObjectSettings->GetSettingValue( "Beta" ).toFloat();
	const float gamma = processObjectSettings->GetSettingValue( "Gamma" ).toFloat();
	const int objectDimension = processObjectSettings->GetSettingValue( "ObjectDimension" ).toInt();
	const bool scaleObjectnessMeasure = processObjectSettings->GetSettingValue( "ScaleObjectnessMeasure" ).toInt() > 0;
	const bool brightObject = processObjectSettings->GetSettingValue( "BrightObject" ).toInt() > 0;
	
	// get images
	typename TImageType::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	
	//read image dimensions
	std::int64_t imgDims[dimsImage], imSize = 1;	
	TImageType::RegionType region = inputImage->GetLargestPossibleRegion();
	TImageType::SizeType imSizeVec = region.GetSize();
	for(int ii = 0; ii < TImageType::ImageDimension; ii++)
	{
		imgDims[ii] = imSizeVec[ii];
		imSize *= imSizeVec[ii];
	}

	//transform sigma from image spacing units (i.e. physical units) to pixel units
	float sigmaPixel[TImageType::ImageDimension];
	typename TImageType::SpacingType spacing = inputImage->GetSpacing();
	for(int ii = 0; ii < TImageType::ImageDimension; ii++)
		sigmaPixel[ii] = sigma / spacing[ii];	
	

	//get pointer to image data within ITK class
	TImageType::PixelType *img = inputImage->GetBufferPointer();

	
	//TODO amatf: add this as a parameter in the XML file
	int devCUDA = 0;

	//declare GPU objectness operator class
	membraneEnhancement_CUDA<TImageType::PixelType> wPBC(img, imgDims, devCUDA);

	//calulate objectness
	wPBC.imageObjectness(sigmaPixel, beta, gamma, scaleObjectnessMeasure);	

	//collect output from GPU
	wPBC.getImageObjectnessFromGPU(img);

	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TImageType>( inputImage );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< GPUHessianToObjectnessMeasureImageFilterWrapper<Image2Float> > GPUHessianToObjectnessMeasureImageFilterWrapperImage2Float;
static ProcessObjectProxy< GPUHessianToObjectnessMeasureImageFilterWrapper<Image3Float> > GPUHessianToObjectnessMeasureImageFilterWrapperImage3Float;

} // namespace XPIWIT

#endif