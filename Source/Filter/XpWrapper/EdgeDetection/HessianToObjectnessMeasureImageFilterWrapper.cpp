/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// namespace header
#include "HessianToObjectnessMeasureImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkHessianToObjectnessMeasureImageFilter.h"
#include "itkHessianRecursiveGaussianImageFilter.h"
#include "itkPasteImageFilter.h"

namespace XPIWIT
{
	
// the default constructor
template < class TInputImage >
HessianToObjectnessMeasureImageFilterWrapper< TInputImage >::HessianToObjectnessMeasureImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = HessianToObjectnessMeasureImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Uses the hessian eigenvalues to enhance specific structures in the image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance used by the Hessian calculation." );
	processObjectSettings->AddSetting( "Alpha", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha parameter for the objectness filter." );
	processObjectSettings->AddSetting( "Beta", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Beta parameter for the objectness filter." );
	processObjectSettings->AddSetting( "Gamma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Gamma parameter for the objectness filter." );
	processObjectSettings->AddSetting( "ScaleObjectnessMeasure", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Scales the object measure if set on." );
	processObjectSettings->AddSetting( "ObjectDimension", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Set the dimension of the objects about to be emphasized." );
	processObjectSettings->AddSetting( "BrightObject", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Gamma parameter for the objectness filter." );
	processObjectSettings->AddSetting( "FilterMask3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Enhance objects in a slice by slice manner." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
HessianToObjectnessMeasureImageFilterWrapper< TInputImage >::~HessianToObjectnessMeasureImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void HessianToObjectnessMeasureImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float sigma = processObjectSettings->GetSettingValue( "Sigma" ).toFloat();
	const float alpha = processObjectSettings->GetSettingValue( "Alpha" ).toFloat();
	const float beta = processObjectSettings->GetSettingValue( "Beta" ).toFloat();
	const float gamma = processObjectSettings->GetSettingValue( "Gamma" ).toFloat();
	const int objectDimension = processObjectSettings->GetSettingValue( "ObjectDimension" ).toInt();
	const bool scaleObjectnessMeasure = processObjectSettings->GetSettingValue( "ScaleObjectnessMeasure" ).toInt() > 0;
	const bool brightObject = processObjectSettings->GetSettingValue( "BrightObject" ).toInt() > 0;
	const bool filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;

	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	if (filterMask3D == true)
	{
		// setup the filter
		typedef itk::HessianRecursiveGaussianImageFilter<TInputImage> HessianFilterType;
		typedef itk::HessianToObjectnessMeasureImageFilter< typename HessianFilterType::OutputImageType, TInputImage> ObjectnessMeasureFilterType;

		typename HessianFilterType::Pointer hessianFilter = HessianFilterType::New();
		hessianFilter->SetInput( inputImage );
		hessianFilter->SetNumberOfThreads( maxThreads );
		hessianFilter->SetReleaseDataFlag( true );
		hessianFilter->SetSigma( sigma );
	
		typename ObjectnessMeasureFilterType::Pointer objectnessFilter = ObjectnessMeasureFilterType::New();
		objectnessFilter->SetInput( hessianFilter->GetOutput() );
		objectnessFilter->SetAlpha( alpha );
		objectnessFilter->SetBeta( beta );
		objectnessFilter->SetGamma( gamma );
		objectnessFilter->SetObjectDimension( objectDimension );
		objectnessFilter->SetBrightObject( brightObject );
		objectnessFilter->SetNumberOfThreads( maxThreads );
		objectnessFilter->SetScaleObjectnessMeasure( scaleObjectnessMeasure );
		itkTryCatch(objectnessFilter->Update(), "Error: HessianToObjectnessMeasureImageFilterWrapper Update Function.");
	
		ImageWrapper *outputImage = new ImageWrapper();
		outputImage->SetImage<TInputImage>( objectnessFilter->GetOutput() );
		mOutputImages.append( outputImage );
	}
	else
	{
		if (TInputImage::ImageDimension != 3)
		{	
			Logger::GetInstance()->WriteLine( "Error: Slice-by-slice processing only works for 3D images!" );
			return;
		}
		
		typedef typename itk::Image<typename TInputImage::PixelType, 2> TInputImageSlice;

		typename TInputImage::Pointer resultImage = TInputImage::New();
		resultImage->SetRegions( inputImage->GetLargestPossibleRegion() );
		resultImage->SetSpacing( inputImage->GetSpacing() );
		resultImage->Allocate();
		resultImage->FillBuffer( 0 );

		const unsigned int numSlices = inputImage->GetLargestPossibleRegion().GetSize()[2];
		typename TInputImage::SizeType currentSize = inputImage->GetLargestPossibleRegion().GetSize(); 
		typename TInputImage::IndexType currentIndex = inputImage->GetLargestPossibleRegion().GetIndex();
		typename TInputImage::RegionType currentRegion;
		currentSize[2] = 1;
		currentRegion.SetSize( currentSize );
		currentRegion.SetIndex( currentIndex );
		
		typename TInputImageSlice::Pointer tmpImage = TInputImageSlice::New();
		typename TInputImageSlice::SizeType tmpImageSize;
		typename TInputImageSlice::IndexType tmpImageIndex;
		typename TInputImageSlice::RegionType tmpImageRegion;
		typename TInputImageSlice::SpacingType tmpImageSpacing;
		tmpImageSize[0] = currentSize[0];
		tmpImageSize[1] = currentSize[1];
		tmpImageIndex[0] = 0;
		tmpImageIndex[1] = 0;
		tmpImageRegion.SetIndex( tmpImageIndex );
		tmpImageRegion.SetSize( tmpImageSize );
		tmpImageSpacing[0] = inputImage->GetSpacing()[0];
		tmpImageSpacing[1] = inputImage->GetSpacing()[1];
		tmpImage->SetRegions(tmpImageRegion);
		tmpImage->SetSpacing(tmpImageSpacing);
		tmpImage->Allocate();
		tmpImage->FillBuffer(0);
		
		for (int i=0; i<numSlices; ++i)
		{
			// increase the current index
			currentIndex[2] = i;
			currentRegion.SetIndex( currentIndex );

			typename itk::ImageRegionIterator<TInputImage> inputIterator(inputImage, currentRegion);
			typename itk::ImageRegionIterator<TInputImageSlice> sliceIterator(tmpImage, tmpImage->GetLargestPossibleRegion());
			inputIterator.GoToBegin();
			sliceIterator.GoToBegin();

			while (!inputIterator.IsAtEnd())
			{
				sliceIterator.Set( inputIterator.Get() );
				++inputIterator;
				++sliceIterator;
			}

			// setup the filter
			typedef itk::HessianRecursiveGaussianImageFilter< TInputImageSlice > HessianFilterType;
			typedef itk::HessianToObjectnessMeasureImageFilter< typename HessianFilterType::OutputImageType, TInputImageSlice > ObjectnessMeasureFilterType;

			typename HessianFilterType::Pointer hessianFilter = HessianFilterType::New();
			hessianFilter->SetInput( tmpImage );
			hessianFilter->SetNumberOfThreads( maxThreads );
			hessianFilter->SetReleaseDataFlag( true );
			hessianFilter->SetSigma( sigma );
	
			typename ObjectnessMeasureFilterType::Pointer objectnessFilter = ObjectnessMeasureFilterType::New();
			objectnessFilter->SetInput( hessianFilter->GetOutput() );
			objectnessFilter->SetAlpha( alpha );
			objectnessFilter->SetBeta( beta );
			objectnessFilter->SetGamma( gamma );
			objectnessFilter->SetObjectDimension( objectDimension );
			objectnessFilter->SetBrightObject( brightObject );
			objectnessFilter->SetNumberOfThreads( maxThreads );
			objectnessFilter->SetScaleObjectnessMeasure( scaleObjectnessMeasure );
			itkTryCatch(objectnessFilter->Update(), "Error: HessianToObjectnessMeasureImageFilterWrapper Update Function.");

			typename itk::ImageRegionIterator<TInputImage> outputIterator(resultImage, currentRegion);
			typename itk::ImageRegionIterator<TInputImageSlice> resultSliceIterator(objectnessFilter->GetOutput(), objectnessFilter->GetOutput()->GetLargestPossibleRegion());
			outputIterator.GoToBegin();
			resultSliceIterator.GoToBegin();

			while (!outputIterator.IsAtEnd())
			{
				outputIterator.Set( resultSliceIterator.Get() );
				++outputIterator;
				++resultSliceIterator;
			}
		}
			
		ImageWrapper *outputImage = new ImageWrapper();
		outputImage->SetImage<TInputImage>( resultImage );
		mOutputImages.append( outputImage );
	}
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< HessianToObjectnessMeasureImageFilterWrapper<Image2Float> > HessianToObjectnessMeasureImageFilterWrapperImage2Float;
static ProcessObjectProxy< HessianToObjectnessMeasureImageFilterWrapper<Image3Float> > HessianToObjectnessMeasureImageFilterWrapperImage3Float;
static ProcessObjectProxy< HessianToObjectnessMeasureImageFilterWrapper<Image2UShort> > HessianToObjectnessMeasureImageFilterWrapperImage2UShort;
static ProcessObjectProxy< HessianToObjectnessMeasureImageFilterWrapper<Image3UShort> > HessianToObjectnessMeasureImageFilterWrapperImage3UShort;

} // namespace XPIWIT

