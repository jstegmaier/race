/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "RescaleIntensityImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
RescaleIntensityImageFilterWrapper< TInputImage >::RescaleIntensityImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = RescaleIntensityImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Rescales the image from min to max for integer types and from 0 to 1 for float types";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
RescaleIntensityImageFilterWrapper< TInputImage >::~RescaleIntensityImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void RescaleIntensityImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::MinimumMaximumImageCalculator<TInputImage> MinMaxCalculatorType;
	typename MinMaxCalculatorType::Pointer minMaxCalc = MinMaxCalculatorType::New();
    minMaxCalc->SetImage( inputImage );
    minMaxCalc->Compute();

	typename TInputImage::PixelType minimum = minMaxCalc->GetMinimum();
    typename TInputImage::PixelType maximum = minMaxCalc->GetMaximum();

	typedef itk::IntensityWindowingImageFilter<TInputImage, TInputImage> IntensityFilterType;
	typename IntensityFilterType::Pointer intensityFilter = IntensityFilterType::New();
	intensityFilter->SetInput( inputImage );
	intensityFilter->SetReleaseDataFlag( true );
	intensityFilter->SetWindowMinimum( minimum );
    intensityFilter->SetWindowMaximum( maximum );

	if( typeid( typename TInputImage::PixelType ) == typeid( float ) ||
		typeid( typename TInputImage::PixelType ) == typeid( double ) )
	{
		// float type
		intensityFilter->SetOutputMinimum( itk::NumericTraits< typename TInputImage::PixelType >::ZeroValue() );
        intensityFilter->SetOutputMaximum( itk::NumericTraits< typename TInputImage::PixelType >::OneValue() );
	}
	else
	{
		// integral type
		intensityFilter->SetOutputMinimum( itk::NumericTraits< typename TInputImage::PixelType >::min() );
        intensityFilter->SetOutputMaximum( itk::NumericTraits< typename TInputImage::PixelType >::max() );
	}
	
	itkTryCatch(intensityFilter->Update(), "Error: RescaleIntensityImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( intensityFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< RescaleIntensityImageFilterWrapper<Image2Float> > RescaleIntensityImageFilterWrapperImage2Float;
static ProcessObjectProxy< RescaleIntensityImageFilterWrapper<Image3Float> > RescaleIntensityImageFilterWrapperImage3Float;
static ProcessObjectProxy< RescaleIntensityImageFilterWrapper<Image2UShort> > RescaleIntensityImageFilterWrapperImage2UShort;
static ProcessObjectProxy< RescaleIntensityImageFilterWrapper<Image3UShort> > RescaleIntensityImageFilterWrapperImage3UShort;

} // namespace XPIWIT

