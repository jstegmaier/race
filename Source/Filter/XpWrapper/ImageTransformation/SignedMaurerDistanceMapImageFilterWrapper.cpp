/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "SignedMaurerDistanceMapImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "../../ITKCustom/itkSliceBySliceSignedMaurerDistanceMapImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkIntensityWindowingImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
SignedMaurerDistanceMapImageFilterWrapper< TInputImage >::SignedMaurerDistanceMapImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = SignedMaurerDistanceMapImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "This filter computes the distance map of the input image as an approximation with pixel accuracy to the Euclidean distance.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "BackgroundValue", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set background value." );
	processObjectSettings->AddSetting( "InsideIsPositive", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set if inside value is positive." );
	processObjectSettings->AddSetting( "UseSquareDistance", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set to use the square of the distance." );
	processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set to use the image spacing." );
	processObjectSettings->AddSetting( "PositiveValuesOnly", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Discard negative values and only consider positive ones." );
	processObjectSettings->AddSetting( "SliceBySlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the distance map is calcualted for each slice individually." );
	processObjectSettings->AddSetting( "NormalizeResult", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the output image is scaled to the range [0, 1]. Else the true (squared) distances values are preserved." );
		
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
SignedMaurerDistanceMapImageFilterWrapper< TInputImage >::~SignedMaurerDistanceMapImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void SignedMaurerDistanceMapImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const float backgroundValue = processObjectSettings->GetSettingValue( "BackgroundValue" ).toFloat();
	const bool insidePositive = processObjectSettings->GetSettingValue( "InsideIsPositive" ).toInt() > 0;
	const bool useSquareDistance = processObjectSettings->GetSettingValue( "UseSquareDistance" ).toInt() > 0;
	const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
	const bool positiveValuesOnly = processObjectSettings->GetSettingValue( "PositiveValuesOnly" ).toInt() > 0;
	const bool normalizeResult = processObjectSettings->GetSettingValue( "NormalizeResult" ).toInt() > 0;
	const bool sliceBySlice = processObjectSettings->GetSettingValue( "SliceBySlice" ).toInt() > 0;
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// define the distance map image
	typename TInputImage::Pointer distanceMapImage;

	// setup the filter
	if (sliceBySlice == false)
	{
		typedef itk::SignedMaurerDistanceMapImageFilter<TInputImage, TInputImage> DistanceMapFilterType;
		typename DistanceMapFilterType::Pointer distanceMap = DistanceMapFilterType::New();
		distanceMap->SetInput( inputImage );
		distanceMap->SetReleaseDataFlag( true );
		distanceMap->SetInsideIsPositive( insidePositive );
		distanceMap->SetSquaredDistance( useSquareDistance );
		distanceMap->SetBackgroundValue( backgroundValue );
		distanceMap->SetUseImageSpacing( useImageSpacing );
		distanceMap->SetNumberOfThreads( maxThreads );
		itkTryCatch(distanceMap->Update(), "Error: SignedMaurerDistanceMapImageFilterWrapper Update Function.");
		distanceMapImage = distanceMap->GetOutput();
	}
	else
	{
		typedef itk::SliceBySliceSignedMaurerDistanceMapImageFilter<TInputImage, TInputImage> SliceBySliceDistanceMapFilterType;
		typename SliceBySliceDistanceMapFilterType::Pointer sliceBySliceDistanceMap = SliceBySliceDistanceMapFilterType::New();
		sliceBySliceDistanceMap->SetInput( inputImage );
		sliceBySliceDistanceMap->SetReleaseDataFlag( true );
		sliceBySliceDistanceMap->SetInsideIsPositive( insidePositive );
		sliceBySliceDistanceMap->SetUseSquaredDistance( useSquareDistance );
		sliceBySliceDistanceMap->SetBackgroundValue( backgroundValue );
		sliceBySliceDistanceMap->SetUseImageSpacing( useImageSpacing );
		sliceBySliceDistanceMap->SetNumberOfThreads( maxThreads );
		itkTryCatch(sliceBySliceDistanceMap->Update(), "Error: SignedMaurerDistanceMapImageFilterWrapper Update Function.");
		distanceMapImage = sliceBySliceDistanceMap->GetOutput();
	}

	if (positiveValuesOnly == true)
	{
		// calculate the minimum and maximum (TODO: why again here??)
		typename itk::MinimumMaximumImageCalculator<TInputImage>::Pointer minMaxCalc = itk::MinimumMaximumImageCalculator<TInputImage>::New();
		minMaxCalc->SetImage( distanceMapImage );
		minMaxCalc->Compute();
		typename TInputImage::PixelType minimum = minMaxCalc->GetMinimum();
		typename TInputImage::PixelType maximum = minMaxCalc->GetMaximum();

		typedef itk::IntensityWindowingImageFilter<TInputImage, TInputImage> RescaleIntensityFilterType;
		typename RescaleIntensityFilterType::Pointer rescaleIntensityFilter = RescaleIntensityFilterType::New();
		rescaleIntensityFilter->SetInput( distanceMapImage );
		rescaleIntensityFilter->SetReleaseDataFlag( true );
		rescaleIntensityFilter->SetWindowMinimum( 0 );
		rescaleIntensityFilter->SetWindowMaximum( maximum );
		rescaleIntensityFilter->SetOutputMinimum( 0.0 );

		if (normalizeResult == true)
			rescaleIntensityFilter->SetOutputMaximum( 1.0 );
		else
			rescaleIntensityFilter->SetOutputMaximum( maximum );

		itkTryCatch(rescaleIntensityFilter->Update(), "Error: SignedMaurerDistanceMapImageFilterWrapper Update Rescale Intensity Function.");

		ImageWrapper *outputWrapper = new ImageWrapper();
		outputWrapper->SetImage<TInputImage>( rescaleIntensityFilter->GetOutput() );
		outputWrapper->SetRescaleFlag( false );
		mOutputImages.append( outputWrapper );
	}
	else
	{
		ImageWrapper *outputWrapper = new ImageWrapper();
		outputWrapper->SetImage<TInputImage>( distanceMapImage );
		outputWrapper->SetRescaleFlag( false );
		mOutputImages.append( outputWrapper );
	}
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SignedMaurerDistanceMapImageFilterWrapper<Image2Float> > SignedMaurerDistanceMapImageFilterWrapperImage2Float;
static ProcessObjectProxy< SignedMaurerDistanceMapImageFilterWrapper<Image3Float> > SignedMaurerDistanceMapImageFilterWrapperImage3Float;
static ProcessObjectProxy< SignedMaurerDistanceMapImageFilterWrapper<Image2UShort> > SignedMaurerDistanceMapImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SignedMaurerDistanceMapImageFilterWrapper<Image3UShort> > SignedMaurerDistanceMapImageFilterWrapperImage3UShort;

} // namespace XPIWIT

