/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "SubtractImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkSubtractImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
SubtractImageFilterWrapper< TInputImage >::SubtractImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = SubtractImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Pixel-wise subtraction of two images.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
SubtractImageFilterWrapper< TInputImage >::~SubtractImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void SubtractImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::SubtractImageFilter<TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput1( inputImage1 );
	filter->SetInput2( inputImage2 );
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: SubtractImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SubtractImageFilterWrapper<Image2Float> > SubtractImageFilterWrapperImage2Float;
static ProcessObjectProxy< SubtractImageFilterWrapper<Image3Float> > SubtractImageFilterWrapperImage3Float;
static ProcessObjectProxy< SubtractImageFilterWrapper<Image2UShort> > SubtractImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SubtractImageFilterWrapper<Image3UShort> > SubtractImageFilterWrapperImage3UShort;
    
} // namespace XPIWIT

