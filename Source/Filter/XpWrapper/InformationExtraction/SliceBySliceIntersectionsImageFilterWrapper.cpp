/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// project header
#include "SliceBySliceIntersectionsImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkSliceBySliceIntersectionsImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
SliceBySliceIntersectionsImageFilterWrapper< TInputImage >::SliceBySliceIntersectionsImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = SliceBySliceIntersectionsImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Rescales the intensity of an image separately for each slice.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaInputType("SliceBySliceRegionProps");
	this->mObjectType->AppendMetaOutputType("Intersections");

	// add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "DebugOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, debug output is printed to log." );
	processObjectSettings->AddSetting( "MinSlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices larger than this number are processed." );
	processObjectSettings->AddSetting( "MaxSlice", "100000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices smaller than this number are processed." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
SliceBySliceIntersectionsImageFilterWrapper< TInputImage >::~SliceBySliceIntersectionsImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void SliceBySliceIntersectionsImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const unsigned int minSlice = processObjectSettings->GetSettingValue( "MinSlice" ).toInt();
	const unsigned int maxSlice = processObjectSettings->GetSettingValue( "MaxSlice" ).toInt();
	const bool debugOutput = processObjectSettings->GetSettingValue( "DebugOutput" ).toInt() > 0;

	// Create the meta data Object
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "Intersections";

	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::SliceBySliceIntersectionsImageFilter<TInputImage, TInputImage> IntersectionsFilterType;
	typename IntersectionsFilterType::Pointer intersectionsFilter = IntersectionsFilterType::New();
	intersectionsFilter->SetMinSlice( minSlice );
	intersectionsFilter->SetMaxSlice( maxSlice );
	intersectionsFilter->SetNumberOfThreads( maxThreads );
	intersectionsFilter->SetDebugOutput( debugOutput );
	intersectionsFilter->SetInput( inputImage );
	intersectionsFilter->SetInputMetaFilter( this->mMetaInputs.at(0) );
    intersectionsFilter->SetOutputMetaFilter( this->mMetaOutputs.at(0) );
	intersectionsFilter->SetReleaseDataFlag( true );

	itkTryCatch(intersectionsFilter->Update(), "Error: SliceBySliceIntersectionsFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( intersectionsFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SliceBySliceIntersectionsImageFilterWrapper<Image2Float> > SliceBySliceIntersectionsImageFilterWrapperImage2Float;
static ProcessObjectProxy< SliceBySliceIntersectionsImageFilterWrapper<Image3Float> > SliceBySliceIntersectionsImageFilterWrapperImage3Float;
static ProcessObjectProxy< SliceBySliceIntersectionsImageFilterWrapper<Image2UShort> > SliceBySliceIntersectionsImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SliceBySliceIntersectionsImageFilterWrapper<Image3UShort> > SliceBySliceIntersectionsImageFilterWrapperImage3UShort;

} // namespace XPIWIT

