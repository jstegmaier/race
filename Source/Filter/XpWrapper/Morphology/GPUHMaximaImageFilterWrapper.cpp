/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

#ifdef XPIWIT_USE_GPU

// namespace header
#include "GPUHMaximaImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkHMaximaImageFilter.h"

// qt header

// system header

namespace XPIWIT {

template < class TInputImage >
GPUHMaximaImageFilterWrapper< TInputImage >::GPUHMaximaImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = GPUHMaximaImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Supresses local maxima for which the height is smaller than the specified baseline";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Height", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The height above which maxima should be searched." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Fully connected flag uses 8/26 neighborhood if enabled." );

	// initialize the widget
	ProcessObjectBase::Init();
}


template < class TInputImage >
GPUHMaximaImageFilterWrapper< TInputImage >::~GPUHMaximaImageFilterWrapper()
{
}


template < class TInputImage >
void GPUHMaximaImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float height = processObjectSettings->GetSettingValue( "Height" ).toFloat();
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	// --> Result image should be written into the output wrapper, i.e. replace "inputImage" in line 82 by the processed image
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( inputImage );
	outputImage->SetRescaleFlag( false );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< GPUHMaximaImageFilterWrapper<Image2Float> > GPUHMaximaImageFilterWrapperImage2Float;
static ProcessObjectProxy< GPUHMaximaImageFilterWrapper<Image3Float> > GPUHMaximaImageFilterWrapperImage3Float;
static ProcessObjectProxy< GPUHMaximaImageFilterWrapper<Image2UShort> > GPUHMaximaImageFilterWrapperImage2UShort;
static ProcessObjectProxy< GPUHMaximaImageFilterWrapper<Image3UShort> > GPUHMaximaImageFilterWrapperImage3UShort;

} // namespace XPIWIT

#endif