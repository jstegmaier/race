/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// namespace header
#include "HConvexImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkHConvexImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
HConvexImageFilterWrapper< TInputImage >::HConvexImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = HConvexImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Extracts local maxima for which he height is larger than the specified baseline.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Height", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The height above which maxima should be searched." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Fully connected flag uses 8/26 neighborhood if enabled." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
HConvexImageFilterWrapper< TInputImage >::~HConvexImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void HConvexImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float height = processObjectSettings->GetSettingValue( "Height" ).toFloat();
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::HConvexImageFilter<TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetHeight( height );
	filter->SetFullyConnected( fullyConnected );
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: HConvexImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( filter->GetOutput() );
	outputImage->SetRescaleFlag( false );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< HConvexImageFilterWrapper<Image2Float> > HConvexImageFilterWrapperImage2Float;
static ProcessObjectProxy< HConvexImageFilterWrapper<Image3Float> > HConvexImageFilterWrapperImage3Float;
static ProcessObjectProxy< HConvexImageFilterWrapper<Image2UShort> > HConvexImageFilterWrapperImage2UShort;
static ProcessObjectProxy< HConvexImageFilterWrapper<Image3UShort> > HConvexImageFilterWrapperImage3UShort;

} // namespace XPIWIT

