/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// namespace header
#include "MorphologicalWatershedFromMarkersImageWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkMorphologicalWatershedFromMarkersImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
MorphologicalWatershedFromMarkersImageWrapper< TInputImage >::MorphologicalWatershedFromMarkersImageWrapper() : ProcessObjectBase()
{
	this->mName = MorphologicalWatershedFromMarkersImageWrapper<TInputImage>::GetName();
	this->mDescription = "Calculates the watershed transform of the input image based on a marker image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "MarkWatershedLine", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, watershed lines are highlighted by zero values." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled 8-neighborhood (2D) or 27-neighborhood (3D) is used." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
MorphologicalWatershedFromMarkersImageWrapper< TInputImage >::~MorphologicalWatershedFromMarkersImageWrapper()
{
}


// the update function
template < class TInputImage >
void MorphologicalWatershedFromMarkersImageWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int markWatershedLine = processObjectSettings->GetSettingValue( "MarkWatershedLine" ).toInt() > 0;
	const int fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	
	// get input image and allocate the marker image
	typedef TInputImage LabelImageType;
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::SpacingType spacing = inputImage->GetSpacing();

	typename LabelImageType::Pointer markerImage = mInputImages.at(1)->template GetImage<TInputImage>();
/*	markerImage->SetRegions( inputImage->GetLargestPossibleRegion() );
	markerImage->SetSpacing( spacing );
	markerImage->Allocate();
	markerImage->FillBuffer( 0 );

	// get the number of seed points
	MetaDataFilter* seedPoints = this->mMetaInputs.at(0);
	unsigned int numSeedPoints = seedPoints->mData.length();
	
    for(int i=0; i<numSeedPoints; ++i)
	{
        QList<float> line = seedPoints->mData.at(i);

        typename TInputImage::IndexType index;
        for ( int j = 0; j < TInputImage::ImageDimension; ++j )
            index[j] = int( 0.5+line.at(j+2)/spacing[j] );

        markerImage->SetPixel( index, i+1 );
    }

    // print the spacing of the investigated image
    Logger::GetInstance()->WriteLine( "+ Seed locations were transformed to image space with the following spacing: [" +
        QString::number(spacing[0]) + ", " +  QString::number(spacing[1]) + ", " +  QString::number(spacing[2]) + "]" );
	*/

	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::MorphologicalWatershedFromMarkersImageFilter<TInputImage, LabelImageType> WatershedFromMarkersFilterType;
	typename WatershedFromMarkersFilterType::Pointer watershedFromMarkersFilter = WatershedFromMarkersFilterType::New();
	watershedFromMarkersFilter->SetInput1( inputImage );
	watershedFromMarkersFilter->SetInput2( markerImage );
	watershedFromMarkersFilter->SetFullyConnected( fullyConnected );
	watershedFromMarkersFilter->SetMarkWatershedLine( markWatershedLine );
	watershedFromMarkersFilter->SetReleaseDataFlag( true );

	// update the filter
	itkTryCatch(watershedFromMarkersFilter->Update(), "Error: MorphologicalWatershedFromMarkersImageWrapper Update Function.");

	// set the output
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( watershedFromMarkersFilter->GetOutput() );
	outputImage->SetRescaleFlag( true );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MorphologicalWatershedFromMarkersImageWrapper<Image2Float> > MorphologicalWatershedFromMarkersImageWrapperImage2Float;
static ProcessObjectProxy< MorphologicalWatershedFromMarkersImageWrapper<Image3Float> > MorphologicalWatershedFromMarkersImageWrapperImage3Float;
static ProcessObjectProxy< MorphologicalWatershedFromMarkersImageWrapper<Image2UShort> > MorphologicalWatershedFromMarkersImageWrapperImage2UShort;
static ProcessObjectProxy< MorphologicalWatershedFromMarkersImageWrapper<Image3UShort> > MorphologicalWatershedFromMarkersImageWrapperImage3UShort;

} // namespace XPIWIT

