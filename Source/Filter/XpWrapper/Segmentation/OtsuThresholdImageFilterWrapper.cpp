/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// namespace header
#include "OtsuThresholdImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkOtsuThresholdImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
OtsuThresholdImageFilterWrapper< TInputImage >::OtsuThresholdImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = OtsuThresholdImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Threshold an image using the Otsu Threshold. Inside value 1, outside value 0";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
OtsuThresholdImageFilterWrapper< TInputImage >::~OtsuThresholdImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void OtsuThresholdImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::OtsuThresholdImageFilter <TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetInsideValue(0);
	filter->SetOutsideValue(1);
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: OtsuThresholdImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( filter->GetOutput() );
	outputImage->SetRescaleFlag( false );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< OtsuThresholdImageFilterWrapper<Image2Float> > OtsuThresholdImageFilterWrapperImage2Float;
static ProcessObjectProxy< OtsuThresholdImageFilterWrapper<Image3Float> > OtsuThresholdImageFilterWrapperImage3Float;
static ProcessObjectProxy< OtsuThresholdImageFilterWrapper<Image2UShort> > OtsuThresholdImageFilterWrapperImage2UShort;
static ProcessObjectProxy< OtsuThresholdImageFilterWrapper<Image3UShort> > OtsuThresholdImageFilterWrapperImage3UShort;
    
} // namespace XPIWIT

