/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */


// namespace header
#include "DropLineEdit.h"
#include <qmimedata.h>
#include <qfileinfo.h>

namespace XPIWIT
{

// the default contstructor
DropLineEdit::DropLineEdit(QWidget *parent) : QLineEdit( parent )
{
	this->setAcceptDrops( true );
	connect(this, SIGNAL(editingFinished()), this, SLOT(createValidPath()));
}


// constructor that initializes the text of the edit field
DropLineEdit::DropLineEdit(const QString& text, QWidget *parent) : QLineEdit( text, parent )
{
	this->setAcceptDrops( true );
	connect(this, SIGNAL(editingFinished()), this, SLOT(createValidPath()));
}


// the destructor
DropLineEdit::~DropLineEdit()
{
}
		
   
// drag enter event
void DropLineEdit::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}


// drag move event
void DropLineEdit::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}


// drop move event
void DropLineEdit::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();
    
	if (mimeData->hasUrls() == true)
	{
         QList<QUrl> urlList = mimeData->urls();
         QString text;
         for (int i = 0; i < urlList.size() && i < 32; ++i)
		 {
			 QString url = urlList.at(i).path();
             text += url + QString("\n");
         }

		 #ifdef _WIN32
			text.remove(0,1);
		 #endif
		text.remove(text.length()-1,1);
		this->setText( text );

		// convert the path to a valid XPIWIT representation
		createValidPath();
	}

    event->acceptProposedAction();
}


// function to generate a valid input path
void DropLineEdit::createValidPath()
{
	QString currentText = this->text();
	currentText.replace( "\\", "/" );

	QFileInfo info1( currentText );
	if (info1.isDir() && currentText.at(currentText.length()-1) != '/')
		currentText.append("/");

	while (currentText.contains( "//" ))
		currentText.replace("//", "/");
		
	this->setText( currentText );
}

} // namespace XPIWIT
