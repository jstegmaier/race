/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */

// file header
#include "XPIWITGUIMainWindow.h"
#include "DropLineEdit.h"

// qt header
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QApplication>
#include <QGroupBox>
#include <QMessageBox>
#include <QDesktopServices>

#ifdef TGMM_IMPORTER_FOUND
#include <string>
#include "TGMMparserXML.h"
#include "TGMMparserSQL.h"
#include <QDirIterator>
#include <QInputDialog>
#endif

namespace XPIWIT
{

// The contructor
XPIWITGUIMainWindow::XPIWITGUIMainWindow(QWidget* parent) : QMainWindow(parent)
{
	// call the init function
	Init();
}


// The destructor
XPIWITGUIMainWindow::~XPIWITGUIMainWindow()
{
}

    
// Init function to intitialize the basic layout of the GUI
void XPIWITGUIMainWindow::Init()
{
    // maximize the window
//	showMaximized();

    // enable drag&drop
    //setAcceptDrops( true );

	// create the input and output path widgets
	QGroupBox* ioParametersGroup = new QGroupBox(tr("Input/Output Settings (Step 1)"));
	QLabel* membraneFileLabel = new QLabel( "Membrane Input (File/Folder):" );
	QLabel* nucleiFileLabel = new QLabel( "Nuclei/Seed Input (File/Folder):" );
	QLabel* outputDirLabel = new QLabel( "Output (Folder):" );
	mMembraneFileEdit = new DropLineEdit(  );
    mMembraneFileEdit->setToolTip( "Drag & Drop your membrane input file/folder here. <br/> This file/folder should contain a membrane channel image, preferably in 16 bit TIFF format." );
	mNucleiFileEdit = new DropLineEdit(  );
    mNucleiFileEdit->setToolTip( "Drag & Drop your seed input file/folder here. <br/> This file/folder should contain a nucleus channel image (NS), preferably in 16 bit TIFF format. <br/> Alternatively, you may use a folder containing CSV files that are, e.g., imported from TGMM or CATMAID." );
	
	mOutputDirEdit = new DropLineEdit( );
    mOutputDirEdit->setToolTip( "Drag & Drop your output folder here. <br/> Note that the folder needs write access and it already needs to exist before the segmentation can be executed." );
	QGridLayout* ioLayout = new QGridLayout();
	ioLayout->addWidget(membraneFileLabel, 0, 0);
	ioLayout->addWidget(mMembraneFileEdit, 0, 1);
	ioLayout->addWidget(nucleiFileLabel, 1, 0);
	ioLayout->addWidget(mNucleiFileEdit, 1, 1);
	ioLayout->addWidget(outputDirLabel, 2, 0);
	ioLayout->addWidget(mOutputDirEdit, 2, 1);

#ifdef TGMM_IMPORTER_FOUND
	mImportTGMMSeeds = new QPushButton( "Import TGMM Seeds" );
	mImportCATMAIDSeeds = new QPushButton( "Import CATMAID Seeds" );
	QHBoxLayout* importButtonLayout = new QHBoxLayout();
	importButtonLayout->addWidget( mImportTGMMSeeds );
	importButtonLayout->addWidget( mImportCATMAIDSeeds );
	ioLayout->addLayout(importButtonLayout, 3, 0, 1, 2);
	connect(mImportTGMMSeeds, SIGNAL(clicked()), this, SLOT(slotImportTGMMSeeds()));
	connect(mImportCATMAIDSeeds, SIGNAL(clicked()), this, SLOT(slotImportCATMAIDSeeds()));
#endif
	ioParametersGroup->setLayout( ioLayout );

	// RACE seeding selection
	QGroupBox* raceVersion = new QGroupBox(tr("RACE Seeding (Step 2)"));
	mRACEMSRadioButton = new QRadioButton( "Membranes" );
    mRACEMSRadioButton->setChecked( true );
    mRACEMSRadioButton->setToolTip( "Extracts seed points from the membrane channel image (RACE_MS)." );
	mRACENSRadioButton = new QRadioButton( "Nuclei" );
    mRACENSRadioButton->setToolTip( "Extracts seed points from a nucleus channel image (RACE_NS). <br/> Make sure to set the second input path to a file or folder containing appropriate nucleus channel images." );
	mRACECSVRadioButton = new QRadioButton( "CSV" );
    mRACECSVRadioButton->setToolTip( "Uses centroids of tracked objects that are stored in CSV format (Column separator: ';', row separator: line break). <br/> RACE uses the first column as seed point label (unique integer id) and columns 3-5 for x, y and z position in image coordinates." );
	QHBoxLayout* raceVersionLayout = new QHBoxLayout();
	raceVersionLayout->addWidget( mRACEMSRadioButton );
	raceVersionLayout->addWidget( mRACENSRadioButton );
	raceVersionLayout->addWidget( mRACECSVRadioButton );
	raceVersion->setLayout( raceVersionLayout );
    
	QGroupBox* accellerationType = new QGroupBox(tr("RACE Acceleration (Step 3)"));
    mRACECPURadioButton = new QRadioButton( "ITK" );
    mRACECPURadioButton->setToolTip( "Uses the CPU-based default ITK implementation for <br/> median filtering, morphology, hmaxima and watershed (Supported on all platforms)." );
    mRACECPURadioButton->setChecked( true );
    mRACENScaleRadioButton = new QRadioButton( "NScale" );
    mRACENScaleRadioButton->setToolTip( "Uses the fast CPU-based implementation included in the NScale library for <br/> hmaxima and watershed (Supported on Ubuntu and Windows)." );
    mRACEGPURadioButton = new QRadioButton( "CUDA" );
    mRACEGPURadioButton->setToolTip( "Uses GPU-based CUDA implementations to speed up median filtering <br/> and morphological operators (Supported on Windows)." );
    mRACENScaleGPURadioButton = new QRadioButton( "NScale+CUDA" );
    mRACENScaleGPURadioButton->setToolTip( "Uses NScale to speed up hmaxima/watershed and CUDA-based <br/> implementations for median filtering/morphological operators (Supported on Windows)." );
    
    // disable some controls for linux and osx
#ifdef Q_OS_MAC
    mRACENScaleGPURadioButton->setEnabled(false);
    mRACENScaleRadioButton->setEnabled(false);
    mRACEGPURadioButton->setEnabled(false);
#endif
    
#ifdef Q_OS_LINUX
    mRACENScaleGPURadioButton->setEnabled(false);
    mRACEGPURadioButton->setEnabled(false);
#endif
    
    QHBoxLayout* raceAccellerationLayout = new QHBoxLayout();
    raceAccellerationLayout->addWidget( mRACECPURadioButton );
    raceAccellerationLayout->addWidget( mRACENScaleRadioButton );
    raceAccellerationLayout->addWidget( mRACEGPURadioButton );
    raceAccellerationLayout->addWidget( mRACENScaleGPURadioButton );
    accellerationType->setLayout( raceAccellerationLayout );
	
	// microscope dependent parameters
	QGroupBox* microscopeParameters = new QGroupBox(tr("Microscope/Specimen Parameters (Step 4)"));
	mMaxThreadsSpinBox = new QSpinBox();
	mMaxThreadsSpinBox->setValue( 16 );
	mMaxThreadsSpinBox->setMinimum( 1 );
	mMaxThreadsSpinBox->setMaximum( 64 );
	mMaxThreadsSpinBox->setSingleStep( 1 );
	mXYVSZRatioSpinBox = new QDoubleSpinBox();
	mXYVSZRatioSpinBox->setValue( 5 );
	mXYVSZRatioSpinBox->setMinimum( 1 );
	mXYVSZRatioSpinBox->setMaximum( 20 );
	mXYVSZRatioSpinBox->setSingleStep( 1 );
	mMinClosingRadiusSpinBox = new QSpinBox();
	mMinClosingRadiusSpinBox->setValue( 1 );
	mMinClosingRadiusSpinBox->setMinimum( 1 );
	mMinClosingRadiusSpinBox->setMaximum( 10 );
	mMinClosingRadiusSpinBox->setSingleStep( 1 );
	mMaxClosingRadiusSpinBox = new QSpinBox();
	mMaxClosingRadiusSpinBox->setValue( 4 );
	mMaxClosingRadiusSpinBox->setMinimum( 1 );
	mMaxClosingRadiusSpinBox->setMaximum( 10 );
	mMaxClosingRadiusSpinBox->setSingleStep( 1 );
	mMinimumSeedAreaSpinBox = new QSpinBox();
	mMinimumSeedAreaSpinBox->setRange( 0, 100 );
	mMinimumSeedAreaSpinBox->setValue( 1 );
	mMinimumSeedAreaSpinBox->setSingleStep( 1 );
	mMaxSegmentAreaSpinBox = new QSpinBox();
	mMaxSegmentAreaSpinBox->setRange( 1, 1000000 );
	mMaxSegmentAreaSpinBox->setValue( 1000 );
	mMaxSegmentAreaSpinBox->setSingleStep( 100 );
	mMinVolumeSpinBox = new QSpinBox();
	mMinVolumeSpinBox->setRange( 0, 1000000 );
	mMinVolumeSpinBox->setValue( 1500 );
	mMinVolumeSpinBox->setSingleStep( 100 );
	mMaxVolumeSpinBox = new QSpinBox();
	mMaxVolumeSpinBox->setRange( 0, 1000000 );
	mMaxVolumeSpinBox->setValue( 4000 );
	mMaxVolumeSpinBox->setSingleStep( 500 );
	QGridLayout* microscopeParametersLayout = new QGridLayout();
	microscopeParametersLayout->addWidget( new QLabel("Max Threads"), 0, 0 );
	microscopeParametersLayout->addWidget( new QLabel("Z vs. XY Ratio"), 1, 0 );
	microscopeParametersLayout->addWidget( new QLabel("Min Closing Radius"), 2, 0 );
	microscopeParametersLayout->addWidget( new QLabel("Max Closing Radius"), 3, 0 );
	microscopeParametersLayout->addWidget( new QLabel("Minimum Seed Area"), 4, 0 );
	microscopeParametersLayout->addWidget( new QLabel("Max 2D Segment Area"), 5, 0 );
	microscopeParametersLayout->addWidget( new QLabel("Min 3D Cell Volume"), 6, 0 );
	microscopeParametersLayout->addWidget( new QLabel("Max 3D Cell Volume"), 7, 0 );
	microscopeParametersLayout->addWidget( mMaxThreadsSpinBox, 0, 1 );
	microscopeParametersLayout->addWidget( mXYVSZRatioSpinBox, 1, 1 );
	microscopeParametersLayout->addWidget( mMinClosingRadiusSpinBox, 2, 1 );
	microscopeParametersLayout->addWidget( mMaxClosingRadiusSpinBox, 3, 1 );
	microscopeParametersLayout->addWidget( mMinimumSeedAreaSpinBox, 4, 1 );
	microscopeParametersLayout->addWidget( mMaxSegmentAreaSpinBox, 5, 1 );
	microscopeParametersLayout->addWidget( mMinVolumeSpinBox, 6, 1 );
	microscopeParametersLayout->addWidget( mMaxVolumeSpinBox, 7, 1 );
	microscopeParameters->setLayout( microscopeParametersLayout );

	// set the intensity dependent parameters
	QGroupBox* intensityParameters = new QGroupBox(tr("Intensity Parameters (Step 5)"));
	mSeedThresholdSpinBox = new QDoubleSpinBox();
	mSeedThresholdSpinBox->setDecimals( 5 );
	mSeedThresholdSpinBox->setValue( 0.0008 );
	mSeedThresholdSpinBox->setMinimum(0.0);
	mSeedThresholdSpinBox->setMaximum(1.0);
	mSeedThresholdSpinBox->setSingleStep(1.0 / 65535);
	mHMaximaLevelSpinBox = new QDoubleSpinBox();
	mHMaximaLevelSpinBox->setValue( 20 );
	mHMaximaLevelSpinBox->setMinimum(0);
	mHMaximaLevelSpinBox->setMaximum(65535);
	mHMaximaLevelSpinBox->setSingleStep(1);
	mSliceBySliceWatershedLevelSpinBox = new QDoubleSpinBox();
	mSliceBySliceWatershedLevelSpinBox->setValue( 39 );
	mSliceBySliceWatershedLevelSpinBox->setMinimum(0);
	mSliceBySliceWatershedLevelSpinBox->setMaximum(65535);
	mSliceBySliceWatershedLevelSpinBox->setSingleStep(1);
	QGridLayout* intensityParametersLayout = new QGridLayout();
	intensityParametersLayout->addWidget( new QLabel("Binary Threshold (MS/NS)"), 0, 0 );
	intensityParametersLayout->addWidget( mSeedThresholdSpinBox, 0, 1 );
	intensityParametersLayout->addWidget( new QLabel("H-Maxima Level"), 1, 0 );
	intensityParametersLayout->addWidget( mHMaximaLevelSpinBox, 1, 1 );
	intensityParametersLayout->addWidget( new QLabel("Slice-by-Slice Watershed Level"), 2, 0);
	intensityParametersLayout->addWidget( mSliceBySliceWatershedLevelSpinBox, 2, 1 );
	intensityParameters->setLayout( intensityParametersLayout );

	QGroupBox* optionalParameters = new QGroupBox(tr("Optional Parameters (Step 6)"));
	mSingleSliceHeuristicCheckBox = new QCheckBox( "SSH Fusion Heuristic" );
	mSingleSliceHeuristicCheckBox->setCheckable( true );
	mSingleSliceHeuristicCheckBox->setChecked( true );
	mJaccardIndexHeuristicCheckBox = new QCheckBox( "JI Fusion Heuristic" );
	mJaccardIndexHeuristicCheckBox->setCheckable( true );
	mJaccardIndexHeuristicCheckBox->setChecked( false );
	mRandomLabelsCheckBox = new QCheckBox( "Random Labels" );
	mRandomLabelsCheckBox->setCheckable( true );
	mRandomLabelsCheckBox->setChecked( false );
	mWriteResultsCheckBox = new QCheckBox( "Write Intermediate Results" );
	mWriteResultsCheckBox->setCheckable( true );
	mWriteResultsCheckBox->setChecked( false );	
	mWriteResultsCheckBox->setEnabled( true );
	QVBoxLayout* optionalParametersLayout = new QVBoxLayout();
	optionalParametersLayout->addWidget( mSingleSliceHeuristicCheckBox );
	optionalParametersLayout->addWidget( mJaccardIndexHeuristicCheckBox );
	optionalParametersLayout->addWidget( mRandomLabelsCheckBox );
	optionalParametersLayout->addWidget( mWriteResultsCheckBox );
	optionalParameters->setLayout( optionalParametersLayout );

	QHBoxLayout* parametersLayout = new QHBoxLayout();
	parametersLayout->addWidget( microscopeParameters );
	parametersLayout->addWidget( intensityParameters );
	parametersLayout->addWidget( optionalParameters );
    
	// create the main buttons
    QPushButton* runButton = new QPushButton( "Run!" );
	QPushButton* openResultsButton = new QPushButton( "Open Result Folder" );
	QPushButton* quitButton = new QPushButton( "Quit" );
	QHBoxLayout* buttonsLayout = new QHBoxLayout();
	buttonsLayout->addWidget( quitButton );
	buttonsLayout->addWidget( openResultsButton );
	buttonsLayout->addWidget( runButton );	
	connect( runButton, SIGNAL(clicked()), this, SLOT(runPipeline()) );
	connect( quitButton, SIGNAL(clicked()), this, SLOT(slotFinished()) );
	connect( openResultsButton, SIGNAL(clicked()), this, SLOT(openResultFolder()) );

	// set the main layout
	QVBoxLayout* mainLayout = new QVBoxLayout();
	mainLayout->addWidget( ioParametersGroup );
    
    QHBoxLayout* versionLayout = new QHBoxLayout();
    versionLayout->addWidget( raceVersion );
    versionLayout->addWidget( accellerationType );
    mainLayout->addLayout( versionLayout );
	mainLayout->addLayout( parametersLayout );
	mainLayout->addLayout( buttonsLayout );
	QWidget* mainWidget = new QWidget();
	mainWidget->setLayout( mainLayout );
	this->setCentralWidget( mainWidget );
}

// create the input files for XPIWIT
void XPIWITGUIMainWindow::CreateXPIWITInputFile()
{
	// create the xml file with the adjusted parameters
	QString templateTXT = "RACEMSTemplate.txt";
	QString templateXML = "RACEMSTemplate.xml";
	QString outputXML = "RACEMS.xml";
	QString outputTXT = "RACE.txt";

	// adjust xml paths if another RACE version is selected
	if (mRACENSRadioButton->isChecked() == true)
	{
		templateTXT = "RACENSTemplate.txt";
		templateXML = "RACENSTemplate.xml";
		outputXML = "RACENS.xml";
	}
	else if (mRACECSVRadioButton->isChecked() == true)
	{
		templateTXT = "RACECSVTemplate.txt";
		templateXML = "RACECSVTemplate.xml";
		outputXML = "RACECSV.xml";
	}

	templateTXT = QCoreApplication::applicationDirPath() + QString("/templates/") + templateTXT;
	templateXML = QCoreApplication::applicationDirPath() + QString("/templates/") + templateXML;
	outputXML = QCoreApplication::applicationDirPath() + QString("/templates/") + outputXML;
	outputTXT = QCoreApplication::applicationDirPath() + QString("/templates/") + outputTXT;

	// open xml template file
	QFile templateFile( templateXML );
	if(!templateFile.open(QIODevice::ReadOnly))
		QMessageBox::information(0, "error", templateFile.errorString());
	
	// open output xml file
	QFile::remove( outputXML ); 
	QFile outputFile( outputXML );
	if(!outputFile.open(QIODevice::ReadWrite | QIODevice::Text))
		QMessageBox::information(0, "error", outputFile.errorString());
	
	QTextStream in(&templateFile);
	QTextStream out(&outputFile);
	while(!in.atEnd())
	{
		QString line = in.readLine();
		line.replace( "%XYVSZRatio%", QString::number( mXYVSZRatioSpinBox->value() ) ); 
		line.replace( "%SEEDTHRESHOLD%", QString::number( mSeedThresholdSpinBox->value() ) ); //
		line.replace( "%HMAXIMALEVEL%", QString::number( mHMaximaLevelSpinBox->value() ) ); //
		line.replace( "%MINCLOSINGRADIUS%", QString::number( mMinClosingRadiusSpinBox->value() ) );
		line.replace( "%MAXCLOSINGRADIUS%", QString::number( mMaxClosingRadiusSpinBox->value() ) );
		line.replace( "%SLICEBYSLICEWATERSHEDLEVEL%", QString::number( mSliceBySliceWatershedLevelSpinBox->value() ) ); //
		line.replace( "%MINSEEDAREA%", QString::number( mMinimumSeedAreaSpinBox->value() ) );
		line.replace( "%MAXSEGMENTAREA%", QString::number( mMaxSegmentAreaSpinBox->value() ) );
		line.replace( "%MINIMUMVOLUME%", QString::number( mMinVolumeSpinBox->value() ) );
		line.replace( "%MAXIMUMVOLUME%", QString::number( mMaxVolumeSpinBox->value() ) );
		line.replace( "%SINGLESLICEHEURISTIC%", QString::number( mSingleSliceHeuristicCheckBox->isChecked() ) );
		line.replace( "%JACCARDINDEXHEURISTIC%", QString::number( mJaccardIndexHeuristicCheckBox->isChecked() ) );
		line.replace( "%RANDOMLABELS%", QString::number( mRandomLabelsCheckBox->isChecked() ) );
		line.replace( "%MAXTHREADS%", QString::number( mMaxThreadsSpinBox->value() ) );
		line.replace( "%WRITERESULT%", QString::number( mWriteResultsCheckBox->isChecked() ) );

		// add the optimization type to the XML (ITK, NScale, GPU or GPU+NScale)
		if (mRACENScaleRadioButton->isChecked() == true)
		{
			line.replace( "%MEDIANIMPLEMENTATION%", "" );
			line.replace( "%HMAXIMPLEMENTATION%", "NScale" );
			line.replace( "%HEEIMPLEMENTATION%", "" );
			line.replace( "%WATERSHEDIMPLEMENTATION%", "NScale" );
			line.replace( "%MORPHOLOGYIMPLEMENTATION%", "" );
		}
		else if (mRACEGPURadioButton->isChecked() == true)
		{
			line.replace( "%MEDIANIMPLEMENTATION%", "GPU" );
			line.replace( "%HMAXIMPLEMENTATION%", "" );
			line.replace( "%HEEIMPLEMENTATION%", "GPU" );
			line.replace( "%WATERSHEDIMPLEMENTATION%", "" );
			line.replace( "%MORPHOLOGYIMPLEMENTATION%", "GPU" );
		}
		else if (mRACENScaleGPURadioButton->isChecked() == true)
		{
			line.replace( "%MEDIANIMPLEMENTATION%", "GPU" );
			line.replace( "%HMAXIMPLEMENTATION%", "NScale" );
			line.replace( "%HEEIMPLEMENTATION%", "GPU" );
			line.replace( "%WATERSHEDIMPLEMENTATION%", "NScale" );
			line.replace( "%MORPHOLOGYIMPLEMENTATION%", "GPU" );
		}
		else
		{
			line.replace( "%MEDIANIMPLEMENTATION%", "" );
			line.replace( "%HMAXIMPLEMENTATION%", "" );
			line.replace( "%HEEIMPLEMENTATION%", "" );
			line.replace( "%WATERSHEDIMPLEMENTATION%", "" );
			line.replace( "%MORPHOLOGYIMPLEMENTATION%", "" );
		}

		out << line.toStdString().c_str() << "\n";
	}
	templateFile.close();
	outputFile.close();

	// create the XPIWIT input file using the specified input and output paths
	QFile inputFileTemplate( templateTXT );
	if(!inputFileTemplate.open(QIODevice::ReadOnly))
		QMessageBox::information(0, "error", inputFileTemplate.errorString());
	
	QFile::remove( outputTXT );
	QFile outputTXTFile( outputTXT );
	if(!outputTXTFile.open(QIODevice::ReadWrite | QIODevice::Text))
		QMessageBox::information(0, "error", outputTXTFile.errorString());
	
	QTextStream inTXT(&inputFileTemplate);
	QTextStream outTXT(&outputTXTFile);
	while(!inTXT.atEnd())
	{
		QString line = inTXT.readLine();
		line.replace( "%OUTPUTPATH%", mOutputDirEdit->text() );
		line.replace( "%INPUTFILEMEMBRANE%", mMembraneFileEdit->text() );
		line.replace( "%INPUTFILENUCLEI%", mNucleiFileEdit->text() );
		outTXT << line.toStdString().c_str() << "\n";
	}

	inputFileTemplate.close();
	outputTXTFile.close();
}

void XPIWITGUIMainWindow::runPipeline()
{
    // check if input and output folders have been set
    QMessageBox msgBox;
    if (mMembraneFileEdit->text().isEmpty())
    {
        msgBox.setText("Please drag & drop a valid membrane channel image to the membrane input file edit field (*.tif).");
        msgBox.exec();
        return;
    }
    
    if (mNucleiFileEdit->text().isEmpty() && mRACENSRadioButton->isChecked())
    {
        msgBox.setText("Please drag & drop a valid nucleus channel image or a valid csv file containing seed information to the nucleus input file edit field (*.tif, *.csv).");
        msgBox.exec();
        return;
    }
    
    if (mOutputDirEdit->text().isEmpty())
    {
        msgBox.setText("Please drag & drop a valid output folder to the output folder edit field. Make sure you have writing permissions to this folder.");
        msgBox.exec();
        return;
    }
    
	// create the XPIWIT input file
	CreateXPIWITInputFile();

	// check if the XPIWIT executable exists
	#ifdef _WIN32
		QFile xpiwitFile( QCoreApplication::applicationDirPath() + QString("/XPIWIT.exe") );
	#else
		QFile xpiwitFile( QCoreApplication::applicationDirPath() + QString("/XPIWIT.sh") );
	#endif

	if(!xpiwitFile.exists()) 
	{
		QMessageBox msgBox;
		#ifdef _WIN32
			msgBox.setText("The file XPIWIT.exe was not found.");
		#else
			msgBox.setText("The file XPIWIT.sh was not found.");
		#endif
		msgBox.setInformativeText("Make sure the file is placed in the same directory as the RACEGUI application.");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		int ret = msgBox.exec();
	}

	// execute xpiwit
	#ifdef _WIN32
		system( "XPIWIT.exe < templates/RACE.txt" );
	#else
		system( "./XPIWIT.sh < templates/RACE.txt" );
	#endif
}

/*
void XPIWITGUIMainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void XPIWITGUIMainWindow::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void XPIWITGUIMainWindow::dropEvent(QDropEvent* event)
{
    //const QMimeData* mimeData = event->mimeData();
    
    event->acceptProposedAction();
}
*/

#ifdef TGMM_IMPORTER_FOUND
void XPIWITGUIMainWindow::slotImportTGMMSeeds()
{
	// select the input and output folders
	QString inputFolder = QFileDialog::getExistingDirectory(this, tr("Select TGMM Result Folder"));

	if (QFileInfo(inputFolder).isDir() == false)
	{
		std::cout << "ERROR: The selected input folder does not exist. RACEGUI expects an input folder containing TGMM XML files and a separate folder to write the converted data to!" << std::endl;
		return;
	}

	QString outputFolder = QFileDialog::getExistingDirectory(this, tr("Select the CSV Output Folder (Make sure you have write permissions in this directory!)"));
	if (QFileInfo(outputFolder).isDir() == false)
	{
		std::cout << "ERROR: The selected output folder does not exist. RACEGUI expects an input folder containing TGMM XML files and a separate folder to write the converted data to!" << std::endl;
		return;
	}

	// check if input and output files are different
	if (inputFolder.compare(outputFolder) == 0)
	{
		std::cout << "ERROR: Input and output folder are identical or unknown. RACEGUI expects an empty folder for the converted data!" << std::endl;

		QMessageBox msgBox;
		msgBox.setText("ERROR: Input and output folder are identical or unknown. RACEGUI expects an input folder containing TGMM XML files and a separate folder to write the converted data to!");
		msgBox.setInformativeText("The files contained in both the image and the CSV input folders have to match in order to obtain a valid correspondence between image and csv data.");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		int ret = msgBox.exec();
		return;
	}

	QDirIterator dirIt(inputFolder);
	while (dirIt.hasNext()) 
	{
		dirIt.next();
		if (QFileInfo(dirIt.filePath()).isFile() && QFileInfo(dirIt.filePath()).suffix() == "xml")
		{
			std::cout << "Processing file " << dirIt.filePath().toStdString() << "...";
			QString filename = dirIt.filePath();

			//initialize the xml parser
			TGMM_parse_XML parser;
			IOclass_local_file fileInfo(filename.toStdString());

			//setup info
			parser.set_IO_path(fileInfo);

			//read the XML file
			int err = parser.read_from_TGMM_output();
			if (err != 0)
			{
				std::cout << "FAILED" << std::endl << "--> Failed to import the specified XML file. Please make sure the file exists and is a valid TGMM tracking result file." << std::endl;
				continue;
			}

			//write output as CSV file
			QString filenameCSV(filename.replace(inputFolder, outputFolder));
			filenameCSV.replace(".xml", ".csv");
			err = parser.write_to_xpiwit_CSV(filenameCSV.toStdString());
			if (err != 0)
			{
				std::cout << "FAILED" << "Failed to write the results to the specified CSV file. Please make sure the output path is writable and that the XML import was successful." << std::endl;
				continue;
			}
			else
			{
				std::cout << "SUCCEEDED." << std::endl;
			}
		}
		else
		{
			std::cout << "Skipping file " << dirIt.filePath().toStdString() << "..." << std::endl;
		}
	}

	// set the output folder and check the CSV radio button
	mNucleiFileEdit->setText( outputFolder );
	((DropLineEdit*)mNucleiFileEdit)->ValidatePath();
	mRACECSVRadioButton->setChecked( true );
}


// function to import corrected seed points from a CATMAID project
void XPIWITGUIMainWindow::slotImportCATMAIDSeeds()
{
	// select the input and output folders
	QString outputFolder = QFileDialog::getExistingDirectory(this, tr("Select the CSV Output Folder (Make sure you have write permissions in this directory!)"));
	QString filename = QFileDialog::getOpenFileName(this, tr("Select CATMAID Database Information File"));
	QString projectName = QInputDialog::getText(this, "Please enter your project name", "Project Name:");
	int TM = QInputDialog::getInt(this, "Please enter the time point you want to import", "Time Point Index:", 0, 0);
	
	std::cout << "Trying to import time point " << TM << " of project " << projectName.toStdString() << "...";

	//initialize classes
	TGMM_parse_SQL parser;

	//setup info
	int err = parser.read_DBinfo_from_txt_file(filename.toStdString(), projectName.toStdString());
	if (err != 0)
		std::cout << "FAILED" << std::endl << "--> Failed to import database info. Please make sure the database information file exists and contains a valid CATMAID database configuration." << std::endl;
	
	//read XML file
	err = parser.read_from_TGMM_output(TM);
	if (err != 0)
	{
		std::cout << "FAILED" << std::endl << "--> Failed to import the specified time point. Please make sure the database was successfully openend and that the time point exists." << std::endl;
		return;
	}

	//write output as CSV file
	QString filenameCSV = QString().sprintf("%s/%s_TM%04d.csv", outputFolder.toStdString().c_str(), projectName.toStdString().c_str(), TM);
	filenameCSV.replace(" ", "_");
	err = parser.write_to_xpiwit_CSV(filenameCSV.toStdString());
	if (err != 0)
	{
		std::cout << "FAILED" << "Failed to write the results to the specified CSV file. Please make sure the output path is writable and that the database import was successful." << std::endl;
		return;
	}
	else
	{
		std::cout << "SUCCEEDED." << std::endl;
		mNucleiFileEdit->setText( filenameCSV );
		((DropLineEdit*)mNucleiFileEdit)->ValidatePath();
		mRACECSVRadioButton->setChecked( true );
	}
}
#endif

void XPIWITGUIMainWindow::slotFinished()
{
	emit signalFinished();
}

void XPIWITGUIMainWindow::openResultFolder()
{
	QString outputPath = mOutputDirEdit->text();
	outputPath.replace("\\", "/");

	QString applicationPath = QCoreApplication::applicationDirPath();

	if (outputPath.contains("../") == true)
		outputPath = applicationPath + QString("/") + outputPath;

	QDesktopServices::openUrl( QString( "file:///" ) + QString(outputPath) + QString("item_0022_SliceBySliceFusionFilter/") );
}

} // namespace XPIWIT
