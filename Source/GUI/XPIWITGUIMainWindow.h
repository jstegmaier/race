/**
 * Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos.
 * Copyright (C) 2015 J. Stegmaier, F. Amat, G. Teodoro, R. Mikut, P. J. Keller and Howard Hughes Medical Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the
 * following publication:
 *
 * J. Stegmaier, F. Amat, B. Lemon, K. McDole, Y. Wan, G. Teodoro, R. Mikut, P. J. Keller: 
 * "Real-time three-dimensional cell segmentation in large-scale microscopy data of developing embryos", submitted manuscript, 2015.
 */


#ifndef XPIWITGUIMAINWINDOW_H
#define XPIWITGUIMAINWINDOW_H

// namespace header
#include "../Core/Utilities/logger.h"

// qt header
#include <QtCore/QObject>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/qpushbutton.h>
#include <QtWidgets/QLabel>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/qlineedit.h>
#include <QMessageBox>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QSpinBox>
#include <QCheckBox>
#include <QRadioButton>


namespace XPIWIT
{

/**
 *	@class XPIWITGUIMainWindow
 *	Main class of the XPIWIT GUI project
 */
class XPIWITGUIMainWindow : public QMainWindow
{
    Q_OBJECT
    public:
		XPIWITGUIMainWindow(QWidget *parent = 0);
        ~XPIWITGUIMainWindow();

        void Init();
		void CreateXPIWITInputFile();

    signals:
		void signalFinished();

	private slots:
        /*
		void dropEvent(QDropEvent* event);
        void dragEnterEvent(QDragEnterEvent* event);
        void dragMoveEvent(QDragMoveEvent* event);
		*/
		void runPipeline();
		void slotFinished();
		void openResultFolder();

#ifdef TGMM_IMPORTER_FOUND
		void slotImportTGMMSeeds();
		void slotImportCATMAIDSeeds();
#endif

    private:
		QLineEdit* mMembraneFileEdit;
		QLineEdit* mNucleiFileEdit;
		QLineEdit* mOutputDirEdit;

		QPushButton* mImportTGMMSeeds;
		QPushButton* mImportCATMAIDSeeds;

		// race seeding radio buttons
		QRadioButton* mRACEMSRadioButton;
		QRadioButton* mRACENSRadioButton;
		QRadioButton* mRACECSVRadioButton;
    
        // race seeding radio buttons
        QRadioButton* mRACECPURadioButton;
        QRadioButton* mRACENScaleRadioButton;
        QRadioButton* mRACEGPURadioButton;
        QRadioButton* mRACENScaleGPURadioButton;

		// microscopy parameters
		QSpinBox* mMaxThreadsSpinBox;
		QDoubleSpinBox* mXYVSZRatioSpinBox;
		QSpinBox* mMinClosingRadiusSpinBox;
		QSpinBox* mMaxClosingRadiusSpinBox;
		QSpinBox* mMinimumSeedAreaSpinBox;
		QSpinBox* mMaxSegmentAreaSpinBox;
		QSpinBox* mMinVolumeSpinBox;
		QSpinBox* mMaxVolumeSpinBox;

		// intensity parameters
		QDoubleSpinBox* mSeedThresholdSpinBox;
		QDoubleSpinBox* mHMaximaLevelSpinBox;
		QDoubleSpinBox* mSliceBySliceWatershedLevelSpinBox;

		// optional parameters
		QCheckBox* mSingleSliceHeuristicCheckBox;
		QCheckBox* mJaccardIndexHeuristicCheckBox;
		QCheckBox* mRandomLabelsCheckBox;
		QCheckBox* mWriteResultsCheckBox;
};

} // namespace XPIWIT

#endif  // XPIWITGUIMainWindow_H
